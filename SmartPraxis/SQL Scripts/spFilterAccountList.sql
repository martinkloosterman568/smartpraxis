IF EXISTS (SELECT TOP 1 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFilterAccountList]') AND type in (N'P', N'PC')) BEGIN
	DROP PROCEDURE [dbo].[spFilterAccountList]
END
GO

CREATE PROCEDURE [dbo].[spFilterAccountList] 
	@valuePassedIn varchar(50),
	@DataAccountIds varchar(50),
	@IsAdmin bit = 0
AS
BEGIN

	SET NOCOUNT ON

	SELECT
		*
	FROM DataAccounts dataAccount
	WHERE
	(
		@IsAdmin = 1
		OR
		(
			-- If none of the AccountIds are admin DataAccounts, then filter by AccountIds and @valuePassedIn
			dataAccount.AccountID IN
			(
				SELECT
					accountID = CONVERT(INT, [value])
				FROM STRING_SPLIT(@DataAccountIds, '|')
			)
			AND
			(
				ISNULL(@valuePassedIn, '') = '' OR
				(
					dataAccount.CompanyName LIKE '%' + @valuePassedIn + '%' OR
					dataAccount.FirstName   LIKE '%' + @valuePassedIn + '%' OR
					dataAccount.LastName    LIKE '%' + @valuePassedIn + '%'
				)
			)
		)
	)
	
	SET NOCOUNT OFF

END
GO


