IF EXISTS (SELECT TOP 1 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFilterPatientList]') AND type in (N'P', N'PC')) BEGIN
	DROP PROCEDURE [dbo].[spFilterPatientList]
END
GO


CREATE PROCEDURE [dbo].[spFilterPatientList] 
	@valuePassedIn varchar(50),
	@DataAccountIds varchar(50),
	@dropdownFilter varchar(50) = '- ALL -',
	@IsAdmin bit = 0
AS
BEGIN

	SET NOCOUNT ON

	-- If the user is a 'Device Co' or a 'Device Tech Co' then @DataAccountIds will only reference 'Device Companies' and subsequently will match 
	-- on column DataPatientRequests.DeviceCompanyGuid. Else, @DataAccountIds will reference only 'MRI companies' and will match only on
	-- column DataPatientRequests.MRICompanyAccountGuid.

	SELECT
		u.PatientId,
		u.PatientGuid,
		u.PatientName,
		u.DOB,
		u.Phone,
		u.Insurance,
		u.MRIScanDate,
		u.MRIScanTime,
		u.PatientStatusDDL,
		u.AttachedFile,
		u.MRICompany,
		u.Completed,
		MRIDay = datename(dw,MRIScanDate) 
	FROM DataPatientRequests u
	INNER JOIN
	(
		SELECT
			*
		FROM DataAccounts 
		WHERE @IsAdmin = 1 OR AccountId IN
		(
			SELECT
				accountID = CONVERT(INT, [value])
			FROM STRING_SPLIT(@DataAccountIds, '|')
		) 
	) dataAccount ON dataAccount.AccountGuid IN (u.DeviceCompanyGuid, u.MRICompanyAccountGuid)
	WHERE 
	(
		ISNULL(@valuePassedIn, '') = '' OR
		(
			u.PatientName LIKE '%' + @valuePassedIn + '%' OR
			u.Phone       LIKE '%' + @valuePassedIn + '%' OR
			u.Insurance   LIKE '%' + @valuePassedIn + '%' OR
			u.MRIScanDate LIKE '%' + @valuePassedIn + '%' OR
			u.MRIScanTime LIKE '%' + @valuePassedIn + '%'
		)
	)
	AND
	(
		1 = CASE @dropdownFilter
				WHEN  '- ALL -'         THEN 1
				WHEN u.PatientStatusDDL THEN 1
				ELSE 0
			END
	)

	SET NOCOUNT OFF

END
GO


