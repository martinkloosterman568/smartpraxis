IF EXISTS (SELECT TOP 1 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAccountEmails]') AND type in (N'P', N'PC')) BEGIN
	DROP PROCEDURE [dbo].[spGetAccountEmails]
END
GO


CREATE PROCEDURE [dbo].[spGetAccountEmails] 
	@AccountId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	SELECT
		*
	FROM DataAccountEmails dataAccountEmail
	INNER JOIN DataAccounts dataAccount ON dataAccount.AccountId = dataAccountEmail.
	WHERE AccountID

	declare @sql varchar(500)
	set @sql = 'select * from [dbo].[DataAccountEmails] where [AccountGuid] = ''' + convert(varchar(50),@valuePassedIn) + ''' order by ' + @sortBy
	print @sql
    exec(@sql)

	SET NOCOUNT OFF

END
GO