IF EXISTS (SELECT TOP 1 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFilterDeviceTechPatientList]') AND type in (N'P', N'PC')) BEGIN
	DROP PROCEDURE [dbo].[spFilterDeviceTechPatientList]
END
GO


CREATE PROCEDURE [dbo].[spFilterDeviceTechPatientList] 
	@filter varchar(50) = ''
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		u.PatientId,
		u.PatientGuid,
		u.PatientName,
		u.DOB,
		u.Phone,
		u.Insurance,
		u.MRIScanDate, 
		u.MRIScanTime,
		u.PatientStatusDDL,
		u.AttachedFile,
		u.MRICompany,
		MRIDay = DATENAME(DW, u.MRIScanDate)
	FROM DataPatientRequests u
	INNER JOIN
	(
		SELECT
			dataAccount.AccountGuid
		FROM STRING_SPLIT(@filter, '|') accountID
		INNER JOIN DataAccounts dataAccount ON dataAccount.AccountId = CONVERT(INT, accountID.value)
	) dataAccount ON dataAccount.AccountGuid = u.DeviceCompanyGuid
	WHERE 
		u.Completed = 0 AND
		u.PatientStatusDDL NOT IN ('Rejected','Done')
	ORDER BY u.MRIScanDate ASC, u.MRIScanTime ASC
		
	SET NOCOUNT OFF;

END
GO


