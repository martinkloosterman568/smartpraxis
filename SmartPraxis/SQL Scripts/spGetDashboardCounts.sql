IF EXISTS (SELECT TOP 1 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetDashboardCounts]') AND type in (N'P', N'PC')) BEGIN
	DROP PROCEDURE [dbo].[spGetDashboardCounts]
END
GO

CREATE PROCEDURE [dbo].[spGetDashboardCounts]
	@DataAccountIdsDelimited varchar(50)
AS
BEGIN

	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#Dashboard_Counts_Temp') IS NOT NULL DROP TABLE #Dashboard_Counts_Temp

	SELECT
		dataPatientRequest.*
	INTO #Dashboard_Counts_Temp
	FROM DataAccounts dataAccount
	INNER JOIN DataPatientRequests dataPatientRequest ON dataPatientRequest.DeviceCompanyGuid = dataAccount.AccountGuid
	WHERE
	(
		ISNULL(@DataAccountIdsDelimited, '') = '' OR
		dataAccount.AccountId IN (SELECT CONVERT(INT, [value]) FROM STRING_SPLIT(@DataAccountIdsDelimited, '|'))
	)


	SELECT
		Mode = 'PatientCount',
		Quantity = COUNT(*)
	FROM #Dashboard_Counts_Temp
	WHERE CONVERT(VARCHAR(50), MRIScanDate, 112) = CONVERT(VARCHAR(50), GETDATE(), 112) -- The date format is: yyyymmdd

	UNION SELECT
		Mode = 'PatientRequests',
		Quantity = COUNT(*)
	FROM #Dashboard_Counts_Temp
	WHERE PatientStatusDDL = 'Pending' AND
		MriScanDate >= CONVERT(VARCHAR(50), GETDATE(), 112) AND 
		MriScanDate < CONVERT(VARCHAR(50), GETDATE() + 1, 112) 

	UNION SELECT
		Mode = 'Accepted',
		Quantity = COUNT(*)
	FROM #Dashboard_Counts_Temp
	WHERE PatientStatusDDL = 'Accepted'

	UNION SELECT
		Mode = 'Rejected',
		Quantity = COUNT(*)
	FROM #Dashboard_Counts_Temp
	WHERE PatientStatusDDL = 'Rejected'

	UNION SELECT
		Mode = 'Comments',
		Quantity = COUNT(*)
	FROM #Dashboard_Counts_Temp
	WHERE PatientStatusDDL = 'Comments'

	UNION SELECT
		Mode = 'PatientRecords',
		Quantity = COUNT(*)
	FROM #Dashboard_Counts_Temp

	UNION SELECT
		Mode = 'NumberAgreedToFollowMKScan',
		Quantity = COUNT(*)
	FROM #Dashboard_Counts_Temp
	WHERE IsAgreedToFollowMKScan = 1

	UNION SELECT
		Mode = 'ActiveUsers',
		Quantity = COUNT(*)
	FROM DataUserMast
		
    
    SET NOCOUNT OFF

END
GO


