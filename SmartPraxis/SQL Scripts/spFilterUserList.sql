IF EXISTS (SELECT TOP 1 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFilterUserList]') AND type in (N'P', N'PC')) BEGIN
	DROP PROCEDURE [dbo].[spFilterUserList]
END
GO

CREATE PROCEDURE [dbo].[spFilterUserList] 
	@valuePassedIn varchar(50),
	@filter varchar(110),
	@typeOfUser varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	SELECT
		dataUserMastsQuery.*,
		dataAccountsQuery.*
	FROM DataUserMast dataUserMastsQuery
	CROSS APPLY
	(
		SELECT
			DataAccountIdsDelimited = STUFF
			(
				(
					SELECT
						'|' + CONVERT(VARCHAR(5), dataAccount.AccountId)
					FROM UserDataAccounts userDataAccount
					INNER JOIN DataAccounts dataAccount on dataAccount.AccountId = userDataAccount.AccountId
					WHERE userDataAccount.UserID = dataUserMastsQuery.ID
					GROUP BY userDataAccount.UserID, dataAccount.AccountId
					ORDER BY dataAccount.AccountId
					FOR XML PATH('')
				), 1, 1, ''
			),
			CompanyNamesDelimited = STUFF
			(
				(
					SELECT
						'|' + dataAccount.CompanyName
					FROM UserDataAccounts userDataAccount
					INNER JOIN DataAccounts dataAccount on dataAccount.AccountId = userDataAccount.AccountId
					WHERE userDataAccount.UserID = dataUserMastsQuery.ID
					GROUP BY userDataAccount.UserID, dataAccount.CompanyName
					ORDER BY dataAccount.CompanyName
					FOR XML PATH('')
				), 1, 1, ''
			),
			CompanyTypesDelimited = STUFF
			(
				(
					SELECT
						'|' + dataAccount.TypeOf
					FROM UserDataAccounts userDataAccount
					INNER JOIN DataAccounts dataAccount on dataAccount.AccountId = userDataAccount.AccountId
					WHERE userDataAccount.UserID = dataUserMastsQuery.ID
					GROUP BY userDataAccount.UserID, dataAccount.TypeOf
					ORDER BY dataAccount.TypeOf
					FOR XML PATH('')
				), 1, 1, ''
			)
		FROM UserDataAccounts userDataAccount
		INNER JOIN DataAccounts dataAccount on dataAccount.AccountId = userDataAccount.AccountId
		WHERE userDataAccount.UserID = dataUserMastsQuery.ID
		GROUP BY userDataAccount.UserID
	) dataAccountsQuery
	WHERE
	(
		-- Perform any search for any particular data related to the tables [DataUserMast] and [DataAccount] in here 
		ISNULL(@valuePassedIn, '') = '' OR
		(
			dataUserMastsQuery.FullName    LIKE '%' + @valuePassedIn + '%' OR
			dataUserMastsQuery.CellPhone   LIKE '%' + @valuePassedIn + '%' OR
			dataUserMastsQuery.OfficePhone LIKE '%' + @valuePassedIn + '%' OR
			dataUserMastsQuery.[Role]      LIKE '%' + @valuePassedIn + '%' OR
			EXISTS
			(
				SELECT TOP 1 1
				FROM STRING_SPLIT(dataAccountsQuery.CompanyNamesDelimited, '|')
				WHERE [value] LIKE '%' + @valuePassedIn + '%'
			)
		)
	)
	AND
	(
		-- Perform filtering in here, as we do not want to expose certain records to a non-Admin restricted user.
		1 = CASE @typeOfUser
				WHEN 'Login' THEN CASE WHEN dbo.GetScalarValueFromJSON(@filter, 'UserName') = dataUserMastsQuery.UserName AND 
											dbo.GetScalarValueFromJSON(@filter, 'Password') = dataUserMastsQuery.[Password]   THEN 1 ELSE 0 END	
				WHEN 'U'     THEN CASE WHEN CONVERT(UNIQUEIDENTIFIER, @filter)              = dataUserMastsQuery.UserGuid     THEN 1 ELSE 0 END
				WHEN 'C'     THEN CASE WHEN @filter = dataAccountsQuery.DataAccountIdsDelimited                                        THEN 1 ELSE 0 END 
				WHEN 'Admin' THEN 1 -- An Admin user gets all records (no filtering)
				ELSE 0              -- This is possibly an error condition as the @typeOfUser should always have a recognized value
			END
	)

END
GO




-- *** Do to the way Stored Procedures are compiled, the built-in method JSON_VALUE may cause runtime errors, so wrap it in a Function.
IF EXISTS (SELECT TOP 1 1 FROM sys.objects WHERE OBJECT_ID(N'[dbo].[GetScalarValueFromJSON]') IS NOT NULL) BEGIN
	DROP FUNCTION [dbo].[GetScalarValueFromJSON] 
END
GO

CREATE FUNCTION [dbo].[GetScalarValueFromJSON] 
(
	@JSON VARCHAR(200),
	@Field VARCHAR(100)
) 
RETURNS VARCHAR(200)
BEGIN

	RETURN JSON_VALUE(@JSON, 'lax$.' + @Field) -- 'lax' specifies that if the JSON parse fails, JSON_VALUE returns NULL instead of throwing an exception.

END
GO

