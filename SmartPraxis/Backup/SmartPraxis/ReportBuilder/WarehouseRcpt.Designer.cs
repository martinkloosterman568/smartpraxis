using Telerik.Reporting;

namespace SmartPraxis.ReportBuilder
{
    partial class WarehouseRcpt
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WarehouseRcpt));
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.groupFooterSection = new Telerik.Reporting.GroupFooterSection();
            this.groupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
            this.dsSettings = new Telerik.Reporting.SqlDataSource();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.dsGrid = new Telerik.Reporting.SqlDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.panel7 = new Telerik.Reporting.Panel();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.reportHeaderSection = new Telerik.Reporting.ReportHeaderSection();
            this.companyLogoPictureBox = new Telerik.Reporting.PictureBox();
            this.billToPanel = new Telerik.Reporting.Panel();
            this.billToHeaderTextBox = new Telerik.Reporting.TextBox();
            this.table11 = new Telerik.Reporting.Table();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.dsTopHeader = new Telerik.Reporting.SqlDataSource();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.panel3 = new Telerik.Reporting.Panel();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.panel4 = new Telerik.Reporting.Panel();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.panel5 = new Telerik.Reporting.Panel();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.dsCarrier = new Telerik.Reporting.SqlDataSource();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.sqlDataSource2 = new Telerik.Reporting.SqlDataSource();
            this.panel6 = new Telerik.Reporting.Panel();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.table10 = new Telerik.Reporting.Table();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.panel8 = new Telerik.Reporting.Panel();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.sqlDataSource1 = new Telerik.Reporting.SqlDataSource();
            this.dsShipperName = new Telerik.Reporting.SqlDataSource();
            this.dsConsignee = new Telerik.Reporting.SqlDataSource();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57000058889389038D), Telerik.Reporting.Drawing.Unit.Inch(0.17279581725597382D));
            this.textBox1.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox1.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.StyleName = "Normal.TableHeader";
            this.textBox1.Value = "# Pcs";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85208356380462646D), Telerik.Reporting.Drawing.Unit.Inch(0.1727958619594574D));
            this.textBox2.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox2.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.StyleName = "Normal.TableHeader";
            this.textBox2.Value = "Pkg Type";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57000058889389038D), Telerik.Reporting.Drawing.Unit.Inch(0.17279581725597382D));
            this.textBox3.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox3.StyleName = "Normal.TableHeader";
            this.textBox3.Value = "Length";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57000058889389038D), Telerik.Reporting.Drawing.Unit.Inch(0.17279581725597382D));
            this.textBox4.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox4.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.StyleName = "Normal.TableHeader";
            this.textBox4.Value = "Width";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57000058889389038D), Telerik.Reporting.Drawing.Unit.Inch(0.17279581725597382D));
            this.textBox5.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox5.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.StyleName = "Normal.TableHeader";
            this.textBox5.Value = "Height";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.5700000524520874D), Telerik.Reporting.Drawing.Unit.Inch(0.17279577255249023D));
            this.textBox6.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.StyleName = "Normal.TableHeader";
            this.textBox6.Value = "Gross";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3486113548278809D), Telerik.Reporting.Drawing.Unit.Inch(0.1727958470582962D));
            this.textBox7.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox7.StyleName = "Normal.TableHeader";
            this.textBox7.Value = "Tracking #";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.28527805209159851D), Telerik.Reporting.Drawing.Unit.Inch(0.17279581725597382D));
            this.textBox8.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.textBox8.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.StyleName = "Normal.TableHeader";
            this.textBox8.Value = "Haz";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.92499983310699463D), Telerik.Reporting.Drawing.Unit.Inch(0.17279581725597382D));
            this.textBox9.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox9.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.textBox9.StyleName = "Normal.TableHeader";
            this.textBox9.Value = "Un";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72361147403717041D), Telerik.Reporting.Drawing.Unit.Inch(0.17279580235481262D));
            this.textBox10.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox10.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox10.StyleName = "Normal.TableHeader";
            this.textBox10.Value = "Location";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.17279580235481262D));
            this.textBox11.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox11.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.StyleName = "Normal.TableHeader";
            this.textBox11.Value = "Exception";
            // 
            // textBox19
            // 
            this.textBox19.CanGrow = false;
            this.textBox19.CanShrink = true;
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D), Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D));
            this.textBox19.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.StyleName = "Normal.TableHeader";
            this.textBox19.Value = "BOLWeight";
            // 
            // textBox26
            // 
            this.textBox26.CanGrow = false;
            this.textBox26.CanShrink = true;
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D), Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D));
            this.textBox26.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox26.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.StyleName = "Normal.TableHeader";
            this.textBox26.Value = "Total Pieces";
            // 
            // textBox30
            // 
            this.textBox30.CanGrow = false;
            this.textBox30.CanShrink = true;
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D), Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D));
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox30.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.textBox30.Style.BorderColor.Right = System.Drawing.Color.White;
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox30.StyleName = "Normal.TableHeader";
            this.textBox30.Value = "Gross LBS";
            // 
            // textBox33
            // 
            this.textBox33.CanGrow = false;
            this.textBox33.CanShrink = true;
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D), Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D));
            this.textBox33.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox33.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox33.StyleName = "Normal.TableHeader";
            this.textBox33.Value = "Total Cube";
            // 
            // groupFooterSection
            // 
            this.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.052083324640989304D);
            this.groupFooterSection.Name = "groupFooterSection";
            this.groupFooterSection.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.groupFooterSection.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // groupHeaderSection
            // 
            this.groupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.groupHeaderSection.Name = "groupHeaderSection";
            this.groupHeaderSection.PrintOnEveryPage = true;
            this.groupHeaderSection.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.19685038924217224D);
            // 
            // dsSettings
            // 
            this.dsSettings.ConnectionString = "SmartPraxis";
            this.dsSettings.Name = "dsSettings";
            this.dsSettings.SelectCommand = "select top 1 * from settings";
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.39059054851531982D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1});
            this.detailSection1.Name = "detailSection1";
            this.detailSection1.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.detailSection1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.detailSection1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.detailSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.57000082731246948D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.85208356380462646D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.57000082731246948D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.57000082731246948D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.57000082731246948D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.570000171661377D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3486112356185913D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.28527823090553284D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.924999475479126D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.72361153364181519D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.980000376701355D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox12);
            this.table1.Body.SetCellContent(0, 1, this.textBox13);
            this.table1.Body.SetCellContent(0, 2, this.textBox14);
            this.table1.Body.SetCellContent(0, 3, this.textBox15);
            this.table1.Body.SetCellContent(0, 4, this.textBox16);
            this.table1.Body.SetCellContent(0, 5, this.textBox17);
            this.table1.Body.SetCellContent(0, 6, this.textBox18);
            this.table1.Body.SetCellContent(0, 8, this.textBox20);
            this.table1.Body.SetCellContent(0, 9, this.textBox21);
            this.table1.Body.SetCellContent(0, 10, this.textBox22);
            this.table1.Body.SetCellContent(0, 7, this.pictureBox1);
            tableGroup1.ReportItem = this.textBox1;
            tableGroup2.ReportItem = this.textBox2;
            tableGroup3.ReportItem = this.textBox3;
            tableGroup4.ReportItem = this.textBox4;
            tableGroup5.ReportItem = this.textBox5;
            tableGroup6.ReportItem = this.textBox6;
            tableGroup7.ReportItem = this.textBox7;
            tableGroup8.ReportItem = this.textBox8;
            tableGroup9.ReportItem = this.textBox9;
            tableGroup10.ReportItem = this.textBox10;
            tableGroup11.ReportItem = this.textBox11;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.DataSource = this.dsGrid;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.pictureBox1,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.table1.Name = "table1";
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "Detail";
            this.table1.RowGroups.Add(tableGroup12);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9645876884460449D), Telerik.Reporting.Drawing.Unit.Inch(0.3927958607673645D));
            this.table1.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table1.StyleName = "Normal.TableNormal";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57000052928924561D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox12.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.StyleName = "Normal.TableBody";
            this.textBox12.Value = "= Fields.NoPieces";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85208356380462646D), Telerik.Reporting.Drawing.Unit.Inch(0.22000004351139069D));
            this.textBox13.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox13.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.StyleName = "Normal.TableBody";
            this.textBox13.Value = "= Fields.PackageType";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57000052928924561D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.textBox14.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox14.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox14.StyleName = "Normal.TableBody";
            this.textBox14.Value = "= Fields.Length";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57000052928924561D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.textBox15.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox15.StyleName = "Normal.TableBody";
            this.textBox15.Value = "= Fields.Width";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57000052928924561D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.textBox16.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox16.StyleName = "Normal.TableBody";
            this.textBox16.Value = "= Fields.Height";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.5700000524520874D), Telerik.Reporting.Drawing.Unit.Inch(0.2200000137090683D));
            this.textBox17.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox17.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.StyleName = "Normal.TableBody";
            this.textBox17.Value = "= Fields.GrossWeight";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3486113548278809D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.textBox18.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.textBox18.StyleName = "Normal.TableBody";
            this.textBox18.Value = "= Fields.TrackingNumber";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.92499971389770508D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.textBox20.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox20.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox20.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.textBox20.StyleName = "Normal.TableBody";
            this.textBox20.Value = "= Fields.Un";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72361147403717041D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.textBox21.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox21.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox21.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox21.StyleName = "Normal.TableBody";
            this.textBox21.Value = "= Fields.Location";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.textBox22.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox22.StyleName = "Normal.TableBody";
            this.textBox22.Value = "= Fields.ExceptionType";
            // 
            // pictureBox1
            // 
            this.pictureBox1.DocumentMapText = "=Fields.Haz";
            this.pictureBox1.MimeType = "";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.28527802228927612D), Telerik.Reporting.Drawing.Unit.Inch(0.22000002861022949D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.pictureBox1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.pictureBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.pictureBox1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.pictureBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox1.StyleName = "Normal.TableBody";
            this.pictureBox1.Value = "=Fields.Haz";
            // 
            // dsGrid
            // 
            this.dsGrid.ConnectionString = "SmartPraxis";
            this.dsGrid.Name = "dsGrid";
            this.dsGrid.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@warehouseRctGuid", System.Data.DbType.Guid, "= Parameters.warehouseRctGuid.Value")});
            this.dsGrid.SelectCommand = "spGetWarehouseRctDetailsForReport";
            this.dsGrid.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.75732570886611938D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel7});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            // 
            // panel7
            // 
            this.panel7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox42,
            this.textBox57});
            this.panel7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000894069672D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.panel7.Name = "panel7";
            this.panel7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7000007629394531D), Telerik.Reporting.Drawing.Unit.Inch(0.75724649429321289D));
            this.panel7.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel7.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel7.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel7.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel7.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.panel7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.panel7.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.panel7.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(-6.6227383577199816E-09D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5999612808227539D), Telerik.Reporting.Drawing.Unit.Inch(0.57598745822906494D));
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.textBox42.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox42.Value = resources.GetString("textBox42.Value");
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(9.9341077586245774E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.57606637477874756D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.textBox57.Value = "Authorized Signature:";
            // 
            // reportHeaderSection
            // 
            this.reportHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(4.4812207221984863D);
            this.reportHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.companyLogoPictureBox,
            this.billToPanel,
            this.table5,
            this.textBox27,
            this.panel3,
            this.panel4,
            this.panel1,
            this.panel5,
            this.panel2,
            this.panel6,
            this.panel8});
            this.reportHeaderSection.Name = "reportHeaderSection";
            this.reportHeaderSection.Style.BorderColor.Default = System.Drawing.Color.White;
            this.reportHeaderSection.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.19685038924217224D);
            // 
            // companyLogoPictureBox
            // 
            this.companyLogoPictureBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.00043360391282476485D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.companyLogoPictureBox.MimeType = "image/jpeg";
            this.companyLogoPictureBox.Name = "companyLogoPictureBox";
            this.companyLogoPictureBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6995664834976196D), Telerik.Reporting.Drawing.Unit.Inch(0.60000002384185791D));
            this.companyLogoPictureBox.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.companyLogoPictureBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.companyLogoPictureBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.companyLogoPictureBox.Value = ((object)(resources.GetObject("companyLogoPictureBox.Value")));
            // 
            // billToPanel
            // 
            this.billToPanel.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.billToHeaderTextBox,
            this.table11});
            this.billToPanel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0099999997764825821D), Telerik.Reporting.Drawing.Unit.Inch(1.293055534362793D));
            this.billToPanel.Name = "billToPanel";
            this.billToPanel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(1.2069443464279175D));
            this.billToPanel.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.billToPanel.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToPanel.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToPanel.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToPanel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.billToPanel.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // billToHeaderTextBox
            // 
            this.billToHeaderTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.0069444444961845875D));
            this.billToHeaderTextBox.Name = "billToHeaderTextBox";
            this.billToHeaderTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.billToHeaderTextBox.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.billToHeaderTextBox.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.billToHeaderTextBox.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToHeaderTextBox.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToHeaderTextBox.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToHeaderTextBox.Style.Color = System.Drawing.Color.DimGray;
            this.billToHeaderTextBox.Style.Font.Bold = true;
            this.billToHeaderTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.billToHeaderTextBox.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.billToHeaderTextBox.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.billToHeaderTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.billToHeaderTextBox.Value = "SHIPPER:";
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.6816532611846924D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table11.Body.SetCellContent(0, 0, this.textBox67);
            this.table11.Body.SetCellContent(1, 0, this.textBox60);
            this.table11.Body.SetCellContent(2, 0, this.textBox73);
            this.table11.Body.SetCellContent(4, 0, this.textBox78);
            this.table11.Body.SetCellContent(3, 0, this.textBox83);
            this.table11.ColumnGroups.Add(tableGroup13);
            this.table11.DataSource = this.dsTopHeader;
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox67,
            this.textBox60,
            this.textBox73,
            this.textBox83,
            this.textBox78});
            this.table11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.010433620773255825D), Telerik.Reporting.Drawing.Unit.Inch(0.30694451928138733D));
            this.table11.Name = "table11";
            tableGroup15.Name = "group26";
            tableGroup16.Name = "group27";
            tableGroup17.Name = "group28";
            tableGroup18.Name = "group30";
            tableGroup19.Name = "group29";
            tableGroup14.ChildGroups.Add(tableGroup15);
            tableGroup14.ChildGroups.Add(tableGroup16);
            tableGroup14.ChildGroups.Add(tableGroup17);
            tableGroup14.ChildGroups.Add(tableGroup18);
            tableGroup14.ChildGroups.Add(tableGroup19);
            tableGroup14.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup14.Name = "Detail";
            this.table11.RowGroups.Add(tableGroup14);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6816532611846924D), Telerik.Reporting.Drawing.Unit.Inch(0.8399999737739563D));
            this.table11.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table11.StyleName = "Normal.TableNormal";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6816532611846924D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox67.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox67.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox67.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.StyleName = "Normal.TableBody";
            this.textBox67.Value = "= Fields.ShipperName";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6816532611846924D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox60.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox60.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox60.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox60.StyleName = "Normal.TableBody";
            this.textBox60.Value = "= Fields.ShipperForwarderContact";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6816532611846924D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox73.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox73.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox73.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox73.StyleName = "Normal.TableBody";
            this.textBox73.Value = "= Fields.ShipperAddress1";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6816532611846924D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox78.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox78.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox78.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox78.StyleName = "Normal.TableBody";
            this.textBox78.Value = "= Fields.ShipperAddress3";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6816532611846924D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox83.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox83.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox83.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox83.StyleName = "Normal.TableBody";
            this.textBox83.Value = "= Fields.ShipperAddress2";
            // 
            // dsTopHeader
            // 
            this.dsTopHeader.ConnectionString = "SmartPraxis";
            this.dsTopHeader.Name = "dsTopHeader";
            this.dsTopHeader.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@warehouseRctGuid", System.Data.DbType.Guid, "= Parameters.warehouseRctGuid.Value")});
            this.dsTopHeader.SelectCommand = "spGetWarehouseReceiptHeaderInfo";
            this.dsTopHeader.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.5D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox32);
            this.table5.Body.SetCellContent(1, 0, this.textBox25);
            this.table5.Body.SetCellContent(2, 0, this.textBox40);
            this.table5.Body.SetCellContent(4, 0, this.textBox46);
            this.table5.Body.SetCellContent(3, 0, this.textBox53);
            this.table5.ColumnGroups.Add(tableGroup20);
            this.table5.DataSource = this.dsSettings;
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox32,
            this.textBox25,
            this.textBox40,
            this.textBox53,
            this.textBox46});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6930556297302246D), Telerik.Reporting.Drawing.Unit.Inch(1.0761949553739214E-08D));
            this.table5.Name = "table5";
            tableGroup22.Name = "group2";
            tableGroup23.Name = "group3";
            tableGroup24.Name = "group4";
            tableGroup25.Name = "group16";
            tableGroup26.Name = "group15";
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.ChildGroups.Add(tableGroup23);
            tableGroup21.ChildGroups.Add(tableGroup24);
            tableGroup21.ChildGroups.Add(tableGroup25);
            tableGroup21.ChildGroups.Add(tableGroup26);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "Detail";
            this.table5.RowGroups.Add(tableGroup21);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D));
            this.table5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table5.StyleName = "Normal.TableNormal";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.StyleName = "Normal.TableBody";
            this.textBox32.Value = "= Fields.ReportCompany";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox25.StyleName = "Normal.TableBody";
            this.textBox25.Value = "= Fields.ReportAddress";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox40.StyleName = "Normal.TableBody";
            this.textBox40.Value = "= Fields.ReportCityStateZip";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox46.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox46.StyleName = "Normal.TableBody";
            this.textBox46.Value = "= Fields.ReportCom";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox53.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox53.StyleName = "Normal.TableBody";
            this.textBox53.Value = "= Fields.ReportPhone";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6870841979980469D), Telerik.Reporting.Drawing.Unit.Inch(0.89305561780929565D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5059716701507568D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Value = "Warehouse Receipt";
            // 
            // panel3
            // 
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table8});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0099999997764825821D), Telerik.Reporting.Drawing.Unit.Inch(0.8930554986000061D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.panel3.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.98263901472091675D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.86458337306976318D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.1631944477558136D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox44);
            this.table8.Body.SetCellContent(0, 1, this.textBox47);
            this.table8.ColumnGroups.Add(tableGroup27);
            this.table8.ColumnGroups.Add(tableGroup28);
            this.table8.DataSource = this.dsTopHeader;
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox44,
            this.textBox47});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.18684503436088562D), Telerik.Reporting.Drawing.Unit.Inch(0.087098754942417145D));
            this.table8.Name = "table8";
            tableGroup29.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup29.Name = "Detail";
            this.table8.RowGroups.Add(tableGroup29);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8472224473953247D), Telerik.Reporting.Drawing.Unit.Inch(0.1631944477558136D));
            this.table8.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table8.StyleName = "Normal.TableNormal";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9826388955116272D), Telerik.Reporting.Drawing.Unit.Inch(0.1631944477558136D));
            this.textBox44.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox44.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox44.StyleName = "Normal.TableBody";
            this.textBox44.Value = "{Format(\"{{0:MM/dd/yyyy}}\", Fields.CreatedDate)}";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.864583432674408D), Telerik.Reporting.Drawing.Unit.Inch(0.1631944477558136D));
            this.textBox47.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox47.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox47.StyleName = "Normal.TableBody";
            this.textBox47.Value = "{Format(\"{{0:hh:MM:ss tt}}\", Fields.CreatedTime)}";
            // 
            // panel4
            // 
            this.panel4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox28,
            this.table9});
            this.panel4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.8930554986000061D));
            this.panel4.Name = "panel4";
            this.panel4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.008507251739502D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.panel4.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0999993234872818D), Telerik.Reporting.Drawing.Unit.Inch(0.066944383084774017D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89992141723632812D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox28.Value = "W/R Number:";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.89791619777679443D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D)));
            this.table9.Body.SetCellContent(0, 0, this.textBox49);
            this.table9.ColumnGroups.Add(tableGroup30);
            this.table9.DataSource = this.dsTopHeader;
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox49});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.10694450885057449D));
            this.table9.Name = "table9";
            tableGroup31.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup31.Name = "Detail";
            this.table9.RowGroups.Add(tableGroup31);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89791619777679443D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.table9.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table9.StyleName = "Normal.TableNormal";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89791619777679443D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox49.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox49.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox49.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.StyleName = "Normal.TableBody";
            this.textBox49.Value = "= Fields.RctNo";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox23,
            this.table3});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0089414380490779877D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7100000381469727D), Telerik.Reporting.Drawing.Unit.Inch(1.3000003099441528D));
            this.panel1.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            this.panel1.StyleName = "Normal.TableBody";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.0010762214660644531D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7077739238739014D), Telerik.Reporting.Drawing.Unit.Inch(0.2800000011920929D));
            this.textBox23.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.Color = System.Drawing.Color.DimGray;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "CONSIGNEE:";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.6916532516479492D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox51);
            this.table3.Body.SetCellContent(1, 0, this.textBox24);
            this.table3.Body.SetCellContent(2, 0, this.textBox66);
            this.table3.Body.SetCellContent(3, 0, this.textBox74);
            this.table3.Body.SetCellContent(4, 0, this.textBox80);
            this.table3.ColumnGroups.Add(tableGroup32);
            this.table3.DataSource = this.dsTopHeader;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox51,
            this.textBox24,
            this.textBox66,
            this.textBox74,
            this.textBox80});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0014921823749318719D), Telerik.Reporting.Drawing.Unit.Inch(0.30000007152557373D));
            this.table3.Name = "table3";
            tableGroup34.Name = "group6";
            tableGroup35.Name = "group7";
            tableGroup36.Name = "group8";
            tableGroup37.Name = "group9";
            tableGroup38.Name = "group20";
            tableGroup33.ChildGroups.Add(tableGroup34);
            tableGroup33.ChildGroups.Add(tableGroup35);
            tableGroup33.ChildGroups.Add(tableGroup36);
            tableGroup33.ChildGroups.Add(tableGroup37);
            tableGroup33.ChildGroups.Add(tableGroup38);
            tableGroup33.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup33.Name = "Detail";
            this.table3.RowGroups.Add(tableGroup33);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6916532516479492D), Telerik.Reporting.Drawing.Unit.Inch(0.99999994039535522D));
            this.table3.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table3.StyleName = "Normal.TableNormal";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6916532516479492D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox51.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.StyleName = "Normal.TableBody";
            this.textBox51.Value = "= Fields.ConsigneeName";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6916532516479492D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox24.StyleName = "Normal.TableBody";
            this.textBox24.Value = "= Fields.ConsigneeForwarderContact";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6916532516479492D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox66.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox66.StyleName = "Normal.TableBody";
            this.textBox66.Value = "= Fields.ConsigneeAddress1";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6916532516479492D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox74.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox74.StyleName = "Normal.TableBody";
            this.textBox74.Value = "= Fields.ConsigneeAddress2";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6916532516479492D), Telerik.Reporting.Drawing.Unit.Inch(0.19999998807907105D));
            this.textBox80.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox80.StyleName = "Normal.TableBody";
            this.textBox80.Value = "= Fields.ConsigneeAddress3";
            // 
            // panel5
            // 
            this.panel5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table7,
            this.textBox31});
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.25D), Telerik.Reporting.Drawing.Unit.Inch(1.2930556535720825D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.93978971242904663D));
            this.panel5.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.98141801357269287D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.6341965198516846D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D)));
            this.table7.Body.SetCellContent(0, 1, this.textBox41);
            this.table7.Body.SetCellContent(1, 1, this.textBox54);
            this.table7.Body.SetCellContent(0, 0, this.textBox58);
            this.table7.Body.SetCellContent(1, 0, this.textBox59);
            this.table7.Body.SetCellContent(2, 1, this.textBox48);
            this.table7.Body.SetCellContent(2, 0, this.textBox55);
            tableGroup39.Name = "group24";
            this.table7.ColumnGroups.Add(tableGroup39);
            this.table7.ColumnGroups.Add(tableGroup40);
            this.table7.DataSource = this.dsTopHeader;
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox58,
            this.textBox41,
            this.textBox59,
            this.textBox54,
            this.textBox55,
            this.textBox48});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0321638323366642D), Telerik.Reporting.Drawing.Unit.Inch(0.30694451928138733D));
            this.table7.Name = "table7";
            tableGroup42.Name = "group22";
            tableGroup43.Name = "group23";
            tableGroup44.Name = "group25";
            tableGroup41.ChildGroups.Add(tableGroup42);
            tableGroup41.ChildGroups.Add(tableGroup43);
            tableGroup41.ChildGroups.Add(tableGroup44);
            tableGroup41.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup41.Name = "Detail";
            this.table7.RowGroups.Add(tableGroup41);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.615614652633667D), Telerik.Reporting.Drawing.Unit.Inch(0.59999990463256836D));
            this.table7.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.table7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table7.StyleName = "Normal.TableNormal";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6341967582702637D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox41.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox41.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.StyleName = "Normal.TableBody";
            this.textBox41.Value = "= Fields.WRStatus";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6341967582702637D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox54.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox54.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox54.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox54.StyleName = "Normal.TableBody";
            this.textBox54.Value = "= Fields.CustomerRef";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98141813278198242D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox58.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox58.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox58.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox58.StyleName = "Normal.TableBody";
            this.textBox58.Value = "W/R Status:";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98141813278198242D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox59.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox59.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox59.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox59.StyleName = "Normal.TableBody";
            this.textBox59.Value = "Customer Ref:";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6341967582702637D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox48.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox48.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox48.StyleName = "Normal.TableBody";
            this.textBox48.Value = "= Fields.Documents";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98141813278198242D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox55.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox55.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox55.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox55.StyleName = "Normal.TableBody";
            this.textBox55.Value = "Documents:";
            // 
            // textBox31
            // 
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.0069444542750716209D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.textBox31.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox31.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.Color = System.Drawing.Color.DimGray;
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox31.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.Value = "STATUS:";
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2,
            this.textBox39,
            this.table6});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.25D), Telerik.Reporting.Drawing.Unit.Inch(2.2000002861022949D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D));
            this.panel2.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.078076958656311D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.4611885547637939D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table2.Body.SetCellContent(0, 1, this.textBox35);
            this.table2.Body.SetCellContent(0, 0, this.textBox29);
            tableGroup45.Name = "group5";
            this.table2.ColumnGroups.Add(tableGroup45);
            this.table2.ColumnGroups.Add(tableGroup46);
            this.table2.DataSource = this.dsCarrier;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox29,
            this.textBox35});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.03212399035692215D), Telerik.Reporting.Drawing.Unit.Inch(0.2999998927116394D));
            this.table2.Name = "table2";
            tableGroup47.Name = "group1";
            this.table2.RowGroups.Add(tableGroup47);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5392653942108154D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.table2.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table2.StyleName = "Normal.TableNormal";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4611883163452148D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox35.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.textBox35.StyleName = "Normal.TableBody";
            this.textBox35.Value = "= Fields.CompanyName";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0780768394470215D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox29.StyleName = "Normal.TableBody";
            this.textBox29.Value = "Delivered By:";
            // 
            // dsCarrier
            // 
            this.dsCarrier.ConnectionString = "SmartPraxis";
            this.dsCarrier.Name = "dsCarrier";
            this.dsCarrier.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@warehouseRctGuid", System.Data.DbType.Guid, "= Parameters.warehouseRctGuid.Value")});
            this.dsCarrier.SelectCommand = resources.GetString("dsCarrier.SelectCommand");
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(-3.3113691788599908E-09D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.textBox39.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox39.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox39.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.Color = System.Drawing.Color.DimGray;
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.Value = "DELIVERY:";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1162830591201782D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.5314948558807373D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table6.Body.SetCellContent(0, 1, this.textBox52);
            this.table6.Body.SetCellContent(0, 0, this.textBox61);
            tableGroup48.Name = "group10";
            this.table6.ColumnGroups.Add(tableGroup48);
            this.table6.ColumnGroups.Add(tableGroup49);
            this.table6.DataSource = this.sqlDataSource2;
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox61,
            this.textBox52});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3113691788599908E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.table6.Name = "table6";
            tableGroup50.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup50.Name = "Detail";
            this.table6.RowGroups.Add(tableGroup50);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6477780342102051D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.table6.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table6.StyleName = "Normal.TableNormal";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5314948558807373D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox52.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox52.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.textBox52.StyleName = "Normal.TableBody";
            this.textBox52.Value = "= Fields.CoordinatorUserName";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1162830591201782D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox61.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox61.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox61.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox61.StyleName = "Normal.TableBody";
            this.textBox61.Value = "Received By:";
            // 
            // sqlDataSource2
            // 
            this.sqlDataSource2.ConnectionString = "SmartPraxis";
            this.sqlDataSource2.Name = "sqlDataSource2";
            this.sqlDataSource2.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@warehouseRctGuid", System.Data.DbType.Guid, "= Parameters.warehouseRctGuid.Value")});
            this.sqlDataSource2.SelectCommand = "spGetWarehouseReceiptHeaderInfo";
            this.sqlDataSource2.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // panel6
            // 
            this.panel6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox62,
            this.table10});
            this.panel6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0099999997764825821D), Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D));
            this.panel6.Name = "panel6";
            this.panel6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9485077857971191D), Telerik.Reporting.Drawing.Unit.Inch(0.58121997117996216D));
            this.panel6.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel6.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel6.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel6.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel6.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(-3.3113691788599908E-09D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9484691619873047D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.textBox62.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox62.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox62.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox62.Style.Color = System.Drawing.Color.DimGray;
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox62.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox62.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.Value = "COMMENTS:";
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(7.8600001335144043D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table10.Body.SetCellContent(0, 0, this.textBox50);
            this.table10.ColumnGroups.Add(tableGroup51);
            this.table10.DataSource = this.dsTopHeader;
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox50});
            this.table10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.33878028392791748D));
            this.table10.Name = "table10";
            tableGroup52.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup52.Name = "Detail";
            this.table10.RowGroups.Add(tableGroup52);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8600001335144043D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.table10.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.table10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table10.StyleName = "Normal.TableNormal";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8600001335144043D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox50.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox50.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox50.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox50.StyleName = "Normal.TableBody";
            this.textBox50.Value = "= Fields.Comments";
            // 
            // panel8
            // 
            this.panel8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox37,
            this.table4});
            this.panel8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.25D), Telerik.Reporting.Drawing.Unit.Inch(2.8999998569488525D));
            this.panel8.Name = "panel8";
            this.panel8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.panel8.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel8.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(-2.3179584474064541E-08D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.textBox37.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox37.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox37.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.Color = System.Drawing.Color.DimGray;
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = "TOTALS:";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.13999998569488525D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox34);
            this.table4.Body.SetCellContent(0, 1, this.textBox36);
            this.table4.Body.SetCellContent(0, 2, this.textBox38);
            this.table4.Body.SetCellContent(0, 3, this.textBox43);
            tableGroup53.ReportItem = this.textBox19;
            tableGroup54.ReportItem = this.textBox26;
            tableGroup55.ReportItem = this.textBox30;
            tableGroup56.ReportItem = this.textBox33;
            this.table4.ColumnGroups.Add(tableGroup53);
            this.table4.ColumnGroups.Add(tableGroup54);
            this.table4.ColumnGroups.Add(tableGroup55);
            this.table4.ColumnGroups.Add(tableGroup56);
            this.table4.DataSource = this.sqlDataSource1;
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox34,
            this.textBox36,
            this.textBox38,
            this.textBox43,
            this.textBox19,
            this.textBox26,
            this.textBox30,
            this.textBox33});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000017285346985D), Telerik.Reporting.Drawing.Unit.Inch(0.30000007152557373D));
            this.table4.Name = "table4";
            tableGroup57.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup57.Name = "Detail";
            this.table4.RowGroups.Add(tableGroup57);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5199997425079346D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.table4.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table4.StyleName = "Normal.TableNormal";
            // 
            // textBox34
            // 
            this.textBox34.CanGrow = false;
            this.textBox34.CanShrink = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D), Telerik.Reporting.Drawing.Unit.Inch(0.13999998569488525D));
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.StyleName = "Normal.TableBody";
            this.textBox34.Value = "= Fields.BOLWeight";
            // 
            // textBox36
            // 
            this.textBox36.CanGrow = false;
            this.textBox36.CanShrink = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D), Telerik.Reporting.Drawing.Unit.Inch(0.13999998569488525D));
            this.textBox36.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox36.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.StyleName = "Normal.TableBody";
            this.textBox36.Value = "= Fields.TotalPieces";
            // 
            // textBox38
            // 
            this.textBox38.CanGrow = false;
            this.textBox38.CanShrink = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D), Telerik.Reporting.Drawing.Unit.Inch(0.13999998569488525D));
            this.textBox38.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox38.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.textBox38.Style.BorderColor.Right = System.Drawing.Color.White;
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox38.StyleName = "Normal.TableBody";
            this.textBox38.Value = "= Fields.GrossWeight";
            // 
            // textBox43
            // 
            this.textBox43.CanGrow = false;
            this.textBox43.CanShrink = true;
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87999993562698364D), Telerik.Reporting.Drawing.Unit.Inch(0.13999998569488525D));
            this.textBox43.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox43.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox43.StyleName = "Normal.TableBody";
            this.textBox43.Value = "= Fields.TotalCube";
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionString = "SmartPraxis";
            this.sqlDataSource1.Name = "sqlDataSource1";
            this.sqlDataSource1.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@warehouseRctGuid", System.Data.DbType.Guid, "= Parameters.warehouseRctGuid.Value")});
            this.sqlDataSource1.SelectCommand = "spGetWarehouseReceiptHeaderInfo";
            this.sqlDataSource1.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // dsShipperName
            // 
            this.dsShipperName.ConnectionString = "SmartPraxis";
            this.dsShipperName.Name = "dsShipperName";
            this.dsShipperName.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@warehouseRctGuid", System.Data.DbType.Guid, "= Parameters.warehouseRctGuid.Value")});
            this.dsShipperName.SelectCommand = resources.GetString("dsShipperName.SelectCommand");
            // 
            // dsConsignee
            // 
            this.dsConsignee.ConnectionString = "SmartPraxis";
            this.dsConsignee.Name = "dsConsignee";
            this.dsConsignee.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@warehouseRctGuid", System.Data.DbType.Guid, "= Parameters.warehouseRctGuid.Value")});
            this.dsConsignee.SelectCommand = resources.GetString("dsConsignee.SelectCommand");
            // 
            // WarehouseRcpt
            // 
            this.Culture = new System.Globalization.CultureInfo("en-US");
            this.DataSource = this.dsSettings;
            group1.GroupFooter = this.groupFooterSection;
            group1.GroupHeader = this.groupHeaderSection;
            group1.Name = "group";
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection,
            this.groupFooterSection,
            this.detailSection1,
            this.pageFooterSection1,
            this.reportHeaderSection});
            this.Name = "Invoice1";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "warehouseRctGuid";
            reportParameter1.Text = "warehouseRctGuid";
            reportParameter1.Value = "08F7ED36-32AE-4110-8CF8-5192803DCC74";
            this.ReportParameters.Add(reportParameter1);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Normal.TableNormal")});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Color = System.Drawing.Color.Black;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableBody")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableHeader")});
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule5.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule5.Style.Font.Name = "Tahoma";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Inch;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.96999979019165D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private SqlDataSource dsSettings;
        private DetailSection detailSection1;
        private PageFooterSection pageFooterSection1;
        private ReportHeaderSection reportHeaderSection;
        private GroupHeaderSection groupHeaderSection;
        private GroupFooterSection groupFooterSection;
        private Table table1;
        private TextBox textBox12;
        private TextBox textBox13;
        private TextBox textBox14;
        private TextBox textBox15;
        private TextBox textBox16;
        private TextBox textBox17;
        private TextBox textBox18;
        private TextBox textBox20;
        private TextBox textBox21;
        private TextBox textBox22;
        private TextBox textBox1;
        private TextBox textBox2;
        private TextBox textBox3;
        private TextBox textBox4;
        private TextBox textBox5;
        private TextBox textBox6;
        private TextBox textBox7;
        private TextBox textBox8;
        private TextBox textBox9;
        private TextBox textBox10;
        private TextBox textBox11;
        private SqlDataSource dsGrid;
        private Panel panel2;
        private Table table2;
        private TextBox textBox35;
        private TextBox textBox29;
        private SqlDataSource dsCarrier;
        private SqlDataSource dsShipperName;
        private PictureBox companyLogoPictureBox;
        private Panel panel1;
        private TextBox textBox23;
        private Panel billToPanel;
        private TextBox billToHeaderTextBox;
        private Table table5;
        private TextBox textBox32;
        private TextBox textBox25;
        private TextBox textBox40;
        private TextBox textBox46;
        private TextBox textBox53;
        private TextBox textBox27;
        private Panel panel3;
        private Panel panel4;
        private SqlDataSource dsConsignee;
        private PictureBox pictureBox1;
        private TextBox textBox28;
        private Table table8;
        private TextBox textBox44;
        private TextBox textBox47;
        private SqlDataSource dsTopHeader;
        private Table table9;
        private TextBox textBox49;
        private Panel panel5;
        private Table table7;
        private TextBox textBox41;
        private TextBox textBox54;
        private TextBox textBox58;
        private TextBox textBox59;
        private Table table10;
        private TextBox textBox50;
        private TextBox textBox31;
        private TextBox textBox39;
        private Panel panel6;
        private TextBox textBox62;
        private TextBox textBox48;
        private TextBox textBox55;
        private Panel panel7;
        private TextBox textBox42;
        private TextBox textBox57;
        private Table table11;
        private TextBox textBox67;
        private TextBox textBox60;
        private TextBox textBox73;
        private TextBox textBox78;
        private TextBox textBox83;
        private Table table3;
        private TextBox textBox51;
        private TextBox textBox24;
        private TextBox textBox66;
        private TextBox textBox74;
        private TextBox textBox80;
        private Panel panel8;
        private TextBox textBox37;
        private Table table4;
        private TextBox textBox34;
        private TextBox textBox36;
        private TextBox textBox38;
        private TextBox textBox43;
        private TextBox textBox19;
        private TextBox textBox26;
        private TextBox textBox30;
        private TextBox textBox33;
        private SqlDataSource sqlDataSource1;
        private Table table6;
        private TextBox textBox52;
        private TextBox textBox61;
        private SqlDataSource sqlDataSource2;
    }
}