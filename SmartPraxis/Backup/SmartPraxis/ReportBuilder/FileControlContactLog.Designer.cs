using Telerik.Reporting;

namespace SmartPraxis.ReportBuilder
{
    partial class FileControlContactLog
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileControlContactLog));
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.groupFooterSection = new Telerik.Reporting.GroupFooterSection();
            this.groupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
            this.dsSettings = new Telerik.Reporting.SqlDataSource();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.dsDetails = new Telerik.Reporting.SqlDataSource();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.reportHeaderSection = new Telerik.Reporting.ReportHeaderSection();
            this.companyLogoPictureBox = new Telerik.Reporting.PictureBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.panel4 = new Telerik.Reporting.Panel();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.sqlDataSource4 = new Telerik.Reporting.SqlDataSource();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.sqlDataSource3 = new Telerik.Reporting.SqlDataSource();
            this.panel3 = new Telerik.Reporting.Panel();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.sqlDataSource1 = new Telerik.Reporting.SqlDataSource();
            this.panel5 = new Telerik.Reporting.Panel();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.sqlDataSource5 = new Telerik.Reporting.SqlDataSource();
            this.billToPanel = new Telerik.Reporting.Panel();
            this.billToHeaderTextBox = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.sqlDataSource2 = new Telerik.Reporting.SqlDataSource();
            this.panel2 = new Telerik.Reporting.Panel();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.sqlDataSource6 = new Telerik.Reporting.SqlDataSource();
            this.dsHeader = new Telerik.Reporting.SqlDataSource();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5563132762908936D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.StyleName = "Normal.TableHeader";
            this.textBox1.Value = "Created Date";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.6323070526123047D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.StyleName = "Normal.TableHeader";
            this.textBox2.Value = "Activity";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.761379599571228D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.StyleName = "Normal.TableHeader";
            this.textBox3.Value = "Status";
            // 
            // groupFooterSection
            // 
            this.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.052083324640989304D);
            this.groupFooterSection.Name = "groupFooterSection";
            this.groupFooterSection.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.groupFooterSection.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // groupHeaderSection
            // 
            this.groupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.groupHeaderSection.Name = "groupHeaderSection";
            this.groupHeaderSection.PrintOnEveryPage = true;
            this.groupHeaderSection.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.19685038924217224D);
            // 
            // dsSettings
            // 
            this.dsSettings.ConnectionString = "SmartPraxis";
            this.dsSettings.Name = "dsSettings";
            this.dsSettings.SelectCommand = "select top 1 * from settings";
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.42155730724334717D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1});
            this.detailSection1.Name = "detailSection1";
            this.detailSection1.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.detailSection1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.detailSection1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.detailSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.5563132762908936D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(4.6323065757751465D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.761379599571228D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox4);
            this.table1.Body.SetCellContent(0, 1, this.textBox5);
            this.table1.Body.SetCellContent(0, 2, this.textBox6);
            tableGroup1.ReportItem = this.textBox1;
            tableGroup2.ReportItem = this.textBox2;
            tableGroup3.ReportItem = this.textBox3;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.DataSource = this.dsDetails;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox1,
            this.textBox2,
            this.textBox3});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3113691788599908E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.021557280793786049D));
            this.table1.Name = "table1";
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup4.Name = "Detail";
            this.table1.RowGroups.Add(tableGroup4);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9499998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table1.StyleName = "Normal.TableNormal";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5563132762908936D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.StyleName = "Normal.TableBody";
            this.textBox4.Value = "= Fields.CreatedDate";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.6323070526123047D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox5.StyleName = "Normal.TableBody";
            this.textBox5.Value = "= Fields.Activity";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.761379599571228D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox6.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.StyleName = "Normal.TableBody";
            this.textBox6.Value = "= Fields.Status";
            // 
            // dsDetails
            // 
            this.dsDetails.ConnectionString = "SmartPraxis";
            this.dsDetails.Name = "dsDetails";
            this.dsDetails.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@fileControlGuid", System.Data.DbType.Guid, "= Parameters.fileControlGuid.Value")});
            this.dsDetails.SelectCommand = "spGetFileControlContactLogForReport";
            this.dsDetails.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // reportHeaderSection
            // 
            this.reportHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(4.9000000953674316D);
            this.reportHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.companyLogoPictureBox,
            this.table5,
            this.textBox27,
            this.panel4,
            this.panel1,
            this.panel3,
            this.panel5,
            this.billToPanel,
            this.panel2});
            this.reportHeaderSection.Name = "reportHeaderSection";
            this.reportHeaderSection.Style.BorderColor.Default = System.Drawing.Color.White;
            this.reportHeaderSection.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.19685038924217224D);
            // 
            // companyLogoPictureBox
            // 
            this.companyLogoPictureBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.00043360391282476485D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.companyLogoPictureBox.MimeType = "image/jpeg";
            this.companyLogoPictureBox.Name = "companyLogoPictureBox";
            this.companyLogoPictureBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6995664834976196D), Telerik.Reporting.Drawing.Unit.Inch(0.60000002384185791D));
            this.companyLogoPictureBox.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.companyLogoPictureBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.companyLogoPictureBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.companyLogoPictureBox.Value = ((object)(resources.GetObject("companyLogoPictureBox.Value")));
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.5D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox32);
            this.table5.Body.SetCellContent(1, 0, this.textBox25);
            this.table5.Body.SetCellContent(2, 0, this.textBox40);
            this.table5.Body.SetCellContent(4, 0, this.textBox46);
            this.table5.Body.SetCellContent(3, 0, this.textBox53);
            this.table5.ColumnGroups.Add(tableGroup5);
            this.table5.DataSource = this.dsSettings;
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox32,
            this.textBox25,
            this.textBox40,
            this.textBox53,
            this.textBox46});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6930556297302246D), Telerik.Reporting.Drawing.Unit.Inch(1.0761949553739214E-08D));
            this.table5.Name = "table5";
            tableGroup7.Name = "group2";
            tableGroup8.Name = "group3";
            tableGroup9.Name = "group4";
            tableGroup10.Name = "group16";
            tableGroup11.Name = "group15";
            tableGroup6.ChildGroups.Add(tableGroup7);
            tableGroup6.ChildGroups.Add(tableGroup8);
            tableGroup6.ChildGroups.Add(tableGroup9);
            tableGroup6.ChildGroups.Add(tableGroup10);
            tableGroup6.ChildGroups.Add(tableGroup11);
            tableGroup6.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup6.Name = "Detail";
            this.table5.RowGroups.Add(tableGroup6);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D));
            this.table5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table5.StyleName = "Normal.TableNormal";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.StyleName = "Normal.TableBody";
            this.textBox32.Value = "= Fields.ReportCompany";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox25.StyleName = "Normal.TableBody";
            this.textBox25.Value = "= Fields.ReportAddress";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox40.StyleName = "Normal.TableBody";
            this.textBox40.Value = "= Fields.ReportCityStateZip";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox46.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox46.StyleName = "Normal.TableBody";
            this.textBox46.Value = "= Fields.ReportCom";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D));
            this.textBox53.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox53.StyleName = "Normal.TableBody";
            this.textBox53.Value = "= Fields.ReportPhone";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6870841979980469D), Telerik.Reporting.Drawing.Unit.Inch(0.89305561780929565D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5059716701507568D), Telerik.Reporting.Drawing.Unit.Inch(0.29999998211860657D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Value = "Shipment Update";
            // 
            // panel4
            // 
            this.panel4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox28,
            this.table6});
            this.panel4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.8930554986000061D));
            this.panel4.Name = "panel4";
            this.panel4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.008507251739502D), Telerik.Reporting.Drawing.Unit.Inch(0.30694451928138733D));
            this.panel4.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0999993234872818D), Telerik.Reporting.Drawing.Unit.Inch(0.066944383084774017D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60000061988830566D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox28.Value = "Number:";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox48);
            this.table6.ColumnGroups.Add(tableGroup12);
            this.table6.DataSource = this.sqlDataSource4;
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox48});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.070000000298023224D));
            this.table6.Name = "table6";
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup13.Name = "Detail";
            this.table6.RowGroups.Add(tableGroup13);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.table6.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table6.StyleName = "Normal.TableNormal";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox48.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox48.StyleName = "Normal.TableBody";
            this.textBox48.Value = "= Fields.RefNo";
            // 
            // sqlDataSource4
            // 
            this.sqlDataSource4.ConnectionString = "SmartPraxis";
            this.sqlDataSource4.Name = "sqlDataSource4";
            this.sqlDataSource4.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@fileControlGuid", System.Data.DbType.Guid, "= Parameters.fileControlGuid.Value")});
            this.sqlDataSource4.SelectCommand = "spGetFileControlHeaderInfo";
            this.sqlDataSource4.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox23,
            this.table4});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.25D), Telerik.Reporting.Drawing.Unit.Inch(2.5199999809265137D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7100000381469727D), Telerik.Reporting.Drawing.Unit.Inch(1.2100000381469727D));
            this.panel1.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            this.panel1.StyleName = "Normal.TableBody";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.0010762214660644531D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7077739238739014D), Telerik.Reporting.Drawing.Unit.Inch(0.2800000011920929D));
            this.textBox23.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.Color = System.Drawing.Color.DimGray;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "CONSIGNEE:";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.5999999046325684D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox41);
            this.table4.Body.SetCellContent(4, 0, this.textBox9);
            this.table4.Body.SetCellContent(3, 0, this.textBox16);
            this.table4.Body.SetCellContent(2, 0, this.textBox26);
            this.table4.Body.SetCellContent(1, 0, this.textBox60);
            this.table4.ColumnGroups.Add(tableGroup14);
            this.table4.DataSource = this.sqlDataSource3;
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox41,
            this.textBox60,
            this.textBox26,
            this.textBox16,
            this.textBox9});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.31599998474121094D));
            this.table4.Name = "table4";
            tableGroup16.Name = "group14";
            tableGroup17.Name = "group20";
            tableGroup18.Name = "group19";
            tableGroup19.Name = "group18";
            tableGroup20.Name = "group17";
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.ChildGroups.Add(tableGroup17);
            tableGroup15.ChildGroups.Add(tableGroup18);
            tableGroup15.ChildGroups.Add(tableGroup19);
            tableGroup15.ChildGroups.Add(tableGroup20);
            tableGroup15.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup15.Name = "Detail";
            this.table4.RowGroups.Add(tableGroup15);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.87999999523162842D));
            this.table4.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table4.StyleName = "Normal.TableNormal";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.StyleName = "Normal.TableBody";
            this.textBox41.Value = "= Fields.ConsigneeName";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox9.StyleName = "Normal.TableBody";
            this.textBox9.Value = "= Fields.ConsigneeAddress3";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.StyleName = "Normal.TableBody";
            this.textBox16.Value = "= Fields.ConsigneeAddress2";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.StyleName = "Normal.TableBody";
            this.textBox26.Value = "= Fields.ConsigneeAddress1";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox60.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox60.StyleName = "Normal.TableBody";
            this.textBox60.Value = "= Fields.ConsigneeForwarderContact";
            // 
            // sqlDataSource3
            // 
            this.sqlDataSource3.ConnectionString = "SmartPraxis";
            this.sqlDataSource3.Name = "sqlDataSource3";
            this.sqlDataSource3.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@fileControlGuid", System.Data.DbType.Guid, "= Parameters.fileControlGuid.Value")});
            this.sqlDataSource3.SelectCommand = "spGetFileControlHeaderInfo";
            this.sqlDataSource3.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // panel3
            // 
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox19,
            this.table2});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6556846560433769E-08D), Telerik.Reporting.Drawing.Unit.Inch(1.2899999618530273D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7089414596557617D), Telerik.Reporting.Drawing.Unit.Inch(1.2069443464279175D));
            this.panel3.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.0696057367874943E-10D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.textBox19.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox19.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox19.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.Color = System.Drawing.Color.DimGray;
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.Value = "CLIENT:";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.6000003814697266D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.16000001132488251D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox12);
            this.table2.Body.SetCellContent(1, 0, this.textBox7);
            this.table2.Body.SetCellContent(2, 0, this.textBox58);
            this.table2.Body.SetCellContent(3, 0, this.textBox63);
            this.table2.Body.SetCellContent(4, 0, this.textBox68);
            this.table2.ColumnGroups.Add(tableGroup21);
            this.table2.DataSource = this.sqlDataSource1;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12,
            this.textBox7,
            this.textBox58,
            this.textBox63,
            this.textBox68});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6556845894299954E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.31000009179115295D));
            this.table2.Name = "table2";
            tableGroup23.Name = "group1";
            tableGroup24.Name = "group5";
            tableGroup25.Name = "group6";
            tableGroup26.Name = "group7";
            tableGroup27.Name = "group8";
            tableGroup22.ChildGroups.Add(tableGroup23);
            tableGroup22.ChildGroups.Add(tableGroup24);
            tableGroup22.ChildGroups.Add(tableGroup25);
            tableGroup22.ChildGroups.Add(tableGroup26);
            tableGroup22.ChildGroups.Add(tableGroup27);
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup22.Name = "Detail";
            this.table2.RowGroups.Add(tableGroup22);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.880000114440918D));
            this.table2.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table2.StyleName = "Normal.TableNormal";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.1600000262260437D));
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.StyleName = "Normal.TableBody";
            this.textBox12.Value = "= Fields.ClientName";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.1600000262260437D));
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox7.StyleName = "Normal.TableBody";
            this.textBox7.Value = "= Fields.ClientForwarderContact";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.1600000262260437D));
            this.textBox58.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox58.StyleName = "Normal.TableBody";
            this.textBox58.Value = "= Fields.ClientAddress1";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox63.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox63.StyleName = "Normal.TableBody";
            this.textBox63.Value = "= Fields.ClientAddress2";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox68.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox68.StyleName = "Normal.TableBody";
            this.textBox68.Value = "= Fields.ClientAddress3";
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionString = "SmartPraxis";
            this.sqlDataSource1.Name = "sqlDataSource1";
            this.sqlDataSource1.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@fileControlGuid", System.Data.DbType.Guid, "= Parameters.fileControlGuid.Value")});
            this.sqlDataSource1.SelectCommand = "spGetFileControlHeaderInfo";
            this.sqlDataSource1.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // panel5
            // 
            this.panel5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox29,
            this.table7});
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.00043360391282476485D), Telerik.Reporting.Drawing.Unit.Inch(3.75D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(1.0999997854232788D));
            this.panel5.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // textBox29
            // 
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(-3.3113691788599908E-09D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.textBox29.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.Color = System.Drawing.Color.DimGray;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.Value = "#\'s";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.9097217321395874D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.56000018119812D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table7.Body.SetCellContent(0, 1, this.textBox54);
            this.table7.Body.SetCellContent(0, 0, this.textBox11);
            this.table7.Body.SetCellContent(1, 0, this.textBox13);
            this.table7.Body.SetCellContent(1, 1, this.textBox15);
            this.table7.Body.SetCellContent(3, 0, this.textBox22);
            this.table7.Body.SetCellContent(3, 1, this.textBox30);
            this.table7.Body.SetCellContent(2, 0, this.textBox35);
            this.table7.Body.SetCellContent(2, 1, this.textBox36);
            tableGroup28.Name = "group21";
            this.table7.ColumnGroups.Add(tableGroup28);
            this.table7.ColumnGroups.Add(tableGroup29);
            this.table7.DataSource = this.sqlDataSource5;
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox11,
            this.textBox54,
            this.textBox13,
            this.textBox15,
            this.textBox35,
            this.textBox36,
            this.textBox22,
            this.textBox30});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6556846560433769E-08D), Telerik.Reporting.Drawing.Unit.Inch(0.2999996542930603D));
            this.table7.Name = "table7";
            tableGroup31.Name = "group22";
            tableGroup32.Name = "group23";
            tableGroup33.Name = "group25";
            tableGroup34.Name = "group24";
            tableGroup30.ChildGroups.Add(tableGroup31);
            tableGroup30.ChildGroups.Add(tableGroup32);
            tableGroup30.ChildGroups.Add(tableGroup33);
            tableGroup30.ChildGroups.Add(tableGroup34);
            tableGroup30.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup30.Name = "Detail";
            this.table7.RowGroups.Add(tableGroup30);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.469721794128418D), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D));
            this.table7.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table7.StyleName = "Normal.TableNormal";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.559999942779541D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox54.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.StyleName = "Normal.TableBody";
            this.textBox54.Value = "= Fields.PONos";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90972179174423218D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox11.StyleName = "Normal.TableBody";
            this.textBox11.Value = "PO #(s):";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90972179174423218D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox13.StyleName = "Normal.TableBody";
            this.textBox13.Value = "Release #(s):";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.559999942779541D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.StyleName = "Normal.TableBody";
            this.textBox15.Value = "= Fields.ReleaseNos";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90972179174423218D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.StyleName = "Normal.TableBody";
            this.textBox22.Value = "Pickup #(s):";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.559999942779541D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.StyleName = "Normal.TableBody";
            this.textBox30.Value = "= Fields.PickupNos";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90972179174423218D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.StyleName = "Normal.TableBody";
            this.textBox35.Value = "Order #(s):";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.559999942779541D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.StyleName = "Normal.TableBody";
            this.textBox36.Value = "= Fields.OrderNos";
            // 
            // sqlDataSource5
            // 
            this.sqlDataSource5.ConnectionString = "SmartPraxis";
            this.sqlDataSource5.Name = "sqlDataSource5";
            this.sqlDataSource5.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@fileControlGuid", System.Data.DbType.Guid, "= Parameters.fileControlGuid.Value")});
            this.sqlDataSource5.SelectCommand = "spGetFileControlHeaderInfo";
            this.sqlDataSource5.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // billToPanel
            // 
            this.billToPanel.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.billToHeaderTextBox,
            this.table3});
            this.billToPanel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.00043360391282476485D), Telerik.Reporting.Drawing.Unit.Inch(2.5199999809265137D));
            this.billToPanel.Name = "billToPanel";
            this.billToPanel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(1.2069443464279175D));
            this.billToPanel.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.billToPanel.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToPanel.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToPanel.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToPanel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.billToPanel.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // billToHeaderTextBox
            // 
            this.billToHeaderTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.0069444444961845875D));
            this.billToHeaderTextBox.Name = "billToHeaderTextBox";
            this.billToHeaderTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.billToHeaderTextBox.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.billToHeaderTextBox.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.billToHeaderTextBox.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToHeaderTextBox.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToHeaderTextBox.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.billToHeaderTextBox.Style.Color = System.Drawing.Color.DimGray;
            this.billToHeaderTextBox.Style.Font.Bold = true;
            this.billToHeaderTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.billToHeaderTextBox.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.billToHeaderTextBox.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.billToHeaderTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.billToHeaderTextBox.Value = "SHIPPER:";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.5995664596557617D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17599998414516449D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox24);
            this.table3.Body.SetCellContent(1, 0, this.textBox8);
            this.table3.Body.SetCellContent(3, 0, this.textBox14);
            this.table3.Body.SetCellContent(2, 0, this.textBox61);
            this.table3.Body.SetCellContent(4, 0, this.textBox17);
            this.table3.ColumnGroups.Add(tableGroup35);
            this.table3.DataSource = this.sqlDataSource2;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox24,
            this.textBox8,
            this.textBox61,
            this.textBox14,
            this.textBox17});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6556846560433769E-08D), Telerik.Reporting.Drawing.Unit.Inch(0.30000010132789612D));
            this.table3.Name = "table3";
            tableGroup37.Name = "group9";
            tableGroup38.Name = "group10";
            tableGroup39.Name = "group12";
            tableGroup40.Name = "group11";
            tableGroup41.Name = "group13";
            tableGroup36.ChildGroups.Add(tableGroup37);
            tableGroup36.ChildGroups.Add(tableGroup38);
            tableGroup36.ChildGroups.Add(tableGroup39);
            tableGroup36.ChildGroups.Add(tableGroup40);
            tableGroup36.ChildGroups.Add(tableGroup41);
            tableGroup36.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup36.Name = "Detail";
            this.table3.RowGroups.Add(tableGroup36);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5995664596557617D), Telerik.Reporting.Drawing.Unit.Inch(0.87999999523162842D));
            this.table3.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table3.StyleName = "Normal.TableNormal";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5995664596557617D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.StyleName = "Normal.TableBody";
            this.textBox24.Value = "= Fields.ShipperName";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5995664596557617D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox8.StyleName = "Normal.TableBody";
            this.textBox8.Value = "= Fields.ShipperForwarderContact";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5995664596557617D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox14.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox14.StyleName = "Normal.TableBody";
            this.textBox14.Value = "= Fields.ShipperAddress2";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5995664596557617D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox61.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox61.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox61.StyleName = "Normal.TableBody";
            this.textBox61.Value = "= Fields.ShipperAddress1";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5995664596557617D), Telerik.Reporting.Drawing.Unit.Inch(0.17599999904632568D));
            this.textBox17.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox17.StyleName = "Normal.TableBody";
            this.textBox17.Value = "= Fields.ShipperAddress3";
            // 
            // sqlDataSource2
            // 
            this.sqlDataSource2.ConnectionString = "SmartPraxis";
            this.sqlDataSource2.Name = "sqlDataSource2";
            this.sqlDataSource2.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@fileControlGuid", System.Data.DbType.Guid, "= Parameters.fileControlGuid.Value")});
            this.sqlDataSource2.SelectCommand = "spGetFileControlHeaderInfo";
            this.sqlDataSource2.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox39,
            this.table8});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.25D), Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.70000004768371582D));
            this.panel2.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.panel2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.panel2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.078740157186985016D);
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(-3.3113691788599908E-09D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7085080146789551D), Telerik.Reporting.Drawing.Unit.Inch(0.27559053897857666D));
            this.textBox39.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.textBox39.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox39.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.Color = System.Drawing.Color.DimGray;
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.11811023950576782D);
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.Value = "DELIVERY:";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0347219705581665D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.0200002193450928D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table8.Body.SetCellContent(0, 1, this.textBox20);
            this.table8.Body.SetCellContent(0, 0, this.textBox33);
            this.table8.Body.SetCellContent(1, 0, this.textBox34);
            this.table8.Body.SetCellContent(1, 1, this.textBox37);
            tableGroup42.Name = "group26";
            this.table8.ColumnGroups.Add(tableGroup42);
            this.table8.ColumnGroups.Add(tableGroup43);
            this.table8.DataSource = this.sqlDataSource6;
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox33,
            this.textBox20,
            this.textBox34,
            this.textBox37});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(-2.3179584474064541E-08D), Telerik.Reporting.Drawing.Unit.Inch(0.30000007152557373D));
            this.table8.Name = "table8";
            tableGroup45.Name = "group27";
            tableGroup46.Name = "group28";
            tableGroup44.ChildGroups.Add(tableGroup45);
            tableGroup44.ChildGroups.Add(tableGroup46);
            tableGroup44.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup44.Name = "Detail";
            this.table8.RowGroups.Add(tableGroup44);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0547220706939697D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.table8.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.table8.StyleName = "Normal.TableNormal";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0199999809265137D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox20.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.StyleName = "Normal.TableBody";
            this.textBox20.Value = "{Format(\"{{0:MM/dd/yyyy}}\", Fields.RequiredDate)}";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0347220897674561D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox33.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox33.StyleName = "Normal.TableBody";
            this.textBox33.Value = "Required Date:";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0347220897674561D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox34.StyleName = "Normal.TableBody";
            this.textBox34.Value = "Shipping Mode:";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0199999809265137D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox37.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.StyleName = "Normal.TableBody";
            this.textBox37.Value = "= Fields.ShippingMode";
            // 
            // sqlDataSource6
            // 
            this.sqlDataSource6.ConnectionString = "SmartPraxis";
            this.sqlDataSource6.Name = "sqlDataSource6";
            this.sqlDataSource6.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@fileControlGuid", System.Data.DbType.Guid, "= Parameters.fileControlGuid.Value")});
            this.sqlDataSource6.SelectCommand = "spGetFileControlHeaderInfo";
            this.sqlDataSource6.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // dsHeader
            // 
            this.dsHeader.ConnectionString = "SmartPraxis";
            this.dsHeader.Name = "dsHeader";
            this.dsHeader.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@fileControlGuid", System.Data.DbType.Guid, "= Parameters.fileControlGuid.Value")});
            this.dsHeader.SelectCommand = "spGetFileControlHeaderInfo";
            this.dsHeader.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure;
            // 
            // FileControlContactLog
            // 
            this.Culture = new System.Globalization.CultureInfo("en-US");
            this.DataSource = this.dsSettings;
            group1.GroupFooter = this.groupFooterSection;
            group1.GroupHeader = this.groupHeaderSection;
            group1.Name = "group";
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection,
            this.groupFooterSection,
            this.detailSection1,
            this.pageFooterSection1,
            this.reportHeaderSection});
            this.Name = "Invoice1";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "fileControlGuid";
            reportParameter1.Text = "fileControlGuid";
            reportParameter1.Value = "2603D5F6-A4AC-472D-A65E-4331142A1E46";
            this.ReportParameters.Add(reportParameter1);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Normal.TableNormal")});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Color = System.Drawing.Color.Black;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableBody")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableHeader")});
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule5.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule5.Style.Font.Name = "Tahoma";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Inch;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.96999979019165D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private SqlDataSource dsSettings;
        private DetailSection detailSection1;
        private PageFooterSection pageFooterSection1;
        private ReportHeaderSection reportHeaderSection;
        private GroupHeaderSection groupHeaderSection;
        private GroupFooterSection groupFooterSection;
        private Panel panel2;
        private PictureBox companyLogoPictureBox;
        private Panel panel1;
        private TextBox textBox23;
        private Panel billToPanel;
        private TextBox billToHeaderTextBox;
        private Table table5;
        private TextBox textBox32;
        private TextBox textBox25;
        private TextBox textBox40;
        private TextBox textBox46;
        private TextBox textBox53;
        private TextBox textBox27;
        private Panel panel4;
        private TextBox textBox28;
        private TextBox textBox39;
        private Panel panel3;
        private TextBox textBox19;
        private Panel panel5;
        private TextBox textBox29;
        private Table table1;
        private TextBox textBox4;
        private TextBox textBox5;
        private TextBox textBox6;
        private TextBox textBox1;
        private TextBox textBox2;
        private TextBox textBox3;
        private SqlDataSource dsDetails;
        private SqlDataSource dsHeader;
        private Table table6;
        private TextBox textBox48;
        private SqlDataSource sqlDataSource4;
        private Table table4;
        private TextBox textBox41;
        private TextBox textBox9;
        private TextBox textBox16;
        private TextBox textBox26;
        private TextBox textBox60;
        private SqlDataSource sqlDataSource3;
        private Table table2;
        private TextBox textBox12;
        private TextBox textBox7;
        private TextBox textBox58;
        private TextBox textBox63;
        private TextBox textBox68;
        private SqlDataSource sqlDataSource1;
        private Table table7;
        private TextBox textBox54;
        private TextBox textBox11;
        private TextBox textBox13;
        private TextBox textBox15;
        private TextBox textBox22;
        private TextBox textBox30;
        private TextBox textBox35;
        private TextBox textBox36;
        private SqlDataSource sqlDataSource5;
        private Table table3;
        private TextBox textBox24;
        private TextBox textBox8;
        private TextBox textBox14;
        private TextBox textBox61;
        private TextBox textBox17;
        private SqlDataSource sqlDataSource2;
        private Table table8;
        private TextBox textBox20;
        private TextBox textBox33;
        private TextBox textBox34;
        private TextBox textBox37;
        private SqlDataSource sqlDataSource6;
    }
}