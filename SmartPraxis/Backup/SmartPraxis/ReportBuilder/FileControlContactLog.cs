using System.Drawing;
using System.Web;
using Telerik.Reporting;
using Telerik.Reporting.Expressions;

namespace SmartPraxis.ReportBuilder
{
    /// <summary>
    /// Summary description for WarehouseRcpt.
    /// </summary>
    public partial class FileControlContactLog : Report
    {
        public FileControlContactLog()
        {
            //
            // Required for telerik Reporting designer support
            //
            this.InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        [Function(IsVisible = true)]
        public static Image ResolveUrl(string relativeUrl)
        {
            var path = HttpContext.Current.Server.MapPath(relativeUrl);
            return Image.FromFile(path);
        }
    }
}