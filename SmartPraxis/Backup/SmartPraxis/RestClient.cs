﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace SmartPraxis
{
    public interface IRestClient
    {
        Task<MessageResource> SendMessage(string from, string to, string body, List<Uri> mediaUrl);
    }

    public class RestClient : IRestClient
    {
        private readonly ITwilioRestClient _client;

        public RestClient()
        {
            var sid = "ACd8659eddca2c5a3aae205d29ec9962bf";
            var token = "c67b837b8c9dba974d5abae140cc8655";
            this._client = new TwilioRestClient(sid, token);
        }

        public RestClient(ITwilioRestClient client)
        {
            this._client = client;
        }

        public async Task<MessageResource> SendMessage(string fromPhone, string toPhone, string bodyStr, List<Uri> mediaUrlStr)
        {
            var toPhoneNumber = new PhoneNumber(toPhone);
            return await MessageResource.CreateAsync(
                toPhoneNumber,
                from: new PhoneNumber(fromPhone),
                body: bodyStr,
                mediaUrl: mediaUrlStr,
                client: this._client);
        }
    }
}