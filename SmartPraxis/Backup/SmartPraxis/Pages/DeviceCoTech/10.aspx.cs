﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _10 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 722,
                        Left = 18,
                        Right = 77,
                        Top = 643
                    };

                    // prev
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/8.aspx?x={patientGuid}",
                        Bottom = 692,
                        Left = 887,
                        Right = 945,
                        Top = 642
                    };

                    // vvi1
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/16.aspx?x={patientGuid}",
                        Bottom = 235,
                        Left = 426,
                        Right = 548,
                        Top = 171
                    };

                    // ddd1
                    var ch4 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/30.aspx?x={patientGuid}",
                        Bottom = 235,
                        Left = 601,
                        Right = 742,
                        Top = 170
                    };

                    // vvi2
                    var ch5 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/16.aspx?x={patientGuid}",
                        Bottom = 348,
                        Left = 230,
                        Right = 313,
                        Top = 306
                    };

                    // ddd2
                    var ch6 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/30.aspx?x={patientGuid}",
                        Bottom = 348,
                        Left = 327,
                        Right = 422,
                        Top = 305
                    };

                    // vdd
                    var ch7 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/76.aspx?x={patientGuid}",
                        Bottom = 395,
                        Left = 230,
                        Right = 312,
                        Top = 353
                    };

                    // ddi
                    var ch8 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/53.aspx?x={patientGuid}",
                        Bottom = 394,
                        Left = 325,
                        Right = 420,
                        Top = 354
                    };

                    // medtronic
                    var ch9 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/11.aspx?x={patientGuid}",
                        Bottom = 579,
                        Left = 134,
                        Right = 332,
                        Top = 531
                    };

                    // boston
                    var ch10 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/12.aspx?x={patientGuid}",
                        Bottom = 577,
                        Left = 337,
                        Right = 535,
                        Top = 532
                    };

                    // st jude
                    var ch11 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/13.aspx?x={patientGuid}",
                        Bottom = 575,
                        Left = 542,
                        Right = 740,
                        Top = 532
                    };

                    // biotronik
                    var ch12 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/14.aspx?x={patientGuid}",
                        Bottom = 575,
                        Left = 748,
                        Right = 945,
                        Top = 531
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/10.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };

                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    im.HotSpots.Add(ch5);
                    im.HotSpots.Add(ch6);
                    im.HotSpots.Add(ch7);
                    im.HotSpots.Add(ch8);
                    im.HotSpots.Add(ch9);
                    im.HotSpots.Add(ch10);
                    im.HotSpots.Add(ch11);
                    im.HotSpots.Add(ch12);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}

