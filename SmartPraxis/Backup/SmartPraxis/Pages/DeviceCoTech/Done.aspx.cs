﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class Done : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (this.Request.QueryString["x"] != null && this.Request.QueryString["y"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patient = this.Request.QueryString["x"];
                    var pageNumber = this.Request.QueryString["y"];

                    this.PopulatePresentingModeDropDown();
                    this.PopulateSafeModeDropDown();

                    var intVal = int.Parse(pageNumber);
                    var ListAnswers = (from d in db.ListAnswers
                        where d.AnswerPage == intVal
                        select d).FirstOrDefault();
                    if (ListAnswers != null)
                    {
                        this.divSafeModeText.InnerHtml = ListAnswers.AnswerText;
                    }

                    this.DateTimeOfProcedure.Text = DateTime.Now.ToString(CultureInfo.InvariantCulture);

                    const string tableName = "DataPatientRequests"; // <-- put table name here
                    const string fieldName = "PatientGuid"; // <-- put the field name here
                    string outError;
                    var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByGuid(tableName, fieldName, patient, out outError);
                    if (destination != null)
                    {
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            this.FindOnReturnOfRedirectId(result.PatientId.ToString());
                            //var record = destination.FirstOrDefault();
                            //if (record != null)
                            //{
                            //    this.divSafeModeText.InnerHtml = record.DeviceCoSelectedSafeMode;
                            //}
                        }
                    }
                    else
                    {
                        this.Session.Abandon();
                        this.Response.Redirect(Global.returnToPage);
                    }
                }
            }
            else
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
        }

        private void PopulatePresentingModeDropDown()
        {
            var repository = new ListPresentingModeRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.DeviceCoPresentingMode.DataSource = destination;
            this.DeviceCoPresentingMode.DataValueField = "Description";
            this.DeviceCoPresentingMode.DataTextField = "Description";
            this.DeviceCoPresentingMode.DataBind();

            this.DeviceCoPresentingMode.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void PopulateSafeModeDropDown()
        {
            var repository = new ListSelectedSafeModeRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.DeviceTechPickedSafeMode.DataSource = destination;
            this.DeviceTechPickedSafeMode.DataValueField = "Description";
            this.DeviceTechPickedSafeMode.DataTextField = "Description";
            this.DeviceTechPickedSafeMode.DataBind();

            this.DeviceTechPickedSafeMode.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataPatientRequests"; // <-- put table name here
            const string fieldName = "PatientId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        protected void btnRefreshTime_Click(object sender, EventArgs e)
        {
            this.DateTimeOfProcedure.Text = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        protected void btnSaveComplete_Click(object sender, EventArgs e)
        {
            var required = false;

            if (this.DeviceCoDeviceModelSN.Text == string.Empty)
            {
                this.DeviceCoDeviceModelSN.BackColor = Color.FromArgb(0xffdae0);
                required = true;
            }
            else
            {
                this.DeviceCoDeviceModelSN.BackColor = Color.White;
            }

            if (this.DeviceCoPresentingMode.SelectedIndex == 0)
            {
                this.DeviceCoPresentingMode.BackColor = Color.FromArgb(0xffdae0);
                required = true;
            }
            else
            {
                this.DeviceCoPresentingMode.BackColor = Color.White;
            }

            if (this.DeviceTechPickedSafeMode.SelectedIndex == 0)
            {
                this.DeviceTechPickedSafeMode.BackColor = Color.FromArgb(0xffdae0);
                required = true;
            }
            else
            {
                this.DeviceTechPickedSafeMode.BackColor = Color.White;
            }

            if (required == false)
            {
                var patient = new Guid(this.Request.QueryString["x"]);
                var patientRecord = (from d in this.db.DataPatientRequests
                    where d.PatientGuid == patient
                    select d).FirstOrDefault();

                if (patientRecord != null)
                {
                    var patientGuid = this.Request.QueryString["x"];
                    var pageNo = this.Request.QueryString["y"];
                    var userGuid = this.Session["UserGuid"].ToString();
                    var theDate = Convert.ToDateTime(this.DateTimeOfProcedure.Text);
                    var theTime = theDate.ToString("H:mm:ss");
                    var saveArray = new DonePageSaving.SavingDone
                    {
                        PageNumber = pageNo,
                        PatientGuid = patientGuid,
                        DeviceCoDeviceModelSN = this.DeviceCoDeviceModelSN.Text,
                        DeviceCoPresentingMode = this.DeviceCoPresentingMode.SelectedItem.Text,
                        DeviceTechPickedSafeMode = this.DeviceTechPickedSafeMode.SelectedItem.Text,
                        DeviceCoCommentsAtTimeOfProcedure = this.DeviceCoCommentsAtTimeOfProcedure.Text,
                        DeviceCoRepAtTimeOfProcedure = this.Session["UserName"].ToString(),
                        DeviceCoTechUserGuidAtTimeOfProcedure = userGuid,
                        MRIScanDatePerformed = theDate,
                        MRIScanTimePerformed = theTime
                    };

                    DonePageSaving.SaveDonePage(saveArray, userGuid);

                    this.Response.Redirect("MobileList.aspx");
                }
            }
        }
    }

    
}

