﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mobile.aspx.cs" Inherits="SmartPraxis.Pages.DeviceCoTech.Mobile"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" Title="Smart-Praxis" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50508.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page on this section.--%>
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <script type="text/javascript">
        function OnDayRender(calendarInstance, args) {
            // convert the date-triplet to a javascript date
            // we need Date.getDay() method to determine 
            // which days should be disabled (e.g. every Saturday (day = 6) and Sunday (day = 0))                
            var jsDate = new Date(args.get_date()[0], args.get_date()[1] - 1, args.get_date()[2]);
            if (jsDate.getDay() == 0 || jsDate.getDay() == 6) {
                var otherMonthCssClass = "rcOutOfRange";
                args.get_cell().className = otherMonthCssClass;
                // replace the default cell content (anchor tag) with a span element 
                // that contains the processed calendar day number -- necessary for the calendar skinning mechanism 
                args.get_cell().innerHTML = "<span>" + args.get_date()[2] + "</span>";
                // disable selection and hover effect for the cell
                args.get_cell().DayId = "";
            }
        }
    </script>
    <style type="text/css">
        .modalBackground {
            background-color:Gray;
            filter:alpha(opacity=70);
            opacity:0.7;
        }
        
        .modalPopup {
	        background-color:#ffffdd;
	        border-width:3px;
	        border-style:solid;
	        border-color:Gray;
	        padding:3px;
	        width:250px;
        }
    </style>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
	    var counter;
	    counter = 0;

	    function KeepSessionAlive() {
	        // Increase counter value, so we'll always get unique URL (sample source from page)
	        // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
	        counter++;

	        // Gets reference of image
	        var img = document.getElementById("imgSessionAlive");

	        // Set new src value, which will cause request to server, so
	        // session will stay alive
	        var pathname = this.Session["PathName"] != null ? this.Session["PathName"].ToString() : string.Empty;
	        img.src = string.Format("http://{0}.com/RefreshSessionState.aspx?c=", pathname) + counter;

	        // Schedule new call of KeepSessionAlive function after 60 seconds
	        setTimeout(KeepSessionAlive, 60000);
	    }

	    // Run function for a first time
	    KeepSessionAlive();
	</script> 
   <div>
       <br/><br/>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-lg-10">
                            <div class="panel panel-default">
                                
                                <div style="vertical-align: bottom;" class="panel-heading">
                                    <h4>Patient Request</h4>    
                                </div>
                             
                                <div runat="server" id="divbody" class="panel-body">
                                        <asp:Panel ID="Panel1" runat="server">
                                        
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <div runat="server" id="divBtnSubmit"><asp:Button ID="btnsubmit" Width="250px" runat="server" Text="I Accept Appointment" class="btn btn-success" OnClick="btnsubmit1a_Click" /></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <div runat="server" id="divBtnReject"><asp:Button ID="btnreject" Width="250px" runat="server" Text="Reject without Message" class="btn btn-danger" OnClick="btnreject_Click" /></div>
                                                </div>
                                                <br/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        MRI Center</label>
                                                    <asp:TextBox ID="MRICompany" alt="required" CssClass="form-control" runat="server" placeholder="MRI Company" ValidationGroup="Required" SkinID="Required" Enabled="False" />
                                                    <asp:HiddenField id="MRICompanyAccountGuid" runat="server"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Patient Name</label>
                                                    <asp:TextBox ID="PatientName" alt="required" CssClass="form-control" name="firstname" runat="server" accept="First Name" placeholder="First Name" ValidationGroup="Required" SkinID="Required" Enabled="False" />
                                                    <asp:HiddenField id="hfErrorMessage" runat="server"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Patient DOB</label><br/>
                                                    <telerik:RadDatePicker ID="DOB" runat="server" MinDate="1900-01-01" Enabled="False">
                                                        <Calendar EnableWeekends="True" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                                        </Calendar>
                                                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                                                            <EmptyMessageStyle Resize="None" />
                                                            <ReadOnlyStyle Resize="None" />
                                                            <FocusedStyle Resize="None" />
                                                            <DisabledStyle Resize="None" />
                                                            <InvalidStyle Resize="None" />
                                                            <HoveredStyle Resize="None" />
                                                            <EnabledStyle Resize="None" />
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Patient Phone</label>
                                                        <asp:TextBox ID="Phone" alt="required" CssClass="form-control" name="phone" runat="server" accept="Phone" placeholder="Phone" ValidationGroup="Required" SkinID="Required" Enabled="False" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Insurance</label>
                                                    <asp:TextBox ID="Insurance" alt="required" CssClass="form-control" name="insurance" runat="server" accept="Insurance" placeholder="Insurance" ValidationGroup="Required" SkinID="Required" Enabled="False" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Device Company</label>
                                                    <asp:TextBox ID="DeviceCompanyName" alt="required" CssClass="form-control" name="devicecompany" runat="server" accept="DeviceCompany" placeholder="Device Company" ValidationGroup="Required" SkinID="Required" Enabled="False" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        MRI Scan Schedule</label><br/>
                                                    <label>
                                                        Date</label>
                                                    <telerik:RadDatePicker ID="MRIScanDate" runat="server" Culture="en-US" Enabled="False">
                                                        <Calendar FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                                            <ClientEvents OnDayRender="OnDayRender" />
                                                            <DayStyle BackColor="lightblue" BorderColor="White" BorderStyle="None" />
                                                        </Calendar>
                                                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                                                            <EmptyMessageStyle Resize="None" />
                                                            <ReadOnlyStyle Resize="None" />
                                                            <FocusedStyle Resize="None" />
                                                            <DisabledStyle Resize="None" />
                                                            <InvalidStyle Resize="None" />
                                                            <HoveredStyle Resize="None" />
                                                            <EnabledStyle Resize="None" />
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Time</label>
                                                    <telerik:RadTimePicker ID="MRIScanTime" runat="server" Culture="en-US" Enabled="False">
                                                        <Calendar EnableWeekends="True" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                                        </Calendar>
                                                        <DatePopupButton CssClass="" HoverImageUrl="" ImageUrl="" Visible="False" />
                                                        <TimeView CellSpacing="-1" EndTime="17:30:59" Interval="00:15:00" StartTime="09:00:00">
                                                        </TimeView>
                                                        <TimePopupButton CssClass="" HoverImageUrl="" ImageUrl="" />
                                                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="64px" Width="">
                                                            <EmptyMessageStyle Resize="None" />
                                                            <ReadOnlyStyle Resize="None" />
                                                            <FocusedStyle Resize="None" />
                                                            <DisabledStyle Resize="None" />
                                                            <InvalidStyle Resize="None" />
                                                            <HoveredStyle Resize="None" />
                                                            <EnabledStyle Resize="None" />
                                                        </DateInput>
                                                    </telerik:RadTimePicker>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Comments From MRI Center</label>
                                                    <asp:TextBox ID="Comments" alt="required" CssClass="form-control" name="comments" runat="server" accept="Comments" placeholder="Comments" ValidationGroup="Required" SkinID="NotRequired" Rows="3" TextMode="MultiLine" Enabled="False" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Reply With Comments</label>
                                                    <asp:TextBox ID="TechCommentsReply" alt="required" CssClass="form-control" name="comments" runat="server" accept="Comments" placeholder="Comments" ValidationGroup="Required" SkinID="NotRequired" Rows="3" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <div runat="server" id="div1"><asp:Button ID="btnComment" Width="250px" runat="server" Text="Accept w/ Comment" class="btn btn-info" OnClick="btnComment_Click" /></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <div runat="server" id="div2"><asp:Button ID="Button1" Width="250px" runat="server" Text="Reject w/ Comment" class="btn btn-warning" OnClick="btnCommentReject_Click" /></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.row (nested) -->
                                        </asp:Panel>
                                        <!-- /.row (nested) -->
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>  
           
<br/>     <br/><br/><br/>
    </div>
    <!-- DataTables JavaScript -->
    <script src="/Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="/Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gvGrid').DataTable({
                responsive: true
            });
        });
    </script>
</asp:Content>
