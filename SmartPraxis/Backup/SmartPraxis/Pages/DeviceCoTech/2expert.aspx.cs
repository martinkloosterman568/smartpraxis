﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _2expert : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // Goto Start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/1.aspx?x={patientGuid}",
                        Bottom = 701,
                        Left = 19,
                        Right = 78,
                        Top = 642
                    };


                    // Yes
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/3.aspx?x={patientGuid}",
                        Bottom = 673,
                        Left = 392,
                        Right = 456,
                        Top = 617
                    };

                    // No
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/15.aspx?x={patientGuid}",
                        Bottom = 672,
                        Left = 510,
                        Right = 571,
                        Top = 618
                    };

                    // Goto Prev
                    var ch4 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/1.aspx?x={patientGuid}",
                        Bottom = 697,
                        Left = 893,
                        Right = 946,
                        Top = 655
                    };

                    // Goto Medtronic
                    var ch5 = new RectangleHotSpot
                    {
                        NavigateUrl = "http://www.mrisurescan.com/",
                        Target = "_blank",
                        Bottom = 273,
                        Left = 326,
                        Right = 635,
                        Top = 208
                    };

                    // Goto Boston Scientific
                    var ch6 = new RectangleHotSpot
                    {
                        NavigateUrl = "http://www.bostonscientific.com/imageready/en-US/model-lookup.html",
                        Target = "_blank",
                        Bottom = 347,
                        Left = 327,
                        Right = 636,
                        Top = 279
                    };

                    // Goto St Jude Abbott
                    var ch7 = new RectangleHotSpot
                    {
                        NavigateUrl = "https://mri.merlin.net/",
                        Target = "_blank",
                        Bottom = 423,
                        Left = 326,
                        Right = 637,
                        Top = 356
                    };

                    // Goto BioTronik
                    var ch8 = new RectangleHotSpot
                    {
                        NavigateUrl = "https://www.promricheck.com/spring/main?execution=e3s1",
                        Target = "_blank",
                        Bottom = 496,
                        Left = 327,
                        Right = 635,
                        Top = 429
                    };

                    // Goto Expert
                    var ch9 = new RectangleHotSpot
                    {
                        NavigateUrl = "https://www.cnn.com",
                        Target = "_blank",
                        Bottom = 570,
                        Left = 327,
                        Right = 636,
                        Top = 505
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/2expert.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    im.HotSpots.Add(ch5);
                    im.HotSpots.Add(ch6);
                    im.HotSpots.Add(ch7);
                    im.HotSpots.Add(ch8);
                    im.HotSpots.Add(ch9);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}
