﻿using System;
using System.Diagnostics;
using System.Web.UI;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _2 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];
                    var groupName = this.Session["GroupName"].ToString();

                    if (!string.IsNullOrEmpty(groupName))
                    {
                        this.Response.Redirect($"~/Pages/DeviceCoTech/2{groupName}.aspx?x={patientGuid}");
                    }
                    else
                    {
                        // need to create a new empty patient record - and get the guid
                        Debugger.Break();
                        this.Response.Redirect($"~/Pages/DeviceCoTech/1.aspx?x={patientGuid}");
                    }
                }
            }
        }
    }
}
