﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _8 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // START
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/1.aspx?x={patientGuid}",
                        Bottom = 722,
                        Left = 18,
                        Right = 77,
                        Top = 642
                    };

                    // PREV
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/2.aspx?x={patientGuid}",
                        Bottom = 721,
                        Left = 892,
                        Right = 947,
                        Top = 654
                    };

                    // YES
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/9.aspx?x={patientGuid}",
                        Bottom = 443,
                        Left = 352,
                        Right = 443,
                        Top = 390
                    };

                    // NO
                    var ch4 = new RectangleHotSpot
                    {
                        Bottom = 444,
                        Left = 560,
                        Right = 643,
                        Top = 393
                    };

                    var groupName = this.Session["GroupName"].ToString();
                    if (!groupName.ToLower().Contains("medtronic"))
                    {
                        // no it is not medtronic
                        ch4.NavigateUrl = $"~/Pages/DeviceCoTech/10.aspx?x={patientGuid}";
                    }
                    else
                    {
                        // yes, it is medtronic
                        ch4.NavigateUrl = $"~/Pages/DeviceCoTech/11.aspx?x={patientGuid}";
                    }

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/8.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}