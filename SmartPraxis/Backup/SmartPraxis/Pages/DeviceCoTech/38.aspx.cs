﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _38 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 722,
                        Left = 18,
                        Right = 77,
                        Top = 642
                    };

                    // prev
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/30.aspx?x={patientGuid}",
                        Bottom = 721,
                        Left = 892,
                        Right = 947,
                        Top = 654
                    };

                    // AP
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/46.aspx?x={patientGuid}",
                        Bottom = 276,
                        Left = 377,
                        Right = 481,
                        Top = 220
                    };

                    // SR
                    var ch4 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/39.aspx?x={patientGuid}",
                        Bottom = 275,
                        Left = 615,
                        Right = 712,
                        Top = 219
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/38.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    this.divPage.Controls.Add(im);

                    
                }
            }
        }
    }
}


