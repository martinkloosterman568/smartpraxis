﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _36 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 724,
                        Left = 13,
                        Right = 74,
                        Top = 649
                    };
                   
                    // prev
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/35.aspx?x={patientGuid}",
                        Bottom = 722,
                        Left = 893,
                        Right = 950,
                        Top = 658
                    };

                    // done button
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/Done.aspx?x={patientGuid}&y=36",
                        Bottom = 721,
                        Left = 820,
                        Right = 880,
                        Top = 654
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/36.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}


