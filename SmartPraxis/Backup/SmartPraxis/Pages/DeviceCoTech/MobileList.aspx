﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileList.aspx.cs" Inherits="SmartPraxis.Pages.DeviceCoTech.MobileList"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" Title="Smart-Praxis" %>


<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
     <meta name="viewport" content="width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
     <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrapWithButtons.css" />
    <img alt="" id="imgSessionAlive" width="1" height="1" />
     
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            var pathname = this.Session["PathName"] != null ? this.Session["PathName"].ToString() : string.Empty;
            img.src = string.Format("http://{0}.com/RefreshSessionState.aspx?c=", pathname) + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
    <div class="user-stats">
        <div class="row">
           <%-- <div class="col-md-1">
            </div>--%>
            <div class="col-md-4">
                <h3>
                    <asp:Label ID="lblUserList" runat="server" Style="margin-left:15px;" Text="Label"></asp:Label>
                </h3>                
            </div>
        </div>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                      <%-- <div class="col-md-1">
                            
                        </div>--%>
                        <div class="col-lg-10">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <% // need to put this into a table so mobile can see the filter next to the Create new button %>
                            <% if (this.Session["Role"].ToString() != "MRI Tech" && 
                                    this.Session["Role"].ToString() != "MRI Doctor" &&
                                    this.Session["Role"].ToString() != "Device Co Tech")
                               { %>
                            <div class="col-lg-4">
                                <asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New User" OnClick="CreateNew_Click"  />
                            </div>
                            <% } %>
                            <div class="col-lg-4">
                                <label>Filter:&nbsp;</label>
                                <div>
                                    <asp:TextBox ID="Filter" Placeholder="Filter Text" Height="32px" runat="server" AutoPostBack="True" OnTextChanged="Filter_TextChanged" ></asp:TextBox>&nbsp;
                                </div><br/>
                                <div>
                                    &nbsp;<asp:Button ID="Find" class="btn btn-success" runat="server" Text="Find"  />&nbsp; <%--this donesn't have a method, just leaving the focus will make the search activate--%>
                                    <asp:Button ID="ClearFilter" class="btn btn-warning" runat="server" Text="Clear" OnClick="ClearFilter_Click"  />
                                </div>
                            </div>
                            <br/><br/><br/>
                        </div>
                        <div class="panel-body">
                           <!-- put content here !-->
                            <asp:GridView ID="gvGrid" runat="server" Style="max-width: 400px"  CssClass="footable"
                            AutoGenerateColumns="False"
                            DataKeyNames="PatientId,PatientName" 
                            OnRowDataBound="GvGridRowDataBound" EmptyDataText="No Rows Found" AllowPaging="True" OnPageIndexChanging="gvGrid_PageIndexChanging" ShowHeaderWhenEmpty="True" OnSorting="gvGrid_Sorting" AllowSorting="True">
                                 <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" Font-Size="14pt" />
                                 <HeaderStyle CssClass="gridHeader" />
                            <Columns>
                                <asp:TemplateField HeaderText="Patient Name" SortExpression="PatientName">
                                    <ItemStyle Wrap="True" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink" runat="server" data-toggle="tooltip" data-placement="top"
                                         NavigateUrl='<%# String.Format("/Pages/DeviceCoTech/MobileDetails.aspx?x={0}&action=edit", this.Eval("PatientGuid")) %>'>
                                            <strong><asp:Literal ID="ltrMRICompany" runat="server" Text='<%#this.Eval("MRICompany") %>' /></strong><br/>
                                            <asp:Literal ID="ltrPatientName" Text='<%#this.Eval("PatientName") %>' runat="server" />                                            
                                        </asp:HyperLink></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Date / Time" SortExpression="MRIScanDate">
                                    <ItemTemplate>
                                        <asp:Label ID="ltrMRIScanDate" runat="server" Text='<%#this.Eval("MRIScanDate", "{0:d}") %>' /><br/>
                                        <asp:Literal ID="ltrMRIScanTime" runat="server" Text='<%#this.Eval("MRIScanTime") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No record found</EmptyDataTemplate></asp:GridView><link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css"
                                  rel="stylesheet" type="text/css" /><script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script><script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js"></script><script type="text/javascript">
                                $(function () {
                                    $('[id*=gvGrid]').footable();
                                });
                            </script></div></div></div></ContentTemplate></asp:UpdatePanel><div class="col-md-4">
                                
                </div>
            </div>        
      <%--  <div class="table-responsive">
                
        </div>--%>

    </div>
    <!-- DataTables JavaScript -->
    <script src="../../Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="../../Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#bodyPage_gvGrid').DataTable({
                responsive: true
            });
        });
    </script>
</asp:Content>