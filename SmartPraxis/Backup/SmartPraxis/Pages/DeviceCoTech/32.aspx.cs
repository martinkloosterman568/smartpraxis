﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _32 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 724,
                        Left = 12,
                        Right = 73,
                        Top = 648
                    };

                    // prev
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/31.aspx?x={patientGuid}",
                        Bottom = 724,
                        Left = 892,
                        Right = 949,
                        Top = 657
                    };

                    // done button
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/Done.aspx?x={patientGuid}&y=32",
                        Bottom = 721,
                        Left = 820,
                        Right = 880,
                        Top = 654
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/32.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };

                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}


