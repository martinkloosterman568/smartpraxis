﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileUserList.aspx.cs" Inherits="SmartPraxis.Pages.DeviceCoTech.MobileUserList"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" Title="Smart-Praxis" %>

<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
     <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrapWithButtons.css" />
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <%-- <style type="text/css">
		.gridHeader { font-weight:bold; color:white; background-color:#80a0a0; }
		.gridHeader A { padding-right:15px; padding-left:3px; padding-bottom:0px; color:#ffffff; padding-top:0px; text-decoration:none; }
		.gridHeader A:hover { text-decoration: underline; }
		.gridHeaderSortASC A { background: url(/Content/Images/sortdown.gif) no-repeat 95% 50%; }
		.gridHeaderSortDESC A { background: url(/Content/Images/sortup.gif) no-repeat 95% 50%; }
    </style>--%>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            var pathname = this.Session["PathName"] != null ? this.Session["PathName"].ToString() : string.Empty;
            img.src = string.Format("http://{0}.com/RefreshSessionState.aspx?c=", pathname) + counter;
            ////img.src = "http://localhost:10240/RefreshSessionState.aspx?c=" + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
    <div class="user-stats">
        <div class="row">
            <div class="col-md-4">
                <h3>
                    <asp:Label ID="lblUserList" runat="server" Text="Label"></asp:Label>
                </h3>                
            </div>
        </div>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-10">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <% // need to put this into a table so mobile can see the filter next to the Create new button %>
                            <% if (this.Session["Role"].ToString() != "MRI Tech" && 
                                    this.Session["Role"].ToString() != "MRI Doctor" &&
                                    this.Session["Role"].ToString() != "Device Co Tech")
                               { %>
                            <div class="col-lg-4">
                                <asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New User" OnClick="CreateNew_Click"  />
                            </div>
                            <% } %>
                            <div class="col-lg-4">
                                <label>Filter:&nbsp;</label>
                                <div>
                                    <asp:TextBox ID="Filter" Placeholder="Filter Text" Height="32px" runat="server" AutoPostBack="True" OnTextChanged="Filter_TextChanged" ></asp:TextBox>&nbsp;
                                </div><br/>
                                <div>
                                    &nbsp;<asp:Button ID="Find" class="btn btn-success" runat="server" Text="Find"  />&nbsp; <%--this donesn't have a method, just leaving the focus will make the search activate--%>
                                    <asp:Button ID="ClearFilter" class="btn btn-warning" runat="server" Text="Clear" OnClick="ClearFilter_Click"  />
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                           <!-- put content here !-->
                            <asp:GridView ID="gvGrid" runat="server" Style="max-width: 400px"  CssClass="footable"
                            AutoGenerateColumns="False"
                            DataKeyNames="ID,FullName" 
                            OnRowDataBound="GvGridRowDataBound" EmptyDataText="No Rows Found" AllowPaging="True" OnPageIndexChanging="gvGrid_PageIndexChanging" ShowHeaderWhenEmpty="True" OnSorting="gvGrid_Sorting" AllowSorting="True">
                                 <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" Font-Size="14pt" />
                                 <HeaderStyle CssClass="gridHeader" />
                            <Columns>
                                <asp:TemplateField HeaderText="User Name" SortExpression="FullName">
                                    <ItemStyle Wrap="False" Width="150px" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink" runat="server" data-toggle="tooltip" data-placement="top"
                                                       NavigateUrl='<%# String.Format("/Pages/DeviceCoTech/MobileUserEdit.aspx?id={0}&action=edit", this.Eval("UserGuid")) %>'>
                                        <asp:Literal ID="ltrUserName" Text='<%#this.Eval("FullName") %>' runat="server" /><br/>
                                        <asp:Literal ID="ltrRole" runat="server" Text='<%#this.Eval("Role") %>' />
                                        </asp:HyperLink></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Cell Phone" SortExpression="CellPhone">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrCellPhone" runat="server" Text='<%#this.Eval("CellPhone") %>' /><br/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No record found</EmptyDataTemplate></asp:GridView><link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css"
                                  rel="stylesheet" type="text/css" /><script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script><script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js"></script><script type="text/javascript">
                                    $(function () {
                                        $('[id*=gvGrid]').footable();
                                            });
                                </script></div></div></div></ContentTemplate></asp:UpdatePanel><div class="col-md-4">
                                
                </div>
        </div>        
    </div>
    <!-- DataTables JavaScript -->
    <script src="../../Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="../../Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#bodyPage_gvGrid').DataTable({
                responsive: true
            });
        });
    </script>
</asp:Content>