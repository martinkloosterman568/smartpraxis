﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Web.UI.Calendar;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class MobileDetails : Page
    {
        public string patientGuid = string.Empty;

        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        public static string TypedPassword;
        public static int strRoleId;
        static readonly GeneralHelper dHelper = new GeneralHelper();

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (this.Request.QueryString["x"] != null)
            {
                this.patientGuid = this.Request.QueryString["x"];

                if (!this.Page.IsPostBack)
                {
                    const string tableName = "DataPatientRequests"; // <-- put table name here
                    const string fieldName = "PatientGuid"; // <-- put the field name here
                    string outError;
                    var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByGuid(tableName, fieldName, this.patientGuid, out outError);
                    if (destination != null)
                    {
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            this.FindOnReturnOfRedirectId(result.PatientId.ToString());
                        }
                    }
                    else
                    {
                        this.Session.Abandon();
                        this.Response.Redirect(Global.returnToPage);
                    }
                }
            }
            else
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataPatientRequests"; // <-- put table name here
            const string fieldName = "PatientId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls


        #endregion
        
       
        protected void StartButton_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("1.aspx?x=" + this.patientGuid);
        }

        protected void MRIScanDate_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            var patient = new Guid(this.patientGuid);
            var patientRecord = (from d in this.db.DataPatientRequests
                where d.PatientGuid == patient
                                 select d).FirstOrDefault();
            if (patientRecord != null)
            {
                patientRecord.MRIScanDate = this.MRIScanDate.SelectedDate;
                patientRecord.OverAllStatus = "Accepted";
                patientRecord.DeviceCompanyLastStatus = "Accepted";
                patientRecord.TechCommentsReply = "Date and/or Time Changes - Accepted";
                patientRecord.PatientStatusDDL = "Accepted";
                this.db.SaveChanges();
            }
        }

        protected void MRIScanTime_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            var patient = new Guid(this.Request.QueryString["x"]);
            var patientRecord = (from d in this.db.DataPatientRequests
                where d.PatientGuid == patient
                select d).FirstOrDefault();
            if (patientRecord != null)
            {
                patientRecord.MRIScanTime = this.MRIScanTime.SelectedTime.ToString().Length > 5 ? this.MRIScanTime.SelectedTime.ToString().Substring(0,5) : this.MRIScanTime.SelectedTime.ToString();
                patientRecord.MRIScanDate = this.MRIScanDate.SelectedDate;
                patientRecord.OverAllStatus = "Accepted";
                patientRecord.DeviceCompanyLastStatus = "Accepted";
                patientRecord.TechCommentsReply = "Date and/or Time Changes - Accepted";
                patientRecord.PatientStatusDDL = "Accepted";
                this.db.SaveChanges();
            }
        }
    }
}