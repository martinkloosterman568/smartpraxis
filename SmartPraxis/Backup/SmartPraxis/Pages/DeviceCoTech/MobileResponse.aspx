﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileResponse.aspx.cs" Inherits="SmartPraxis.Pages.DeviceCoTech.MobileResponse"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" Title="Smart-Praxis" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50508.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page on this section.--%>
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <script type="text/javascript">
        function OnDayRender(calendarInstance, args) {
            // convert the date-triplet to a javascript date
            // we need Date.getDay() method to determine 
            // which days should be disabled (e.g. every Saturday (day = 6) and Sunday (day = 0))                
            var jsDate = new Date(args.get_date()[0], args.get_date()[1] - 1, args.get_date()[2]);
            if (jsDate.getDay() == 0 || jsDate.getDay() == 6) {
                var otherMonthCssClass = "rcOutOfRange";
                args.get_cell().className = otherMonthCssClass;
                // replace the default cell content (anchor tag) with a span element 
                // that contains the processed calendar day number -- necessary for the calendar skinning mechanism 
                args.get_cell().innerHTML = "<span>" + args.get_date()[2] + "</span>";
                // disable selection and hover effect for the cell
                args.get_cell().DayId = "";
            }
        }
    </script>
    <style type="text/css">
        .modalBackground {
            background-color:Gray;
            filter:alpha(opacity=70);
            opacity:0.7;
        }
        
        .modalPopup {
	        background-color:#ffffdd;
	        border-width:3px;
	        border-style:solid;
	        border-color:Gray;
	        padding:3px;
	        width:250px;
        }
    </style>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
	    var counter;
	    counter = 0;

	    function KeepSessionAlive() {
	        // Increase counter value, so we'll always get unique URL (sample source from page)
	        // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
	        counter++;

	        // Gets reference of image
	        var img = document.getElementById("imgSessionAlive");

	        // Set new src value, which will cause request to server, so
	        // session will stay alive
	        var pathname = this.Session["PathName"] != null ? this.Session["PathName"].ToString() : string.Empty;
	        img.src = string.Format("http://{0}.com/RefreshSessionState.aspx?c=", pathname) + counter;
	        ////img.src = "http://localhost:4617/RefreshSessionState.aspx?c=" + counter;

	        // Schedule new call of KeepSessionAlive function after 60 seconds
	        setTimeout(KeepSessionAlive, 60000);
	    }

	    // Run function for a first time
	    KeepSessionAlive();
	</script> 

   <div>
       <br/><br/>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-lg-10">
                            <div class="panel panel-default">
                                <div runat="server" id="Error" Visible="false" style="vertical-align: bottom;" class="panel-heading">
                                    <Font Color="Red"><h4>This request was responded to already.</h4></Font>  
                                </div>
                                <div runat="server" id="Accepted" Visible="false" style="vertical-align: bottom;" class="panel-heading">
                                    <Font Color="DarkGreen"><h4>Thank you for Accepting the request.</h4></Font>
                                </div>
                                <div runat="server" id="Rejected" Visible="false" style="vertical-align: bottom;" class="panel-heading">
                                    <Font Color="Red"><h4>You have Rejected the request.</h4></Font> 
                                </div>
                                <div runat="server" id="Commented" Visible="false" style="vertical-align: bottom;" class="panel-heading">
                                    <Font Color="RoyalBlue"><h4>Thank you. Your Comments have been sent.</h4></Font>    
                                </div>
                                <div runat="server" id="divbody" class="panel-body">
                                        <asp:Panel ID="Panel1" runat="server">
                                        
                                        
                                        <!-- /.row (nested) -->
                                        </asp:Panel>
                                        <!-- /.row (nested) -->
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>  
           
<br/>     <br/><br/><br/>
    </div>
    <!-- DataTables JavaScript -->
    <script src="/Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="/Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gvGrid').DataTable({
                responsive: true
            });
        });
    </script>
</asp:Content>
