﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _35 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 722,
                        Left = 18,
                        Right = 77,
                        Top = 642
                    };
                    
                    // prev
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/33.aspx?x={patientGuid}",
                        Bottom = 721,
                        Left = 892,
                        Right = 947,
                        Top = 654
                    };

                    // < 50%
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/37.aspx?x={patientGuid}",
                        Bottom = 498,
                        Left = 361,
                        Right = 481,
                        Top = 445
                    };

                    // >= 50%
                    var ch4 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/36.aspx?x={patientGuid}",
                        Bottom = 501,
                        Left = 624,
                        Right = 747,
                        Top = 448
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/35.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    this.divPage.Controls.Add(im);

                    
                }
            }
        }
    }
}


