﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.Account
{
    public partial class SignUp : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSIGNUP_Click(object sender, EventArgs e)
        {
            var dHelper = new GeneralHelper();
            var phone = this.txtPhone.Text;
            var personName = this.txtPerson.Text;
            if (!personName.Contains(" "))
            {
                this.error_block.Visible = true;
                this.ltrError.Text = @"Error, Please put a space between first and last name.";
                return;
            }

            phone = Regex.Replace(phone, "[^0-9.]", "");
            if (phone.Length != 10)
            {
                this.error_block.Visible = true;
                this.ltrError.Text = @"Error, Please enter your 10 digits cell phone number, we will text you!";
                return;
            }

            var encryptedPhone = dHelper.Encrypt(phone);
            var associateKey = this.txtAssociateKey.Text;
            //var found = false;

            if (string.IsNullOrEmpty(this.txtPerson.Text))
            {
                return;
            }

            var usernameobj = personName.Split();
            var username = usernameobj[0].Substring(0, 1) + usernameobj[1];
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.DataUserMasts
                    join r in db.ListRoleMasts on d.RoleID equals r.ID
                    join a in db.DataAccounts on d.AccountGuid equals a.AccountGuid
                    where d.UserName == username &&
                          d.Password == encryptedPhone
                             select new
                    {
                        d.UserGuid,
                        d.AccountGuid,
                        d.ID,
                        d.UserName,
                        d.FullName,
                        RoleName = r.Name,
                        d.RoleID,
                        d.EmailAddress,
                        d.OfficePhone,
                        d.CellPhone,
                        a.KeyToAddUsers,
                        a.CompanyName
                    }).FirstOrDefault();

                if (query != null)
                {
                    this.error_block.Visible = true;
                    this.ltrError.Text = @"You have already registered, please login.";
                }
                else
                {
                    try
                    {
                        var roleNameGiven = string.Empty;
                        var roleIDGiven = 0;
                        var findAccount = (from a in db.DataAccounts
                                           join b in db.ListRoleMasts on a.TypeOf equals b.DisplayName
                                           where a.KeyToAddUsers == associateKey
                                           select a).FirstOrDefault();
                        if (findAccount != null)
                        {
                            if (findAccount.TypeOf == "MRI Center")
                            {
                                roleIDGiven = 3;
                                roleNameGiven = "MRI Tech";
                            }
                            else if (findAccount.TypeOf == "Device Company")
                            {
                                roleIDGiven = 5;
                                roleNameGiven = "Device Co Tech";
                            }

                            var userGuid = Guid.NewGuid();
                            // create a new user
                            var newrecord = new DataUserMast
                            {
                                AccountGuid = findAccount.AccountGuid,
                                Role = roleNameGiven,
                                RoleID = roleIDGiven,
                                UserName = username,
                                Password = encryptedPhone,
                                CellPhone = phone,
                                UserGuid = userGuid,
                                FirstName = usernameobj[0],
                                LastName = usernameobj[1],
                                FullName = personName,
                                EmailAddress = string.Empty,
                                LastUpdatePerson = "Initial Signup",
                                LastModified = GetTimeNow.GetTime(),
                                IsSendEmail = false,
                                IsSendText = true
                            };

                            db.DataUserMasts.Add(newrecord);
                            db.SaveChanges();
                            //found = true;
                            this.DoLoginAfterSignup(personName, phone);
                        }
                        else
                        {
                            this.error_block.Visible = true;
                            this.ltrError.Text = @"Error, Associate Key was not found.";
                        }
                    }
                    finally
                    {
                    }
                }
            }

            //if (!found)
            //{
            //    this.error_block.Visible = true;
            //    this.ltrError.Text = @"Invalid Username or password.";
            //}
        }

        private void DoLoginAfterSignup(string username, string password)
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.DataUserMasts
                    join r in db.ListRoleMasts on d.RoleID equals r.ID
                    join a in db.DataAccounts on d.AccountGuid equals a.AccountGuid
                    where d.UserName == username &&
                          d.Password == password
                    select new
                    {
                        d.UserGuid,
                        d.AccountGuid,
                        d.ID,
                        d.UserName,
                        d.FullName,
                        RoleName = r.Name,
                        d.RoleID,
                        d.EmailAddress,
                        d.OfficePhone,
                        d.CellPhone,
                        a.KeyToAddUsers,
                        a.CompanyName
                    }).FirstOrDefault();

                if (query != null)
                {
                    //found = true;
                    this.Session["UserGuid"] = query.UserGuid;
                    this.Session["AccountGuid"] = query.AccountGuid;
                    this.Session["CompanyName"] = query.CompanyName;
                    this.Session["UserID"] = query.ID;
                    this.Session["UserName"] = query.FullName;
                    this.Session["Role"] = query.RoleName;
                    this.Session["RoleId"] = query.RoleID;
                    this.Session["EmailAddress"] = query.EmailAddress;
                    this.Session["OfficePhone"] = query.OfficePhone;
                    this.Session["CellPhone"] = query.CellPhone;
                    this.Session["KeyToAddUsers"] = query.KeyToAddUsers;

                    var query2 = (from d in db.DataSettings
                        select d).FirstOrDefault();
                    if (query2 != null)
                    {
                        this.Session["PathName"] = query2.PathName;
                        this.Session["BackgroundColor"] = query2.BackgroundColor;
                        this.Session["PostmarkAuthenticationKey"] = query2.PostmarkAuthenticationKey;
                    }

                    if (this.Session["Role"] != null)
                    {
                        // this is a desktop page
                        //this.Response.Redirect("~/Pages/MRICompany/PatientList.aspx?x=" + this.Session["AccountGuid"]);
                        var redirect = string.Format("/Pages/Account/UserEdit.aspx?id={0}&action=edit&x={1}", query.UserGuid, query.AccountGuid);
                        this.Response.Redirect(redirect);
                    }
                }
            }
        }

        protected void ButtonLOGIN_Click(object sender, EventArgs e)
        {
            this.Session.Abandon();
            this.Response.Redirect(Global.returnToPage);
        }
    }
}