﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountList.aspx.cs" Inherits="SmartPraxis.Pages.Account.AccountList"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" 
    Title="Smart-Praxis" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
     <style type="text/css">
		.gridHeader { font-weight:bold; color:white; background-color:#80a0a0; }
		.gridHeader A { padding-right:15px; padding-left:3px; padding-bottom:0px; color:#ffffff; padding-top:0px; text-decoration:none; }
		.gridHeader A:hover { text-decoration: underline; }
		.gridHeaderSortASC A { background: url(/Content/Images/sortdown.gif) no-repeat 95% 50%; }
		.gridHeaderSortDESC A { background: url(/Content/Images/sortup.gif) no-repeat 95% 50%; }
    </style>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            var pathname = this.Session["PathName"] != null ? this.Session["PathName"].ToString() : string.Empty;
            img.src = string.Format("http://{0}.com/RefreshSessionState.aspx?c=", pathname) + counter;
            ////img.src = "http://localhost:10240/RefreshSessionState.aspx?c=" + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
       <div class="user-stats">
       <div class="row">
           <%-- <div class="col-md-1">
            </div>--%>
            <div class="col-md-4">
                <h3>Accounts List</h3>                
            </div>
        </div>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <%--<div class="col-md-1">
                            
                        </div>--%>
                        <div class="col-lg-10">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <% // need to put this into a table so mobile can see the filter next to the Create new button %>
                            <% if (this.Session["Role"].ToString() == "Super Admin")
                               { %>
                            <div class="col-lg-4">
                                <asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New Account" OnClick="CreateNew_Click"  />
                            </div>
                            <% } %>
                            <div class="col-lg-4">
                                <asp:Panel runat="server">
                                    <label>Filter:&nbsp;</label><asp:TextBox ID="Filter" Placeholder="Filter Text" Height="32px" runat="server" AutoPostBack="True" OnTextChanged="Filter_TextChanged" ></asp:TextBox>&nbsp;
                                    <asp:Button ID="ClearFilter" class="btn btn-warning" runat="server" Text="Clear" OnClick="ClearFilter_Click"  />
                                    </asp:Panel>
                                
                            </div>
                            <br/><br/><br/>
                        </div>
                                <div class="panel-body">
                                   <!-- put content here !-->
                                    <asp:GridView ID="gvGrid" runat="server" CssClass="table table-striped table-bordered table-hover"
                                    AutoGenerateColumns="False"
                                    DataKeyNames="AccountId,CompanyName" 
                                    OnRowDataBound="GvGridRowDataBound" EmptyDataText="No Rows Found" AllowPaging="True" OnPageIndexChanging="gvGrid_PageIndexChanging" ShowHeaderWhenEmpty="True" OnSorting="gvGrid_Sorting" AllowSorting="True">
                                        <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" Font-Size="14pt" />
                                         <HeaderStyle CssClass="gridHeader" />
                                    <Columns>
                                        <asp:TemplateField ItemStyle-CssClass="tooltip-demo" HeaderText="">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridHeaderSortASC" />
                                            <ItemStyle HorizontalAlign="Center" ForeColor="Black" Width="100px" Wrap="False" BackColor="#EEEEEE"  />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" runat="server" data-toggle="tooltip" data-placement="top"
                                                    title="View/Edit" NavigateUrl='<%# String.Format("/Pages/Account/Account.aspx?id={0}&action=edit&x={1}", this.Eval("AccountGuid"), this.Session["AccountGuid"]) %>'><img src="/Content/Images/edit-tools.png" alt="edit group" /></asp:HyperLink>
                                                <asp:LinkButton ID="LinkButtonDelete" runat="server" data-toggle="tooltip" data-placement="top"
                                                    title="Delete" OnClientClick="return confirm('Are you sure that you want to delete this record?');"
                                                    OnClick="LinkButtonDeleteClick" CommandArgument='<%#this.Eval("AccountId") %>'><img src="/Content/Images/delete32.png" alt="delete group" /></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company Name" SortExpression="CompanyName">
                                            <ItemStyle Wrap="False" Width="150px" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrCompanyName" Text='<%#this.Eval("CompanyName") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Short Name" SortExpression="ShortCompanyName">
                                            <ItemStyle Wrap="False" Width="150px" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrShortCompanyName" Text='<%#this.Eval("ShortCompanyName") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type" SortExpression="TypeOf">
                                            <ItemStyle Wrap="False" Width="150px" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrTypeOf" Text='<%#this.Eval("TypeOf") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contact First Name" SortExpression="FirstName">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrFirstName" runat="server" Text='<%#this.Eval("FirstName") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLastName" runat="server" Text='<%#this.Eval("LastName") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Office Phone" SortExpression="Phone">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrPhone" runat="server" Text='<%#this.Eval("Phone") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" SortExpression="EmailAddress">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrEmailAddress" runat="server" Text='<%#this.Eval("EmailAddress") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="10px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No record found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="col-md-4">
            </div>
    </div>        
    </div>
    <!-- DataTables JavaScript -->
    <script src="/Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="/Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#UpdatePanel1_gvGrid').DataTable({
                responsive: true
            });
        });
    </script>
</asp:Content>