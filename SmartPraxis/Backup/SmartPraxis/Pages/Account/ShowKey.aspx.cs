﻿using System;
using System.IO;
using System.Web.UI;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.Account
{
    public partial class ShowKey : Page
    {
        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (this.Session["UserID"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (this.Session["Role"].ToString() != "Super Admin" &&
               this.Session["Role"].ToString() != "MRI Center" &&
               this.Session["Role"].ToString() != "Device Co")
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            var responsed = string.Empty;

            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            if ((this.Session["Role"].ToString() == "MRI Center") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if ((this.Session["Role"].ToString() == "Device Co") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            this.Session["MobilePageTitle"] = "Dashboard";

            this.KeyToAddUsers.Text = this.Session["KeyToAddUsers"].ToString();

            if (!this.Page.IsPostBack)
            {
                
            }
        }
    }
}