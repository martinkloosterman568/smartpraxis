﻿using System;
using System.Linq;
using System.Web.UI;
using SmartPraxis.Helper;
using SmartPraxis.Models;

namespace SmartPraxis
{
    public partial class ResetPassword : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:alert(screen.width); ", true);
            this.ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:GetTimeZoneOffset(); ", true);
            //

            if (!this.IsPostBack)
            {

                using (var db = new DB_SmartPraxisEntities())
                {
                    var query2 = (from d in db.DataSettings
                        select d).FirstOrDefault();
                    if (query2 != null)
                    {
                        this.Session["PathName"] = query2.PathName;
                        this.Session["BackgroundColor"] = query2.BackgroundColor;
                        this.Session["PostmarkAuthenticationKey"] = query2.PostmarkAuthenticationKey;
                        this.Session["InternalMessage"] = query2.InternalMessage;
                        this.Session["ExternalMessage"] = query2.ExternalMessage;
                        this.Session["InternalMsgEnabled"] = query2.InternalMsgEnabled;
                        this.Session["ExternalMsgEnabled"] = query2.ExternalMsgEnabled;

                        if (query2.ExternalMsgEnabled.ToString() == "True")
                        {
                            this.ExternalMessage.Visible = true;
                            this.LiteralExternalMessage.Text = query2.ExternalMessage;
                        }
                    }
                }
            }
        }

        protected void ButtonContinue_Click1(object sender, EventArgs e)
        {
            
            // this will change the user password and log them in
            var userid = new Guid(Request.QueryString["UserGuid"]);
            var dHelper = new GeneralHelper();
            var password1 = dHelper.Encrypt(this.txtPassword1.Text);
            var password2 = dHelper.Encrypt(this.txtPassword2.Text);
            var found = false;

            if (string.IsNullOrEmpty(password1) || password1 != password2)
            {
                return;
            }

            using (var db = new DB_SmartPraxisEntities())
            {
                // change password
                var update = (from d in db.DataUserMasts where d.UserGuid == userid select d).FirstOrDefault();
                if (update != null)
                {
                    update.Password = password1;
                    db.SaveChanges();
                }

                // do the usual login routine
                var query = (from d in db.DataUserMasts
                             join r in db.ListRoleMasts on d.RoleID equals r.ID
                             join a in db.DataAccounts on d.AccountGuid equals a.AccountGuid
                             where d.UserGuid == userid
                             select new
                             {
                                d.UserGuid,
                                d.AccountGuid,
                                d.ID,
                                d.UserName,
                                d.FullName,
                                RoleName = r.Name,
                                d.RoleID,
                                d.EmailAddress,
                                d.OfficePhone,
                                d.CellPhone,
                                a.KeyToAddUsers,
                                a.CompanyName
                             }).FirstOrDefault();

                if (query != null)
                {
                    found = true;
                    this.Session["UserGuid"] = query.UserGuid;
                    this.Session["AccountGuid"] = query.AccountGuid;
                    this.Session["CompanyName"] = query.CompanyName;
                    this.Session["UserID"] = query.ID;
                    this.Session["UserName"] = query.FullName;
                    this.Session["Role"] = query.RoleName;
                    this.Session["RoleId"] = query.RoleID;
                    this.Session["EmailAddress"] = query.EmailAddress;
                    this.Session["OfficePhone"] = query.OfficePhone;
                    this.Session["CellPhone"] = query.CellPhone;
                    this.Session["KeyToAddUsers"] = query.KeyToAddUsers;
                        
                    var o = this.Session["Role"];
                    if (o != null)
                    {
                        var role = o.ToString();
                        var former = (from d in db.FormAuthorizes
                            where d.Role == role
                            select new
                            {
                                d.Role,
                                d.Path,
                                d.FormName
                            }).ToList();

                        foreach (var item in former)
                        {
                            var formitem = new FormAuthorizer
                            {
                                Role = item.Role,
                                Path = item.Path,
                                FormName = item.FormName
                            };

                            Global.formAuth.Add(formitem);
                        }
                    }

                    if (this.screenSize.Text != string.Empty && this.screenSize.Text.Contains(";"))
                    {
                        this.Session["ScreenWidth"] = this.screenSize.Text.Split(';')[0];
                        this.Session["ScreenHeight"] = this.screenSize.Text.Split(';')[1];
                    }

                    if (Utils.fBrowserIsMobile())
                    {
                        this.Session["UsingDevice"] = "Mobile";
                    }
                    else
                    {
                        this.Session["UsingDevice"] = "Desktop";
                    }

                    if (this.Session["Role"] != null)
                    {
                        this.Response.Redirect("~/Pages/Account/Notify.aspx");
                    }
                }
            }

            if (!found)
            {
                this.error_block.Visible = true;
                this.ltrError.Text = @"Invalid Username or password.";
            }
        }
    }
}


