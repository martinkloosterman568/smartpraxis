﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Web.UI;
using Image = System.Web.UI.WebControls.Image;

namespace SmartPraxis.Pages.Account
{
    public partial class Account : Page
    {
        public static string TypedPassword;
        public static int strRoleId;
        private static bool GridRowsFound;
        private static int gridRowInt;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific for this page
            var responsed = string.Empty;

            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            if ((this.Session["Role"]?.ToString() == "MRI Center") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if ((this.Session["Role"]?.ToString() == "Device Co") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (this.Session["Role"]?.ToString() != "Super Admin")
            {
                this.GeographyRegionName.Enabled = false;
                this.ShortCompanyName.Enabled = false;
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                var myobj = new Account();
                if (myobj.GetAccountId() == "00000000-0000-0000-0000-000000000000")
                {
                    this.TypeOf.Enabled = true;
                    this.CompanyCode.Enabled = true;
                }

                if (this.IDType.Items.Count == 0)
                {
                    this.PopulateIDType();
                }

                if (this.TypeOf.Items.Count == 0)
                {
                    this.PopulateTypeOf();
                }

                if (this.StateCode.Items.Count == 0)
                {
                    this.PopulateStateCode();
                }

                if (this.CountryCode.Items.Count == 0)
                {
                    this.PopulateCountryCode();
                }

                if (this.Request.QueryString["id"] == null)
                {
                  // ignore
                }
                else
                {
                    const string tableName = "DataAccounts"; // <-- put table name here
                    const string fieldName = "AccountGuid"; // <-- put the field name here
                    string outError;
                    var id = this.Request.QueryString["id"];
                    var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByGuid(tableName, fieldName, id, out outError);
                    var result = destination?.FirstOrDefault();
                    if (result != null)
                    {
                        this.FindOnReturnOfRedirectId(result.AccountId.ToString(), true);
                    }
                }

                this.ViewState["sort"] = "ShortCompanyName ASC, CompanyName ASC";
            }
        }

        private void PopulateTypeOf()
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var codes = db.ListCompanyTypes.ToList();
                this.TypeOf.DataSource = codes;
                this.TypeOf.DataValueField = "CompanyTypeGuid";
                this.TypeOf.DataTextField = "Description";

                this.TypeOf.Items.Insert(0, new ListItem("- Select One -", "0"));
                this.TypeOf.DataBind();
            }
        }

        private void PopulateStateCode()
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var codes = db.ListStates.ToList();
                this.StateCode.DataSource = codes;
                this.StateCode.DataValueField = "StateGuid";
                this.StateCode.DataTextField = "Code";

                this.StateCode.Items.Insert(0, new ListItem("- Select -", "0"));
                this.StateCode.DataBind();
            }
        }

        private void PopulateCountryCode()
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var codes = db.ListCountries.ToList();
                this.CountryCode.DataSource = codes;
                this.CountryCode.DataValueField = "CountryGuid";
                this.CountryCode.DataTextField = "Code";

                this.CountryCode.Items.Insert(0, new ListItem("- Select -", "0"));
                this.CountryCode.DataBind();
            }
        }

        private void PopulateIDType()
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var codes = db.ListIDTypes.ToList();
                this.IDType.DataSource = codes;
                this.IDType.DataValueField = "IDTypeGuid";
                this.IDType.DataTextField = "Description";

                this.IDType.Items.Insert(0, new ListItem("- Select One -", "0"));
                this.IDType.DataBind();
            }
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination, HtmlGenericControl ctrlBtnClear, HtmlGenericControl ctrlBtnSubmit)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id, bool dontShowSave)
        {
            var ctrl = this.AccountId;

            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataAccounts"; // <-- put table name here
            const string fieldName = "AccountId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2.ToString(), out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination, null, null);
            }
            else
            {
                this.ElseError(ctrl, id, outError);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        private void ElseError(HiddenField ctrl, string ctlvalue, string outError)
        {
            this.ClearButton();
            ctrl.Value = ctlvalue + @" - " + outError;
        }

        #region hidden - protected controls

        // these controls are actually on the page
        protected void btnnew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/Account.aspx?x=" + this.Session["AccountGuid"]);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/AccountList.aspx?x=" + this.Session["AccountGuid"]);
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }

        #endregion

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.FindOnReturnOfRedirectId(id, true);
            this.PopulateGrid();
        }

        public string SaveRecord(List<string> arr)
        {
            #region "Entry Table"

            // purpose: jquery will collect all controls in div called "divbody" and pass then as an array list, the code below will determine if it is a Add or update
            string ret;
            var ret2 = "0";
            var ret3 = "0";

            var fieldname = string.Empty;
            try
            {
                object destination = new DataAccount();
                // this line needs to be changed to be table specific <<---------------------------------------------------------
                var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                // this line needs to be changed to be Repository specific <<---------------------------------------------------------

                #region hidden

                var obj = new ControlSaver();

                var id = obj.GetIdFromControls(ref destination, arr, ref fieldname);
                if (id != string.Empty)
                {
                    var id2 = int.Parse(id);
                    repository.GetById(id2);
                }

                obj.SaveControls(ref destination, arr);

                #endregion

                var myobj = new Account();
                var saving = (DataAccount)destination;
                // this line here needs the model object <<---------------------------------------------------------

                #region hidden

                saving.LastModified = GetTimeNow.GetTime();
                
                if (saving.AccountId == 0) saving.AccountGuid = Guid.NewGuid();

                if (!string.IsNullOrEmpty(saving.StateCode))
                {
                    saving.StateGuid = myobj.GetState(saving.StateCode);
                }

                if (!string.IsNullOrEmpty(saving.TypeOf))
                {
                    saving.TypeOfGuid = myobj.GetTypeOf(saving.TypeOf);
                }

                if (!string.IsNullOrEmpty(saving.IDType))
                {
                    saving.IDTypeGuid = myobj.GetIDType(saving.IDType);
                }

                if (!string.IsNullOrEmpty(saving.CountryCode))
                {
                    saving.CountryGuid = myobj.GetCountry(saving.CountryDescription);
                }

                if (!string.IsNullOrEmpty(saving.GeographyRegionName))
                {
                    saving.GeographyGuid = myobj.GetGeography(saving.GeographyRegionName);
                }

                saving.LastUpdatePerson = myobj.GetLastUserUpdated();

                if (string.IsNullOrEmpty(saving.ShortCompanyName))
                {
                    saving.ShortCompanyName = saving.CompanyName;
                }

                if (id == string.Empty)
                {
                    // this is Add
                    saving.CreatedBy = this.Session["UserName"].ToString();
                    saving.CreatedDate = GetTimeNow.GetTime();
                    saving.LastModified = GetTimeNow.GetTime();
                    saving.LastUpdatePerson = this.Session["UserName"].ToString();
                    saving.KeyToAddUsers = saving.AccountGuid.ToString().Substring(0, 8).ToUpper();
                    repository.Add(saving);
                }
                else if (id != string.Empty)
                {
                    // this is update
                    saving.LastModified = GetTimeNow.GetTime();
                    saving.LastUpdatePerson = this.Session["UserName"].ToString();
                    repository.Update(saving);
                }

                repository.Save();

                #endregion

                ret = saving.AccountId.ToString();
                ret2 = saving.AccountGuid.ToString();
            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    ret3 = "Error, the user name already exists";
                }

                ret = "failure";
            }

            #endregion

            var ret5 = $"success|{ret}|{ret2}|{ret3}";
            return ret5;
        }

        private Guid GetIDType(string idType)
        {
            if (!string.IsNullOrEmpty(idType))
            {
                using (var db = new DB_SmartPraxisEntities())
                {
                    var result = (from d in db.ListIDTypes
                        where d.Description == idType
                        select d).FirstOrDefault();

                    if (result?.IDTypeGuid != null) return result.IDTypeGuid.Value;
                }
            }

            return Guid.Empty;
        }

        private Guid GetCountry(string country)
        {
            if (!string.IsNullOrEmpty(country))
            {
                using (var db = new DB_SmartPraxisEntities())
                {
                    var result = (from d in db.ListCountries
                        where d.Description == country
                        select d).FirstOrDefault();

                    if (result?.CountryGuid != null) return result.CountryGuid.Value;
                }
            }

            return Guid.Empty;
        }

        private Guid GetTypeOf(string type)
        {
            if (!string.IsNullOrEmpty(type))
            {
                using (var db = new DB_SmartPraxisEntities())
                {
                    var result = (from d in db.ListCompanyTypes
                        where d.Description == type
                        select d).FirstOrDefault();

                    if (result?.CompanyTypeGuid != null) return result.CompanyTypeGuid.Value;
                }
            }

            return Guid.Empty;
        }

        private Guid GetState(string state)
        {
            // auto add State Name
            if (!string.IsNullOrEmpty(state))
            {
                using (var db = new DB_SmartPraxisEntities())
                {
                    var result = (from d in db.ListStates
                        where d.Code == state
                        select d).FirstOrDefault();

                    // update ** but nothing to do here
                    if (result?.StateGuid != null) return result.StateGuid.Value;
                }
            }

            return Guid.Empty;
        }

        public static bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, "^\\d+$");
        }

        public string GetAccountId()
        {
            return this.Session["AccountGuid"].ToString();
        }

        public string GetLastUserUpdated()
        {
            return this.Session["UserName"].ToString();
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields(false);
            if (result != null)
            {
                var test = this.SaveRecord(result);
                if (!test.Contains("failure"))
                {
                    this.Response.Redirect("/Pages/Account/AccountList.aspx?x=" + this.Session["AccountGuid"]);
                }
                else if(test.Contains("already"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotificationAccount('hello')", true);
                }
            }
        }

        protected void btnsubmit2a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields(false);
            if (result != null)
            {
                this.SaveRecord(result);
                this.Response.Redirect("/Pages/Account/Account.aspx?x=" + this.Session["AccountGuid"]);
            }
        }

        private List<string> CheckFields(bool overrideRequired)
        {
            var required = false;
            var arr = new List<string>();
            foreach (var text in this.Panel1.Controls)
            {
                if (text.ToString().Contains("TextBox"))
                {
                    var btn = (TextBox)text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                    if (btn.ClientID.Contains("Password") && btn.Text != string.Empty)
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                    else if(btn.ClientID.Contains("Password") && btn.Text == string.Empty)
                    {
                        const string tableName = "DataAccounts"; // <-- put table name here
                        const string fieldName = "AccountGuid"; // <-- put the field name here
                        string outError;
                        var id = this.Request.QueryString["id"];
                        var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                        var destination = repository.FindAllByGuid(tableName, fieldName, id, out outError);
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            //var pwd = dHelper.Decrypt(result.Password);
                            //arr.Add(string.Format("{0}:{1}", btn.ClientID, pwd));
                        }
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                }
                else if (text.ToString().Contains("Hidden"))
                {
                    var btn = (HiddenField)text;
                    arr.Add($"{btn.ClientID}:{btn.Value}");
                }
                else if (text.ToString().Contains("RadCombo"))
                {
                    var btn = (RadComboBox)text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                    arr.Add($"{btn.ClientID.Replace("ctl00_", string.Empty)}:{btn.SelectedItem.Text}");
                }
                else if (text.ToString().Contains("Combo") || text.ToString().Contains("Drop"))
                {
                    var btn = (DropDownList) text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                    arr.Add($"{btn.ClientID}:{btn.SelectedItem.Text}");
                }
            }

            if (required && !overrideRequired)
            {
                return null;
            }

            return arr;
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/Account.aspx?x=" + this.Session["AccountGuid"]);
        }

        protected void txtPassword_TextChanged(object sender, EventArgs e)
        {
           // TypedPassword = this.Password.Text;
        }

        [ScriptMethod]
        [WebMethod]
        public static List<string> GetAgentCode(string prefixText)
        {
            var result = new List<string>();
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.DataAccounts
                             where d.CompanyName.Contains(prefixText)
                             orderby d.CompanyName
                             select new { d.CompanyName }).Take(20).Distinct().ToList();

                result.AddRange(query.Select(item => item.CompanyName));
            }

            return result;
        }

        [ScriptMethod]
        [WebMethod]
        public static List<string> GetAccountManagerName(string prefixText)
        {
            var result = new List<string>();
            using (var db = new DB_SmartPraxisEntities())
            {
                // below RoleId == 1 --> means Office Worker
                var query = (from d in db.DataUserMasts
                             where d.FullName.Contains(prefixText) && d.RoleID == 1
                             orderby d.FullName
                             select new { d.FullName }).Take(20).Distinct().ToList();

                result.AddRange(query.Select(item => item.FullName));
            }

            return result;
        }

        protected void StateCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.ListStates
                             where d.Code == this.StateCode.SelectedItem.Text
                             orderby d.Code
                             select new { d.Description }).FirstOrDefault();

                if (query != null)
                {
                    this.StateDescription.Text = query.Description;
                }
            }
        }

        protected void CountryCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.ListCountries
                             where d.Code == this.CountryCode.SelectedItem.Text
                             orderby d.Code
                             select new { d.Description }).FirstOrDefault();

                if (query != null)
                {
                    this.CountryDescription.Text = query.Description;
                }
            }
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
        }

        protected void gvGrid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            this.gvGrid.EditIndex = -1;
        }

        protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Add"))
            {
                var result = this.CheckFields(true);
                var result2 = string.Empty;
                if (result != null)
                {
                    // we are just saving the record, not posting here
                    result2 = this.SaveRecord(result);
                }

                this.AccountId.Value = result2.Split('|')[1];
                this.AccountGuid.Value = result2.Split('|')[2];
            }
            else if (e.CommandName.Equals("MainContactClick"))
            {
                var index = Convert.ToInt32(e.CommandArgument);
                var selectedRow = this.gvGrid.Rows[index];
                var mimgButtonMainContact = (ImageButton)e.CommandSource;
                if (mimgButtonMainContact != null && mimgButtonMainContact.ImageUrl.Contains("On"))
                {
                    mimgButtonMainContact.ImageUrl = "/Content/Images/CheckOffsmall.png";
                    ((DataControlFieldCell)(selectedRow.Cells[2])).Enabled = false;
                    ((DataControlFieldCell)(selectedRow.Cells[2])).Text = string.Empty;
                }
                else if (mimgButtonMainContact != null && mimgButtonMainContact.ImageUrl.Contains("Off"))
                {
                    mimgButtonMainContact.ImageUrl = "/Content/Images/CheckOnsmall.png";
                    ((DataControlFieldCell)(selectedRow.Cells[2])).Enabled = true;
                }
            }
            else if (e.CommandName.Equals("ButtonMainContact"))
            {
                var mimgButtonMainContact = this.gvGrid.FooterRow.FindControl("imgButtonMainContact") as Image;
                
                if (mimgButtonMainContact != null && mimgButtonMainContact.ImageUrl.Contains("On"))
                {
                    mimgButtonMainContact.ImageUrl = "/Content/Images/CheckOffsmall.png";
                }
                else if (mimgButtonMainContact != null && mimgButtonMainContact.ImageUrl.Contains("Off"))
                {
                    mimgButtonMainContact.ImageUrl = "/Content/Images/CheckOnsmall.png";
                }
            }
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.BackColor = Color.FromArgb(128, 160, 160);
                // show
                e.Row.Cells[0].Visible = true;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.DataItem != null && GridRowsFound)
                {
                    // maybe use this for the Main Contact checkbox
                    var buttonMainContact = DataBinder.Eval(e.Row.DataItem, "MainContact");
                    var imgButtonMainContact = (ImageButton)e.Row.FindControl("imgButtonMainContact");
                    imgButtonMainContact.CommandArgument = e.Row.RowIndex.ToString();
                    try
                    {
                        if (buttonMainContact != null && (bool)buttonMainContact)
                        {
                            imgButtonMainContact.ImageUrl = "/Content/Images/CheckOnsmall.png";
                        }
                        else
                        {
                            imgButtonMainContact.ImageUrl = "/Content/Images/CheckOffsmall.png";
                        }
                    }
                    catch (Exception)
                    {
                        imgButtonMainContact.ImageUrl = "/Content/Images/CheckOffsmall.png";
                    }

                    // show
                    e.Row.Cells[0].Visible = true;
                }
                else if (GridRowsFound == false)
                {
                    if (gridRowInt == 0)
                    {
                        e.Row.Visible = false;
                        gridRowInt++;
                    }
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                // show
                e.Row.Visible = true;
            }
        }

        protected void gvGrid_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
        }

        protected void gvGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                var value = e.Keys[0].ToString();

                var delitem = int.Parse(value);
                //var delete = (from d in this.db.DataAccountEmails
                //              where d.AccountEmailId == delitem
                //              select d).FirstOrDefault();

                //if (delete != null)
                //{
                //    this.db.DataAccountEmails.Remove(delete);
                //    this.db.SaveChanges();
                //}
            }
            catch (Exception)
            {
                // ignored
            }
        }

        protected void gvGrid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            this.gvGrid.EditIndex = e.NewEditIndex;
        }

        protected void gvGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var row = this.gvGrid.Rows[e.RowIndex];

            var result = this.CheckFields(true);
            if (result != null)
            {
                // we are just saving the record, not posting here
                this.SaveRecord(result);
            }

            var id = row.FindControl("ltrAccountEmailId") as HiddenField;
            var email = row.FindControl("txtEmailAddress") as RadTextBox;
            var mimgButtonMainContact = row.FindControl("imgButtonMainContact") as Image;

            // update the record since it is already in the database
            if (id != null)
            {
                var recordId = int.Parse(id.Value);
                //var record = (from d in this.db.DataAccountEmails
                //    where d.AccountEmailId == recordId
                //    select d).FirstOrDefault();

                //if (record != null)
                //{
                //    if (email != null) record.EmailAddress = email.Text;
                //    record.MainContact = mimgButtonMainContact != null && (!mimgButtonMainContact.ImageUrl.Contains("Off"));

                //    this.db.SaveChanges();
                //}
            }

            this.gvGrid.EditIndex = -1;
            //this.PopulateGrid();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] s = this.ViewState["sort"].ToString().Split(); //load the last sort
            string sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                    sSort += " DESC";
                else
                    sSort += " ASC";
            }
            else
                sSort += " ASC";

            //find which column is being sorted to change its style
            int i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" +
                                                                  sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            if (gridRowInt == 0)
            {
                GridRowsFound = false;
            }
            else
            {
                GridRowsFound = true;
            }

            //this.gvGrid.DataSource = this.getData(sSort);
            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }

        private DataTable getData(string sSort)
        {
            var ds = new DataSet();
            var id = this.Request.QueryString["id"];
            var test2 = this.AccountGuid.Value;
            if (id == null && test2 != string.Empty)
            {
                id = test2;
            }

            if (!string.IsNullOrEmpty(id))
            {
                var dbParams = new[]
                {
                    new SqlParameter("@valuePassedIn", new Guid(id)),
                    new SqlParameter("@sortBy", sSort)
                };

                var dt = FillDataSet.FillDt("spGetAccountEmails", dbParams);
                return dt;
            }

            return null;
        }

        private void PopulateGrid()
        {
            var destination = this.getData("AccountEmailId ASC");

            if (destination == null)
            {
                GridRowsFound = false;
                var dt = new DataTable();
                this.ShowNoResultFound(dt, this.gvGrid);
                gridRowInt = 0;
            }
            else
            {
                GridRowsFound = true;
                this.gvGrid.DataSource = destination;
                gridRowInt = destination.Rows.Count;
            }

            //this.gvGrid.DataBind();

            if (destination != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void ShowNoResultFound(DataTable source, GridView gv)
        {
            source.Columns.Add("AccountEmailId", typeof(int));
            source.Columns.Add("EmailAddress", typeof(string));
            source.Columns.Add("MainContact", typeof(bool));
            source.Rows.Add(0, "No Rows Found", false);

            gv.DataSource = source;
            //gv.DataBind();

            // Get the total number of columns in the GridView to know what the Column Span should be
            var columnsCount = gv.Columns.Count;
            gv.Rows[0].Cells.Clear(); // clear all the cells in the row
            gv.Rows[0].Cells.Add(new TableCell()); //add a new blank cell
            gv.Rows[0].Cells[0].ColumnSpan = columnsCount; //set the column span to the new added cell

            //You can set the styles here
            gv.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            gv.Rows[0].Cells[0].ForeColor = Color.Red;
            gv.Rows[0].Cells[0].Font.Bold = true;
            //set No Results found to the new added cell
            gv.Rows[0].Cells[0].Text = "NO RESULTS FOUND!";
        }

        protected void LinkButtonAddClick(object sender, EventArgs e)
        {
            // ignore 
        }

        [ScriptMethod]
        [WebMethod]
        public static List<string> GetRegionDistrict(string prefixText)
        {
            var result = new List<string>();
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.DataGeographyRegions
                    where d.GeographyRegionName.Contains(prefixText)
                    orderby d.GeographyRegionName
                    select new { d.GeographyRegionName }).Take(20).Distinct().ToList();

                result.AddRange(query.Select(item => item.GeographyRegionName));
            }

            return result;
        }

        private Guid GetGeography(string country)
        {
            if (!string.IsNullOrEmpty(country))
            {
                using (var db = new DB_SmartPraxisEntities())
                {
                    var result = (from d in db.DataGeographyRegions
                        where d.GeographyRegionName == country
                        select d).FirstOrDefault();

                    if (result?.GeographyGuid != null) return result.GeographyGuid.Value;
                }
            }

            return Guid.Empty;
        }
    }
}