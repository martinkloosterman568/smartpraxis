﻿using System;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web.UI;
using SmartPraxis.Models;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.Account
{
    public partial class SetNotify : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                this.InternalMessage.Text = this.Session["InternalMessage"].ToString();
                this.ExternalMessage.Text = this.Session["ExternalMessage"].ToString();
                if (this.Session["InternalMsgEnabled"] != null &&
                    this.Session["InternalMsgEnabled"].ToString() == "True")
                {
                    this.InternalMsgEnabled.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                }
                else
                {
                    this.InternalMsgEnabled.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                }
                if (this.Session["ExternalMsgEnabled"] != null && this.Session["ExternalMsgEnabled"].ToString() == "True")
                {
                    this.ExternalMsgEnabled.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                }
                else
                {
                    this.ExternalMsgEnabled.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                }
            }
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            this.Session["InternalMessage"] = this.InternalMessage.Text;
            this.Session["ExternalMessage"] = this.ExternalMessage.Text;

            using (var db2 = new DB_SmartPraxisEntities())
            {
                try
                {
                    var query2 = (from d in this.db.DataSettings where d.SettingsId == 1 select d).FirstOrDefault();
                    if (query2 != null)
                    {
                        query2.InternalMessage = this.InternalMessage.Text;
                        query2.ExternalMessage = this.ExternalMessage.Text;
                        query2.InternalMsgEnabled = this.InternalMsgEnabled.ImageUrl.Contains("On");
                        query2.ExternalMsgEnabled = this.ExternalMsgEnabled.ImageUrl.Contains("On");
                        this.db.DataSettings.AddOrUpdate(query2);
                        this.db.SaveChanges();
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                    throw;
                }
            }
        }

        protected void InternalMsgEnabled_Click(object sender, ImageClickEventArgs e)
        {
            if (this.InternalMsgEnabled.ImageUrl.Contains("Off"))
            {
                this.InternalMsgEnabled.ImageUrl = "~/Content/Images/CheckOnsmall.png";
            }
            else
            {
                this.InternalMsgEnabled.ImageUrl = "~/Content/Images/CheckOffsmall.png";
            }
        }

        protected void ExternalMsgEnabled_Click(object sender, ImageClickEventArgs e)
        {
            if (this.ExternalMsgEnabled.ImageUrl.Contains("Off"))
            {
                this.ExternalMsgEnabled.ImageUrl = "~/Content/Images/CheckOnsmall.png";
            }
            else
            {
                this.ExternalMsgEnabled.ImageUrl = "~/Content/Images/CheckOffsmall.png";
            }
        }
    }
}