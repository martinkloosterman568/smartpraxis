﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="SmartPraxis.Pages.SuperAdmin.Dashboard"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" 
    Title="Smart-Praxis" %>

<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            var pathname = this.Session["PathName"] != null ? this.Session["PathName"].ToString() : string.Empty;
            img.src = string.Format("http://{0}.com/RefreshSessionState.aspx?c=", pathname) + counter;
            ////img.src = "http://localhost:10240/RefreshSessionState.aspx?c=" + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
    <div class="user-stats">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="col-xs-4 text-right">
                        <asp:ImageButton alt="MK Checked" ToolTip="MK Agreed" ID="MKCheckedButton" runat="server" ImageUrl="/Content/Images/prescription.png" OnClick="MKCheckedButton_Click"  />
                    </div>
                    <div class="col-xs-8">
                        <table>
                            <tr>
                                <td style="width:100px">
                                    <h3><%=this.Session["NumberAgreedToFollowMKScanQty"].ToString() %></h3>
                                    <p>MK Checked</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="col-xs-4 text-right">
                        <asp:ImageButton alt="Patients" ToolTip="Patients" ID="PatientsButton" runat="server" ImageUrl="/Content/Images/patient.png" OnClick="PatientsButton_Click" />
                    </div>
                    <div class="col-xs-8">
                        <table>
                            <tr>
                                <td style="width:160px">
                                    <h3><%=this.Session["PatientRecordsQty"].ToString() %></h3>
                                    <p>Patients</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
         
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="col-xs-4 text-right">
                        <asp:ImageButton alt="Users" ToolTip="Users" ID="UsersButton" runat="server" ImageUrl="/Content/Images/girl.png" OnClick="UsersButton_Click" />
                    </div>
                    <div class="col-xs-8">
                        <table>
                            <tr>
                                <td style="width:100px">
                                    <h3><%=this.Session["ActiveUsersQty"].ToString() %></h3>
                                    <p>Users</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>