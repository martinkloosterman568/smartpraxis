﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Web.UI;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.SuperAdmin
{
    public partial class Dashboard : Page
    {
        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    var dt = FillDataSet.FillDt("spGetDashboardCountsSuperAdmin", null);
                    foreach (DataRow row in dt.Rows)
                    {
                        var moder = row.ItemArray[0].ToString();
                        var qty = int.Parse(row.ItemArray[1].ToString());

                        switch (moder)
                        {
                            case "ActiveUsers":
                                this.Session["ActiveUsersQty"] = qty;
                                break;
                            case "PatientRecords":
                                this.Session["PatientRecordsQty"] = qty;
                                break;
                            case "NumberAgreedToFollowMKScan":
                                this.Session["NumberAgreedToFollowMKScanQty"] = qty;
                                break;
                        }
                    }
                }
            }
        }

        protected void MKCheckedButton_Click(object sender, ImageClickEventArgs e)
        {
            Debugger.Break();
        }

        protected void PatientsButton_Click(object sender, ImageClickEventArgs e)
        {
            Debugger.Break();
        }

        protected void UsersButton_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("../Account/UserList.aspx");
        }
    }
}