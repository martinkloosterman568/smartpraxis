﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.SuperAdmin
{
    public partial class GeographyRegionsList : Page
    {
        string responsed = string.Empty;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                //if (this.Request.QueryString["id"] == null)
                //{
                    this.ViewState["sort"] = "GeographyRegionName ASC";
                    this.PopulateGrid();
                //}
            }
        }

        private void PopulateGrid()
        {
            var destination = this.GetDataForGrid("GeographyRegionName ASC");

            this.gvGrid.DataSource = destination;
            this.gvGrid.DataBind();

            if (destination.Rows.Count > 0)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.Response.Redirect("/Pages/Account/AccountList.aspx?x=" + this.Session["AccountGuid"]);
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.Session["Role"].ToString().Contains("Super Admin"))
            {
                e.Row.Cells[0].Visible = true;
            }
            else if (this.Session["Role"].ToString().Contains("MRI Center") ||
                this.Session["Role"].ToString().Contains("Device Co"))
            {
                var deletebutton = (LinkButton)e.Row.FindControl("LinkButtonDelete");
                if (deletebutton != null)
                {
                    deletebutton.Visible = false;
                }
            }
            else
            {
                e.Row.Cells[0].Visible = false;
            }
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("GeographyRegions.aspx");
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
            this.PopulateGrid();
        }

        public DataTable GetDataForGrid(string sSort)
        {
            var dbParams = new[]
            {
                new SqlParameter("@valuePassedIn", this.Filter.Text),
                new SqlParameter("@sortBy", sSort)
            };

            var dt = FillDataSet.FillDt("spFilterGeographyRegionsList", dbParams);
            return dt;
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }

        protected void Filter_TextChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            this.Filter.Text = string.Empty;
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            var s = this.ViewState["sort"].ToString().Split();    //load the last sort
            var sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                {
                    sSort += " DESC";
                }
                else
                {
                    sSort += " ASC";
                }
            }
            else
            {
                sSort += " ASC";
            }

            //find which column is being sorted to change its style
            var i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" + sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            //get the sorted data
            var dt = this.GetDataForGrid(sSort);
            this.gvGrid.DataSource = dt;
            this.gvGrid.DataBind();

            this.gvGrid.DataSource = this.GetDataForGrid(sSort);
            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }
        
        protected void DropDownListAccountManager_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }
    }
}
 