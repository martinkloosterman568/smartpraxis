﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeographyRegions.aspx.cs" Inherits="SmartPraxis.Pages.SuperAdmin.GeographyRegions"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" Title="Smart-Praxis" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50508.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page on this section.--%>
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <script type="text/javascript">
        function showNotification(message)
        {
            //$find("<%= this.RadNotification1.ClientID %>").text = message;
            $find("<%= this.RadNotification1.ClientID %>").show();
        }
    </script>
    <style type="text/css">
          .autocomplete_CompletionListElement
        {
            margin: 0px;
            background-color: White;
            cursor: default;
            list-style-type: none;
            overflow-y: auto;
            overflow-x: hidden;
            height:180px;
            text-align: left;
            border: 1px solid #777;
            z-index:10000;
        } 

        .modalBackground {
            background-color:Gray;
            filter:alpha(opacity=70);
            opacity:0.7;
        }
        
        .modalPopup {
	        background-color:#ffffdd;
	        border-width:3px;
	        border-style:solid;
	        border-color:Gray;
	        padding:3px;
	        width:250px;
        }
    </style>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
	    var counter;
	    counter = 0;

	    function KeepSessionAlive() {
	        // Increase counter value, so we'll always get unique URL (sample source from page)
	        // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
	        counter++;

	        // Gets reference of image
	        var img = document.getElementById("imgSessionAlive");

	        // Set new src value, which will cause request to server, so
	        // session will stay alive
	        var pathname = this.Session["PathName"] != null ? this.Session["PathName"].ToString() : string.Empty;
	        img.src = string.Format("http://{0}.com/RefreshSessionState.aspx?c=", pathname) + counter;
	        ////img.src = "http://localhost:4617/RefreshSessionState.aspx?c=" + counter;

	        // Schedule new call of KeepSessionAlive function after 60 seconds
	        setTimeout(KeepSessionAlive, 60000);
	    }

	    // Run function for a first time
	    KeepSessionAlive();
	</script> 
    
   <div>
       <br/><br/>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-lg-10">
                            <div class="panel panel-default">
                                
                                <div style="vertical-align: bottom;" class="panel-heading">
                                    <h4>Add / Edit Region / District</h4>    
                                    <%--<asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New File"   />--%>
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                </div>
                             
                                <div runat="server" id="divbody" class="panel-body">
                                        <asp:Panel ID="Panel1" runat="server">
                                        <asp:HiddenField runat="server" ID="GeographyId" Value="" />
                                        <asp:HiddenField runat="server" ID="GeographyGuid" Value="" />
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Region&nbsp;/&nbsp;District Name</label><asp:Label ID="Label1" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:TextBox ID="GeographyRegionName" alt="required" CssClass="form-control" runat="server" placeholder="Region/District Name" ValidationGroup="Required" SkinID="Required" />
                                                    <asp:HiddenField id="hfErrorMessage" runat="server"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.row (nested) -->
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Last Update Person</label>
                                                    <asp:TextBox ID="LastUpdatePerson" CssClass="form-control" runat="server" accept="Last Update Person" placeholder="Last Update Person" ValidationGroup="AlwaysDisable" Enabled="False" ReadOnly="True" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Last Update Date</label>
                                                    <asp:TextBox ID="LastModified" CssClass="form-control" runat="server" accept="Last Update Date" placeholder="Last Update Date" ValidationGroup="AlwaysDisable" Enabled="False" ReadOnly="True" />
                                                </div>
                                             </div>
                                        </div>
                                        <div style="float: left;" class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <!-- This html button triggers the jquery above -->
                                                    <div style="float:left" runat="server" id="divBtnSubmit"><asp:Button ID="btnsubmit" runat="server" Text="Save" class="btn btn-success" OnClick="btnsubmit1a_Click" />&nbsp;</div>
                                                    <div style="float:left" runat="server" id="divBtnClear">&nbsp;<asp:Button ID="btnclear" runat="server" Text="Clear" class="btn btn-warning" OnClick="btnclear_Click" /></div>
                                                    <div style="float:left" runat="server" id="divBtnNew">&nbsp;<asp:Button ID="btnnew" runat="server" Text="New" class="btn btn-primary" OnClick="btnnew_Click"  /></div>
                                                    <div style="float:left" runat="server" id="divBtnCancel">&nbsp;<asp:Button ID="btncancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="btncancel_Click" /></div>
                                                </div>
                                                <br/><br/>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                        <!-- /.row (nested) -->
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-md-10">
                    <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotification1" runat="server" Position="Center"
                             Width="330px" Height="160px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                             Title="Error" Text="Error, the Region / District name already exists."
                             Style="z-index: 100000">
                    </telerik:RadNotification>
                </div>
            </div>  
           
<br/>     <br/><br/><br/>
    </div>
    <!-- DataTables JavaScript -->
    <script src="/Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="/Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gvGrid').DataTable({
                responsive: true
            });
        });
    </script>
</asp:Content>
