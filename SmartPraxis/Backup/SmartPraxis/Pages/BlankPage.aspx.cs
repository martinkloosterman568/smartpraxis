﻿using System;
using System.Web.UI;

namespace SmartPraxis.Pages
{
    public partial class BlankPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Session["MobilePageTitle"] = "Blank Page";
        }
    }
}