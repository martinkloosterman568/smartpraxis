﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Web.UI;
using DayRenderEventArgs = Telerik.Web.UI.Calendar.DayRenderEventArgs;

namespace SmartPraxis.Pages.Account
{
    public partial class Patient : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        static readonly GeneralHelper dHelper = new GeneralHelper();
        private static Guid ourAccountId;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region page specific
            var responsed = string.Empty;

            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            if ((this.Session["Role"]?.ToString() == "MRI Center") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if ((this.Session["Role"]?.ToString() == "Device Co") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            //this.SetupNotification2();
            this.TechCommentsReply.Enabled = false;

            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            ourAccountId = new Guid(this.Session["AccountGuid"].ToString());

            if (!this.Page.IsPostBack)
            {
                this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                this.btnsubmit.Enabled = false;
                this.btnsubmit.Text = @"Click Agree";
                this.btnsubmit.CssClass = "btn btn-success.disabled";

                this.MRIScanDate.Calendar.SpecialDays.Clear();
                this.LastUpdatePerson.Text = this.Session["UserName"].ToString();

                // fill the dropdown before using it below
                this.PopulateDoctorNameDropDown();
                this.PopulateStatusDropDown();
                this.PopulateMRITypeDropDown();
                this.PopulateDeviceCompanyDropDown();

                this.PatientStatusDDL.Items.FindByText("Pending").Selected = true;
                this.PatientStatusDDL.Enabled = false;

                if (this.Request.QueryString["id"] == null)
                {
                    this.statusDDL.Visible = false;
                }

                if (this.Session["Role"]?.ToString() == "MRI Doctor")
                {
                    this.MRIDoctorName.Enabled = false;
                    this.MRIDoctorName.SelectedIndex = this.MRIDoctorName.Items.IndexOf(this.MRIDoctorName.Items.FindByText(this.Session["UserName"].ToString()));
                }

                if (this.Request.QueryString["id"] != null)
                {
                    const string tableName = "DataPatientRequests"; // <-- put table name here
                    const string fieldName = "PatientGuid"; // <-- put the field name here
                    string outError;
                    var id = this.Request.QueryString["id"];
                    var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByGuid(tableName, fieldName, id, out outError);
                    var result = destination.FirstOrDefault();
                    if (result != null)
                    {
                        this.FindOnReturnOfRedirectId(result.PatientId.ToString());
                    }
                }

                this.MRIScanDate.MinDate = GetTimeNow.GetTime().AddDays(1);
            }
        }

        private void PopulateMRITypeDropDown()
        {
            var repository = new ListMRITypeRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.MRIType.DataSource = destination;
            this.MRIType.DataValueField = "Code";
            this.MRIType.DataTextField = "Description";
            this.MRIType.DataBind();

            this.MRIType.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void PopulateDeviceCompanyDropDown()
        {
            //var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            using (var db = new DB_SmartPraxisEntities())
            {
                var data = db.DataAccounts.Where(e=>e.TypeOf.Contains("Device Company")).Select(p => new { p.ShortCompanyName }).Distinct().ToList();

                this.DeviceCompanyName.DataSource = data;
                this.DeviceCompanyName.DataValueField = "ShortCompanyName";
                this.DeviceCompanyName.DataTextField = "ShortCompanyName";
                this.DeviceCompanyName.DataBind();

                this.DeviceCompanyName.Items.Insert(0, new ListItem("- Select One -", "0"));
            }
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataPatientRequests"; // <-- put table name here
            const string fieldName = "PatientId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);

                if (this.IsAgreedToFollowMKScan.ImageUrl.Contains("On"))
                {
                    // this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                    this.btnsubmit.Enabled = true;
                    this.btnsubmit.Text = @"Send Request";
                    this.btnsubmit.CssClass = "btn btn-success";
                }
                else
                {
                    // this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                    this.btnsubmit.Enabled = false;
                    this.btnsubmit.Text = @"Click Agree";
                    this.btnsubmit.CssClass = "btn btn-success.disabled";
                }
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls

        // these controls are actually on the page
        protected void btnnew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/Patient.aspx?x=" + this.Session["AccountGuid"]);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=" + this.Session["AccountGuid"]);
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }
        #endregion

        public string SaveRecord(List<string> arr, bool RequiredOverride = false)
        {
            if (arr == null)
            {
                return string.Empty;
            }
            #region "Entry Table"

            // purpose: jquery will collect all controls in div called "divbody" and pass then as an array list, the code below will determine if it is a Add or update
            string ret;
            var ret2 = "0";
            var ret3 = "0";
            var ret4 = "0";
           
            var fieldname = string.Empty;
            try
            {
                object destination = new DataPatientRequest();
                // this line needs to be changed to be table specific <<---------------------------------------------------------
                var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
                // this line needs to be changed to be Repository specific <<---------------------------------------------------------

                #region hidden

                var obj = new ControlSaver();

                var id = obj.GetIdFromControls(ref destination, arr, ref fieldname);
                if (id != string.Empty)
                {
                    var id2 = int.Parse(id);
                    repository.GetById(id2);
                }

                obj.SaveControls(ref destination, arr);
                
                #endregion

                var saving = (DataPatientRequest)destination;
                // this line here needs the model object <<---------------------------------------------------------

                var myobj = new Patient();

                if (!string.IsNullOrEmpty(saving.DeviceCompanyName))
                {
                    saving.DeviceCompanyGuid = myobj.GetDeviceCompanyGuid(saving.DeviceCompanyName);
                }

                var user = this.Session["UserName"].ToString();
                saving.LastUpdatedBy = user;
                saving.LastUpdatedByGuid = myobj.GetUser(user);

                if (this.Session["Role"].ToString() == "MRI Doctor")
                {
                    saving.MRIDoctorName = user;
                    saving.MRIDoctorGuid = myobj.GetUser(user);
                }
                else if (this.Session["Role"].ToString() == "MRI Tech")
                {
                    saving.MRITechName = user;
                    saving.MRITechUserGuid = myobj.GetUser(user);
                    saving.MRIDoctorGuid = myobj.GetUser(saving.MRIDoctorName);
                }
                else if (this.Session["Role"].ToString() == "MRI Center")
                {
                    saving.MRIDoctorGuid = myobj.GetUser(saving.MRIDoctorName);
                }

                #region hidden

                
                saving.LastUpdatedDate = GetTimeNow.GetTime();
                saving.MRICompanyAccountGuid = new Guid(myobj.GetAccountId());
                saving.IsAgreedToFollowMKScan = true; 
                var typeofaction = string.Empty;
                saving.MRICompany = this.Session["CompanyName"].ToString();

                if (saving.MRIScanTime != null)
                {
                    var fixtime = saving.MRIScanTime.Split(':');
                    saving.MRIScanTime = fixtime[0] + ":" + fixtime[1];
                }
                

                // format the phone number to be human readable
                if (!string.IsNullOrEmpty(saving.Phone))
                {
                    // remove anything but numbers
                    var tmpPhone = Regex.Replace(saving.Phone, "[^0-9.]", "");

                    //format the phone number
                    saving.Phone = Regex.Replace(tmpPhone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                }

                if (id == string.Empty)
                {
                    saving.PatientGuid = Guid.NewGuid();
                    saving.Patient8 = saving.PatientGuid.ToString().Substring(0, 8);

                    saving.MRICompanyLastStatus = "Sent Request";   // this is the inital value
                    if (RequiredOverride)
                    {
                        saving.MRICompanyLastStatus = "Saved";   // this is the inital value
                    }
                    
                    saving.OverAllStatus = saving.MRICompanyLastStatus;
                    repository.Add(saving);
                    typeofaction = "Add";
                }
                else if (id != string.Empty)
                {
                    saving.MRICompanyLastStatus = "Updated";   // this is the inital value
                    saving.OverAllStatus = saving.MRICompanyLastStatus;
                    repository.Update(saving);
                    typeofaction = "Update";
                }
                
                repository.Save();

                // below is just for sending Text, an admin of the DeviceCo may opt to have no text sent
                // then this section will not produce any text messages
                //--------------------------------------------------------------------------------
                // NEED TO SEND EMAIL TO ADMIN also
                if (typeofaction == "Add" && RequiredOverride == false)
                {
                    int vendorSentTo = 0;
                    var phones = new List<PhoneVal>();
                    var sql = from a in this.db.DataGeographyRegions
                              join b in this.db.DataAccounts on a.GeographyGuid equals b.GeographyGuid
                              where b.AccountGuid == saving.DeviceCompanyGuid
                               && b.CompanyName.Contains(this.DeviceCompanyName.SelectedItem.Text)
                              select b;
                    var devicecos = sql.ToList();
                    foreach (var devco in devicecos)
                    {
                        var users = (from d in this.db.DataUserMasts
                                     where d.AccountGuid == devco.AccountGuid
                                     && d.Role == "Device Co Tech" && d.IsSendText == true
                                     select d).Distinct().ToList();
                        foreach (var cellphone in users)
                        {
                            var phone = new PhoneVal { phone = cellphone.CellPhone, personName = cellphone.FirstName, UserGuid = new Guid(cellphone.UserGuid.ToString()) };
                            if (phone.phone.Length > 0)
                            {
                                if (phone.phone.Substring(0, 1) != "1")
                                {
                                    phone.phone = @"1" + Regex.Replace(phone.phone, "[^0-9.]", "");
                                }
                                else
                                {
                                    phone.phone = Regex.Replace(phone.phone, "[^0-9.]", "");
                                }
                                phone.UserGuid = new Guid(cellphone.UserGuid.ToString());

                                var checkfirst = (from d in phones
                                                  where phone.phone.Contains(phone.phone)
                                                      && phone.UserGuid.ToString().Contains(phone.UserGuid.ToString())
                                                  select d).ToList();
                                if (checkfirst.Count == 0) { phones.Add(phone); }
                            }
                        }

                        foreach (var sendto in phones)
                        {
                            vendorSentTo += 1;
                            var patient81 = saving.Patient8 + vendorSentTo;
                            var phone = new List<string> { sendto.phone };

                            SendTextMsg.TextMessage(phone,
                                $"Hi {sendto.personName}, Smart-Praxis New patient request for {this.Session["CompanyName"]}, login @ http://spmk.us/{patient81}", this.Session["CompanyName"]?.ToString());

                            // add the code here to save to the MobilePageSecurity table
                            var addtodb = new MobilePageSecurity
                            {
                                DeviceCoTechGuid = sendto.UserGuid,
                                Patient81 = patient81
                            };
                            this.db.MobilePageSecurities.Add(addtodb);
                            this.db.SaveChanges();
                        }
                    }
                }
                #endregion

                ret = saving.MRICompanyAccountGuid.ToString();
                ret2 = saving.PatientGuid.ToString();
            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    ret4 = "Error, the user name already exists";
                }

                ret = "failure";
            }

            #endregion
            
            var ret5 = $"success|{ret}|{ret2}|{ret3}|{ret4}";
            return ret5;
        }

        protected void Calendar_OnDayRender(object sender, DayRenderEventArgs e)
        {
            // took out special days, check previous version of code
            // modify the cell rendered content for the days we want to be disabled (e.g. every Saturday and Sunday)
            if (e.Day.Date.DayOfWeek == DayOfWeek.Saturday || e.Day.Date.DayOfWeek == DayOfWeek.Sunday)
            {
                // if you are using the skin bundled as a webresource("Default"), the Skin property returns empty string
                var otherMonthCssClass = "rcOutOfRange";

                // clear the default cell content (anchor tag) as we need to disable the hover effect for this cell
                e.Cell.Text = string.Empty;
                e.Cell.CssClass = otherMonthCssClass; //set new CssClass for the disabled calendar day cells (e.g. look like other month days here)

                // render a span element with the processed calendar day number instead of the removed anchor -- necessary for the calendar skinning mechanism 
                var label = new Label {Text = e.Day.Date.Day.ToString()};
                e.Cell.Controls.Add(label);

                // disable the selection for the specific day
                var calendarDay = new RadCalendarDay
                {
                    Date = e.Day.Date,
                    IsSelectable = false
                };
                calendarDay.ItemStyle.CssClass = otherMonthCssClass;
                this.MRIScanDate.Calendar.SpecialDays.Add(calendarDay);
            }
        }

        private Guid GetDeviceCompanyGuid(string company)
        {
            if (!string.IsNullOrEmpty(company))
            {
                var result = (from d in this.db.DataAccounts
                              where d.CompanyName.Contains(company) || d.ShortCompanyName.Contains(company)
                              select d).FirstOrDefault();
                if (result != null)
                {
                    // update ** but nothing to do here
                    return result.AccountGuid;
                }
            }
            return new Guid();
        }

        private Guid GetUser(string user)
        {
            if (!string.IsNullOrEmpty(user))
            {
                var result = (from d in this.db.DataUserMasts
                              where d.FullName == user
                              select d).FirstOrDefault();
                // update ** but nothing to do here
                if (result?.UserGuid != null) return (Guid)result.UserGuid;
            }
            return new Guid();
        }

        public string GetAccountId()
        {
            return this.Session["AccountGuid"].ToString();
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields();
            if (result != null)
            {
                var valid = this.SaveRecord(result);
                if (!valid.Contains("failure"))
                {
                    this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=" + this.Session["AccountGuid"]);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotFound()", true);
                }
            }
        }

        private List<string> CheckFields(bool RequiredOverride = false)
        {
            var required = false;
            var arr = new List<string>();

            var id = this.Request.QueryString["id"];
            
            foreach (var text in this.Panel1.Controls)
            {
                if (text.ToString().Contains("TextBox"))
                {
                    var btn = (TextBox)text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }

                    if (btn.ClientID.Contains("Password") && btn.Text != string.Empty && id == null)
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                    else if (btn.ClientID.Contains("Password") && btn.Text == string.Empty)
                    {
                        const string tableName = "DataUserMast"; // <-- put table name here
                        const string fieldName = "UserGuid"; // <-- put the field name here
                        string outError;
                        if (id == null)
                        {
                            break;
                        }

                        var repository = new UserListRepository(new DB_SmartPraxisEntities());
                        var destination = repository.FindAllByUserGuid(tableName, fieldName, id, out outError);
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            var pwd = dHelper.Decrypt(result.Password);
                            arr.Add($"{btn.ClientID}:{pwd}");
                            required = false;
                            btn.BackColor = Color.FromArgb(255,255,255);
                        }
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                }
                else if (text.ToString().Contains("Combo") || text.ToString().Contains("Drop"))
                {
                    var btn = (DropDownList)text;
                    if ((btn.SelectedItem.Text == string.Empty || btn.SelectedItem.Text == @"- Select One -") && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                    arr.Add($"{btn.ClientID}:{btn.SelectedItem.Text}");
                }
                else if (text.ToString().Contains("Hidden"))
                {
                    var btn = (HiddenField)text;
                    arr.Add($"{btn.ClientID}:{btn.Value}");
                }
                else if (text.ToString().Contains("ImageUrl"))
                {
                    var btn = (ImageButton)text;
                    arr.Add($"{btn.ClientID}:{btn.ClientID}");
                }
                else if (text.ToString().Contains("DatePicker"))
                {
                    var btn = (RadDatePicker)text;
                    var datevalue = btn.SelectedDate.ToString().Replace(":", "~");
                    if (btn.SelectedDate == null)
                    {
                        datevalue = btn.InvalidTextBoxValue.Replace(":", "~");
                        arr.Add($"{btn.ClientID}:{datevalue}");
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{datevalue}");
                    }

                    if (btn.SelectedDate.ToString() == string.Empty)
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(255, 255, 255);
                    }
                }
                else if (text.ToString().Contains("TimePicker"))
                {
                    var btn = (RadTimePicker)text;
                    var timevalue = btn.SelectedTime.ToString().Replace(":","~");
                    arr.Add($"{btn.ClientID}:{timevalue}");
                    if (btn.SelectedTime.ToString() == string.Empty)
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(255, 255, 255);
                    }
                }
                else if (text.ToString().Contains("Doctor"))
                {
                    if (this.Session["Role"].ToString().Contains("MRI Doctor"))
                    {
                        Debugger.Break();
                    }
                }
            }

            if (required && RequiredOverride == false)
            {
                return null;
            }

            return arr;
        }

        protected void txtDeviceCompany_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ImageButtonCheck_Click(object sender, ImageClickEventArgs e)
        {
            if (this.IsAgreedToFollowMKScan.ImageUrl.Contains("Off"))
            {
                this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                this.btnsubmit.Enabled = true;
                this.btnsubmit.Text = @"Send Request";
                this.btnsubmit.CssClass = "btn btn-success";
            }
            else
            {
                this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                this.btnsubmit.Enabled = false;
                this.btnsubmit.Text = @"Click Agree";
                this.btnsubmit.CssClass = "btn btn-success.disabled";
            }
        }

        protected void btnRequestPdf_Click(object sender, EventArgs e)
        {
            // first save the record
            var RequiredOverride = true;
            var result = this.CheckFields(RequiredOverride);
            //if (result != null)
            //{
            var valid = this.SaveRecord(result, RequiredOverride);
                if (!valid.Contains("failure"))
                {
                    // then go to view the popup page
                    var patientvalue = valid.Split('|')[2];
                    var patientid = patientvalue;
                    var action = "edit";
                    var company = this.Session["AccountGuid"].ToString();
                    this.Session["BackButton"] = $"/pages/MRICompany/Patient.aspx?id={patientid}&action={action}&x={company}";
                    this.Response.Redirect("~/PdfViewer.aspx?y=Sample.pdf");
                }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotFound()", true);
                //}
            //}
        }

        public void PopulateDoctorNameDropDown()
        {
            var myAccountGuid = new Guid(this.Session["AccountGuid"].ToString());
            const string tableName = "DataUserMast"; // <-- put table name here
            const string fieldName = "Role"; // <-- put the field name here
            const string fieldName2 = "AccountGuid"; // <-- put the field name here
            string outError;

            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAllByRoleAndAccountId(tableName, fieldName, @"Doctor", fieldName2, myAccountGuid.ToString(), out outError);

            this.MRIDoctorName.DataSource = destination;
            this.MRIDoctorName.DataValueField = "UserGuid";
            this.MRIDoctorName.DataTextField = "FullName";
            this.MRIDoctorName.DataBind();

            this.MRIDoctorName.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        public void PopulateStatusDropDown()
        {
            var repository = new ListStatusRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.PatientStatusDDL.DataSource = destination;
            this.PatientStatusDDL.DataValueField = "Code";
            this.PatientStatusDDL.DataTextField = "Description";
            this.PatientStatusDDL.DataBind();

            this.PatientStatusDDL.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        [ScriptMethod]
        [WebMethod]
        public static List<string> GetDeviceCompany(string prefixText)
        {
            var result = new List<string>();
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.DataAccounts
                             join a in db.DataAccounts on d.GeographyGuid equals a.GeographyGuid
                             where d.CompanyName.Contains(prefixText) && a.AccountGuid == ourAccountId
                             orderby d.CompanyName
                             select new { d.ShortCompanyName }).Take(20).Distinct().ToList();

                result.AddRange(query.Select(item => item.ShortCompanyName));
            }

            return result;
        }

        //private void SetupNotification2()
        //{
        //    var sb = new StringBuilder();
        //    sb.Append("<strong>Tell us about your practice.</strong>");
        //    sb.Append("<br><br>");
        //    sb.Append("I joined Cardiac Arrhythmia Service, located in South Florida, after completing my EP fellowship 6 years ago at Brigham and Women’s Hospital. I have 2 partners, Dr. Murray Rosenbaum and Dr. Martin Kloosterman. We also have 2 physician assistants. We have a research coordinator and are involved in clinical studies. Our practice is limited to electrophysiology, and we have a large referral network from general cardiologists in the area.");
        //    sb.Append("<br><br>");
        //    sb.Append("We lecture at the College of Medicine at Florida Atlantic University (FAU), and teach EKGs and electrophysiology to FAU residents. Our practice is affiliated with 3 hospitals: Boca Raton Regional Hospital, Delray Medical Center, and West Boca Medical Center. ");
        //    sb.Append("<br><br>");
        //    sb.Append("We perform complex ablations, including for atrial fibrillation and ventricular tachycardia. We routinely perform fluoroless ablation for SVT, atrial flutter, and atrial fibrillation. Thanks to a generous donation from our patient, we were the third site in the U.S. to obtain the CardioInsight™ Noninvasive 3D Mapping System (Medtronic). Dr. Rosenbaum is the largest volume operator of CardioInsight in the U.S. We also perform all types of device implantations, including subcutaneous ICDs, leadless pacemakers, and direct His bundle pacing. We have a very active cardiac device remote monitoring service with over 2,000 patients. Dr. Kloosterman has pioneered the use of remote monitoring in the hospital setting. We are the largest users of CareLink Express (Medtronic) in the country.");
        //    sb.Append("<br><br>");
        //    sb.Append("<strong>Why is remote monitoring important?</strong>");
        //    sb.Append("<br><br>");
        //    sb.Append("Remote monitoring is associated with a morbidity and mortality benefit for patients with pacemakers and defibrillators.1-3 By monitoring patients on a daily basis, we are able to immediately diagnose and treat arrhythmias and device/lead-related problems. If we discover atrial fibrillation, we start the patient on anticoagulation right away, then have them follow up in our office or with their cardiologist. If we find a ventricular arrhythmia, we start antiarrhythmic medications over the phone or admit the patient to the hospital to prevent recurrent VT and ICD shocks. If we see lead- or device-related issues, we immediately arrange appropriate care to prevent inappropriate ICD shocks or loss of pacemaker function. Remote monitoring has revolutionized the practice of cardiac electrophysiology and has markedly improved patient outcomes.1-2");
        //    sb.Append("<br><br>");
        //    sb.Append("<strong>How have you incorporated remote monitoring into your practice?</strong>");
        //    sb.Append("<br><br>");
        //    sb.Append("Remote monitoring maximizes our group’s efficiency. We can manage and treat more patients by reducing the number of in-office visits needed for patients followed remotely. We currently remotely monitor 850 patients with implantable defibrillators, 750 patients with implantable loop recorders, and 400 patients with pacemakers. We also have 300 patients in our remote heart failure monitoring program. We have 2 technicians who download and review transmissions on a daily basis. Transmissions that require review are evaluated by a physician, and after a plan of care has been established, the transmission and note are scanned into the EMR. A physician or physician assistant will contact the patient by phone to review the information and plan of care. We are a team of secretaries, technicians, nurses, physician assistants, and physicians all working together to optimize patient care for our remote monitoring program.");
        //    sb.Append("<br><br>");
        //    sb.Append("We also use remote monitoring in the hospital setting. Instead of waiting for a device representative or physician to check a patient’s device, the nurse is able to send a remote transmission, which we can instantly access for evaluation. Remote transmission for all post-op device patients are done by the night shift nurse, and the report is available in the chart by 7am. The ER also performs remote transmissions for all their device checks. We are able to immediately access and review these transmissions, allowing for timely and efficient patient care. ");
        //    sb.Append("<br><br>");
        //    sb.Append("<strong>Describe remote monitoring for heart failure.</strong>");
        //    sb.Append("<br><br>");
        //    sb.Append("Implantable defibrillators can monitor intrathoracic impedance. The lungs are located between the RV lead and defibrillator, and by measuring the resistance between them, the defibrillator can assess pulmonary fluid status. Conduction is faster through water; therefore, lower intrathoracic impedance (lower resistance) suggests pulmonary fluid accumulation. Low intrathoracic impedance values precede clinical signs and symptoms of congestive heart failure.4-5 Studies show that patients with low intrathoracic impedance measurements are at increased near-term risk of CHF hospitalization.4-6 By remotely monitoring changes in intrathoracic impedance, we can potentially prevent heart failure exacerbations and hospitalizations.");
        //    sb.Append("<br><br>");
        //    sb.Append("<strong>Have studies demonstrated a clinical benefit for heart failure monitoring?</strong>");
        //    sb.Append("<br><br>");
        //    sb.Append("Studies have consistently demonstrated the predictive nature of low intrathoracic impedance and the increased risk of CHF exacerbation and hospitalization.6-7 However, the utilization of this data to improve clinical outcomes is not well established.8 The DOT-HF study evaluated whether intrathoracic impedance values could be utilized to decrease hospitalizations.9 Patients received an audible alert to a decrease in intrathoracic impedance. Results showed an increase in hospitalizations in the treatment group compared with the control group. However, the presence of an audible alert and the lack of remote monitoring may have impacted these results. Heart failure treatment and hospitalizations in DOT-HF were often driven by the audible alert, even in the absence of clinical symptoms. Similarly, the LIMIT-CHF study studied the presence of an audible alert to low intrathoracic impedance to reduce CHF hospitalizations.10 Every patient with an audible alert was instructed to increase their diuretic dose for a week. There was no difference in outcome between the two groups. However, patients were treated solely based on an alert, even in the absence of clinical symptoms. ");
        //    sb.Append("<br><br>");
        //    sb.Append("<strong>How does your approach differ from prior studies?</strong>");
        //    sb.Append("<br><br>");
        //    sb.Append("When treating patients with congestive heart failure, it is imperative to involve the patient. Prior studies have focused primarily on intrathoracic impedance measurements rather than patients and their symptoms. In LIMIT-CHF, patients were treated with diuretics irrespective of clinical symptoms.10 In DOT-HF, every patient alert had to be treated until the intrathoracic impedance normalized, even in the absence of symptoms.9 This likely led to overtreatment and unnecessary hospitalizations. ");
        //    sb.Append("<br><br>");
        //    sb.Append("Intrathoracic impedance is an early marker of increased pulmonary fluid levels. However, the body self-adjusts and intrathoracic impedance levels often normalize without any intervention. Therefore, we sought to use a low intrathoracic impedance value as a screening tool rather than an indication to treat. We utilize a patient-centered educational approach to abnormal intrathoracic impedance values. With monthly remote evaluation of patients’ intrathoracic impedance, we are able to detect patients who are at higher risk of hospitalization. All patients with low intrathoracic impedance are contacted by a nurse, physician, or physician assistant. The health care professional assesses the patient, and then educates them on signs and symptoms of heart failure. Only patients who are symptomatic are sent to their cardiologists for further evaluation and treatment. This minimizes overtreatment and, at the same time, empowers patients to participate in their medical care. With our approach, we have an extremely low rate of heart failure hospitalizations.11");
        //    sb.Append("<br><br>");
        //    sb.Append("<strong>How do patients and cardiologists feel about your approach to remote heart failure monitoring?</strong>");
        //    sb.Append("<br><br>");
        //    sb.Append("We work together with our referring cardiologists to provide the best care for our patients. Since we only practice electrophysiology, we refer all of our symptomatic heart failure patients back to their cardiologist for further treatment. Our patients appreciate the close monitoring and follow-up that we provide. We do a follow-up phone call the month after we detect an abnormal intrathoracic impedance, to ensure the patient is doing well and was not hospitalized. By involving our patients in their treatment, they feel more responsible for their medical care. Our patient-centered educational approach to remote heart failure monitoring is unique, effective, and can reduce heart failure exacerbations and hospitalizations.");
        //    sb.Append("<br><br>");
        //    sb.Append("<br><br>");
        //    this.RadNotification2.Title = "Info";
        //    this.RadNotification2.Text = sb.ToString();
        //}
    }
}

public class PhoneVal
{
    public string personName { get; set; }
    public string phone { get; set; }
    public Guid UserGuid { get; set; }
}