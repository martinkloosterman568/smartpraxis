﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Patient.aspx.cs" Inherits="SmartPraxis.Pages.Account.Patient"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" Title="Smart-Praxis" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50508.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadButton1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                        <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    
    <script type="text/javascript">
        function showNotification()
        {
            $find("<%= this.RadNotification1.ClientID %>").show();
        }
        function showRequestPopup()
        {
            $find("<%= this.RadNotification2.ClientID %>").show();
        }
        function OnDayRender(calendarInstance, args) {
            // convert the date-triplet to a javascript date
            // we need Date.getDay() method to determine 
            // which days should be disabled (e.g. every Saturday (day = 6) and Sunday (day = 0))                
            var jsDate = new Date(args.get_date()[0], args.get_date()[1] - 1, args.get_date()[2]);
            if (jsDate.getDay() === 0 || jsDate.getDay() === 6) {
                var otherMonthCssClass = "rcOutOfRange";
                args.get_cell().className = otherMonthCssClass;
                // replace the default cell content (anchor tag) with a span element 
                // that contains the processed calendar day number -- necessary for the calendar skinning mechanism 
                args.get_cell().innerHTML = "<span>" + args.get_date()[2] + "</span>";
                // disable selection and hover effect for the cell
                args.get_cell().DayId = "";
            }
        }
    </script>

    <style type="text/css">
          .autocomplete_CompletionListElement
        {
            margin: 0px;
            background-color: White;
            cursor: default;
            list-style-type: none;
            overflow-y: auto;
            overflow-x: hidden;
            height:180px;
            text-align: left;
            border: 1px solid #777;
            z-index:10000;
        } 

        .modalBackground {
            background-color:Gray;
            filter:alpha(opacity=70);
            opacity:0.7;
        }
        
        .modalPopup {
	        background-color:#ffffdd;
	        border-width:3px;
	        border-style:solid;
	        border-color:Gray;
	        padding:3px;
	        width:250px;
        }
    </style>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
	    var counter;
	    counter = 0;

	    function KeepSessionAlive() {
	        // Increase counter value, so we'll always get unique URL (sample source from page)
	        // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
	        counter++;

	        // Gets reference of image
	        var img = document.getElementById("imgSessionAlive");

	        // Set new src value, which will cause request to server, so
	        // session will stay alive
	        var pathname = this.Session["PathName"] != null ? this.Session["PathName"].ToString() : string.Empty;
	        img.src = string.Format("http://{0}.com/RefreshSessionState.aspx?c=", pathname) + counter;

	        // Schedule new call of KeepSessionAlive function after 60 seconds
	        setTimeout(KeepSessionAlive, 60000);
	    }

	    // Run function for a first time
	    KeepSessionAlive();
	</script> 
  
    <br/>
    <div>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <h3>Add New Patient Order Request</h3>                
            </div>
        </div>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-lg-10">
                            <div class="panel panel-default">
                                <div style="vertical-align: bottom;" class="panel-heading">
                                    <center><h4><strong>MRI Scan protocol for patients with implantable cardiac electronic devices CIEDs
                                          <br/> <br/>* Pacemakers  * Defibrillators  * Implantable Cardiac Monitors 
                                        <br/><br/>Request for Device Company Service and Protocol Order</strong></h4></center>
                                        <br/>
                                        <%--<asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New File"   />--%>
                                        <div class="col-lg-3">
                                            &nbsp;
                                        </div>
                                        <div class="col-lg-3">
                                            &nbsp;
                                        </div>
                                        <div class="col-lg-3">
                                            &nbsp;
                                        </div>
                                        <div class="col-lg-3">
                                            &nbsp;
                                        </div>
                                </div>
                             
                                <div runat="server" id="divbody" class="panel-body">
                                        <asp:Panel ID="Panel1" runat="server">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Patient Name</label><asp:Label ID="Label1" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:TextBox ID="PatientName" alt="required" CssClass="form-control" name="firstname" runat="server" accept="First Name" placeholder="First Name" ValidationGroup="Required" SkinID="Required" />
                                                    <asp:HiddenField id="hfErrorMessage" runat="server"/>
                                                    <asp:HiddenField ID="PatientId" runat="server" />
                                                    <asp:HiddenField ID="PatientGuid" runat="server" />
                                                    <asp:HiddenField ID="Patient8" runat="server" />
                                                    <asp:HiddenField ID="MRICompany" runat="server" />
                                                    <asp:HiddenField ID="MRICompanyAccountGuid" runat="server" />
                                                    <asp:HiddenField ID="MRITechName" runat="server" />
                                                    <asp:HiddenField ID="MRITechUserGuid" runat="server" />
                                                    <asp:HiddenField ID="OverAllStatus" runat="server" />
                                                    <asp:HiddenField ID="MRICompanyLastStatus" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="form-group">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>
                                                        Logged In Person</label>
                                                    <asp:TextBox ID="LastUpdatePerson" CssClass="form-control" runat="server" accept="Last Update Person" placeholder="Last Update Person" ValidationGroup="AlwaysDisable" Enabled="False" ReadOnly="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Patient DOB</label><asp:Label ID="Label4" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <br/>
                                                    <telerik:RadDatePicker ID="DOB" runat="server" MinDate="1900-01-01">
                                                        <Calendar EnableWeekends="True" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                                        </Calendar>
                                                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                                                            <EmptyMessageStyle Resize="None" />
                                                            <ReadOnlyStyle Resize="None" />
                                                            <FocusedStyle Resize="None" />
                                                            <DisabledStyle Resize="None" />
                                                            <InvalidStyle Resize="None" />
                                                            <HoveredStyle Resize="None" />
                                                            <EnabledStyle Resize="None" />
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Patient Phone</label><asp:Label ID="Label7" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:TextBox ID="Phone" alt="required" CssClass="form-control" name="phone" runat="server" accept="Phone" placeholder="Phone" ValidationGroup="Required" SkinID="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="form-group">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label runat="server" id="lblPhysician">Physician Name</label><asp:Label ID="LabelRequired3" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:DropDownList ID="MRIDoctorName" CssClass="form-control" runat="server" SkinID="Required"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label runat="server" id="Label9">MRI Type</label><asp:Label ID="Label10" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:DropDownList ID="MRIType" CssClass="form-control" runat="server" SkinID="Required">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Insurance</label>
                                                    <asp:Label ID="Label2" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *" />&nbsp;
                                                    <asp:TextBox ID="Insurance" alt="required" CssClass="form-control" name="insurance" runat="server" accept="Insurance" placeholder="Insurance" ValidationGroup="Required" SkinID="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                &nbsp;
                                            </div>
                                            <div class="col-lg-1">
                                                <label>
                                                    Agree</label><br/>
                                                <asp:ImageButton ID="IsAgreedToFollowMKScan" runat="server" ImageUrl="~/Content/Images/CheckOffsmall.png" OnClick="ImageButtonCheck_Click" />
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Company representative to follow MRI Safe Scan MK-Protocol-Algorithm©.
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Device Company</label>
                                                    <asp:Label ID="Label5" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *" />&nbsp;
                                                    <asp:DropDownList ID="DeviceCompanyName" CssClass="form-control" runat="server" SkinID="Required">
                                                    </asp:DropDownList>
                                                    <%--<asp:TextBox ID="DeviceCompanyName" alt="required" CssClass="form-control" name="devicecompany" runat="server" accept="DeviceCompany" placeholder="Device Company" ValidationGroup="Required" OnTextChanged="txtDeviceCompany_TextChanged" SkinID="Required" />
                                                     <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                                                    CompletionSetCount="20" EnableCaching="true" 
                                                    MinimumPrefixLength="1"  ServiceMethod="GetDeviceCompany" TargetControlID="DeviceCompanyName" 
                                                    UseContextKey="True" 
                                                    CompletionListCssClass="autocomplete_CompletionListElement">
                                                    </asp:AutoCompleteExtender>--%>
                                                    <asp:HiddenField ID="DeviceCompanyGuid" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group" style="vertical-align: bottom">
                                                    <label>
                                                        MRI Scan Schedule</label>
                                                    <asp:Label ID="Label8" Visible="True" runat="server" ForeColor="Red" Text=" *" />&nbsp;
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <telerik:RadDatePicker ID="MRIScanDate" runat="server" Culture="en-US">
                                                                    <Calendar OnDayRender="Calendar_OnDayRender" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                                                        <ClientEvents OnDayRender="OnDayRender" />
                                                                        <DayStyle BackColor="lightblue" BorderColor="White" BorderStyle="None" />
                                                                    </Calendar>
                                                                    <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                                                                        <EmptyMessageStyle Resize="None" />
                                                                        <ReadOnlyStyle Resize="None" />
                                                                        <FocusedStyle Resize="None" />
                                                                        <DisabledStyle Resize="None" />
                                                                        <InvalidStyle Resize="None" />
                                                                        <HoveredStyle Resize="None" />
                                                                        <EnabledStyle Resize="None" />
                                                                    </DateInput>
                                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                                </telerik:RadDatePicker>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <telerik:RadTimePicker ID="MRIScanTime" runat="server" Culture="en-US">
                                                                    <Calendar EnableWeekends="True" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                                                    </Calendar>
                                                                    <DatePopupButton CssClass="" HoverImageUrl="" ImageUrl="" Visible="False" />
                                                                    <TimeView CellSpacing="-1" EndTime="17:30:59" Interval="00:15:00" StartTime="09:00:00">
                                                                    </TimeView>
                                                                    <TimePopupButton CssClass="" HoverImageUrl="" ImageUrl="" />
                                                                    <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="64px" Width="">
                                                                        <EmptyMessageStyle Resize="None" />
                                                                        <ReadOnlyStyle Resize="None" />
                                                                        <FocusedStyle Resize="None" />
                                                                        <DisabledStyle Resize="None" />
                                                                        <InvalidStyle Resize="None" />
                                                                        <HoveredStyle Resize="None" />
                                                                        <EnabledStyle Resize="None" />
                                                                    </DateInput>
                                                                </telerik:RadTimePicker>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.row (nested) -->
                                        <div class="row">
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Comment To Device Company Tech</label>
                                                    <asp:TextBox ID="Comments" alt="required" CssClass="form-control" name="comments" runat="server" accept="Comments" placeholder="Comments" ValidationGroup="Required" SkinID="NotRequired" Rows="3" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                &nbsp;
                                            </div>
                                            
                                            <div style="background: lightblue; margin-top: -150px;" class="col-lg-4">
                                                <br/>
                                                <label>
                                                        Click here for full MRI Safe Scan MK-Protocol-Algorithm© order set as seen and to be followed by company representative.
                                                    </label><br/>
                                                    <div runat="server" id="div1">&nbsp;
                                                        <asp:Button ID="ButtonRequestPdf" runat="server" Text="Request Pdf" class="btn btn-primary" OnClick="btnRequestPdf_Click" TabIndex="99" />
                                                        
                                                    </div>
                                                    
                                                <br/>
                                            </div>
                                              <br/>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Comment From Device Company Tech</label>
                                                    <asp:TextBox ID="TechCommentsReply" alt="required" CssClass="form-control" runat="server" placeholder="Tech Comments Reply" ValidationGroup="Required" SkinID="NotRequired" Rows="3" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="statusDDL" runat="server" class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label runat="server" id="Label3">Status</label><asp:Label ID="Label6" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:DropDownList ID="PatientStatusDDL" CssClass="form-control" runat="server" SkinID="Required">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>
                                                        &nbsp;</label>
                                                </div>
                                            </div> 
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Last Update Date</label>
                                                    <asp:TextBox ID="LastUpdatedDate" CssClass="form-control" runat="server" accept="Last Update Date" placeholder="Last Update Date" ValidationGroup="AlwaysDisable" Enabled="False" ReadOnly="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div style="float: left;" class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <!-- This html button triggers the jquery above -->
                                                    <div style="float:left" runat="server" id="divBtnSubmit"><asp:Button ID="btnsubmit" runat="server" Text="Send Request" class="btn btn-success" OnClick="btnsubmit1a_Click" />&nbsp;</div>
                                                    <div style="float:left" runat="server" id="divBtnClear">&nbsp;<asp:Button ID="btnclear" runat="server" Text="Clear" class="btn btn-warning" OnClick="btnclear_Click" /></div>
                                                    <div style="float:left" runat="server" id="divBtnNew">&nbsp;<asp:Button ID="btnnew" runat="server" Text="New" class="btn btn-primary" OnClick="btnnew_Click"  /></div>
                                                    <div style="float:left" runat="server" id="divBtnCancel">&nbsp;<asp:Button ID="btncancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="btncancel_Click" /></div>
                                                </div>
                                                <br/><br/>
                                            </div>
                                            <br/><br/>
                                        </div>
                                        
                                           
                                        </asp:Panel>
                                        <!-- /.row (nested) -->
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-md-10">
                    <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotification1" runat="server" Position="Center"
                             Width="330px" Height="160px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                             Title="Error" Text="Error, Trying To Add New Patient."
                             Style="z-index: 100000" Skin="Outlook">
                    </telerik:RadNotification>
                    <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotification2" runat="server" Position="Center"
                             Width="830px" ContentScrolling="Auto" Height="560px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                                        Title="Title" Text="This text will be replaced from inside the code"
                             Style="z-index: 100000"  Skin="Outlook">
                    </telerik:RadNotification>
                </div>
                <div class="col-md-4">
                                
                </div>
            </div>  
    </div>
</asp:Content>
