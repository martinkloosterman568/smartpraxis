﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Helper;
using SmartPraxis.Repositories;
using Telerik.Web.UI;

namespace SmartPraxis.Reusable
{
    public class ControlFiller : RadAjaxPage
    {
        public static HtmlGenericControl DivBody { get; set; }

        public void HideRequiredControls(HtmlGenericControl divbody)
        {
            foreach (var control in divbody.Controls)
            {
                if (control is Label)
                {
                    var ctrl = (Label) control;
                    if (ctrl.SkinID.ToLower().Contains("required"))
                    {
                        ctrl.Visible = true;
                    }

                    if (ctrl.SkinID.ToLower().Contains("notrequired"))
                    {
                        ctrl.Visible = false;
                    }
                }
            }
        }

        public void ShowRequiredControls(HtmlGenericControl divbody)
        {
            foreach (var control in divbody.Controls)
            {
                if (control is Label)
                {
                    var ctrl = (Label) control;
                    if (ctrl.SkinID.ToLower().Contains("required"))
                    {
                        ctrl.Visible = true;
                    }

                    if (ctrl.SkinID.ToLower().Contains("notrequired"))
                    {
                        ctrl.Visible = false;
                    }
                }
                //else if (control is DropDownList)
                //{
                //    Debugger.Break();
                //}
            }
        }

        public static bool AnyRequiredNotFilledControls(HtmlGenericControl divbody)
        {
            var retVal = false;
            foreach (var control in divbody.Controls)
            {
                if (control is TextBox)
                {
                    var ctrl = (TextBox) control;
                    if (!ctrl.ValidationGroup.ToLower().Contains("notrequired") &&
                        ctrl.ValidationGroup.ToLower().Contains("required") && ctrl.Text == string.Empty)
                    {
                        ctrl.Text = @"Missing Value";
                        ctrl.ForeColor = Color.Red;
                        retVal = true;
                    }
                    else
                    {
                        ctrl.ForeColor = Color.Black;
                    }
                }
            }

            return retVal;
        }

        public void ClearControls(Panel divbody)
        {
            foreach (var control in divbody.Controls)
            {
                if (control is TextBox)
                {
                    var ctrl = (TextBox) control;
                    ctrl.Text = string.Empty;
                    ctrl.ForeColor = Color.Black;
                }
                if (control is DropDownList)
                {
                    ((DropDownList) control).SelectedIndex = -1;
                }

                if (control is RadioButtonList)
                {
                    Debugger.Break();
                }

                if (control.GetType().ToString().Contains("CheckBox"))
                {
                    var ctrl = (CheckBox) control;
                    ctrl.Checked = false;
                }
            }
        }

        public void EnableControls(HtmlGenericControl divbody)
        {
            foreach (var control in divbody.Controls)
            {
                if (control is TextBox)
                {
                    var ctrl = (TextBox) control;
                    if (ctrl.ValidationGroup.ToLower().Contains("alwaysdisable"))
                    {
                        ctrl.BorderStyle = BorderStyle.None;
                        ctrl.Enabled = false;
                    }
                    else
                    {
                        ctrl.Enabled = true;
                    }

                    ctrl.ForeColor = Color.Black;
                }
                if (control is DropDownList)
                {
                    ((DropDownList) control).Enabled = true;
                }

                if (control is RadioButtonList)
                {
                    Debugger.Break();
                }

                if (control.GetType().ToString().Contains("CheckBox"))
                {
                    var ctrl = (CheckBox) control;
                    if (ctrl.ValidationGroup.ToLower().Contains("alwaysdisable"))
                    {
                        ctrl.Enabled = false;
                    }
                    else
                    {
                        ctrl.Enabled = true;
                    }
                }
            }
        }

        public void DisableControls(HtmlGenericControl divbody)
        {
            foreach (var control in divbody.Controls)
            {
                if (control is TextBox)
                {
                    var ctrl = (TextBox) control;
                    if (ctrl.ValidationGroup.ToLower().Contains("keepenabled"))
                    {
                        ctrl.Enabled = true;
                    }
                    else
                    {
                        ctrl.Enabled = false;
                    }
                }
                if (control is DropDownList)
                {
                    var ctrl = (DropDownList) control;
                    if (ctrl.ValidationGroup.ToLower().Contains("keepenabled"))
                    {
                        ctrl.Enabled = true;
                    }
                    else
                    {
                        ctrl.Enabled = false;
                    }
                }

                if (control is RadioButtonList)
                {
                    var ctrl = (RadioButtonList) control;
                    if (ctrl.ValidationGroup.ToLower().Contains("keepenabled"))
                    {

                    }
                    Debugger.Break();
                }

                if (control.GetType().ToString().Contains("CheckBox"))
                {
                    var ctrl = (CheckBox) control;
                    if (ctrl.ValidationGroup.ToLower().Contains("keepenabled"))
                    {
                        ctrl.Enabled = true;
                    }
                    else
                    {
                        ctrl.Enabled = false;
                    }
                }
            }
        }

        public static bool IsDate(Object obj, out DateTime dt)
        {
            dt = DateTime.MaxValue;
            string strDate = obj.ToString();
            try
            {
                dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        public void FillControls(object destination, Panel divbody)
        {
            if (destination == null)
            {
                return;
            }

            // this is a generic control filler
            var destinationProperties = destination.GetType().GetProperties();
            foreach (var control in divbody.Controls)
            {
                foreach (var destinationProperty in destinationProperties)
                {
                    try
                    {
                        if (control is ImageButton)
                        {
                            if (((Control)control).ID == destinationProperty.Name)
                            {
                                var value = destinationProperty.GetValue(destination);
                                if (value != null)
                                {
                                    if (destinationProperty.Name.Contains("IsAgreed"))
                                    {
                                        if (value.ToString() == "True")
                                        {
                                            ((ImageButton)control).ImageUrl = "~/Content/Images/CheckOnsmall.png";
                                        }
                                        else
                                        {
                                            ((ImageButton)control).ImageUrl = "~/Content/Images/CheckOffsmall.png";
                                        }
                                    }
                                    else
                                    {
                                        ((ImageButton)control).ImageUrl = value.ToString();
                                    }

                                    ((ImageButton)control).ForeColor = Color.Black;
                                }
                            }
                            else if (control is ImageButton)
                            {
                                // Debugger.Break();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Debugger.Break();
                    }

                    try
                    {
                        if (control is TextBox)
                        {
                            if (((Control)control).ID == destinationProperty.Name)
                            {
                                var value = destinationProperty.GetValue(destination);
                                if (value != null)
                                {
                                    if (destinationProperty.Name == "Password")
                                    {
                                        var dHelper = new GeneralHelper();
                                        var value2 = dHelper.Decrypt(value.ToString());
                                        ((TextBox)control).Text = value2;
                                    }
                                    else if (destinationProperty.Name.Contains("Date"))
                                    {
                                        DateTime outDate;
                                        if (IsDate(value, out outDate))
                                        {
                                            var test = new TimeSpan();
                                            test = ((DateTime)value).TimeOfDay;

                                            if (test.Hours == 0 && test.Minutes == 0 && test.Seconds == 0)
                                            {
                                                ((TextBox)control).Text = outDate.ToString("M/d/yyyy");
                                            }
                                            else
                                            {
                                                ((TextBox)control).Text = outDate.ToString("M/d/yyyy H:mm:ss tt");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ((TextBox)control).Text = value.ToString();
                                    }

                                    ((TextBox)control).ForeColor = Color.Black;
                                }
                            }
                            else if (control is TextBox)
                            {

                            }
                        }
                    }
                    catch (Exception)
                    {
                        Debugger.Break();
                    }

                    try
                    {
                        if (control is HiddenField)
                        {
                            if (((Control)control).ID == destinationProperty.Name)
                            {
                                var value = destinationProperty.GetValue(destination);
                                if (value != null && destinationProperty.Name != "IsPosted")
                                {
                                    ((HiddenField)control).Value = value.ToString();
                                }
                                else if (value != null && destinationProperty.Name == "IsPosted")
                                {
                                    if ((bool)value)
                                    {
                                        ((HiddenField)control).Value = "True";
                                    }
                                    else
                                    {
                                        ((HiddenField)control).Value = "False";
                                    }

                                }
                                else if (value == null && destinationProperty.Name == "IsPosted")
                                {
                                    ((HiddenField)control).Value = "False";
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Debugger.Break();
                    }

                    try
                    {
                        if (control is Literal)
                        {
                            Debugger.Break();
                        }
                    }
                    catch (Exception)
                    {

                        Debugger.Break();
                    }

                    try
                    {
                        if (control is RadComboBox)
                        {
                            if (((Control)control).ID == destinationProperty.Name)
                            {
                                var value = destinationProperty.GetValue(destination);
                                if (value != null)
                                {
                                    if (Utility.IsNumeric(value))
                                    {
                                        // is number
                                        ((RadComboBox)control).SelectedIndex = ((RadComboBox)control).Items.IndexOf(((RadComboBox)control).Items.FindItemByValue(value.ToString()));
                                    }
                                    else
                                    {
                                        // is text
                                        ((RadComboBox)control).SelectedIndex = ((RadComboBox)control).Items.IndexOf(((RadComboBox)control).Items.FindItemByText(value.ToString()));
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // ignore
                    }

                    try
                    {
                        if (control is DropDownList)
                        {
                            if (((Control)control).ID == destinationProperty.Name)
                            {
                                var value = destinationProperty.GetValue(destination);
                                if (value != null)
                                {
                                    if (Utility.IsNumeric(value))
                                    {
                                        // is number
                                        ((DropDownList)control).SelectedIndex = ((DropDownList)control).Items.IndexOf(((DropDownList)control).Items.FindByValue(value.ToString()));
                                    }
                                    else if (destinationProperty.Name.Contains("AccountGuid"))
                                    {
                                        // is text
                                        ((DropDownList)control).SelectedIndex = ((DropDownList)control).Items.IndexOf(((DropDownList)control).Items.FindByValue(value.ToString()));
                                    }
                                    else
                                    {
                                        // is text
                                        ((DropDownList)control).SelectedIndex = ((DropDownList)control).Items.IndexOf(((DropDownList)control).Items.FindByText(value.ToString()));
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // ignore
                    }

                    try
                    {
                        if (control is RadTimePicker)
                        {
                            if (((Control)control).ID == destinationProperty.Name)
                            {
                                var value = destinationProperty.GetValue(destination);
                                if (value != null)
                                {
                                    if (destinationProperty.Name.Contains("Time"))
                                    {
                                        DateTime outDate;
                                        if (IsDate(value, out outDate))
                                        {
                                            var tod = new TimeSpan();
                                            tod = outDate.TimeOfDay;

                                            ((RadTimePicker)control).SelectedTime = tod;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // ignore
                    }

                    try
                    {
                        if (control is RadDatePicker)
                        {
                            if (((Control)control).ID == destinationProperty.Name)
                            {
                                var value = destinationProperty.GetValue(destination);
                                if (value != null)
                                {
                                    if (destinationProperty.Name.Contains("Date") ||
                                        destinationProperty.Name.Contains("DOB"))
                                    {
                                        DateTime outDate;
                                        if (IsDate(value, out outDate))
                                        {
                                            ((RadDatePicker)control).SelectedDate = outDate;
                                        }
                                    }

                                    //((RadDatePicker)control).ForeColor = Color.Black;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // ignore
                    }

                    try
                    {
                        if (destinationProperty.PropertyType.ToString().Contains("Boolean") && destinationProperty.Name != "IsPosted")
                        {
                            if (((Control)control).ID == destinationProperty.Name)
                            {
                                var value = destinationProperty.GetValue(destination);
                                ((CheckBox)control).Checked = Convert.ToBoolean(value);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // ignore
                    }

                    try
                    {
                        if (control is RadioButtonList)
                        {
                            Debugger.Break();
                        }
                    }
                    catch (Exception)
                    {
                        // ignore
                    }
                }
            }
        }
    }
}