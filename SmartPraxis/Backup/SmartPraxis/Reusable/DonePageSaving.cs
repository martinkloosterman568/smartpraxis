﻿using System;
using System.Linq;
using SmartPraxis.Models;

namespace SmartPraxis.Reusable
{
    public static class DonePageSaving
    {
        public static bool SaveBluePagesAnswer(string answer, string patientGuid, string userGuid)
        {
            DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();

            var retVal = false;
            try
            {
                var patient = new Guid(patientGuid);
                var patientRecord = (from d in db.DataPatientRequests
                    where d.PatientGuid == patient
                    select d).FirstOrDefault();

                if (patientRecord != null)
                {
                    var deviceTechGuid = new Guid(userGuid);
                    patientRecord.DeviceCoSelectedSafeMode = answer;
                    patientRecord.DeviceCoTechUserGuid = deviceTechGuid;
                    patientRecord.DeviceCoTechLastUpdateDate = DateTime.Now;

                    db.SaveChanges();
                    retVal = true;
                }
                
            }
            catch (Exception e)
            {
                // if you are here, probably because the x= value is not a guid
                Console.WriteLine(e);
                //throw;
            }

            return retVal;
        }

        public static bool SaveDonePage(SavingDone savingDone, string userGuid)
        {
            var db = new DB_SmartPraxisEntities();

            var retVal = false;
            var intVal = int.Parse(savingDone.PageNumber);
            var ListAnswers = (from d in db.ListAnswers
                where d.AnswerPage == intVal
                               select d).FirstOrDefault();
            if (ListAnswers != null)
            {
                var patient = new Guid(savingDone.PatientGuid);
                var patientRecord = (from d in db.DataPatientRequests
                    where d.PatientGuid == patient
                    select d).FirstOrDefault();

                if (patientRecord != null)
                {
                    var deviceTechGuid = new Guid(savingDone.DeviceCoTechUserGuidAtTimeOfProcedure);
                    patientRecord.DeviceCoDeviceModelSN = savingDone.DeviceCoDeviceModelSN;
                    patientRecord.DeviceCoPresentingMode = savingDone.DeviceCoPresentingMode;
                    patientRecord.DeviceCoCommentsAtTimeOfProcedure = savingDone.DeviceCoCommentsAtTimeOfProcedure;
                    patientRecord.DeviceCoRepAtTimeOfProcedure = savingDone.DeviceCoRepAtTimeOfProcedure;
                    patientRecord.DeviceCoTechUserGuidAtTimeOfProcedure = deviceTechGuid;
                    patientRecord.MRIScanDatePerformed = savingDone.MRIScanDatePerformed;
                    patientRecord.MRIScanTimePerformed = savingDone.MRIScanTimePerformed;
                    patientRecord.DeviceTechPickedSafeMode = savingDone.DeviceTechPickedSafeMode;
                    patientRecord.Completed = true;
                    patientRecord.PatientStatusDDL = "Done";
                    patientRecord.OverAllStatus = "Done";
                    patientRecord.DeviceCoTechLastUpdateDate = DateTime.Now;
                    patientRecord.DeviceCompanyLastStatus = "Done";
                    patientRecord.BluePageAnswer = ListAnswers.AnswerText;
                    patientRecord.BluePageAnswerPageNo = intVal;
                    db.SaveChanges();
                    retVal = true;
                }
            }
            

            return retVal;
        }

        public class SavingDone
        {
            public string PageNumber { get; set; }
            public string PatientGuid { get; set; }
            public string DeviceCoDeviceModelSN { get; set; }
            public string DeviceCoPresentingMode { get; set; }
            public string DeviceCoSelectedSafeMode { get; set; }  // this is selected on blue screens
            public string DeviceCoCommentsAtTimeOfProcedure { get; set; }
            public string DeviceCoRepAtTimeOfProcedure { get; set; }
            public string DeviceCoTechUserGuidAtTimeOfProcedure { get; set; }
            public DateTime MRIScanDatePerformed { get; set; }
            public string MRIScanTimePerformed { get; set; }
            public string DeviceTechPickedSafeMode { get; set; }  // this is the selected drop down by the device tech

        }
    }
}