﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using SmartPraxis.Models;

namespace SmartPraxis.Reusable
{
    public class AsyncGetDataForCache
    {
        private DateTime CacheExpirationDateTime
        {
            get
            {
                return DateTime.Today.AddDays(144).AddHours(0);
            }
        }

        // study: consider using PLINQ

        #region "For Office"

        public async Task PopulateContactLogStatusList()
        {
            if (HttpContext.Current.Cache["ContactLogStatusList"] == null)
            {
                using (var db = new DB_SmartPraxisEntities())
                {
                    var contactLogStatusList = (from d in db.ListContactLogStatus
                                            orderby d.OrderId
                                            select d);

                    HttpContext.Current.Cache.Add(
                        "ContactLogStatusList",
                        contactLogStatusList.ToList(),
                        null,
                        this.CacheExpirationDateTime,
                        Cache.NoSlidingExpiration,
                        CacheItemPriority.AboveNormal,
                        null);
                }
            }
        }

        #endregion  

        public async Task PopulateDataForWarehouseDock()
        {
            //await this.PopulatePackageTypeList();
            //await this.PopulateExceptionTypeList();
            //await this.PopulateDocumentTypeList();
        }

        public async Task PopulateDataForOffice()
        {
            await this.PopulateContactLogStatusList();
        }
    }
}