﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SmartPraxis.Repositories;

namespace SmartPraxis.Reusable
{
    public class ControlSaver
    {
        // this is the values from the screen when split
        public class SplitObj
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public void SaveControls(ref object destination, List<string> arr)
        {
            var source = new List<SplitObj>();
            foreach (var item in arr)
            {
                try
                {
                    if (!item.ToLower().Contains("undefined") && !item.ToLower().Contains("grid") && !item.ToLower().Contains("checkall"))
                    {
                        var line = item;
                        if (item.Contains("ctl"))
                        {
                            var div = item.IndexOf('_');
                            line = item.Substring(div+1);
                        }
                        var text = line.Split('_')[1].Split(':')[0];
                        var value = line.Split('_')[1].Split(':')[1];
                        var s = new SplitObj { name = text, value = value };

                        source.Add(s);
                    }
                }
                catch (Exception ex)
                {
                    Debugger.Break();
                }
            }

            // --------------------------------------------------------------
            // uses reflection to push the values into the db object
            // using this will allow us to add many fields on the screen 
            // without having to update the code
            var destinationProperties = destination.GetType().GetProperties();
            foreach (var destinationProperty in destinationProperties)
            {
                foreach (var s in source)
                {
                    try
                    {
                        //if (destinationProperty.Name.Contains("Important") && s.name.Contains("Important"))
                        //{
                        //    Debugger.Break();
                        //}

                        if (destinationProperty.Name == s.name && destinationProperty.Name.ToLower().Contains("guid") &&
                            s.value != null)
                        {
                            var guidItem = new Guid(s.value);
                            destinationProperty.SetValue(destination, guidItem);
                        }
                        else if (destinationProperty.Name == s.name && destinationProperty.Name.ToLower().Contains("id") &&
                            Utility.IsNumeric(s.value) && s.value != null)
                        {
                            var svalue = s.value == string.Empty
                                ? 0
                                : int.Parse(s.value.Replace(" - Error", string.Empty));
                            destinationProperty.SetValue(destination, svalue);
                        }
                        else if (destinationProperty.Name == s.name && s.value != null &&
                                 !s.name.ToLower().Contains("updatedate") && !Utility.IsDate(s.value))
                        {
                            if (destinationProperty.PropertyType.ToString().Contains("Boolean"))
                            {
                                if (s.value == "on")
                                {
                                    destinationProperty.SetValue(destination, Convert.ToBoolean(1));
                                }
                                if (s.value == "off")
                                {
                                    destinationProperty.SetValue(destination, Convert.ToBoolean(0));
                                }
                                if (s.value == "True")
                                {
                                    destinationProperty.SetValue(destination, Convert.ToBoolean(1));
                                }
                            }
                            else if (destinationProperty.PropertyType.ToString().Contains("DateTime"))
                            {
                                destinationProperty.SetValue(destination, DateTime.Parse(s.value.Replace("~", ":")));
                            }
                            else if (destinationProperty.PropertyType.ToString().Contains("Time"))
                            {
                                var thistime = s.value.Replace("~", ":");
                                destinationProperty.SetValue(destination, thistime);
                            }
                            else
                            {
                                if (destinationProperty.PropertyType.ToString().Contains("Int") &&
                                    s.value != string.Empty)
                                {
                                    destinationProperty.SetValue(destination, int.Parse(s.value));
                                }
                                else if (!destinationProperty.PropertyType.ToString().Contains("Int") &&
                                         s.value != string.Empty)
                                {
                                    //destinationProperty.SetValue(destination, s.value);
                                    destinationProperty.SetValue(destination, s.value.Replace("~", ":"));
                                }
                            }
                        }
                        else if (destinationProperty.Name == s.name && s.value != null && Utility.IsDate(s.value))
                        {
                            destinationProperty.SetValue(destination, Convert.ToDateTime(s.value));
                        }
                    }
                    catch (Exception ex)
                    {
                       // Debugger.Break();
                    }
                }
            }
        }

        public string GetIdFromControls(ref object destination, List<string> arr, ref string filedname)
        {
            var id = string.Empty;
            var source = new List<SplitObj>();
            foreach (var item in arr)
            {
                try
                {
                    if (!item.ToLower().Contains("undefined") && !item.ToLower().Contains("grid") && !item.ToLower().Contains("checkall"))
                    {
                        var line = item;
                        if (item.Contains("ctl"))
                        {
                            var div = item.IndexOf('_');
                            line = item.Substring(div+1);
                        }
                        var text = line.Split('_')[1].Split(':')[0];
                        var value = line.Split('_')[1].Split(':')[1].Replace(" - Error", string.Empty);
                        var s = new SplitObj { name = text, value = value };
                        source.Add(s);
                    }
                }
                catch (Exception ex)
                {
                    Debugger.Break();
                }
            }

            // --------------------------------------------------------------
            // uses reflection to push the values into the db object
            // using this will allow us to add many fields on the screen 
            // without having to update the code
            var destinationProperties = destination.GetType().GetProperties();
            var retval = string.Empty;
            foreach (var destinationProperty in destinationProperties)
            {
                foreach (var s in source)
                {
                    try
                    {
                        if (destinationProperty.Name == s.name && destinationProperty.Name.ToLower().Contains("guid") && !destinationProperty.Name.ToLower().Contains("mrid"))
                        {
                            retval = s.value;
                            filedname = destinationProperty.Name;
                            return retval;
                        }
                        if (destinationProperty.Name == s.name && destinationProperty.Name.ToLower().Contains("id") && !destinationProperty.Name.ToLower().Contains("mrid"))
                        {
                            retval = s.value;
                            filedname = destinationProperty.Name;
                            return retval;
                        }
                    }
                    catch (Exception ex)
                    {
                        Debugger.Break();
                    }
                    
                }
            }

            return retval;
        }

        public string GetValueFromControls(ref object destination, List<string> arr, ref string filedname, string field)
        {
            var id = string.Empty;
            var source = new List<SplitObj>();
            foreach (var item in arr)
            {
                try
                {
                    if (!item.ToLower().Contains("undefined") && !item.ToLower().Contains("checkall"))
                    {
                        var text = item.Split('_')[1].Split(':')[0];
                        if (field == text)
                        {
                            id = item.Split('_')[1].Split(':')[1].Replace(" - Error", string.Empty);
                            //var s = new SplitObj { name = text, value = value };
                            //source.Add(s);
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debugger.Break();
                }
            }

            return id;
        }

        public int GetIdFromControls2(ref object destination, List<string> arr, string filedname)
        {

            var id = -1;
            var source = new List<SplitObj>();
            foreach (var item in arr)
            {
                try
                {
                    if (!item.ToLower().Contains("undefined") && !item.ToLower().Contains("grid") && !item.ToLower().Contains("checkall"))
                    {
                        var text = item.Split('_')[1].Split(':')[0];
                        var value = item.Split('_')[1].Split(':')[1].Replace(" - Error", string.Empty);
                        var s = new SplitObj { name = text, value = value };
                        source.Add(s);
                    }
                }
                catch (Exception ex)
                {
                    Debugger.Break();
                }
            }

            // --------------------------------------------------------------
            // uses reflection to push the values into the db object
            // using this will allow us to add many fields on the screen 
            // without having to update the code
            var destinationProperties = destination.GetType().GetProperties();
            foreach (var destinationProperty in destinationProperties)
            {
                foreach (var s in source)
                {
                    try
                    {
                        if (destinationProperty.Name == s.name && destinationProperty.Name.ToLower().Contains("id") && destinationProperty.Name != filedname && !destinationProperty.Name.ToLower().Contains("guid"))
                        {
                            id = s.value == string.Empty ? 0 : int.Parse(s.value);
                            break;
                        }

                        if (id != -1) { break; }
                    }
                    catch (Exception ex)
                    {
                        Debugger.Break();
                    }
                }
            }

            return id;
        }
    }
}