﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarMRICompany.ascx.cs" 
    Inherits="SmartPraxis.Shared.Sections.SidebarMRICompany" %>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav wiwet-navigation">
        <li class="sidebar-brand hidden-xs">
            <a href="/">
                Smart-Praxis
            </a>
        </li>
        <li style="display: none;">
            <a href="/Pages/MRICompany/Dashboard.aspx"><i class="fa fa-home"></i> Dashboard</a>
        </li>
        <li>
            <a href="/Pages/Account/AccountList.aspx?x=<% =this.Session["AccountGuid"].ToString() %>">Our Company Info</a>
        </li>
        <li>
            <a href="/Pages/Account/UserList.aspx?x=<% =this.Session["AccountGuid"].ToString() %>">Our Team</a>
        </li>  
        <li>
            <a href="/Pages/MRICompany/PatientList.aspx?x=<% =this.Session["AccountGuid"].ToString() %>">Requests</a>
        </li>  
        <li>
            <a href="/Pages/Account/ShowKey.aspx?x=<% =this.Session["AccountGuid"].ToString() %>">Show New User Key</a>
        </li> 
        
        <%--<li class="ui-components">
            <a role="button" id="reports" data-toggle="collapse" href="#collapseUI" aria-expanded="false" aria-controls="collapseUI">
                <i class="fa fa-trello"></i> Reports
            </a>
            <div class="collapse" id="collapseUI">
                <ul>
                <li><a href="/Pages/Typography.aspx">Filler Page 1</a></li>
                <li><a href="/Pages/BlankPage.aspx">Filler Page 2</a></li>
                <li><a href="/Pages/Icons.aspx">Icons</a></li>
                </ul>
            </div>
        </li>
        <li class="ui-components">
            <a role="button" id="setup" data-toggle="collapse" href="#collapseUI2" aria-expanded="false" aria-controls="collapseUI">
                <i class="fa fa-male"></i> Setup
            </a>
            <div class="collapse" id="collapseUI2">
                <ul>
                <li><a href="/Pages/Office/AccountList.aspx">Accounts List</a></li>
                <li><a href="/Pages/Office/UserList.aspx">User List</a></li>
                <li><a href="/Pages/Graphs.aspx">Oh! Charts</a></li>
                </ul>
            </div>
        </li>--%>
       <%-- <li class="ui-components">
            <a role="button" data-toggle="collapse" href="#collapseUI" aria-expanded="false" aria-controls="collapseUI">
                <i class="fa fa-trello"></i> UI Components
            </a>
            <div class="collapse" id="collapseUI">
                <ul>
                <li><a href="/Pages/Icons.aspx">Icons</a></li>
                <li><a href="/Pages/Graphs.aspx">Graphs</a></li>
                <li><a href="/Pages/Typography.aspx">Typography</a></li>
                </ul>
            </div>
        </li>--%>
    </ul>
</div>