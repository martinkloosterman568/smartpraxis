﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarMRITech.ascx.cs" 
    Inherits="SmartPraxis.Shared.Sections.SidebarMRITech" %>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav wiwet-navigation">
        <li class="sidebar-brand hidden-xs">
            <a href="/">
                Smart-Praxis
            </a>
        </li>
        <li style="display: none;">
            <a href="/Pages/MRITech/Dashboard.aspx"><i class="fa fa-home"></i> Dashboard</a>
        </li>
        <li>
            <a href="/Pages/MRICompany/PatientList.aspx?x=<% =this.Session["AccountGuid"].ToString() %>"><i class="fa fa-book"></i> Requests</a>
        </li>
         <li>
            <a href="/Pages/Account/UserList.aspx?x=<% =this.Session["AccountGuid"].ToString() %>"><i class="fa fa-user-md"></i> My Profile</a>
        </li>    
    </ul>
</div>