﻿using System;
using System.Web.UI;

namespace SmartPraxis.Shared.Sections
{
    public partial class HeaderOffice : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["UserName"] == null)
            {
                this.Session["UserName"] = string.Empty;
            }

            this.SetMobilePageTitle();

            if (!this.IsPostBack)
            {
                //this.LoadDDLUsersImpersonate();
            }
        }

        public void SetMobilePageTitle()
        {
            //if (this.Session["MobilePageTitle"] != null)
            //{
            //    this.lblMobilePageTitle.Text = this.Session["MobilePageTitle"].ToString();
            //}
        }

        //public void LoadDDLUsersImpersonate()
        //{
        //    const string tableName = "DataUserMast"; // <-- put table name here
        //    const string fieldName = "RoleId"; // <-- put the field name here
        //    string outError;
        //    var id = "1";  // Role 1 = Office Staff

        //    var repository = new UserListRepository(new DB_SmartPraxisEntities());
        //    var destination = repository.GetAllByRoleId(tableName, fieldName, id, out outError);

        //    this.ddlUserImpersonate.DataSource = destination;
        //    this.ddlUserImpersonate.DataValueField = "UserGuid";
        //    this.ddlUserImpersonate.DataTextField = "FullName";
        //    this.ddlUserImpersonate.DataBind();

        //    var httpSessionState = this.Session;
        //    if (httpSessionState != null && this.Session["UserNameImpersonate"] != null)
        //    {
        //        var impersonate = this.Session["UserNameImpersonate"].ToString();
        //        this.ddlUserImpersonate.SelectedIndex = this.ddlUserImpersonate.Items.IndexOf(this.ddlUserImpersonate.Items.FindByText(impersonate));
        //    }
        //}

        protected void ddlUserImpersonate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var myListItem = new ListItem
            //{
            //    Text = this.ddlUserImpersonate.SelectedItem.Text,
            //    Value = this.ddlUserImpersonate.SelectedItem.Value
            //};

            //this.Session["UserNameImpersonate"] = this.ddlUserImpersonate.SelectedItem.Text;
            //this.Session["UserGuid"] = this.ddlUserImpersonate.SelectedItem.Value;

            this.Response.Redirect("/Pages/Office/Dashboard.aspx");
        }
    }
}