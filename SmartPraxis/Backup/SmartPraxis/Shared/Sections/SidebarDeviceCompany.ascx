﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarDeviceCompany.ascx.cs" Inherits="SmartPraxis.Shared.Sections.SidebarDeviceCompany" %>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav wiwet-navigation">
        <li class="sidebar-brand hidden-xs">
            <a href="/">
                Smart-Praxis
            </a>
        </li>
        <li style="display: none;">
            <a href="/Pages/DeviceCompany/Dashboard.aspx"><i class="fa fa-home"></i> Dashboard</a>
        </li>
        <li>
            <a href="/Pages/Account/AccountList.aspx?x=<% =this.Session["AccountGuid"].ToString() %>">Our Company</a>
        </li>
        <li>
            <a href="/Pages/Account/UserList.aspx?x=<% =this.Session["AccountGuid"].ToString() %>">Our Team</a>
        </li>  

        <% var width = this.Session["ScreenWidth"].ToString();
           if (Convert.ToInt32(width) < 1366)
           { %>
            <li><a href="/Pages/DeviceCoTech/MobileList.aspx">Work List</a></li>
        <% }
        else
        { %>
            <li><a href="/Pages/DeviceCompany/PatientList.aspx">Work List</a></li>
        <% } %>
        <li>
            <a href="/Pages/Account/ShowKey.aspx?x=<% =this.Session["AccountGuid"].ToString() %>">Show New User Key</a>
        </li> 
    </ul>
</div>