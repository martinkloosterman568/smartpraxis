﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarDeviceCoTech.ascx.cs" 
    Inherits="SmartPraxis.Shared.Sections.SidebarDeviceCoTech" %>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav wiwet-navigation">
        <li class="sidebar-brand hidden-xs">
            <a href="/">
                Smart-Praxis
            </a>
        </li>
        <li class="ui-components">
            <a role="button" id="setup" data-toggle="collapse" href="#collapseUI2" aria-expanded="false" aria-controls="collapseUI2">
                <i class="fa fa-male"></i> Menu
            </a>
            <div class="collapse" id="collapseUI2">
                <ul>
                    <li style="display: none;"><a href="/Pages/DeviceCoTech/Dashboard.aspx">Dashboard</a></li>
                    
                    <% var width = this.Session["ScreenWidth"].ToString();
                       if (Convert.ToInt32(width) < 1366)
                       { %>
                    <li><a href="/Pages/DeviceCoTech/MobileList.aspx">Work List</a></li>
                    <% }
                       else
                       { %>
                        <li><a href="/Pages/DeviceCompany/PatientList.aspx">Work List</a></li>
                    <% } %>
                    
                    <% var width2 = this.Session["ScreenWidth"].ToString();
                       if (Convert.ToInt32(width2) < 1366)
                       { %>
                        <li><a href="/Pages/DeviceCoTech/MobileUserList.aspx">My Profile</a></li>
                    <% }
                       else
                       { %>
                        <li><a href="/Pages/Account/UserList.aspx">My Profile</a></li>
                    <% } %>
                    
                </ul>
            </div>
        </li>
    </ul>
</div>