﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarSuperAdmin.ascx.cs" Inherits="SmartPraxis.Shared.Sections.SidebarSuperAdmin" %>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav wiwet-navigation">
        <li class="sidebar-brand hidden-xs">
            <a href="/">
                Smart-Praxis
            </a>
        </li>
        <li style="display: none;">
            <a href="/Pages/SuperAdmin/Dashboard.aspx"><i class="fa fa-home"></i> Dashboard</a>
        </li>
        <br/><br />
        <li>
            <a href="/Pages/Account/AccountList.aspx"><i class="fa fa-users"></i> Account List</a>
        </li>
        <li>
            <a href="/Pages/SuperAdmin/GeographyRegionsList.aspx"><i class="fa fa-globe"></i> Geography Regions List</a>
        </li> 
        <li>
            <a href="/Pages/Account/UserList.aspx"><i class="fa fa-male"></i> User List</a>
        </li>  
        <br /><br />
        <li>
            <a href="/Pages/SuperAdmin/SetNotify.aspx"><i class="fa fa-comments"></i> Set Notify</a>
        </li> 
    </ul>
</div>