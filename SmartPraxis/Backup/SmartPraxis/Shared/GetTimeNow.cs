﻿using System;

namespace SmartPraxis.Shared
{
    public static class GetTimeNow
    {
        public static DateTime GetTime()
        {
            var datetimenow = new DateTime();
#if DEBUG
            datetimenow = DateTime.Now;
#else
            datetimenow = DateTime.Now.AddHours(3);
#endif
            return datetimenow;
        }
    }
}