﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartPraxis.Models;

namespace SmartPraxis.Repositories
{
    public class GeographyRegionsRepository : Repository<DataGeographyRegion>
    {
        public DB_SmartPraxisEntities _context { get; set; }

        public GeographyRegionsRepository(DB_SmartPraxisEntities context) : base(context)
        {
            try
            {
                this._context = context;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Save()
        {
            try
            {
                this._context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                var t = this.GetById(id);
                if (t != null)
                {
                    this._context.DataGeographyRegions.Remove(t);
                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(DataGeographyRegion a)
        {
            try
            {
                var t = this.GetById(a.GeographyId);
                if (t != null)
                {
                    var util = new Utility();
                    util.CopyPropertyValues(a, t);

                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DataGeographyRegion> FindByLikeAll(string table, string column, string value)
        {
            try
            {
                value = !Utility.IsNumeric(value) ? $"'%{value}%'" : $"%{value}%";
                var str = $"Select * from {table} where {column} like {value}";
                return this._context.DataGeographyRegions.SqlQuery(str).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataGeographyRegion FindByLikeSingle(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                value = !Utility.IsNumeric(value) ? string.Format("'%{}%'", value) : $"%{value}%";
                var str = $"Select * from {table} where {column} like {value}";
                var result = this._context.DataGeographyRegions.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() > 1)
                {
                    outError = $"Error: Qty: {result.Count()} - Multiples Found";
                }
                else if (result.Count() == 1)
                {
                    return result.FirstOrDefault();
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataGeographyRegion FindById(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                var str = $"Select * from {table} where {column} = '{value}'";
                var result = this._context.DataGeographyRegions.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() > 1)
                {
                    outError = $"Error: Qty: {result.Count()} - Multiples Found";
                }
                else if (result.Count() == 1)
                {
                    return result.FirstOrDefault();
                }
            }
            finally
            {
            }

            return null;
        }

        public List<DataGeographyRegion> FindAllByFileControlGuid(string table, string column, string value, out string outError)
        {
            outError = string.Empty;
            
            try
            {
                if (value != null)
                {
                    var str = $"Select * from {table} where {column} = '{value}'";
                    var result = this._context.DataGeographyRegions.SqlQuery(str);
                    if (!result.Any())
                    {
                        outError = "Error: Not Found";
                    }
                    else if (result.Count() > 1)
                    {
                        outError = $"Error: Qty: {result.Count()} - Multiples Found";
                    }
                    else if (result.Count() == 1)
                    {
                        return result.ToList();
                    }
                }
            }
            finally
            {
            }

            return null;
        }

        public List<DataGeographyRegion> FindAllByGuid(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                if (value != null)
                {
                    var str = $"Select * from {table} where {column} = '{value}'";
                    var result = this._context.DataGeographyRegions.SqlQuery(str);
                    if (!result.Any())
                    {
                        outError = "Error: Not Found";
                    }
                    else if (result.Any())
                    {
                        return result.ToList();
                    }
                }
            }
            finally
            {
            }

            return null;
        }
    }
}