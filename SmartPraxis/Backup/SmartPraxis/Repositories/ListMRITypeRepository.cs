﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartPraxis.Models;

namespace SmartPraxis.Repositories
{
    public class ListMRITypeRepository : Repository<ListMRIType>
    {
        public DB_SmartPraxisEntities _context { get; set; }

        public ListMRITypeRepository(DB_SmartPraxisEntities context) : base(context)
        {
            try
            {
                this._context = context;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Save()
        {
            try
            {
                this._context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                var t = this.GetById(id);
                if (t != null)
                {
                    this._context.ListMRITypes.Remove(t);
                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(ListStatu a)
        {
            try
            {
                var t = this.GetById(a.StatusID);
                if (t != null)
                {
                    var util = new Utility();
                    util.CopyPropertyValues(a, t);

                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DataUserMast> FindByLikeAll(string table, string column, string value)
        {
            try
            {
                value = !Utility.IsNumeric(value) ? string.Format("'%{0}%'", value) : string.Format("%{0}%", value);
                var str = string.Format("Select * from {0} where {1} like {2}", table, column, value);
                return this._context.DataUserMasts.SqlQuery(str).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataUserMast FindByLikeSingle(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                value = !Utility.IsNumeric(value) ? string.Format("'%{0}%'", value) : string.Format("%{0}%", value);
                var str = string.Format("Select * from {0} where {1} like {2}", table, column, value);
                var result = this._context.DataUserMasts.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() > 1)
                {
                    outError = string.Format("Error: Qty: {0} - Multiples Found", result.Count());
                }
                else if (result.Count() == 1)
                {
                    return result.FirstOrDefault();
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataUserMast FindById(string table, string column, int value, out string outError)
        {
            outError = string.Empty;

            try
            {
                var str = string.Format("Select * from {0} where {1} = {2}", table, column, value);
                var result = this._context.DataUserMasts.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() > 1)
                {
                    outError = string.Format("Error: Qty: {0} - Multiples Found", result.Count());
                }
                else if (result.Count() == 1)
                {
                    return result.FirstOrDefault();
                }
            }
            finally
            {
            }

            return null;
        }

        public List<DataUserMast> FindAllByUserGuid(string table, string column, string value, out string outError)
        {
            outError = string.Empty;
            if ((table == null) || (column == null) || (value == null))
            {
                return null;
            }

            try
            {
                var str = string.Format("Select * from {0} where {1} = '{2}'", table, column, value);
                var result = this._context.DataUserMasts.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() >= 1)
                {
                    return result.ToList();
                }
            }
            finally
            {
            }

            return null;
        }

        public List<DataUserMast> GetAllByRoleAndAccountId(string table, string column, string value, string column2, string value2, out string outError)
        {
            outError = string.Empty;
            /* ALERT - in this case, this is used for Patient.aspx - for MRIDoctors Dropdown list */
            try
            {
                var str = string.Format("Select * from {0} where {1} like '%{2}%' and {3} = '{4}' order by FullName", table, column, value, column2, value2);
                var result = this._context.DataUserMasts.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() >= 1)
                {
                    return result.ToList();
                }
            }
            finally
            {
            }

            return null;
        }
    }
}