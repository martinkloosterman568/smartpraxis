﻿using System;
using System.Linq;
using SmartPraxis.Models;

namespace SmartPraxis
{
    public partial class Terms : System.Web.UI.Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ShowPdf1.FilePath = "~/Content/TermsConditions/TERMS.pdf";
        }

        protected void AcceptTerms_Click(object sender, EventArgs e)
        {
            var userGuid = this.Session["UserGuid"].ToString();
            var updateTerms = (from d in this.db.DataUserMasts where d.UserGuid.ToString() == userGuid select d).FirstOrDefault();
            if (updateTerms != null)
            {
                // you found a record, now update it
                updateTerms.IsTermsAccepted = true;
                updateTerms.TermsAcceptedDateTime = DateTime.Now;
                this.db.SaveChanges();
            }

            this.Response.Redirect("~/Pages/Account/Notify.aspx");
        }
    }
}
