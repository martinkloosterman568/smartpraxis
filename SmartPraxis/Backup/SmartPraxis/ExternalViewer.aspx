﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExternalViewer.aspx.cs" Inherits="SmartPraxis.ExternalViewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDF Viewer</title>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" style="font-family: Calibri" bgcolor="#cccccc">
<form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server" BackColor="LightSlateGray" BorderStyle="Outset" BorderWidth="2px"
                   Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" ForeColor="White" Height="80px"
                   Style="z-index: 100; left: 0px; position: absolute; top: 0px" Width="100%">
            <br />
            &nbsp;&nbsp; External Page Viewer<br />
            &nbsp;&nbsp;
        </asp:Panel>
        &nbsp;

        <iframe src="https://www.w3schools.com"></iframe>

    
    </div>
</form>
</body>
</html>




