﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="SmartPraxis.Index"
    MasterPageFile="~/Shared/MasterPages/SiteBasicLayoutSignIn.Master" Title="Sign In | Smart-Praxis" %>

<asp:Content ID="HeadCurrentPage" ContentPlaceHolderID="head" runat="server" >
    <%--You can add your custom code for each page header in this section.--%>
</asp:Content>
<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page in this section.--%>
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <script type="text/javascript">
        function GetTimeZoneOffset() {
            var d = new Date();
            var gmtOffSet = -d.getTimezoneOffset();
            var gmtHours = Math.floor(gmtOffSet / 60);
            var GMTMin = Math.abs(gmtOffSet % 60);
            var dot = ".";
            var retVal = "" + gmtHours + dot + GMTMin;
            document.getElementById('<%= this.screenSize.ClientID%>').value = screen.width + ';' + screen.height;
        }
    </script>
    <asp:HiddenField ID="screenWidth" runat="server" />
    <asp:HiddenField ID="screenHeight" runat="server" />
    <div style="width: 500px;">
        <img width="100%" src="/Content/Images/SmartPraxisLogoLg.png"/>
    </div>
    <div class="row">
    </div>
    <% for (var i = 0; i < 3; i++)
       { %><br/><% } %>
    <div class="row">
        <div class="col-md-4">
            <div id="ExternalMessage" runat="server" visible="false" class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    ×</button>
                <asp:Literal ID="LiteralExternalMessage" runat="server" />
            </div>            
        </div>
    </div>
    
    <div id="divlogin" runat="server" class="index-sign-box">
        <h3>Login</h3>
        <asp:TextBox ID="txtUsername" placeholder="User Name" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPassword" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>
        
        <div style="display:none">
            <asp:TextBox BorderColor="white" BackColor="yellow"  ID="screenSize" placeholder="screenSize" runat="server"></asp:TextBox>    
        </div>
        
        <div class="text-right">
            <asp:Button ID="ButtonSIGNIN" runat="server" Text="SIGN IN" OnClick="ButtonSIGNIN_Click1" />
        </div>
        <div class="register-forgot">
            <a href="/Pages/Account/SignUp.aspx">Register&nbsp; </a>
            |
            <a href="/Pages/Account/ForgotPassword.aspx">Forgot password?</a>
        </div>
        <p style="font-size: 8pt;" align="center">version: 2018-03-14 rev - 1.0</p>
    </div>
    <br/><br/>
        <div id="error_block" runat="server" visible="false" class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            ×</button>
        <asp:Literal ID="ltrError" runat="server" />
         
    </div>
</asp:Content>
<asp:Content ID="JavaScriptCurrentPage" ContentPlaceHolderID="JavaScriptPage" runat="server">
    <%--You can add your custom JavaScript for each page in this section.--%>
</asp:Content>