﻿using System;
using System.Diagnostics;

namespace SmartPraxis
{
    public partial class PdfViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var file = this.Request.QueryString["y"];
            if (!string.IsNullOrEmpty(file))
            {
                this.ShowPdf1.FilePath = "~/Documents/" + file;
            }
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            Debugger.Break();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (this.Session["BackButton"] != null)
            {
                this.Response.Redirect(this.Session["BackButton"].ToString());
            }
        }
    }
}
