﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using SmartPraxis.Helper;
using SmartPraxis.Models;

namespace SmartPraxis
{
    public partial class Index : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:alert(screen.width); ", true);
            this.ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:GetTimeZoneOffset(); ", true);
            //

            if (!this.IsPostBack)
            {
                using (var db = new DB_SmartPraxisEntities())
                {
                    var query2 = (from d in db.DataSettings
                        select d).FirstOrDefault();
                    if (query2 != null)
                    {
                        this.Session["PathName"] = query2.PathName;
                        this.Session["BackgroundColor"] = query2.BackgroundColor;
                        this.Session["PostmarkAuthenticationKey"] = query2.PostmarkAuthenticationKey;
                        this.Session["InternalMessage"] = query2.InternalMessage;
                        this.Session["ExternalMessage"] = query2.ExternalMessage;
                        this.Session["InternalMsgEnabled"] = query2.InternalMsgEnabled;
                        this.Session["ExternalMsgEnabled"] = query2.ExternalMsgEnabled;

                        if (query2.ExternalMsgEnabled.ToString() == "True")
                        {
                            this.ExternalMessage.Visible = true;
                            this.LiteralExternalMessage.Text = query2.ExternalMessage;
                        }
                    }
                }
            }
        }

        protected void ButtonSIGNIN_Click1(object sender, EventArgs e)
        {
            var dHelper = new GeneralHelper();
            var password = dHelper.Encrypt(this.txtPassword.Text);
            var username = this.txtUsername.Text;
            var found = false;

            if (string.IsNullOrEmpty(this.txtUsername.Text))
            {
                return;
            }

            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.DataUserMasts
                             join r in db.ListRoleMasts on d.RoleID equals r.ID
                             join a in db.DataAccounts on d.AccountGuid equals a.AccountGuid
                             where d.UserName == username &&
                             d.Password == password
                             select new
                             {
                                d.UserGuid,
                                d.AccountGuid,
                                d.ID,
                                d.UserName,
                                d.FullName,
                                RoleName = r.Name,
                                d.RoleID,
                                d.EmailAddress,
                                d.OfficePhone,
                                d.CellPhone,
                                a.KeyToAddUsers,
                                a.CompanyName,
                                a.GroupName
                             }).FirstOrDefault();

                if (query != null)
                {
                    found = true;
                    this.Session["UserGuid"] = query.UserGuid;
                    this.Session["AccountGuid"] = query.AccountGuid;
                    this.Session["CompanyName"] = query.CompanyName;
                    this.Session["UserID"] = query.ID;
                    this.Session["UserName"] = query.FullName;
                    this.Session["Role"] = query.RoleName;
                    this.Session["RoleId"] = query.RoleID;
                    this.Session["EmailAddress"] = query.EmailAddress;
                    this.Session["OfficePhone"] = query.OfficePhone;
                    this.Session["CellPhone"] = query.CellPhone;
                    this.Session["KeyToAddUsers"] = query.KeyToAddUsers;
                    this.Session["GroupName"] = query.GroupName;

                    var o = this.Session["Role"];
                    if (o != null)
                    {
                        var role = o.ToString();
                        var former = (from d in db.FormAuthorizes
                            where d.Role == role
                            select new
                            {
                                d.Role,
                                d.Path,
                                d.FormName
                            }).ToList();

                        foreach (var item in former)
                        {
                            var formitem = new FormAuthorizer
                            {
                                Role = item.Role,
                                Path = item.Path,
                                FormName = item.FormName
                            };

                            Global.formAuth.Add(formitem);
                        }
                    }

                    if (this.screenSize.Text != string.Empty && this.screenSize.Text.Contains(";"))
                    {
                        this.Session["ScreenWidth"] = this.screenSize.Text.Split(';')[0];
                        this.Session["ScreenHeight"] = this.screenSize.Text.Split(';')[1];
                    }

                    if (Utils.fBrowserIsMobile())
                    {
                        this.Session["UsingDevice"] = "Mobile";
                    }
                    else
                    {
                        this.Session["UsingDevice"] = "Desktop";
                    }

                    if (this.Session["Role"] != null)
                    {
                        this.Response.Redirect("~/Pages/Account/Notify.aspx");
                    }
                }
            }

            if (!found)
            {
                this.error_block.Visible = true;
                this.ltrError.Text = @"Invalid Username or password.";
            }
        }
    }
}

public static class Utils
{
    static Regex MobileCheck = new Regex(@"android|(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
    static Regex MobileVersionCheck = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);

    public static bool fBrowserIsMobile()
    {
        Debug.Assert(HttpContext.Current != null);

        if (HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"] != null)
        {
            var u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];

            if (u.Length < 4)
                return false;

            if (MobileCheck.IsMatch(u) || MobileVersionCheck.IsMatch(u.Substring(0, 4)))
                return true;
        }

        return false;
    }
}

