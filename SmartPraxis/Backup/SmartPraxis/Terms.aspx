﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Terms.aspx.cs" Inherits="SmartPraxis.Terms" %>

<%@ Register Assembly="PdfViewer" Namespace="PdfViewer" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDF Viewer</title>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" style="font-family: Calibri" bgcolor="#cccccc">
<form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server" BackColor="LightSlateGray" BorderStyle="Outset" BorderWidth="2px"
                   Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" ForeColor="White" Height="80px"
                   Style="z-index: 100; left: 0px; position: absolute; top: 0px" Width="100%">
            <br />
            &nbsp;&nbsp; Terms & Conditions<br />
            &nbsp;&nbsp;
            <cc1:ShowPdf runat="server" ID="ShowPdf1" BorderStyle="Inset" BorderWidth="2px" 
                         Height="856px" Style="z-index: 103; left: 25px; position: absolute; top: 128px"
                         Width="856px" />
            
            <div style="height: 910px;"></div>
            <table>
                <tr>
                    <td style="width: 600px; color:#000000">&nbsp;&nbsp;&nbsp;&nbsp; I accept the terms & condtions by using this software.</td>
                    <td><asp:Button ID="AcceptTerms" runat="server" Height="30px" Text="Accept Terms" class="btn btn-primary" TabIndex="99" OnClick="AcceptTerms_Click" /></td>
                </tr>
            </table>
        </asp:Panel>
        &nbsp;
        
    </div>
</form>
</body>
</html>
