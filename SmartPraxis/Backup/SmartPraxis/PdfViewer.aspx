﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PdfViewer.aspx.cs" Inherits="SmartPraxis.PdfViewer" %>

<%@ Register Assembly="PdfViewer" Namespace="PdfViewer" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDF Viewer</title>
    
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" style="font-family: Calibri" bgcolor="#cccccc">
<form id="form1" runat="server">
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrapWithButtons.css" />
    <div>
        <asp:Panel ID="Panel1" runat="server" BackColor="LightSlateGray" BorderStyle="Outset" BorderWidth="2px"
                   Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" ForeColor="White" Height="80px"
                   Style="z-index: 100; left: 0px; position: absolute; top: 0px" Width="100%">
            <br />
            <table>
                <tr>
                    <td style="width: 300px;">&nbsp;&nbsp; PDF Viewer</td>
                    <td><asp:Button ID="BackButton" runat="server" Height="30px" Text="< Back" class="btn btn-primary" OnClick="btnBack_Click" TabIndex="99" /></td>
                    <td><asp:Button ID="SendEmail" runat="server" Height="30px" Text="Email Copy" class="btn btn-primary" OnClick="btnSendEmail_Click" TabIndex="99" /></td>
                </tr>
            </table>
            <br/>
        </asp:Panel>
        &nbsp;
        
        

        <cc1:ShowPdf runat="server" ID="ShowPdf1" BorderStyle="Inset" BorderWidth="2px" 
                     Height="856px" Style="z-index: 103; left: 24px; position: absolute; top: 128px"
                     Width="856px" />
    
    </div>
</form>
</body>
</html>
