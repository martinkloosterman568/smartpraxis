using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
{
    private readonly DbSet<TEntity> _entities;

    protected Repository(DbContext context)
    {
        this._entities = context.Set<TEntity>();
    }

    public IEnumerable<TEntity> GetAll()
    {
        return this._entities.ToList();
    }

    public TEntity GetById(int id)
    {
        return this._entities.Find(id);
    }

    public void Add(TEntity entity)
    {
        this._entities.Add(entity);
    }

    public void Delete(TEntity entity)
    {
        this._entities.Remove(entity);
    }
}

