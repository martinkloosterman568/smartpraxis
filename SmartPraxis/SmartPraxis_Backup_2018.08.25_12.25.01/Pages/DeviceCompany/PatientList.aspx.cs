﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.DeviceCompany
{
    public partial class PatientList : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        public static bool GridRowsFound;
        private static int gridRowInt;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific to this page

            var o = this.Session["Role"];
            if (o != null && o.ToString().Contains("MRI"))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (!this.Page.IsPostBack)
            {
                this.PopulateStatusDropDown();
                //this.DropDownListStatus.SelectedIndex = this.DropDownListStatus.Items.IndexOf(this.DropDownListStatus.Items.FindByText(responsed));
            }
            
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    this.ViewState["sort"] = "MRIScanDate Desc, MRIScanTime Desc";
                    this.PopulateGrid();
                }
            }

            var o = this.Session["Role"]?.ToString();
            this.HyperLinkHome.NavigateUrl = HyperlinkHome.HomeChooser(o);
        }

        private void PopulateGrid()
        {
            var destination = this.GetDataForGrid(this.ViewState["sort"].ToString());

            this.gvGrid.DataSource = destination;
            this.gvGrid.DataBind();

            if (destination.Rows.Count > 0)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // status column
                var patient = DataBinder.Eval(e.Row.DataItem, "PatientGuid").ToString().ToLower();
                var status = DataBinder.Eval(e.Row.DataItem, "PatientStatusDDL").ToString().ToLower();
                var btnStatus = (Button)e.Row.FindControl("btnPatientStatus");
                if (status.Contains("pending"))
                {
                    btnStatus.CssClass = @"btn btn-warning";
                    btnStatus.Text = @"Pending";
                    btnStatus.Enabled = true;
                    btnStatus.Attributes.Add("Patient", patient);
                }
                else if (status.Contains("rejected"))
                {
                    btnStatus.CssClass = @"btn btn-danger";
                    btnStatus.Text = @"Rejected";
                    btnStatus.Enabled = true;
                    btnStatus.Attributes.Add("Patient", patient);
                }
                else if (status.Contains("comments"))
                {
                    btnStatus.CssClass = @"btn btn-info";
                    btnStatus.Text = @"Comments";
                    btnStatus.Enabled = false;
                }
                else if (status.Contains("accepted"))
                {
                    btnStatus.CssClass = @"btn btn-success";
                    btnStatus.Text = @"Accepted";
                    btnStatus.Enabled = true;
                    btnStatus.Attributes.Add("Patient", patient);
                }
                else if (status.Contains("done"))
                {
                    btnStatus.CssClass = @"btn btn-primary";
                    btnStatus.Text = @"Done";
                    btnStatus.Enabled = false;
                }

                // report column
                var report = DataBinder.Eval(e.Row.DataItem, "AttachedFile").ToString().ToLower();
                if (!string.IsNullOrEmpty(report))
                {
                    var document = (HyperLink)e.Row.FindControl("LinkButtonPdfIcon");
                    if (document != null) document.Visible = true;
                }
                else
                {
                    var document = (HyperLink)e.Row.FindControl("LinkButtonPdfIcon");
                    if (document != null) document.Visible = false;
                }
            }

            var deletebutton = (LinkButton)e.Row.FindControl("LinkButtonDelete");
            if (this.Session["Role"].ToString().Contains("Super Admin") ||
               this.Session["Role"].ToString().Contains("MRI Center") ||
                (this.Session["Role"].ToString().Contains("Device Co Tech") && 
                !this.Session["Role"].ToString().Contains("Device Co Tech")))
            {
                if (deletebutton != null)
                {
                    deletebutton.Visible = true;    
                }
            }
            else
            {
                if (deletebutton != null)
                {
                    deletebutton.Visible = false;
                }
            }
        }
        
        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
            this.PopulateGrid();
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }

        public DataTable GetDataForGrid(string sSort)
        {
            DataView dv = FillDataSet.FillDt("spFilterPatientList", new[]
            {
                new SqlParameter("@valuePassedIn", this.Filter.Text),
                new SqlParameter("@DataAccountIds", (string)Session["DataAccountIdsDelimited"]),
                new SqlParameter("@dropdownFilter", this.DropDownListStatus.SelectedItem.Text),
                new SqlParameter("@IsAdmin", (bool)Session["IsAdmin"])
            }).DefaultView;

            dv.Sort = sSort;
            return dv.ToTable();
        }

        protected void Filter_TextChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            this.Filter.Text = string.Empty;
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void PatientStatusDDL_Click(object sender, EventArgs e)
        {
            // this code will act as a 3 way toggle
            var btn = (Button)sender;
            var patient = string.Empty;
            var attr = btn.Attributes;
            foreach (var item in attr.Keys)
            {
                if (item.ToString() == "Patient")
                {
                    patient = attr["Patient"];
                }
            }

            if (btn.Text == @"Pending")
            {
                var update = (from d in this.db.DataPatientRequests
                              where d.PatientGuid.ToString() == patient
                              select d).FirstOrDefault();

                if (update != null)
                {
                    var status = "Accepted";
                    update.PatientStatusDDL = status;
                    update.DeviceCompanyLastStatus = status;
                    update.DeviceCoTechLastUpdateDate = GetTimeNow.GetTime();
                    update.DeviceCoTechUserGuid = new Guid(this.Session["UserGuid"].ToString());
                    update.OverAllStatus = update.DeviceCompanyLastStatus;
                    update.LastUpdatedBy = this.Session["UserName"].ToString();
                    update.LastUpdatedByGuid = update.DeviceCoTechUserGuid;
                    update.LastUpdatedDate = update.DeviceCoTechLastUpdateDate;
                    this.db.SaveChanges();
                    this.PopulateGrid();
                }
            }
            else if (btn.Text == @"Accepted")
            {
                var update = (from d in this.db.DataPatientRequests
                    where d.PatientGuid.ToString() == patient
                    select d).FirstOrDefault();

                if (update != null)
                {
                    var status = "Rejected";
                    update.PatientStatusDDL = status;
                    update.DeviceCompanyLastStatus = status;
                    update.DeviceCoTechLastUpdateDate = GetTimeNow.GetTime();
                    update.DeviceCoTechUserGuid = new Guid(this.Session["UserGuid"].ToString());
                    update.OverAllStatus = update.DeviceCompanyLastStatus;
                    update.LastUpdatedBy = this.Session["UserName"].ToString();
                    update.LastUpdatedByGuid = update.DeviceCoTechUserGuid;
                    update.LastUpdatedDate = update.DeviceCoTechLastUpdateDate;
                    this.db.SaveChanges();
                    this.PopulateGrid();
                }
            }
            else if (btn.Text == @"Rejected")
            {
                var update = (from d in this.db.DataPatientRequests
                    where d.PatientGuid.ToString() == patient
                    select d).FirstOrDefault();

                if (update != null)
                {
                    var status = "Pending";
                    update.PatientStatusDDL = status;
                    update.DeviceCompanyLastStatus = status;
                    update.DeviceCoTechLastUpdateDate = GetTimeNow.GetTime();
                    update.DeviceCoTechUserGuid = new Guid(this.Session["UserGuid"].ToString());
                    update.OverAllStatus = update.DeviceCompanyLastStatus;
                    update.LastUpdatedBy = this.Session["UserName"].ToString();
                    update.LastUpdatedByGuid = update.DeviceCoTechUserGuid;
                    update.LastUpdatedDate = update.DeviceCoTechLastUpdateDate;
                    this.db.SaveChanges();
                    this.PopulateGrid();
                }
            }
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] s = this.ViewState["sort"].ToString().Split();    //load the last sort
            string sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                    sSort += " DESC";
                else
                    sSort += " ASC";
            }
            else
                sSort += " ASC";

            //find which column is being sorted to change its style
            int i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" + sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            //get the sorted data
            var dt = this.GetDataForGrid(sSort);
            this.gvGrid.DataSource = dt;
            this.gvGrid.DataBind();

            gridRowInt = dt.Rows.Count;
            if (gridRowInt == 0)
            {
                GridRowsFound = false;
            }
            else
            {
                GridRowsFound = true;
            }

            this.gvGrid.DataSource = this.GetDataForGrid(sSort);
            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }

        public void PopulateStatusDropDown()
        {
            const string tableName = "ListStatus"; // <-- put table name here
            const string fieldName = "Role"; // <-- put the field name here
            string outError;

            var destination = this.GetPatientStatusDDL(tableName, fieldName, out outError);

            if (outError == string.Empty)
            {
                this.DropDownListStatus.DataSource = destination;
                this.DropDownListStatus.DataValueField = "StatusGuid";
                this.DropDownListStatus.DataTextField = "Description";
                this.DropDownListStatus.DataBind();

                this.DropDownListStatus.Items.Insert(0, new ListItem("- ALL -", "0"));
            }
        }

        public List<ListStatu> GetPatientStatusDDL(string table, string column, out string outError)
        {
            outError = string.Empty;

            try
            {
                var str = string.Format("Select distinct * from {0} order by Description", table);
                var result = this.db.ListStatus.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() >= 1)
                {
                    return result.ToList();
                }
            }
            finally
            {
            }

            return null;
        }

        protected void DropDownListStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ViewState["sort"] = "PatientId Desc";
            this.PopulateGrid();
        }
    }
}