﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.MRIDoctor
{
    public class Dashboard : Page
    {
        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    DataTable dt = FillDataSet.FillDt("spGetDashboardCounts", new[]
                    {
                        new SqlParameter("@DataAccountIdsDelimited", (string)Session["DataAccountIdsDelimited"])
                    });

                    foreach (DataRow row in dt.Rows)
                    {
                        var moder = row.ItemArray[0].ToString();
                        var opened = int.Parse(row.ItemArray[1].ToString());

                        switch (moder)
                        {
                            case "Accepted":
                                this.Session["Accepted"] = opened;
                                break;
                            case "PatientRequests":
                                this.Session["PatientRequests"] = opened;
                                break;
                            case "Rejected":
                                this.Session["Rejected"] = opened;
                                break;
                            case "Comments":
                                this.Session["Comments"] = opened;
                                break;
                        }
                    }
                }
            }
        }

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            var urlAbsoluteUri = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
            if (urlAbsoluteUri != null) this.Response.Redirect(urlAbsoluteUri);
        }

        protected void Patients_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Pending");
        }

        protected void Accepted_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Accepted");
        }

        protected void Rejected_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Rejected");
        }

        protected void Comments_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Comments");
        }
    }
}