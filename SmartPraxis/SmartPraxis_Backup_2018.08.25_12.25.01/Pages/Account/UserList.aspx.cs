﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.Account
{
    public partial class UserList : Page
    {
        public static bool GridRowsFound;
        private static int gridRowInt;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific


            if (this.Session["Role"]?.ToString() == Global.mriCenter)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (this.Session["Role"]?.ToString() == Global.deviceCo)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    this.ViewState["sort"] = "FullName ASC";
                    this.PopulateGrid();
                }
            }
        }

        private void PopulateGrid()
        {
            var destination = this.GetDataForGrid("FullName ASC");

            this.gvGrid.DataSource = destination;
            this.gvGrid.DataBind();

            if (destination.Rows.Count > 0)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton) sender).CommandArgument;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            this.Response.Redirect(formname);
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            var deletebutton = (LinkButton)e.Row.FindControl("LinkButtonDelete");
            if (this.Session["Role"].ToString().Contains(Global.superAdmin) ||
                this.Session["Role"].ToString().Contains(Global.mriCenter) ||
                this.Session["Role"].ToString().Contains(Global.deviceCo) && 
                !this.Session["Role"].ToString().Contains(Global.deviceCoTech))
            {
                if (deletebutton != null)
                {
                    deletebutton.Visible = true;
                }
            }
            else
            {
                if (deletebutton != null)
                {
                    deletebutton.Visible = false;
                }
            }
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("UserEdit.aspx");
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
            this.PopulateGrid();
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }

        public DataTable GetDataForGrid(string sSort)
        {
            try
            {
                SqlParameter[] dbParams = new SqlParameter[3]
                {
                    new SqlParameter("@valuePassedIn", this.Filter.Text),
                    new SqlParameter("@filter", null),
                    new SqlParameter("@typeOfUser", null)
                };

                switch (Session["Role"].ToString())
                {
                    case "Super Admin":
                        this.lblUserList.Text = "User List";

                        dbParams[1].Value = string.Empty;
                        dbParams[2].Value = "Admin";
                        break;

                    case "MRI Tech":
                    case "MRI Doctor":
                    case "Device Co Tech":
                        this.lblUserList.Text = "My Profile";

                        dbParams[1].Value = Session["UserGuid"].ToString();
                        dbParams[2].Value = "U";
                        break;

                    case "MRI Center":
                    case "Device Co":
                        this.lblUserList.Text = "Our Team";

                        dbParams[1].Value = Session["DataAccountIdsDelimited"].ToString();
                        dbParams[2].Value = "C";
                        break;
                }

                DataView dv = FillDataSet.FillDt("spFilterUserList", dbParams).DefaultView;
                dv.Sort = sSort;
                return dv.ToTable();
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                Debugger.Break();
            }

            return null;
        }

        protected void Filter_TextChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            this.Filter.Text = string.Empty;
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] s = this.ViewState["sort"].ToString().Split();    //load the last sort
            string sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                    sSort += " DESC";
                else
                    sSort += " ASC";
            }
            else
                sSort += " ASC";

            //find which column is being sorted to change its style
            int i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" + sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            //get the sorted data
            var dt = this.GetDataForGrid(sSort);
            this.gvGrid.DataSource = dt;
            this.gvGrid.DataBind();

            gridRowInt = dt.Rows.Count;
            if (gridRowInt == 0)
            {
                GridRowsFound = false;
            }
            else
            {
                GridRowsFound = true;
            }

            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }
    }
}