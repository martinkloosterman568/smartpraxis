﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Web.UI;
using Newtonsoft.Json;

namespace SmartPraxis.Pages.Account
{
    public partial class UserEdit : Page
    {
        private readonly GeneralHelper dHelper = new GeneralHelper();

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific for this form
            var responsed = string.Empty;
            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            if ((this.Session["Role"]?.ToString() == "MRI Center") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if ((this.Session["Role"]?.ToString() == "Device Co") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();
            RadScriptManager.RegisterStartupScript(this, GetType(), "Password control", $"$('#{ Password.ClientID }').attr('type', 'password'); ", true);

            if (!this.Page.IsPostBack)
            {
                PopulateGroupNames();

                if (this.Role.Items.Count == 0)
                {
                    this.PopulateRoles();
                }

                if (this.Session["Role"] != null)
                {
                    if (this.Session["Role"].ToString() == "Super Admin")
                    {
                        this.Role.Enabled = true;
                        this.UserName.Enabled = true;
                        this.divForcePassword.Style.Remove("display");
                        this.divForcePassword.Style.Add("display", "inherit");
                    }
                    else
                    {
                        this.Role.Enabled = false;
                    }
                }

                if (!IsAddAction)
                {
                    using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                    {
                        DataUserMast userForEditing = db.DataUserMasts.First(dataUserMast => dataUserMast.UserGuid.Value == UserGuid.Value);
                        this.FindOnReturnOfRedirectId(userForEditing.ID.ToString());

                        FirstName.Text = userForEditing.FirstName;
                        LastName.Text = userForEditing.LastName;
                        UserName.Text = userForEditing.UserName;
                        Password.Text = userForEditing.Password; // This value is still encrypted
                        OfficePhone.Text = userForEditing.OfficePhone;
                        CellPhone.Text = userForEditing.CellPhone;
                        EmailAddress.Text = userForEditing.EmailAddress;
                    }
                }

                PopulateAccountList();
            }
        }

        private void PopulateRoles()
        {
            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
                List<ListRoleMast> roles;
                var role = this.Session["Role"]?.ToString();
                if (this.Session["Role"]?.ToString() == "Super Admin")
                {
                    roles = (from d in db.ListRoleMasts orderby d.DisplayName select d).ToList();
                }
                else if (this.Session["Role"]?.ToString() == "MRI Center")
                {
                    roles = (from d in db.ListRoleMasts
                             where d.DisplayName == "MRI Tech" || d.DisplayName == "MRI Doctor"
                             orderby d.DisplayName
                             select d).ToList();
                }
                else if (this.Session["Role"]?.ToString() == "Device Co")
                {
                    roles = (from d in db.ListRoleMasts
                             where d.DisplayName == "Device Co Tech"
                             orderby d.DisplayName
                             select d).ToList();
                }
                else
                {
                    // device co tech
                    // mri doctor
                    // mri tech
                    roles = (from d in db.ListRoleMasts
                             where d.DisplayName == role
                             orderby d.DisplayName
                             select d).ToList();
                }

                // add from database
                this.Role.DataSource = roles;
                this.Role.DataValueField = "ID";
                this.Role.DataTextField = "DisplayName";

                // add extra row
                this.Role.Items.Insert(0, new ListItem("- Select One -", "- Select One -"));
                this.Role.DataBind();
            }
        }

        private void PopulateGroupNames()
        {
            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
                // Business Rule: Make GroupNames mandatory only for "Device Co" and "Device Co Tech" users. Hide this control for all others.
                // FYI, this 'GroupName' control is only used for filtering the 'AccountId' control.

                GroupName.DataSource = db.DataAccountGroups.ToList();
                GroupName.DataTextField = "GroupName";
                GroupName.DataValueField = "AccountGroupGuid";
                GroupName.DataBind();
                GroupName.Items.Insert(0, new ListItem("- Select One -", string.Empty));

                if (!IsAddAction)
                {
                    

                    Guid? accountGroupGuid = db.DataUserMasts
                        .Where(dataUserMast => dataUserMast.UserGuid == UserGuid.Value && (dataUserMast.Role == "Device Co" || dataUserMast.Role == "Device Co Tech"))
                        .Join(db.UserDataAccounts, dataUserMast => dataUserMast.ID, userDataAccount => userDataAccount.UserID, (dataUserMast, userDataAccount) => userDataAccount.AccountId)
                        .Join(db.DataAccounts, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => dataAccount.AccountGroupGuid)
                        .FirstOrDefault();

                    if (accountGroupGuid.HasValue)
                    {
                        GroupName.SelectedValue = accountGroupGuid.ToString();
                        GroupName.Enabled = (bool)Session["IsAdmin"];
                    }
                    else
                    {
                        GroupName.SelectedIndex = 0;
                        GroupNameContainer.Visible = false;
                    }
                }
            }
        }

        private void PopulateAccountList()
        {
            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
                IEnumerable<DataAccount> dataAccountsList = db.DataAccounts
                    .Where(dataAccount =>
                        (GroupName.SelectedIndex <= 0 || GroupName.SelectedValue == dataAccount.AccountGroupGuid.ToString()) &&   // Filter the DataAccounts by GroupName
                        (DataAccountTypeByUserRole == null || DataAccountTypeByUserRole == dataAccount.TypeOf))                   // Filter the DataAccounts by Role
                    .OrderBy(dataAccount => dataAccount.CompanyName)
                    .ToList();

                IEnumerable<int> currentDataAccountIds = !IsAddAction ?
                    db.DataUserMasts
                        .Where(dataUserMast => !UserGuid.HasValue || dataUserMast.UserGuid == UserGuid.Value)
                        .Join(db.UserDataAccounts, dataUserMast => dataUserMast.ID, userDataAccount => userDataAccount.UserID, (dataUserMast, userDataAccount) => userDataAccount.AccountId)
                        .ToList()
                        .Join(dataAccountsList, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => accountId)
                        .ToArray() :
                    new int [0];

                AccountId.ItemDataBound += (sender, args) =>
                {
                    ImageButton imageButton = (ImageButton)args.Item.FindControl("DataAccountImageButton");
                    DataAccount dataAccount = (DataAccount)args.Item.DataItem;

                    imageButton.Enabled = (bool)Session["IsAdmin"];
                    imageButton.Attributes["AccountId"] = dataAccount.AccountId.ToString();
                    imageButton.ImageUrl = currentDataAccountIds.Contains(dataAccount.AccountId) ? "~/Content/Images/CheckOnsmall.png" : "~/Content/Images/CheckOffsmall.png";
                };

                AccountId.DataSource = dataAccountsList;
                AccountId.DataBind();

                Dictionary<int, bool> isCheckedDictionary = dataAccountsList.ToDictionary(dataAccount => dataAccount.AccountId, dataAccount => currentDataAccountIds.Contains(dataAccount.AccountId));
                hfAccountIds.Value = JsonConvert.SerializeObject(isCheckedDictionary);
            }
        }

        protected void DataAccountImageButton_Click(object sender, ImageClickEventArgs e)
        {
            Dictionary<int, bool> isCheckedDictionary = JsonConvert.DeserializeObject<Dictionary<int, bool>>(hfAccountIds.Value);
            ImageButton senderImageButton = (ImageButton)sender;

            int accountId = int.Parse(senderImageButton.Attributes["AccountId"]);
            bool isChecked = isCheckedDictionary[accountId];

            senderImageButton.ImageUrl = isChecked ? "~/Content/Images/CheckOnsmall.png" : "~/Content/Images/CheckOffsmall.png";

            if (!IsMultipleDataAccountsEnabled)
            {
                // The user cannot have more than 1 DataAccount, so reset 'isCheckedDictionary' and set all other ImageButtons in the repeater to unchecked
                isCheckedDictionary.Clear();
                isCheckedDictionary.Add(accountId, isChecked);

                foreach (ImageButton imageButton in AccountId.Items.Cast<RepeaterItem>()
                    .Select(repeaterItem => (ImageButton)repeaterItem.FindControl("DataAccountImageButton"))
                    .Where(imageButton => imageButton != sender))
                {
                    imageButton.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                }
            }

            hfAccountIds.Value = JsonConvert.SerializeObject(isCheckedDictionary);
        }

        protected void GroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateAccountList();
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataUserMast"; // <-- put table name here
            const string fieldName = "ID"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
                if (destination != null)
                {
                    if (destination.IsSendEmail == true)
                    {
                        this.ImageButtonSendEmails.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                    }
                    else
                    {
                        this.ImageButtonSendEmails.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                    }

                    if (destination.IsSendText == true)
                    {
                        this.ImageButtonSendTextMessage.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                    }
                    else
                    {
                        this.ImageButtonSendTextMessage.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                    }

                    if (destination.IsForceChangePassword)
                    {
                        this.ImageButtonForcePasswordReset.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                    }
                    else
                    {
                        this.ImageButtonForcePasswordReset.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                    }
                }
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls

        // these controls are actually on the page
        protected void btnnew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/UserEdit.aspx");
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/UserList.aspx");
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }
        #endregion

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.Response.Redirect("/Pages/Account/UserList.aspx");
        }

        public bool SaveRecord()
        {
            try
            {
                using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                {
                    DataUserMast saving;
                    string temp;

                    if (IsAddAction)
                    {
                        Guid newGuid = Guid.NewGuid();
                        saving = new DataUserMast() { UserGuid = newGuid, First8UserGuid = newGuid.ToString().Substring(0, 8) };
                        db.DataUserMasts.Add(saving);
                    }
                    else
                    {
                        saving = db.DataUserMasts.First(dataUserMast => dataUserMast.UserGuid.Value == UserGuid.Value);
                    }

                    if (saving.Role != Role.SelectedItem.Text)
                    {
                        saving.Role = Role.SelectedItem.Text;
                        saving.RoleID = int.Parse(Role.SelectedValue);
                    }

                    if (saving.FirstName != FirstName.Text)
                    {
                        saving.FirstName = FirstName.Text;
                    }

                    if (saving.LastName != LastName.Text)
                    {
                        saving.LastName = LastName.Text;
                    }

                    temp = saving.FirstName + " " + saving.LastName;
                    if (saving.FullName != temp)
                    {
                        saving.FullName = temp;
                    }

                    if (saving.UserName != UserName.Text)
                    {
                        saving.UserName = UserName.Text;
                    }

                    // (1) If this is to be a new user, then Password.Text comes back to the server as plaintext.
                    // (2) If this is an existing user being edited, then Password.Text's default value is encrypted. However if Password.Text is changed then it comes back to the server as plaintext.
                    if (saving.Password != Password.Text)
                    {
                        saving.Password = dHelper.Encrypt(Password.Text);
                    }

                    if (saving.AccountGuid == null)
                    {
                        saving.AccountGuid = new Guid();
                    }

                    saving.LastUpdatePerson = this.Session["UserName"].ToString();
                    saving.LastModified = GetTimeNow.GetTime();

                    temp = Regex.Replace(CellPhone.Text, @"1?\W*(\d{3})\W*(\d{3})\W*(\d{4})", "($1) $2-$3");
                    if (saving.CellPhone != temp)
                    {
                        saving.CellPhone = temp;
                    }

                    temp = Regex.Replace(OfficePhone.Text, @"1?\W*(\d{3})\W*(\d{3})\W*(\d{4})", "($1) $2-$3");
                    if (saving.OfficePhone != temp)
                    {
                        saving.OfficePhone = temp;
                    }

                    if (saving.EmailAddress != EmailAddress.Text)
                    {
                        saving.EmailAddress = EmailAddress.Text;
                    }

                    if (saving.IsForceChangePassword != this.ImageButtonForcePasswordReset.ImageUrl.Contains("On"))
                    {
                        saving.IsForceChangePassword = this.ImageButtonForcePasswordReset.ImageUrl.Contains("On");
                    }

                    if (saving.IsSendEmail != this.ImageButtonSendEmails.ImageUrl.Contains("On"))
                    {
                        saving.IsSendEmail = this.ImageButtonSendEmails.ImageUrl.Contains("On");
                    }

                    if (saving.IsSendText != this.ImageButtonSendTextMessage.ImageUrl.Contains("On"))
                    {
                        saving.IsSendText = this.ImageButtonSendTextMessage.ImageUrl.Contains("On");
                    }

                    db.SaveChanges();

                    // Update the 'UserDataAccounts' table with the selected DataAccounts, adding and removing records as necessary. The DataUserMast record must be created by this point.

                    IEnumerable<DataAccount> currentDataAccounts = db.UserDataAccounts
                        .Where(userDataAccount => userDataAccount.UserID == saving.ID)
                        .Join(db.DataAccounts, userDataAccount => userDataAccount.AccountId, dataAccount => dataAccount.AccountId, (userDataAccount, dataAccount) => dataAccount)
                        .ToList();

                    IEnumerable<DataAccount> selectedDataAccounts = SelectedDataAccountIds
                        .Join(db.DataAccounts, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => dataAccount)
                        .ToList();

                    IEnumerable<UserDataAccount> removeTheseUserDataAccounts = db.UserDataAccounts
                        .Where(userDataAccount => userDataAccount.UserID == saving.ID)
                        .ToList()
                        .Where(userDataAccount => !SelectedDataAccountIds.Contains(userDataAccount.AccountId));

                    IEnumerable<UserDataAccount> addTheseUserDataAccounts = selectedDataAccounts
                        .Except(currentDataAccounts, new DataAccount.Comparer())
                        .Select(dataAccount => new UserDataAccount() { UserID = saving.ID, AccountId = dataAccount.AccountId });

                    db.UserDataAccounts.RemoveRange(removeTheseUserDataAccounts);
                    db.UserDataAccounts.AddRange(addTheseUserDataAccounts);
                    db.SaveChanges();

                    if (IsAddAction && !string.IsNullOrWhiteSpace(saving.CellPhone))
                    {
                        string formattedCellPhone = Regex.Match(saving.CellPhone, @"\d+").Value;
                        SendTextMsg.TextMessage(new List<string> { formattedCellPhone }, "Welcome to Smart-Praxis you can login @ http://mrisafemode.com", this.Session["CompanyName"]?.ToString());
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null && ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    //ret4 = "Error, the user name already exists";
                }

                return false;
            }
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (SaveRecord())
                {
                    this.Response.Redirect("/Pages/Account/UserList.aspx");
                }
                else
                {
                    RadScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotification('Error Saving Record')", true);
                }
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Business Rule: Make GroupNames mandatory only for "Device Co" and "Device Co Tech" users. Hide this control for all others.
            GroupNameContainer.Visible = Role.SelectedItem.Text == "Device Co" || Role.SelectedItem.Text == "Device Co Tech";

            if (!GroupNameContainer.Visible)
            {
                GroupName.SelectedIndex = 0;
            }

            PopulateAccountList();
        }

        protected void ImageButtonForcePasswordReset_Click(object sender, ImageClickEventArgs e)
        {
            if (this.ImageButtonForcePasswordReset.ImageUrl.Contains("On"))
            {
                this.ImageButtonForcePasswordReset.ImageUrl = "~/Content/Images/CheckOffsmall.png";
            }
            else
            {
                this.ImageButtonForcePasswordReset.ImageUrl = "~/Content/Images/CheckOnsmall.png";
            }
        }

        protected void ImageButtonSendTextMessage_Click(object sender, ImageClickEventArgs e)
        {
            if (this.ImageButtonSendTextMessage.ImageUrl.Contains("On"))
            {
                this.ImageButtonSendTextMessage.ImageUrl = "~/Content/Images/CheckOffsmall.png";
            }
            else
            {
                this.ImageButtonSendTextMessage.ImageUrl = "~/Content/Images/CheckOnsmall.png";
            }
        }

        protected void ImageButtonSendEmails_Click(object sender, ImageClickEventArgs e)
        {
            if (this.ImageButtonSendEmails.ImageUrl.Contains("On"))
            {
                this.ImageButtonSendEmails.ImageUrl = "~/Content/Images/CheckOffsmall.png";
            }
            else
            {
                this.ImageButtonSendEmails.ImageUrl = "~/Content/Images/CheckOnsmall.png";
            }
        }

        protected void RequiredControlValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            WebControl controlToValidate = (WebControl)Panel1.FindControl(((CustomValidator)source).ControlToValidate);

            if (controlToValidate.Enabled && controlToValidate.Visible)
            {
                if (controlToValidate is TextBox)
                {
                    string textBoxValue = ((TextBox)controlToValidate).Text;

                    if (controlToValidate == UserName)
                    {
                        if (IsAddAction)
                        {
                            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                            {
                                args.IsValid = !string.IsNullOrWhiteSpace(textBoxValue) && db.DataUserMasts.All(dataUserMast => dataUserMast.UserName != textBoxValue);
                            }
                        }
                    }
                    else if (controlToValidate == Password)
                    {
                        args.IsValid = !IsAddAction || !string.IsNullOrWhiteSpace(textBoxValue);
                    }
                    else if (controlToValidate == CellPhone || controlToValidate == OfficePhone)
                    {
                        args.IsValid = string.IsNullOrWhiteSpace(OfficePhone.Text) || Regex.IsMatch(textBoxValue, @"1?\W*\d{3}\W*\d{3}\W*\d{4}");
                    }
                    else if (controlToValidate == EmailAddress)
                    {
                        args.IsValid = new EmailAddressAttribute().IsValid(textBoxValue);
                    }
                    else if (string.IsNullOrWhiteSpace(textBoxValue))
                    {
                        args.IsValid = false;
                    }
                }
                else if (controlToValidate is DropDownList)
                {
                    args.IsValid = ((DropDownList)controlToValidate).SelectedIndex > 0;
                }
                else if (controlToValidate is RadCheckBoxList)
                {
                    args.IsValid = ((RadCheckBoxList)controlToValidate).SelectedItems.Count() > 0;
                }
                else
                {
                    throw new NotSupportedException("The server method 'RequiredControlValidator_ServerValidate' has not been configured to use this type of control.");
                }

                controlToValidate.BackColor = args.IsValid ? Color.White : Color.FromArgb(0xffdae0); // 0xffdae0 is a reddish color indicating a required field that needs to be filled in.
            }
        }

        protected void csvAccountId_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Dictionary<int, bool> isCheckedDictionary = JsonConvert.DeserializeObject<Dictionary<int, bool>>(hfAccountIds.Value);
            args.IsValid = isCheckedDictionary.Any(keyValuePair => keyValuePair.Value);

            AccountIdContainer.BackColor = args.IsValid ? Color.White : Color.FromArgb(0xffdae0); // 0xffdae0 is a reddish color indicating a required field that needs to be filled in.
        }

        private IEnumerable<int> SelectedDataAccountIds
        {
            get
            {
                Dictionary<int, bool> clickedCheckboxes = JsonConvert.DeserializeObject<Dictionary<int, bool>>(hfAccountIds.Value);

                return clickedCheckboxes
                    .Where(keyValuePair => keyValuePair.Value)
                    .Select(keyValuePair => keyValuePair.Key);
            }
        }

        private string DataAccountTypeByUserRole
        {
            get
            {
                return DataAccount.GetDataAccountTypeByUserRole(Role.SelectedItem?.Text);
            }
        }

        private bool IsAddAction
        {
            get
            {
                return string.IsNullOrWhiteSpace(Request.QueryString["id"]);
            }
        }

        private Guid? UserGuid
        {
            get
            {
                return !IsAddAction ? (Guid?)Guid.Parse(Request.QueryString["id"]) : null;
            }
        }

        private bool IsMultipleDataAccountsEnabled
        {
            get { return DataAccount.IsMultipleDataAccountsEnabled(Role.SelectedItem.Text); }
        }
    }
}
