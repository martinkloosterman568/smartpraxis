﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserEdit.aspx.cs" Inherits="SmartPraxis.Pages.Account.UserEdit"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" Title="Smart-Praxis" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50508.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page on this section.--%>
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <script type="text/javascript">
        function showNotification(message)
        {
            //$find("<%= this.RadNotification1.ClientID %>").text = message;
            $find("<%= this.RadNotification1.ClientID %>").show();
        }

        function DataAccountImageButton_OnClientClick(imgButton) {
            var hfAccountIds = document.getElementById("<%= hfAccountIds.ClientID %>");
            var accountId = parseInt(imgButton.getAttribute("AccountId"));

            var isCheckedDictionary = JSON.parse(hfAccountIds.value);
            isCheckedDictionary[accountId] = !(isCheckedDictionary[accountId]);

            hfAccountIds.value = JSON.stringify(isCheckedDictionary);
        }
    </script>
    <style type="text/css">
          .autocomplete_CompletionListElement
        {
            margin: 0px;
            background-color: White;
            cursor: default;
            list-style-type: none;
            overflow-y: auto;
            overflow-x: hidden;
            height:180px;
            text-align: left;
            border: 1px solid #777;
            z-index:10000;
        } 

        .modalBackground {
            background-color:Gray;
            filter:alpha(opacity=70);
            opacity:0.7;
        }
        
        .modalPopup {
	        background-color:#ffffdd;
	        border-width:3px;
	        border-style:solid;
	        border-color:Gray;
	        padding:3px;
	        width:250px;
        }

        .DataAccountCheckBoxList {
            display: block;
            border: 1px solid #ccc;
            border-radius: 4px;
            padding: 6px 12px;
            width: 100%;
        }

        .DataAccountCheckBoxList > label {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            text-align: left;
            width: 225px;
            margin-top: 5px;
            margin-left: 10px;
        }
    </style>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
	    var counter;
	    counter = 0;

	    function KeepSessionAlive() {
	        // Increase counter value, so we'll always get unique URL (sample source from page)
	        // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
	        counter++;

	        // Gets reference of image
	        var img = document.getElementById("imgSessionAlive");

	        // Set new src value, which will cause request to server, so
	        // session will stay alive
	        
	        img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;
	        

	        // Schedule new call of KeepSessionAlive function after 60 seconds
	        setTimeout(KeepSessionAlive, 60000);
	    }

	    // Run function for a first time
	    KeepSessionAlive();
	</script> 
   <div>
       <br/><br/>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-lg-10">
                            <div class="panel panel-default">
                                
                                <div style="vertical-align: bottom;" class="panel-heading">
                                    <h4>Add / Edit Users</h4>    
                                    <%--<asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New File"   />--%>
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                </div>
                             
                                <div runat="server" id="divbody" class="panel-body">
                                        <asp:Panel ID="Panel1" runat="server">
                                        <asp:HiddenField runat="server" ID="ID" Value="" />
                                        
                                        <div id="GroupNameContainer" runat="server" class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Group Name</label><label style="color: Red">&nbsp;*</label>
                                                    <asp:DropDownList ID="GroupName" runat="server" CssClass="form-control" ValidationGroup="Required" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="GroupName_SelectedIndexChanged" />
                                                    <asp:CustomValidator ID="csvGroupName" runat="server" ControlToValidate="GroupName" ValidateEmptyText="true" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Account</label><label style="color: Red">&nbsp;*</label>
                                                    <asp:Panel id="AccountIdContainer" runat="server" CssClass="DataAccountCheckBoxList">
                                                        <asp:Repeater ID="AccountId" runat="server" >
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="DataAccountImageButton" runat="server" CausesValidation="false"
                                                                    OnClientClick="DataAccountImageButton_OnClientClick(this); return true;" OnClick="DataAccountImageButton_Click" />
                                                                <label><%# ((SmartPraxis.Models.DataAccount)Container.DataItem).CompanyName %></label>
                                                                <br />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <asp:HiddenField ID="hfAccountIds" runat="server" />
                                                        <asp:CustomValidator ID="csvAccountId" runat="server" OnServerValidate="csvAccountId_ServerValidate" />
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>First Name</label><label style="color: Red">&nbsp;*</label>
                                                    <asp:TextBox ID="FirstName" alt="required" CssClass="form-control" name="firstname" runat="server" accept="First Name" placeholder="First Name" ValidationGroup="Required"  />
                                                    <asp:HiddenField id="hfErrorMessage" runat="server"/>
                                                    <asp:CustomValidator ID="csvFirstName" runat="server" ControlToValidate="FirstName" ValidateEmptyText="true" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Last Name</label><label style="color: Red">&nbsp;*</label>
                                                    <asp:TextBox ID="LastName" alt="required" CssClass="form-control" name="lastname" runat="server" accept="Last Name" placeholder="Last Name" ValidationGroup="Required"  />
                                                    <asp:CustomValidator ID="csvLastName" runat="server" ControlToValidate="LastName" ValidateEmptyText="true" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>User Name</label><label style="color: Red">&nbsp;*</label>
                                                    <asp:TextBox ID="UserName" alt="required" CssClass="form-control" name="username" runat="server" accept="User Name" placeholder="User Name" ValidationGroup="Required"  Enabled="False" />
                                                    <asp:CustomValidator ID="csvUserName" runat="server" ControlToValidate="UserName" ValidateEmptyText="true" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Password</label><label style="color: Red">&nbsp;*</label>
                                                    <asp:TextBox ID="Password" alt="required" CssClass="form-control" name="password" runat="server" accept="Password" placeholder="Password" ValidationGroup="Required" />
                                                    <asp:CustomValidator ID="csvPassword" runat="server" ControlToValidate="Password" ValidateEmptyText="true" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                            <div runat="server" ID="divForcePassword" class="col-lg-2" style="margin-left: 0px; display: none;">
                                                <label>Force Password Reset</label><br/>
                                                <asp:ImageButton ID="ImageButtonForcePasswordReset" runat="server" ImageUrl="~/Content/Images/CheckOffsmall.png" OnClick="ImageButtonForcePasswordReset_Click" CausesValidation="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Office Phone</label>
                                                    <asp:TextBox ID="OfficePhone" alt="required" CssClass="form-control" name="officephone" runat="server" accept="OfficePhone" placeholder="Office Phone" ValidationGroup="Required" />
                                                    <asp:CustomValidator id="csvOfficePhone" runat="server" ControlToValidate="OfficePhone" ValidateEmptyText="true" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Cell Phone</label><asp:TextBox ID="CellPhone" alt="required" CssClass="form-control" name="cellphone" runat="server" accept="CellPhone" placeholder="Cell Phone" ValidationGroup="Required" />
                                                    <asp:CustomValidator id="csvCellPhone" runat="server" ControlToValidate="CellPhone" ValidateEmptyText="true" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <asp:TextBox ID="EmailAddress" alt="required" CssClass="form-control" name="emailaddress" runat="server" accept="Email Address" placeholder="Email Address" ValidationGroup="Required" />
                                                    <asp:CustomValidator id="csvEmailAddress" runat="server" ControlToValidate="EmailAddress" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Role</label><label style="color: Red">&nbsp;*</label>
                                                    <asp:DropDownList Style="cursor:pointer;" Enabled="false" alt="required" ID="Role" runat="server" AppendDataBoundItems="true" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" ></asp:DropDownList>
                                                    <asp:CustomValidator ID="csvRole" runat="server" ControlToValidate="Role" OnServerValidate="RequiredControlValidator_ServerValidate" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.row (nested) -->
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Last Update Person</label>
                                                    <asp:TextBox ID="LastUpdatePerson" CssClass="form-control" runat="server" accept="Last Update Person" placeholder="Last Update Person" ValidationGroup="AlwaysDisable" Enabled="False" ReadOnly="True" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Last Update Date</label>
                                                    <asp:TextBox ID="LastModified" CssClass="form-control" runat="server" accept="Last Update Date" placeholder="Last Update Date" ValidationGroup="AlwaysDisable" Enabled="False" ReadOnly="True" />
                                                </div>
                                             </div>
                                        </div>
                                        <div style="float: left; margin-top: -20px;" class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="col-lg-6" >
                                                        <label>Send&nbsp;Emails</label><br/>
                                                        <asp:ImageButton ID="ImageButtonSendEmails" runat="server" ImageUrl="~/Content/Images/CheckOffsmall.png" OnClick="ImageButtonSendEmails_Click" CausesValidation="false" />
                                                    </div>
                                                    <div class="col-lg-6" >
                                                        <label>Send&nbsp;Texts</label><br/>
                                                        <asp:ImageButton ID="ImageButtonSendTextMessage" runat="server" ImageUrl="~/Content/Images/CheckOffsmall.png" OnClick="ImageButtonSendTextMessage_Click" CausesValidation="false" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                        <div style="clear: both;float: left; position: relative; margin-top:40px" class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <!-- This html button triggers the jquery above -->
                                                    <div style="float:left" runat="server" id="divBtnSubmit"><asp:Button ID="btnsubmit" runat="server" Text="Save" class="btn btn-success" OnClick="btnsubmit1a_Click" />&nbsp;</div>
                                                    <div style="float:left" runat="server" id="divBtnClear">&nbsp;<asp:Button ID="btnclear" runat="server" Text="Clear" class="btn btn-warning" OnClick="btnclear_Click" /></div>
                                                    <div style="float:left" runat="server" id="divBtnNew">&nbsp;<asp:Button ID="btnnew" runat="server" Text="New" class="btn btn-primary" OnClick="btnnew_Click"  /></div>
                                                    <div style="float:left" runat="server" id="divBtnCancel">&nbsp;<asp:Button ID="btncancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="btncancel_Click" /></div>
                                                </div>
                                                <br/><br/>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                        <!-- /.row (nested) -->
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-md-10">
                    <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotification1" runat="server" Position="Center"
                             Width="330px" Height="160px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                             Title="Error" Text="Error, the user name already exists."
                             Style="z-index: 100000">
                    </telerik:RadNotification>
                </div>
            </div>  
           
<br/>     <br/><br/><br/>
    </div>
    <!-- DataTables JavaScript -->
    <%--<script src="/Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="/Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gvGrid').DataTable({
                responsive: true
            });
        });
    </script>--%>
</asp:Content>
