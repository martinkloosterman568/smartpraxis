﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.Account
{
    public partial class ChangePassword : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        static readonly GeneralHelper dHelper = new GeneralHelper();

        private void PageSetup()
        {
            #region generic for reuse

            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.AccountIds.Items.Count == 0)
                {
                    this.PopulateAccountList();
                }

                if ((bool)Session["IsAdmin"])
                {
                    this.AccountIds.Enabled = true;
                }
                else
                {
                    //this.AccountIds.SelectedIndex = this.AccountIds.Items.IndexOf(this.AccountIds.Items.FindByValue(myobj.GetAccountId()));
                }

                var id = this.Request.QueryString["id"];
                if (!string.IsNullOrEmpty(id))
                {
                    const string tableName = "DataUserMast"; // <-- put table name here
                    const string fieldName = "UserGuid"; // <-- put the field name here
                    string outError;
                    
                    var repository = new UserListRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByUserGuid(tableName, fieldName, id, out outError);
                    var result = destination.FirstOrDefault();
                    if (result != null)
                    {
                        this.FindOnReturnOfRedirectId(result.ID.ToString());
                    }
                }
            }
        }

        private void PopulateAccountList()
        {
            List<DataAccount> account = (from d in this.db.DataAccounts
                orderby d.CompanyName
                select d).ToList();
            
            // add from database
            this.AccountIds.DataSource = account;
            this.AccountIds.DataValueField = "AccountId";
            this.AccountIds.DataTextField = "CompanyName";
            this.AccountIds.DataBind();

            // add extra row
            this.AccountIds.Items.Insert(0, new ListItem("- Select One -", "- Select One -"));
        }


        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataUserMast"; // <-- put table name here
            const string fieldName = "ID"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls

        // these controls are actually on the page
        protected void btnnew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/UserEdit.aspx");
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/UserList.aspx");
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }
        #endregion

        public string SaveRecord(List<string> arr)
        {
            #region "Entry Table"

            string ret;
            var ret2 = "0";
            var ret3 = "0";
            var ret4 = "0";
           
            var fieldname = string.Empty;
            try
            {
                object destination = new DataUserMast();
                // this line needs to be changed to be table specific <<---------------------------------------------------------
                var repository = new UserListRepository(new DB_SmartPraxisEntities());
                // this line needs to be changed to be Repository specific <<---------------------------------------------------------

                #region hidden

                var obj = new ControlSaver();

                DataUserMast entity = new DataUserMast();
                var id = obj.GetIdFromControls(ref destination, arr, ref fieldname);
                if (id != string.Empty)
                {
                    var id2 = int.Parse(id);
                    entity = repository.GetById(id2);
                }

                destination = entity;
                obj.SaveControls(ref destination, arr);

                // need to fetch the record and save what you have in hand over top of the existing saved record

                #endregion

                // *** we are using Entity instead of desitination here because we want to take the existing record and apply the slight change to it. Like the password.
                //var saving = (DataUserMast)destination;
                var saving = (DataUserMast)entity;
                // this line here needs the model object <<---------------------------------------------------------

                var myobj = new ChangePassword();

                if (!string.IsNullOrEmpty(saving.Role))
                {
                    saving.RoleID = myobj.GetRole(saving.Role);
                }

                #region hidden

                saving.LastModified = GetTimeNow.GetTime();
                saving.FullName = saving.FirstName + " " + saving.LastName;

                if (saving.UserGuid == null) saving.UserGuid = Guid.NewGuid();
                saving.First8UserGuid = saving.UserGuid.ToString().Substring(0,8);

                if (this.AccountIds.SelectedIndex > 0)
                {
                    var tmpGuid = new Guid(this.AccountIds.SelectedValue);
                    saving.AccountGuid = tmpGuid;
                }
                
                saving.LastUpdatePerson = myobj.GetLastUserUpdated();
                if (saving.Password != null) saving.Password = dHelper.Encrypt(saving.Password);

                if (!string.IsNullOrEmpty(saving.CellPhone))
                {
                    // remove anything but numbers
                    var tmpPhone = Regex.Replace(saving.CellPhone, "[^0-9.]", "");

                    //format the phone number
                    saving.CellPhone = Regex.Replace(tmpPhone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                }

                if (!string.IsNullOrEmpty(saving.OfficePhone))
                {
                    // remove anything but numbers
                    var tmpPhone = Regex.Replace(saving.OfficePhone, "[^0-9.]", "");

                    //format the phone number
                    saving.OfficePhone = Regex.Replace(tmpPhone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                }

                saving.IsForceChangePassword = false;

                if (id == string.Empty)
                {
                    repository.Add(saving);
                }
                else if (id != string.Empty)
                {
                    repository.Update(saving);
                }

                repository.Save();

                #endregion

                ret = saving.ID.ToString();
            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    ret4 = "Error, the user name already exists";
                }

                ret = "failure";
            }

            #endregion
            
            var ret5 = $"success|{ret}|{ret2}|{ret3}|{ret4}";
            return ret5;
        }

        private int GetRole(string rol)
        {
            if (!string.IsNullOrEmpty(rol))
            {
                var result = (from d in this.db.ListRoleMasts
                              where d.DisplayName == rol
                              select d).FirstOrDefault();
                if (result != null)
                {
                    // update ** but nothing to do here
                    return result.ID;
                }
            }
            return 0;
        }

        public string GetLastUserUpdated()
        {
            return this.Session["UserName"].ToString();
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields();
            if (result != null)
            {
                var valid = this.SaveRecord(result);
                if (!valid.Contains("failure"))
                {
                    this.Response.Redirect("~/Pages/Account/Notify.aspx");
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotification('hello')", true);
                }
            }
        }

        private List<string> CheckFields()
        {
            var required = false;
            var arr = new List<string>();

            var id = this.Request.QueryString["id"];

            // Currently this code allows the person to reuse the same password. - April 11, 2018 - not idea, but no time to fix before going live.

            //if (this.Password.Text == this.PasswordAgain.Text)
            //{
            //    this.Password.BackColor = Color.FromArgb(0xffdae0);
            //    required = true;
            //    this.PasswordAgain.BackColor = Color.FromArgb(0xffdae0);
            //    required = true;
            //}

            foreach (var text in this.Panel1.Controls)
            {
                if (text.ToString().Contains("TextBox"))
                {
                    var btn = (TextBox)text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }

                    if (btn.ClientID.Contains("Password") && btn.Text != string.Empty && id == null)
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                    else if (btn.ClientID.Contains("Password") && btn.Text == string.Empty)
                    {
                        const string tableName = "DataUserMast"; // <-- put table name here
                        const string fieldName = "UserGuid"; // <-- put the field name here
                        string outError;
                        if (id == null)
                        {
                            continue;
                        }

                        var repository = new UserListRepository(new DB_SmartPraxisEntities());
                        var destination = repository.FindAllByUserGuid(tableName, fieldName, id, out outError);
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            var pwd = dHelper.Decrypt(result.Password);
                            arr.Add($"{btn.ClientID}:{pwd}");
                            required = false;
                            btn.BackColor = Color.FromArgb(255,255,255);
                        }
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                }
                else if (text.ToString().Contains("Combo") || text.ToString().Contains("Drop"))
                {
                    var btn = (DropDownList)text;
                    if ((btn.Text == string.Empty || btn.Text == "- Select One -")&& btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                    arr.Add($"{btn.ClientID}:{btn.SelectedItem.Text}");
                }
                else if (text.ToString().Contains("Hidden"))
                {
                    var btn = (HiddenField)text;
                    arr.Add($"{btn.ClientID}:{btn.Value}");
                }
            }

            if (required)
            {
                return null;
            }

            return arr;
        }
    }
}