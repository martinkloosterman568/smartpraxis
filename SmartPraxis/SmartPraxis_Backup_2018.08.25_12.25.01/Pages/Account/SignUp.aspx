﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="SmartPraxis.Pages.Account.SignUp"
    MasterPageFile="~/Shared/MasterPages/SiteBasicLayoutForgotPassword.Master" Title="Sign Up | Smart-Praxis" %>

<asp:Content ID="HeadCurrentPage" ContentPlaceHolderID="head" runat="server" >
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            
            img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;
            ////img.src = "http://localhost:10240/RefreshSessionState.aspx?c=" + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
    <%--You can add your custom code for each page header in this section.--%>
</asp:Content>
<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page in this section.--%>
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <script type="text/javascript">
        function showNotification()
        {
            //$find("<%= this.RadNotification1.ClientID %>").text = message;
            $find("<%= this.RadNotification1.ClientID %>").show();
        }
        function showNotification2()
        {
            //$find("<%= this.RadNotification2.ClientID %>").text = message;
            $find("<%= this.RadNotification2.ClientID %>").show();
        }
    </script>
    <div class="sign-box">
        <h3>
            Signup
        </h3>
        <%--<asp:TextBox ID="txtUsername" placeholder="User Name" runat="server"></asp:TextBox>--%>
        <asp:TextBox ID="txtPerson" placeholder="First and Last Name" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtPhone" placeholder="Cell Phone #" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtAssociateKey" placeholder="Associate Key" runat="server"></asp:TextBox>
        <%--<p>
            By clicking on sign up, you agree to terms and 
            <a href="#">privacy policy</a>
        </p>--%>
        <div class="text-right">
            <asp:Button ID="ButtonSIGNUP" runat="server" Text="SIGN UP" OnClick="ButtonSIGNUP_Click" />
            <asp:Button ID="ButtonLOGIN" runat="server" Text="LOG IN" OnClick="ButtonLOGIN_Click" />
        </div>
    </div>
      <div id="error_block" runat="server" visible="false" class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            ×</button>
        <asp:Literal ID="ltrError" runat="server" />
    </div>
    <div class="col-md-10">
        <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotification1" runat="server" Position="Center"
                    Width="330px" Height="160px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                    Title="Error" Text="Error, the user name already exists."
                    Style="z-index: 100000">
        </telerik:RadNotification>
        <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotification2" runat="server" Position="Center"
                    Width="330px" Height="160px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                    Title="Error" Text="Error, the associate key doesn't exist."
                    Style="z-index: 100000">
        </telerik:RadNotification>
    </div>
</asp:Content>
<asp:Content ID="JavaScriptCurrentPage" ContentPlaceHolderID="JavaScriptPage" runat="server">
    <%--You can add your custom JavaScript for each page in this section.--%>
</asp:Content>
