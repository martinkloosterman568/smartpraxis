﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web.UI;
using SmartPraxis.Models;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.Account
{
    public partial class Notify : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            this.HyperLinkHome.NavigateUrl = HyperlinkHome.HomeChooser(this.Session["Role"].ToString());

            if (!this.IsPostBack)
            {
                var getrecord = (from d in this.db.DataSettings select d).FirstOrDefault();
                if (Convert.ToBoolean(getrecord?.InternalMsgEnabled))
                {
                    this.InternalMessage.Text = getrecord?.InternalMessage;
                }
                else
                {
                    this.Redirect();
                }
            }
        }

        protected void ClickToContinue_Click(object sender, EventArgs e)
        {
            this.Redirect();
        }

        private void Redirect()
        {
            var userGuid = this.Session["UserGuid"].ToString();
            var getTermsAccepted = (from d in this.db.DataUserMasts where d.UserGuid.ToString() == userGuid && d.IsTermsAccepted == false select d).FirstOrDefault();
            if (getTermsAccepted != null)
            {
                // you need to accept the terms page
                var width = this.Session["ScreenWidth"].ToString();
                if (Convert.ToInt32(width) < 1366)
                {
                    this.Response.Redirect("~/MobileTerms.aspx");
                }
                else
                {
                    this.Response.Redirect("~/Terms.aspx");
                }
            }

            var o = this.Session["Role"];
            if (o != null)
            {
                if (o.ToString().Contains("Super Admin"))
                {
                    this.Response.Redirect("~/Pages/SuperAdmin/Dashboard.aspx");
                }
                else if (o.ToString().Contains("Expert"))
                {
                    //this.Response.Redirect("~/Pages/DeviceCoTech/1.aspx?x=Expert");
                    this.Response.Redirect("~/Pages/Expert/PatientList.aspx?x=All");
                }
                else if (o.ToString().Contains("MRI Center"))
                {
                    //this.Response.Redirect("~/Pages/MRICompany/Dashboard.aspx");
                    this.Response.Redirect("~/Pages/MRICompany/PatientList.aspx?x=All");
                }
                else if (o.ToString().Contains("MRI Tech"))
                {
                    this.Response.Redirect("~/Pages/MRICompany/PatientList.aspx?x=Pending");
                }
                else if (o.ToString().Contains("MRI Doctor"))
                {
                    this.Response.Redirect("~/Pages/MRICompany/PatientList.aspx?x=Pending");
                }
                else if (o.ToString().Contains("Device Co Tech"))
                {
                    // use this for Live user mode

                    if (this.Session["UsingDevice"] != null && this.Session["UsingDevice"].ToString() == "Mobile")
                    {
                        this.Response.Redirect("~/Pages/DeviceCoTech/MobileList.aspx?x=Pending");
                    }
                    else if (this.Session["UsingDevice"] != null && this.Session["UsingDevice"].ToString() == "Desktop")
                    {
                        // this is here temporarily for testing only
                        this.Response.Redirect("~/Pages/DeviceCoTech/MobileList.aspx?x=Pending");
                        Debugger.Break();
                        // this.Response.Redirect("~/Pages/DeviceCompany/PatientList.aspx");
                    }
                }
                else if (o.ToString().Contains("Device Co"))
                {
                    this.Response.Redirect("~/Pages/DeviceCompany/PatientList.aspx");
                }
            }
        }
    }
}