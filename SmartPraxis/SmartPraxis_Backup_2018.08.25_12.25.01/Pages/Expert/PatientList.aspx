﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientList.aspx.cs" Inherits="SmartPraxis.Pages.Expert.PatientList"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutEmpty.Master" Title="Smart-Praxis" %>

<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
     <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrapWithButtons.css" />
    <img alt="" id="imgSessionAlive" width="1" height="1" />
     <style type="text/css">
		.gridHeader { font-weight:bold; color:white; background-color:#80a0a0; }
		.gridHeader A { padding-right:15px; padding-left:3px; padding-bottom:0px; color:#ffffff; padding-top:0px; text-decoration:none; }
		.gridHeader A:hover { text-decoration: underline; }
		.gridHeaderSortASC A { background: url(/Content/Images/sortdown.gif) no-repeat 95% 50%; }
		.gridHeaderSortDESC A { background: url(/Content/Images/sortup.gif) no-repeat 95% 50%; }
    </style>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            
            img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;
            ////img.src = "http://localhost:10240/RefreshSessionState.aspx?c=" + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
   
    <div style="visibility: hidden">
        <asp:HyperLink ID="HyperLinkHome"  runat="server"><i class="fa fa-home"></i> / </asp:HyperLink>    
    </div>
    <div class="user-stats">
        <div class="row">
           <%-- <div class="col-md-1">
            </div>--%>
            <div class="col-md-4">
                <h3>Patient Order Request List</h3>                
            </div>
        </div>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                      <%-- <div class="col-md-1">
                            
                        </div>--%>
                        <div class="col-lg-10">
                        <div class="panel panel-default">
                            
                        <% // need to put this into a table so mobile can see the filter next to the Create new button %>
                        <div class="panel-heading">
                            <% if (this.Session["Role"].ToString() != "MRI Center")
                               { %>
                            <div class="col-lg-4">
                                <asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New Patient Request" OnClick="CreateNew_Click" Visible="False"  />
                            </div>
                            <% } %>
                            <div class="col-lg-4">
                                <asp:Panel runat="server">
                                    <label>Filter:&nbsp;</label><asp:TextBox ID="Filter" Placeholder="Filter Text" Height="32px" runat="server" AutoPostBack="True" OnTextChanged="Filter_TextChanged" ></asp:TextBox>&nbsp;
                                    <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Find"  />
                                    <asp:Button ID="ClearFilter" class="btn btn-warning" runat="server" Text="Clear" OnClick="ClearFilter_Click"  />
                                </asp:Panel>
                            </div>
                            <div class="col-lg-4">
                                <table>
                                    <tr>
                                        <td><label>By Status:&nbsp;</label>
                                        </td>
                                        <td><asp:DropDownList ID="DropDownListStatus" CssClass="form-control" Width="150px" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListStatus_SelectedIndexChanged"></asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                        
                                
                            </div>
                            <br/><br/><br/>
                        </div>
                        <div class="panel-body">
                           <!-- put content here !-->
                            <asp:GridView ID="gvGrid" runat="server" CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="False"
                            DataKeyNames="PatientId,PatientName,AttachedFile" 
                            OnRowDataBound="GvGridRowDataBound" EmptyDataText="No Rows Found" AllowPaging="True" OnPageIndexChanging="gvGrid_PageIndexChanging" OnSorting="gvGrid_Sorting" AllowSorting="True">
                                 <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" Font-Size="14pt" />
                                 <HeaderStyle CssClass="gridHeader" />
                            <Columns>
                                <asp:TemplateField ItemStyle-CssClass="tooltip-demo" HeaderText="">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridHeaderSortASC" />
                                    <ItemStyle HorizontalAlign="Center" ForeColor="Black" Width="100px" Wrap="False" BackColor="#EEEEEE"  />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink2" runat="server" data-toggle="tooltip" data-placement="top"
                                            title="View/Edit" NavigateUrl='<%# String.Format("/Pages/Expert/Patient.aspx?id={0}&action=edit", this.Eval("PatientGuid")) %>'><img src="/Content/Images/edit-tools.png" alt="edit group" /></asp:HyperLink>
                                        <asp:LinkButton ID="LinkButtonDelete" runat="server" data-toggle="tooltip" data-placement="top"
                                            title="Delete" OnClientClick="return confirm('Are you sure that you want to delete this record?');"
                                            OnClick="LinkButtonDeleteClick" CommandArgument='<%#this.Eval("PatientId") %>'><img src="/Content/Images/delete32.png" alt="delete group" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Patient Name" SortExpression="PatientName">
                                    <ItemStyle Wrap="False" Width="150px" />
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrPatientName" Text='<%#this.Eval("PatientName") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB" SortExpression="DOB">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrDOB" runat="server" Text='<%#this.Eval("DOB","{0:MM/dd/yyyy}") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrPhone" runat="server" Text='<%#this.Eval("Phone") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="MRI Company" SortExpression="MRICompany">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrMRICompany" runat="server" Text='<%#this.Eval("MRICompany") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scan Date" SortExpression="MRIScanDate">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrMRIScanDate" runat="server" Text='<%#this.Eval("MRIScanDate","{0:MM/dd/yyyy}") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scan Time" SortExpression="MRIScanTime">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrMRIScanTime" runat="server" Text='<%#this.Eval("MRIScanTime","{0:hh:mm}") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="PatientStatusDDL">
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrPatientStatusDDL" runat="server" Text='<%#this.Eval("PatientStatusDDL") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-CssClass="tooltip-demo" HeaderText="Report" SortExpression="Report">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="LinkButtonPdfIcon" runat="server" data-toggle="tooltip" data-placement="top"
                                                        title="pdf" Target="_Blank" NavigateUrl='<%# string.Format("~/PdfViewer.aspx?y={0}",this.Eval("AttachedFile"))  %>'>
                                                <img src="/Content/Images/pdfIcon.png" alt="pdf group" />
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No record found
                            </EmptyDataTemplate>
                        </asp:GridView>
                        </div>
                    </div>
                </div>
                </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-md-4">
                                
                </div>
            </div>        
      <%--  <div class="table-responsive">
                
        </div>--%>

    </div>
    <!-- DataTables JavaScript -->
    <%--<script src="../../Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="../../Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gvGrid').DataTable({
                responsive: true
            });
        });
    </script>--%>
</asp:Content>