﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using PostmarkDotNet;
using PostmarkDotNet.Model;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Web.UI;
using DayRenderEventArgs = Telerik.Web.UI.Calendar.DayRenderEventArgs;

namespace SmartPraxis.Pages.Expert
{
    public partial class Patient : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        static readonly GeneralHelper dHelper = new GeneralHelper();
        private static Guid ourAccountId;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region page specific
            var responsed = string.Empty;

            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            if ((this.Session["Role"]?.ToString() == "MRI Center") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if ((this.Session["Role"]?.ToString() == "Device Co") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            this.TechCommentsReply.Enabled = false;

            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                //this.btnsubmit.Enabled = false;
                //this.btnsubmit.Text = @"Click Agree";
                //this.btnsubmit.CssClass = "btn btn-success.disabled";

                this.MRIScanDate.Calendar.SpecialDays.Clear();
                this.LastUpdatePerson.Text = this.Session["UserName"].ToString();

                // fill the dropdown before using it below
                this.PopulateDoctorNameDropDown();
                this.PopulateStatusDropDown();
                //this.PopulateMRITypeDropDown();
                this.PopulateMRITeslaDropDown();
                this.PopulateDeviceCompanyDropDown();

                this.PatientStatusDDL.Items.FindByText("Pending").Selected = true;
                this.PatientStatusDDL.Enabled = false;

                if (this.Request.QueryString["id"] == null)
                {
                    this.statusDDL.Visible = false;
                }

                if (this.Session["Role"]?.ToString() == "MRI Doctor")
                {
                    this.MRIDoctorName.Enabled = false;
                    this.MRIDoctorName.SelectedIndex = this.MRIDoctorName.Items.IndexOf(this.MRIDoctorName.Items.FindByText(this.Session["UserName"].ToString()));
                }

                if (this.Request.QueryString["id"] != null)
                {
                    const string tableName = "DataPatientRequests"; // <-- put table name here
                    const string fieldName = "PatientGuid"; // <-- put the field name here
                    string outError;
                    var id = this.Request.QueryString["id"];
                    var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByGuid(tableName, fieldName, id, out outError);
                    var result = destination.FirstOrDefault();
                    if (result != null)
                    {
                        this.FindOnReturnOfRedirectId(result.PatientId.ToString());
                    }
                }

                this.MRIScanDate.MinDate = GetTimeNow.GetTime().AddDays(1);
            }
        }

        //private void PopulateMRITypeDropDown()
        //{
        //    var repository = new ListMRITypeRepository(new DB_SmartPraxisEntities());
        //    var destination = repository.GetAll();

        //    this.MRIType.DataSource = destination;
        //    this.MRIType.DataValueField = "Code";
        //    this.MRIType.DataTextField = "Description";
        //    this.MRIType.DataBind();

        //    this.MRIType.Items.Insert(0, new ListItem("- Select One -", "0"));
        //}

        private void PopulateMRITeslaDropDown()
        {
            var repository = new ListMRITeslaRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.MRITesla.DataSource = destination;
            this.MRITesla.DataValueField = "Code";
            this.MRITesla.DataTextField = "Description";
            this.MRITesla.DataBind();

            // this is remarked because we always want to show 1.5 as default
            //this.MRITesla.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void PopulateDeviceCompanyDropDown()
        {
            //var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            using (var db = new DB_SmartPraxisEntities())
            {
                var data = db.DataAccounts.Where(e=>e.TypeOf.Contains("Device Company")).Select(p => new { p.ShortCompanyName }).Distinct().ToList();

                this.DeviceCompanyName.DataSource = data;
                this.DeviceCompanyName.DataValueField = "ShortCompanyName";
                this.DeviceCompanyName.DataTextField = "ShortCompanyName";
                this.DeviceCompanyName.DataBind();

                this.DeviceCompanyName.Items.Insert(0, new ListItem("- Select One -", "0"));
            }
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataPatientRequests"; // <-- put table name here
            const string fieldName = "PatientId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);

                //if (this.IsAgreedToFollowMKScan.ImageUrl.Contains("On"))
                //{
                //    // this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                //    this.btnsubmit.Enabled = true;
                //    this.btnsubmit.Text = @"Send Request";
                //    this.btnsubmit.CssClass = "btn btn-success";
                //}
                //else
                //{
                //    // this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                //    this.btnsubmit.Enabled = false;
                //    this.btnsubmit.Text = @"Click Agree";
                //    this.btnsubmit.CssClass = "btn btn-success.disabled";
                //}
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls

        // these controls are actually on the page
        protected void btnnew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Expert/Patient.aspx");
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Expert/PatientList.aspx");
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }
        #endregion

        public string SaveRecord(List<string> arr, bool RequiredOverride = false)
        {
            if (arr == null)
            {
                return string.Empty;
            }
            #region "Entry Table"

            // purpose: jquery will collect all controls in div called "divbody" and pass then as an array list, the code below will determine if it is a Add or update
            string ret;
            var ret2 = "0";
            var ret3 = "0";
            var ret4 = "0";
           
            var fieldname = string.Empty;
            try
            {
                object destination = new DataPatientRequest();
                // this line needs to be changed to be table specific <<---------------------------------------------------------
                var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
                // this line needs to be changed to be Repository specific <<---------------------------------------------------------

                #region hidden

                var obj = new ControlSaver();

                var id = obj.GetIdFromControls(ref destination, arr, ref fieldname);
                if (id != string.Empty)
                {
                    var id2 = int.Parse(id);
                    repository.GetById(id2);
                }

                obj.SaveControls(ref destination, arr);
                
                #endregion

                var saving = (DataPatientRequest)destination;
                // this line here needs the model object <<---------------------------------------------------------

                var myobj = new Patient();

                if (!string.IsNullOrEmpty(saving.DeviceCompanyName))
                {
                    saving.DeviceCompanyGuid = myobj.GetDeviceCompanyGuid(saving.DeviceCompanyName);
                }

                var user = this.Session["UserName"].ToString();
                saving.LastUpdatedBy = user;
                saving.LastUpdatedByGuid = myobj.GetUser(user);

                if (this.Session["Role"].ToString() == "MRI Doctor")
                {
                    saving.MRIDoctorName = user;
                    saving.MRIDoctorGuid = myobj.GetUser(user);
                }
                else if (this.Session["Role"].ToString() == "MRI Tech")
                {
                    saving.MRITechName = user;
                    saving.MRITechUserGuid = myobj.GetUser(user);
                    saving.MRIDoctorGuid = myobj.GetUser(saving.MRIDoctorName);
                }
                else if (this.Session["Role"].ToString() == "MRI Center")
                {
                    saving.MRIDoctorGuid = myobj.GetUser(saving.MRIDoctorName);
                }

                #region hidden

                
                saving.LastUpdatedDate = GetTimeNow.GetTime();
                saving.MRICompanyAccountGuid = myobj.GetAccountGuid();
                saving.IsAgreedToFollowMKScan = true; 
                var typeofaction = string.Empty;
                saving.MRICompany = this.Session["CompanyName"].ToString();

                if (saving.MRIScanTime != null)
                {
                    var fixtime = saving.MRIScanTime.Split(':');
                    saving.MRIScanTime = fixtime[0] + ":" + fixtime[1];
                }
                

                // format the phone number to be human readable
                if (!string.IsNullOrEmpty(saving.Phone))
                {
                    // remove anything but numbers
                    var tmpPhone = Regex.Replace(saving.Phone, "[^0-9.]", "");

                    //format the phone number
                    saving.Phone = Regex.Replace(tmpPhone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                }

                if (id == string.Empty)
                {
                    saving.PatientGuid = Guid.NewGuid();
                    saving.Patient8 = saving.PatientGuid.ToString().Substring(0, 8);

                    saving.MRICompanyLastStatus = "Sent Request";   // this is the inital value
                    if (RequiredOverride)
                    {
                        saving.MRICompanyLastStatus = "Saved";   // this is the inital value
                    }
                    
                    saving.OverAllStatus = saving.MRICompanyLastStatus;
                    repository.Add(saving);
                    typeofaction = "Add";
                }
                else if (id != string.Empty)
                {
                    saving.MRICompanyLastStatus = "Updated";   // this is the inital value
                    saving.OverAllStatus = saving.MRICompanyLastStatus;
                    repository.Update(saving);
                    typeofaction = "Update";
                }
                
                repository.Save();

                // below is just for sending Text, an admin of the DeviceCo may opt to have no text sent
                // then this section will not produce any text messages
                //--------------------------------------------------------------------------------
                // NEED TO SEND EMAIL TO ADMIN also
                if (typeofaction == "Add" && RequiredOverride == false)
                {
                    int vendorSentTo = 0;
                    var phones = new List<PhoneVal>();
                    var emails = new List<EmailVal>();
                    var sql = from a in this.db.DataGeographyRegions
                              join b in this.db.DataAccounts on a.GeographyGuid equals b.GeographyGuid
                              where b.AccountGuid == saving.DeviceCompanyGuid
                               && b.CompanyName.Contains(this.DeviceCompanyName.SelectedItem.Text)
                              select b;
                    var devicecos = sql.ToList();
                    foreach (var devco in devicecos)
                    {
                        var users = (from d in this.db.DataUserMasts
                                     where d.AccountGuid == devco.AccountGuid
                                     && d.Role == "Device Co Tech" && (d.IsSendText == true || d.IsSendEmail == true)
                                     select d).Distinct().ToList();
                        foreach (var usr in users)
                        {
                            #region Phones
                            var phone = new PhoneVal { phone = usr.CellPhone, personName = usr.FirstName, UserGuid = new Guid(usr.UserGuid.ToString()) };
                            if (phone.phone.Length > 0)
                            {
                                if (phone.phone.Substring(0, 1) != "1")
                                {
                                    phone.phone = @"1" + Regex.Replace(phone.phone, "[^0-9.]", "");
                                }
                                else
                                {
                                    phone.phone = Regex.Replace(phone.phone, "[^0-9.]", "");
                                }
                                phone.UserGuid = new Guid(usr.UserGuid.ToString());

                                var checkfirst = (from d in phones
                                                  where d.phone.Contains(phone.phone)
                                                      && d.UserGuid.ToString().Contains(phone.UserGuid.ToString())
                                                  select d).ToList();
                                if (checkfirst.Count == 0) { phones.Add(phone); }
                            }
                            #endregion

                            #region Emails
                            var email = new EmailVal { emailAddress = usr.EmailAddress, personName = usr.FirstName, UserGuid = new Guid(usr.UserGuid.ToString()) };
                            if (email.emailAddress.Length > 0)
                            {
                                email.emailAddress = this.validateEmail(email.emailAddress);
                                email.UserGuid = new Guid(usr.UserGuid.ToString());

                                var checkfirst = (from d in emails
                                                  where d.emailAddress.Contains(email.emailAddress)
                                          && d.UserGuid.ToString().Contains(email.UserGuid.ToString())
                                    select d).ToList();
                                if (checkfirst.Count == 0) { phones.Add(phone); }
                            }
                            #endregion

                        }

                        // send the text messages out
                        foreach (var sendto in phones)
                        {
                            vendorSentTo += 1;
                            var patient81 = saving.Patient8 + vendorSentTo;
                            var phone = new List<string> { sendto.phone };

                            SendTextMsg.TextMessage(phone,
                                $"Hi {sendto.personName}, MRI Safe Mode: New patient request for {this.Session["CompanyName"]}, login @ http://spmk.us/{patient81}", this.Session["CompanyName"]?.ToString());

                            // add the code here to save to the MobilePageSecurity table
                            var addtodb = new MobilePageSecurity
                            {
                                DeviceCoTechGuid = sendto.UserGuid,
                                Patient81 = patient81
                            };
                            this.db.MobilePageSecurities.Add(addtodb);
                            this.db.SaveChanges();
                        }

                        foreach (var sendemailto in emails)
                        {
                            this.SendEmail(sendemailto);
                        }
                    }
                }
                #endregion

                ret = saving.MRICompanyAccountGuid.ToString();
                ret2 = saving.PatientGuid.ToString();
            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    ret4 = "Error, the user name already exists";
                }

                ret = "failure";
            }

            #endregion
            
            var ret5 = $"success|{ret}|{ret2}|{ret3}|{ret4}";
            return ret5;
        }

        protected void Calendar_OnDayRender(object sender, DayRenderEventArgs e)
        {
            // took out special days, check previous version of code
            // modify the cell rendered content for the days we want to be disabled (e.g. every Saturday and Sunday)
            if (e.Day.Date.DayOfWeek == DayOfWeek.Saturday || e.Day.Date.DayOfWeek == DayOfWeek.Sunday)
            {
                // if you are using the skin bundled as a webresource("Default"), the Skin property returns empty string
                var otherMonthCssClass = "rcOutOfRange";

                // clear the default cell content (anchor tag) as we need to disable the hover effect for this cell
                e.Cell.Text = string.Empty;
                e.Cell.CssClass = otherMonthCssClass; //set new CssClass for the disabled calendar day cells (e.g. look like other month days here)

                // render a span element with the processed calendar day number instead of the removed anchor -- necessary for the calendar skinning mechanism 
                var label = new Label {Text = e.Day.Date.Day.ToString()};
                e.Cell.Controls.Add(label);

                // disable the selection for the specific day
                var calendarDay = new RadCalendarDay
                {
                    Date = e.Day.Date,
                    IsSelectable = false
                };
                calendarDay.ItemStyle.CssClass = otherMonthCssClass;
                this.MRIScanDate.Calendar.SpecialDays.Add(calendarDay);
            }
        }

        private Guid GetDeviceCompanyGuid(string company)
        {
            if (!string.IsNullOrEmpty(company))
            {
                var result = (from d in this.db.DataAccounts
                              where d.CompanyName.Contains(company) || d.ShortCompanyName.Contains(company)
                              select d).FirstOrDefault();
                if (result != null)
                {
                    // update ** but nothing to do here
                    return result.AccountGuid;
                }
            }
            return new Guid();
        }

        private Guid GetUser(string user)
        {
            if (!string.IsNullOrEmpty(user))
            {
                var result = (from d in this.db.DataUserMasts
                              where d.FullName == user
                              select d).FirstOrDefault();
                // update ** but nothing to do here
                if (result?.UserGuid != null) return (Guid)result.UserGuid;
            }
            return new Guid();
        }

        private Guid GetAccountGuid()
        {
            int accountId = ((int[])Session["DataAccountIds"]).First(); // Only 'Device Co' and 'Device Co Tech' roles have multiple DataAccounts
            return db.DataAccounts.First(dataAccount => dataAccount.AccountId == accountId).AccountGuid;
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields();
            if (result != null)
            {
                var valid = this.SaveRecord(result);
                if (!valid.Contains("failure"))
                {
                    this.Response.Redirect("/Pages/Expert/PatientList.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotFound()", true);
                }
            }
        }

        private List<string> CheckFields(bool RequiredOverride = false)
        {
            var required = false;
            var arr = new List<string>();

            var id = this.Request.QueryString["id"];
            
            foreach (var text in this.Panel1.Controls)
            {
                if (text.ToString().Contains("TextBox"))
                {
                    var btn = (TextBox)text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }

                    if (btn.ClientID.Contains("Password") && btn.Text != string.Empty && id == null)
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                    else if (btn.ClientID.Contains("Password") && btn.Text == string.Empty)
                    {
                        const string tableName = "DataUserMast"; // <-- put table name here
                        const string fieldName = "UserGuid"; // <-- put the field name here
                        string outError;
                        if (id == null)
                        {
                            break;
                        }

                        var repository = new UserListRepository(new DB_SmartPraxisEntities());
                        var destination = repository.FindAllByUserGuid(tableName, fieldName, id, out outError);
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            var pwd = dHelper.Decrypt(result.Password);
                            arr.Add($"{btn.ClientID}:{pwd}");
                            required = false;
                            btn.BackColor = Color.FromArgb(255,255,255);
                        }
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                }
                else if (text.ToString().Contains("Combo") || text.ToString().Contains("Drop"))
                {
                    var btn = (DropDownList)text;
                    if (btn.SelectedItem != null)
                    {
                        if ((btn.SelectedItem.Text == string.Empty || btn.SelectedItem.Text == @"- Select One -") && btn.SkinID == "Required")
                        {
                            btn.BackColor = Color.FromArgb(0xffdae0);
                            required = true;
                        }
                        else
                        {
                            btn.BackColor = Color.White;
                        }

                        arr.Add($"{btn.ClientID}:{btn.SelectedItem.Text}");
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                }
                else if (text.ToString().Contains("Hidden"))
                {
                    var btn = (HiddenField)text;
                    arr.Add($"{btn.ClientID}:{btn.Value}");
                }
                else if (text.ToString().Contains("ImageUrl"))
                {
                    var btn = (ImageButton)text;
                    arr.Add($"{btn.ClientID}:{btn.ClientID}");
                }
                else if (text.ToString().Contains("DatePicker"))
                {
                    var btn = (RadDatePicker)text;
                    var datevalue = btn.SelectedDate.ToString().Replace(":", "~");
                    if (btn.SelectedDate == null)
                    {
                        datevalue = btn.InvalidTextBoxValue.Replace(":", "~");
                        arr.Add($"{btn.ClientID}:{datevalue}");
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{datevalue}");
                    }

                    if (btn.SelectedDate.ToString() == string.Empty)
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(255, 255, 255);
                    }
                }
                else if (text.ToString().Contains("TimePicker"))
                {
                    var btn = (RadTimePicker)text;
                    var timevalue = btn.SelectedTime.ToString().Replace(":","~");
                    arr.Add($"{btn.ClientID}:{timevalue}");
                    if (btn.SelectedTime.ToString() == string.Empty)
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(255, 255, 255);
                    }
                }
                else if (text.ToString().Contains("Doctor"))
                {
                    if (this.Session["Role"].ToString().Contains("MRI Doctor"))
                    {
                        Debugger.Break();
                    }
                }
            }

            if (required && RequiredOverride == false)
            {
                return null;
            }

            return arr;
        }

        protected void ImageButtonCheck_Click(object sender, ImageClickEventArgs e)
        {
            if (this.IsAgreedToFollowMKScan.ImageUrl.Contains("Off"))
            {
                this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                //this.btnsubmit.Enabled = true;
                //this.btnsubmit.Text = @"Send Request";
                //this.btnsubmit.CssClass = "btn btn-success";
            }
            else
            {
                this.IsAgreedToFollowMKScan.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                //this.btnsubmit.Enabled = false;
                //this.btnsubmit.Text = @"Click Agree";
                //this.btnsubmit.CssClass = "btn btn-success.disabled";
            }
        }

        protected void btnRequestPdf_Click(object sender, EventArgs e)
        {
            // first save the record
            var RequiredOverride = true;
            var result = this.CheckFields(RequiredOverride);
            
            var valid = this.SaveRecord(result, RequiredOverride);
            if (!valid.Contains("failure"))
            {
                // then go to view the popup page
                var patientvalue = valid.Split('|')[2];
                var patientid = patientvalue;
                var action = "edit";

                this.Session["BackButtonVisible"] = "True";
                this.Session["BackButton"] = $"/pages/Expert/Patient.aspx?id={patientid}&action={action}";
                this.Response.Redirect("~/PdfViewer.aspx?z=Sample.pdf&c="+ this.Session["CompanyName"]);
            }
        }

        private void PopulateDoctorNameDropDown()
        {
            Guid myAccountGuid;

            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
                int accountId = ((int[])Session["DataAccountIds"]).First(); // Only 'Device Co' and 'Device Tech Co' users have more than 1 DataAccount
                myAccountGuid = db.DataAccounts.First(dataAccount => dataAccount.AccountId == accountId).AccountGuid;
            }

            const string tableName = "DataUserMast"; // <-- put table name here
            const string fieldName = "Role"; // <-- put the field name here
            const string fieldName2 = "AccountGuid"; // <-- put the field name here
            string outError;

            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAllByRoleAndAccountId(tableName, fieldName, @"Doctor", fieldName2, myAccountGuid.ToString(), out outError);

            this.MRIDoctorName.DataSource = destination;
            this.MRIDoctorName.DataValueField = "UserGuid";
            this.MRIDoctorName.DataTextField = "FullName";
            this.MRIDoctorName.DataBind();

            this.MRIDoctorName.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void PopulateStatusDropDown()
        {
            var repository = new ListStatusRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.PatientStatusDDL.DataSource = destination;
            this.PatientStatusDDL.DataValueField = "Code";
            this.PatientStatusDDL.DataTextField = "Description";
            this.PatientStatusDDL.DataBind();

            this.PatientStatusDDL.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        [ScriptMethod]
        [WebMethod]
        public static List<string> GetDeviceCompany(string prefixText)
        {
            var result = new List<string>();
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.DataAccounts
                             join a in db.DataAccounts on d.GeographyGuid equals a.GeographyGuid
                             where d.CompanyName.Contains(prefixText) && a.AccountGuid == ourAccountId
                             orderby d.CompanyName
                             select new { d.ShortCompanyName }).Take(20).Distinct().ToList();

                result.AddRange(query.Select(item => item.ShortCompanyName));
            }

            return result;
        }

        public async Task<bool> SendEmail(EmailVal email)
        {
            try
            {
                var companyName = this.Session["CompanyName"].ToString();
                var patientName = this.PatientName.Text;

                var sb = new StringBuilder();
                var nl = Environment.NewLine;
                var personName = email.personName;
                sb.Append("Hi " + personName + ", [replace]");
                sb.Append("[replace]");
                sb.Append("A new patient order request has been sent by [company] [replace]");
                sb.Append("for patient [patientName]. [replace] [replace]");
                sb.Append("Please login to http://MriSafeMode.com [replace]");
                sb.Append("Best Regards,[replace]");

                var body1 = sb.ToString();
                var body2 = sb.ToString();
                for (var x = 1; x <= 3; x++)
                {
                    body1 = body1.Replace("[replace]", nl);
                    body1 = body1.Replace("[company]", companyName);
                    body1 = body1.Replace("[patientName]", patientName);
                    body2 = body2.Replace("[replace]", "<br>");
                    body2 = body2.Replace("[company]", companyName);
                    body2 = body2.Replace("[patientName]", patientName);
                }

                var emails = email.emailAddress;

                var mh = new MailHeader
                {
                    Name = "X-CUSTOM-HEADER",
                    Value = "Header content"
                };
                var header = new HeaderCollection { mh };

                var message = new PostmarkMessage
                {
                    To = emails.TrimEnd(','),
                    From = "martin@smart-praxis.com",
                    TrackOpens = true,
                    TextBody = body1,
                    HtmlBody = "<html><body>" + body2 + "</body></html>",
                    Subject = "A new patient order request sent by " + companyName + " - " +
                              DateTime.Now.AddHours(0).ToString("MMM d, yyyy H:mm:ss tt"),
                    Tag = "New Patient Request",
                    Headers = header
                };

                // no attachments for this type of email needed
                //message.AddAttachment(filepath, "content", "application/octet-stream");

                var client = new PostmarkClient("6a454310-2605-4eaa-9941-3f04d12ca809");
                var sendResult = await client.SendMessageAsync(message);

                if (sendResult.Status == PostmarkStatus.Success)
                {
                    /* Handle success */
                }
                else
                {
                    /* Resolve issue.*/
                }

                await Task.Yield();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private string validateEmail(string Email)
        {
            string pattern = @"^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$";
            Regex rx = new Regex(pattern);
            if (rx.IsMatch(Email))
            {
                return Email;
            }

            return string.Empty;
        }

        protected void StartButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.Request.QueryString["id"] != null)
            {
                var patientGuid = this.Request.QueryString["id"];
                this.Response.Redirect("/Pages/DeviceCoTech/1.aspx?x=" + patientGuid + "&y=1");
            }
        }
    }

    public class PhoneVal
    {
        public string personName { get; set; }
        public string phone { get; set; }
        public Guid UserGuid { get; set; }
    }

    public class EmailVal
    {
        public string personName { get; set; }
        public string emailAddress { get; set; }
        public Guid UserGuid { get; set; }
    }
}
