﻿using System;
using System.Web.UI;

namespace SmartPraxis.Pages
{
    public partial class FileNotFound : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Session["MobilePageTitle"] = "File Not Found";
        }
    }
}