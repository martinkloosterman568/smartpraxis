﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;
using PostmarkDotNet;
using PostmarkDotNet.Legacy;
using PostmarkDotNet.Model;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Web.UI;

namespace SmartPraxis.Pages
{
    public partial class ContactUs : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        static readonly GeneralHelper dHelper = new GeneralHelper();
        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
          
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                this.Message.Text = string.Empty;
            }
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields();
            if (result != null)
            {
                var valid = this.SaveRecord(result);
                if (valid.Contains("failure"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotFound()", true);
                }

                var o = this.Session["Role"];
                if (o.ToString().Contains("Super Admin"))
                {
                    this.Response.Redirect("~/Pages/SuperAdmin/Dashboard.aspx");
                }
                else if (o.ToString().Contains("Expert"))
                {
                    this.Response.Redirect("~/Pages/Expert/PatientList.aspx?x=All");
                }
                else if (o.ToString().Contains("MRI Center"))
                {
                    this.Response.Redirect("~/Pages/MRICompany/Dashboard.aspx");
                }
                else if (o.ToString().Contains("MRI Tech"))
                {
                    this.Response.Redirect("~/Pages/MRICompany/PatientList.aspx?x=Pending");
                }
                else if (o.ToString().Contains("MRI Doctor"))
                {
                    this.Response.Redirect("~/Pages/MRICompany/PatientList.aspx?x=Pending");
                }
                else if (o.ToString().Contains("Device Co Tech"))
                {
                    var width = this.Session["ScreenWidth"].ToString();
                    if (Convert.ToInt32(width) < 1366)
                    {
                        this.Response.Redirect("~/Pages/DeviceCoTech/MobileList.aspx?x=Pending");
                    }
                    else
                    {
                        this.Response.Redirect("~/Pages/DeviceCompany/PatientList.aspx");
                    }
                }
                else if (o.ToString().Contains("Device Co"))
                {
                    this.Response.Redirect("~/Pages/DeviceCompany/PatientList.aspx");
                }
            }
        }

        private List<string> CheckFields(bool RequiredOverride = false)
        {
            var required = false;
            var arr = new List<string>();

            var id = this.Request.QueryString["id"];

            foreach (var text in this.Panel1.Controls)
            {
                if (text.ToString().Contains("TextBox"))
                {
                    var btn = (TextBox)text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }

                    if (btn.ClientID.Contains("Password") && btn.Text != string.Empty && id == null)
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                    else if (btn.ClientID.Contains("Password") && btn.Text == string.Empty)
                    {
                        const string tableName = "DataUserMast"; // <-- put table name here
                        const string fieldName = "UserGuid"; // <-- put the field name here
                        string outError;
                        if (id == null)
                        {
                            break;
                        }

                        var repository = new UserListRepository(new DB_SmartPraxisEntities());
                        var destination = repository.FindAllByUserGuid(tableName, fieldName, id, out outError);
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            var pwd = dHelper.Decrypt(result.Password);
                            arr.Add($"{btn.ClientID}:{pwd}");
                            required = false;
                            btn.BackColor = Color.FromArgb(255, 255, 255);
                        }
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                }
                else if (text.ToString().Contains("Combo") || text.ToString().Contains("Drop"))
                {
                    var btn = (DropDownList)text;
                    if (btn.SelectedItem != null)
                    {
                        if ((btn.SelectedItem.Text == string.Empty || btn.SelectedItem.Text == @"- Select One -") && btn.SkinID == "Required")
                        {
                            btn.BackColor = Color.FromArgb(0xffdae0);
                            required = true;
                        }
                        else
                        {
                            btn.BackColor = Color.White;
                        }

                        arr.Add($"{btn.ClientID}:{btn.SelectedItem.Text}");
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                }
                else if (text.ToString().Contains("Hidden"))
                {
                    var btn = (HiddenField)text;
                    arr.Add($"{btn.ClientID}:{btn.Value}");
                }
                else if (text.ToString().Contains("ImageUrl"))
                {
                    var btn = (ImageButton)text;
                    arr.Add($"{btn.ClientID}:{btn.ClientID}");
                }
                else if (text.ToString().Contains("DatePicker"))
                {
                    var btn = (RadDatePicker)text;
                    var datevalue = btn.SelectedDate.ToString().Replace(":", "~");
                    if (btn.SelectedDate == null)
                    {
                        datevalue = btn.InvalidTextBoxValue.Replace(":", "~");
                        arr.Add($"{btn.ClientID}:{datevalue}");
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{datevalue}");
                    }

                    if (btn.SelectedDate.ToString() == string.Empty)
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(255, 255, 255);
                    }
                }
                else if (text.ToString().Contains("TimePicker"))
                {
                    var btn = (RadTimePicker)text;
                    var timevalue = btn.SelectedTime.ToString().Replace(":", "~");
                    arr.Add($"{btn.ClientID}:{timevalue}");
                    if (btn.SelectedTime.ToString() == string.Empty)
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(255, 255, 255);
                    }
                }
                else if (text.ToString().Contains("Doctor"))
                {
                    if (this.Session["Role"].ToString().Contains("MRI Doctor"))
                    {
                        Debugger.Break();
                    }
                }
            }

            if (required && RequiredOverride == false)
            {
                return null;
            }

            return arr;
        }

        private Guid GetUser(string user)
        {
            if (!string.IsNullOrEmpty(user))
            {
                var result = (from d in this.db.DataUserMasts
                    where d.FullName == user
                    select d).FirstOrDefault();
                // update ** but nothing to do here
                if (result?.UserGuid != null) return (Guid)result.UserGuid;
            }
            return new Guid();
        }

        public string SaveRecord(List<string> arr, bool RequiredOverride = false)
        {
            if (arr == null)
            {
                return string.Empty;
            }
            #region "Entry Table"

            // purpose: jquery will collect all controls in div called "divbody" and pass then as an array list, the code below will determine if it is a Add or update
            string ret;
            var ret2 = "0";
            var ret3 = "0";
            var ret4 = "0";

            var fieldname = string.Empty;
            try
            {
                object destination = new DataMessage();
                // this line needs to be changed to be table specific <<---------------------------------------------------------
                var repository = new DataMessageRepository(new DB_SmartPraxisEntities());
                // this line needs to be changed to be Repository specific <<---------------------------------------------------------

                #region hidden

                var obj = new ControlSaver();

                var id = obj.GetIdFromControls(ref destination, arr, ref fieldname);
                if (id != string.Empty)
                {
                    var id2 = int.Parse(id);
                    repository.GetById(id2);
                }

                obj.SaveControls(ref destination, arr);

                #endregion

                var saving = (DataMessage)destination;
                // this line here needs the model object <<---------------------------------------------------------

                var myobj = new ContactUs();

                
                var user = this.Session["UserName"].ToString();
                saving.UserGuid = myobj.GetUser(user);

                #region hidden

                saving.MessageDateTime = GetTimeNow.GetTime();

                // format the phone number to be human readable

                if (id == string.Empty)
                {
                    saving.Message = this.Message.Text; 

                    repository.Add(saving);
                }

                repository.Save();
                this.SendEmail();

                ret = "success";

                #endregion

            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    ret4 = "Error, the user name already exists";
                }

                ret = "failure";
            }

            #endregion

            var ret5 = $"success|{ret}|{ret2}|{ret3}|{ret4}";
            return ret5;
        }

        public bool SendEmail()
        {
            try
            {
                var type = this.Request.QueryString["y"];
                var contactRequest = string.Empty;
                if (type == "R")
                {
                    contactRequest = "Regular";
                }
                else if (type == "E")
                {
                    contactRequest = "Expert Consult";
                }

                var sb = new StringBuilder();
                var nl = Environment.NewLine;
                var email = this.Session["EmailAddress"]?.ToString() ?? "n/a";
                var officeph = this.Session["OfficePhone"]?.ToString() ?? "n/a";
                var cellph = this.Session["CellPhone"]?.ToString() ?? "n/a";

                sb.Append("Support Request Info[replace]");
                sb.Append("From: " + this.Session["UserName"] + "[replace]");
                sb.Append("Email: " + email + "[replace]");
                sb.Append("Office Phone: " + officeph + "[replace]");
                sb.Append("Cell Phone: " + cellph + "[replace]");
                sb.Append("[replace]");
                sb.Append("Type of Request: " + contactRequest + "[replace]");
                sb.Append("[replace]Message[replace]");
                sb.Append(this.Message.Text);

                var body1 = sb.ToString();
                var body2 = sb.ToString();
                for (var x = 1; x <= 3; x++)
                {
                    body1 = body1.Replace("[replace]", nl);
                    body2 = body2.Replace("[replace]", "<br>");
                }

                var mh = new MailHeader
                {
                    Name = "X-CUSTOM-HEADER",
                    Value = "Header content"
                };
                var header = new HeaderCollection { mh };

                var message = new PostmarkMessage
                {
                    From = "smart-praxis@mrisafemode.com",
                    To = "smart-praxis@mrisafemode.com",
                    TrackOpens = true,
                    TextBody = body1,
                    HtmlBody = "<html><body>" + body2 + "</body></html>",
                    Subject = this.Subject.Text + " - " + DateTime.Now.AddHours(0).ToString("MMM d, yyyy H:mm:ss tt"),
                    Tag = "Support Request",
                    Headers = header
                };


                // no attachments for this type of email needed
                //message.AddAttachment(filepath, "content", "application/octet-stream");

                //Server: smtp.postmarkapp.com
                //Ports: 25, 2525, or 587
                //Username: 9f411740-6f5b-434b-ab83-c1304fba1f0b
                //Password: 9f411740-6f5b-434b-ab83-c1304fba1f0b
                //Authentication: Plain text, CRAM-MD5, or TLS

                System.Threading.Thread.Sleep(1000);  // sleep for 1 second

                var client = new PostmarkClient("6a454310-2605-4eaa-9941-3f04d12ca809");

                var sendResult = client.SendMessage(message);
                
                if (sendResult.Status == PostmarkStatus.Success)
                {
                    // Debugger.Break();
                    /* Handle success */
                }
                else
                {
                    // Debugger.Break();
                    /* Resolve issue.*/
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


    }
}