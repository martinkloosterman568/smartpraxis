﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.MRICompany
{
    public partial class Dashboard : Page
    {
        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    DataTable dt = FillDataSet.FillDt("spGetDashboardCounts", new[]
                    {
                        new SqlParameter("@DataAccountIdsDelimited", (string)Session["DataAccountIdsDelimited"])
                    });

                    foreach (DataRow row in dt.Rows)
                    {
                        var moder = row.ItemArray[0].ToString();
                        var qty = int.Parse(row.ItemArray[1].ToString());

                        switch (moder)
                        {
                            case "Accepted":
                                this.Session["Accepted"] = qty;
                                break;
                            case "PatientRequests":
                                this.Session["PatientRequests"] = qty;
                                break;
                            case "Rejected":
                                this.Session["Rejected"] = qty;
                                break;
                            case "Comments":
                                this.Session["Comments"] = qty;
                                break;
                        }
                    }
                }
            }
        }

        protected void Patients_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Pending");
        }

        protected void Accepted_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Accepted");
        }

        protected void Rejected_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Rejected");
        }

        protected void Comments_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Comments");
        }
    }
}