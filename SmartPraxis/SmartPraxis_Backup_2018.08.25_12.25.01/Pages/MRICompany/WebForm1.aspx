﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="SmartPraxis.Pages.MRICompany.WebForm1" %>

<%@ Register Assembly="Telerik.ReportViewer.Html5.WebForms, Version=12.0.18.125, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.Html5.WebForms" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:ReportViewer ID="ReportViewer1" runat="server">
            <ReportSource Identifier="SmartPraxis.ReportBuilder.MRINotCompletedReport2, SmartPraxis, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" IdentifierType="TypeReportSource">
            </ReportSource>
        </telerik:ReportViewer>
    </div>
    </form>
</body>
</html>
