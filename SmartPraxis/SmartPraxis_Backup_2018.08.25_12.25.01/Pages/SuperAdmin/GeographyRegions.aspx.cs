﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.SuperAdmin
{
    public partial class GeographyRegions : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        public static string TypedPassword;
        public static int strRoleId;
        static readonly GeneralHelper dHelper = new GeneralHelper();

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                var id = this.Request.QueryString["id"];
                if (!string.IsNullOrEmpty(id))
                {
                    const string tableName = "DataGeographyRegions"; // <-- put table name here
                    const string fieldName = "GeographyGuid"; // <-- put the field name here
                    string outError;
                    
                    var repository = new GeographyRegionsRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByGuid(tableName, fieldName, id, out outError);
                    var result = destination.FirstOrDefault();
                    if (result != null)
                    {
                        this.FindOnReturnOfRedirectId(result.GeographyGuid.ToString());
                    }
                }
            }
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataGeographyRegions"; // <-- put table name here
            const string fieldName = "GeographyGuid"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new GeographyRegionsRepository(new DB_SmartPraxisEntities());
            var destination = repository.FindById(tableName, fieldName, id, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls

        // these controls are actually on the page
        protected void btnnew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/SuperAdmin/GeographyRegions.aspx");
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/SuperAdmin/GeographyRegionsList.aspx");
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }
        #endregion

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new GeographyRegionsRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.Response.Redirect("/Pages/SuperAdmin/GeographyRegionsList.aspx");
        }

        public string SaveRecord(List<string> arr)
        {
            #region "Entry Table"

            string ret;
            var ret2 = "0";
            var ret3 = "0";
            var ret4 = "0";
           
            var fieldname = string.Empty;
            try
            {
                object destination = new DataGeographyRegion();
                // this line needs to be changed to be table specific <<---------------------------------------------------------
                var repository = new GeographyRegionsRepository(new DB_SmartPraxisEntities());
                // this line needs to be changed to be Repository specific <<---------------------------------------------------------

                #region hidden

                var obj = new ControlSaver();

                var id = obj.GetIdFromControls(ref destination, arr, ref fieldname);
                if (id != string.Empty)
                {
                    var id2 = int.Parse(id);
                    repository.GetById(id2);
                }

                obj.SaveControls(ref destination, arr);
                
                #endregion

                var saving = (DataGeographyRegion)destination;
                // this line here needs the model object <<---------------------------------------------------------

                var myobj = new GeographyRegions();

                #region hidden

                saving.LastModified = GetTimeNow.GetTime();
                saving.LastUpdatePerson = myobj.GetLastUserUpdated();

                if (saving.GeographyGuid == null) saving.GeographyGuid = Guid.NewGuid();
                if (id == string.Empty)
                {
                    repository.Add(saving);
                }
                else if (id != string.Empty)
                {
                    repository.Update(saving);
                }

                repository.Save();

                #endregion

                ret = saving.GeographyId.ToString();
            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    ret4 = "Error, the Region / District name already exists";
                }

                ret = "failure";
            }

            #endregion
            
            var ret5 = $"success|{ret}|{ret2}|{ret3}|{ret4}";
            return ret5;
        }

        private int GetRole(string rol)
        {
            if (!string.IsNullOrEmpty(rol))
            {
                var result = (from d in this.db.ListRoleMasts
                              where d.DisplayName == rol
                              select d).FirstOrDefault();
                if (result != null)
                {
                    // update ** but nothing to do here
                    return result.ID;
                }
            }
            return 0;
        }

        public static bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, "^\\d+$");
        }

        public string GetLastUserUpdated()
        {
            return this.Session["UserName"].ToString();
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields();
            if (result != null)
            {
                var valid = this.SaveRecord(result);
                if (!valid.Contains("failure"))
                {
                    this.Response.Redirect("/Pages/SuperAdmin/GeographyRegionsList.aspx?x=" + this.Session["GeographyGuid"]);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotification('hello')", true);
                }
            }
        }

        protected void btnsubmit2a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields();
            if (result != null)
            {
                this.SaveRecord(result);
                this.Response.Redirect("/Pages/SuperAdmin/GeographyRegionsList.aspx?x=" + this.Session["GeographyGuid"]);
            }
        }

        private List<string> CheckFields()
        {
            var required = false;
            var arr = new List<string>();

            var id = this.Request.QueryString["id"];
            
            foreach (var text in this.Panel1.Controls)
            {
                if (text.ToString().Contains("TextBox"))
                {
                    var btn = (TextBox)text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }

                    if (btn.Text != string.Empty)
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                }
                else if (text.ToString().Contains("Combo") || text.ToString().Contains("Drop"))
                {
                    var btn = (DropDownList)text;
                    if ((btn.Text == string.Empty || btn.Text == "- Select One -")&& btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                    arr.Add($"{btn.ClientID}:{btn.SelectedItem.Text}");
                }
                else if (text.ToString().Contains("Hidden"))
                {
                    var btn = (HiddenField)text;
                    arr.Add($"{btn.ClientID}:{btn.Value}");
                }
            }

            if (required)
            {
                return null;
            }

            return arr;
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/SuperAdmin/GeographyRegionsList.aspx?x=" + this.Session["GeographyGuid"]);
        }
    }
}