﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.SuperAdmin
{
    public partial class AssignDeviceCoList : Page
    {
        string responsed = string.Empty;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region Specific to this page
            if (this.Request.QueryString["x"] != null)
            {
                this.responsed = this.Request.QueryString["x"];
            }

            if ((this.Session["Role"]?.ToString() == "MRI Center") && (this.responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if ((this.Session["Role"]?.ToString() == "Device Co") && (this.responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    this.ViewState["sort"] = "AccountId ASC";
                    this.PopulateGrid();
                }
            }
        }

        private void PopulateGrid()
        {
            var destination = this.GetDataForGrid("AccountId ASC");

            this.gvGrid.DataSource = destination;
            this.gvGrid.DataBind();

            if (destination != null && destination.Rows.Count > 0)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.Response.Redirect("/Pages/Account/AccountList.aspx?x=" + this.Session["AccountGuid"]);
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.Session["Role"].ToString().Contains("Super Admin"))
            {
                e.Row.Cells[0].Visible = true;
            }
            else if (this.Session["Role"].ToString().Contains("MRI Center") ||
                this.Session["Role"].ToString().Contains("Device Co"))
            {
                var deletebutton = (LinkButton)e.Row.FindControl("LinkButtonDelete");
                if (deletebutton != null)
                {
                    deletebutton.Visible = false;
                }
            }
            else
            {
                e.Row.Cells[0].Visible = false;
            }

            if (e.Row.DataItem != null)
            {
                // show
                var buttonPicked = DataBinder.Eval(e.Row.DataItem, "Picked");
                var ltrLinkButtonPicked = (Image) e.Row.FindControl("imgButtonPicked");
                if (buttonPicked != null && buttonPicked.ToString() == "True")
                {
                    ltrLinkButtonPicked.ImageUrl = "/Content/Images/CheckOnsmall.png";
                }
                else
                {
                    ltrLinkButtonPicked.ImageUrl = "/Content/Images/CheckOffsmall.png";
                }
            }
        }

        protected void LinkButtonPickedClick(object sender, EventArgs e)
        {
            var item = (LinkButton)sender;
            var obj = (Image)item.FindControl("imgButtonPicked");

            string transType;
            if (obj.ImageUrl == "/Content/Images/CheckOffsmall.png")
            {
                transType = "Add";
                obj.ImageUrl = "/Content/Images/CheckOnsmall.png";
            }
            else
            {
                transType = "Delete";
                obj.ImageUrl = "/Content/Images/CheckOffsmall.png";
            }

            var id = string.Empty;
            var devicecoguid = string.Empty;
            var mycompanyguid = string.Empty;
            var values = ((LinkButton)sender).CommandArgument;
            if (values.Contains("|"))
            {
                id = values.Split('|')[0];
                devicecoguid = values.Split('|')[1];
                mycompanyguid = values.Split('|')[2];
            }

            var tableName = "AccountDeviceCoBridge";
            if (id != string.Empty && devicecoguid != string.Empty && mycompanyguid != string.Empty)
            {
                const string fieldName = "DeviceCoAccountGuid";
                var repository2 = new AccountDeviceCoBridgeRepository(new DB_SmartPraxisEntities());

                if (transType == "Add")
                {
                    var destination3 = new AccountDeviceCoBridge
                    {
                        DeviceCoAccountGuid = new Guid(devicecoguid),
                        MRICompanyAccountGuid = new Guid(mycompanyguid),
                        AccountDeviceBridgeGuid = Guid.NewGuid()
                    };

                    // not being used, could probably delete it
                    repository2.Add(destination3);
                    repository2.Save();
                }
                else
                {
                    // this is delete
                    //  go to the bridge table - get the device company and remove it
                    string outError;
                    var destination2 = repository2.FindFirstByGuid(tableName, fieldName, devicecoguid, out outError);
                    repository2.DeleteByGuid(destination2.DeviceCoAccountGuid);
                }
                
                this.PopulateGrid();
            }
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("Account.aspx");
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
            this.PopulateGrid();
        }

        public DataTable GetDataForGrid(string sSort)
        {
            var acctMgr = new Guid(this.Session["AccountGuid"].ToString());
            var dbParams = new[]
            {
                new SqlParameter("@valuePassedIn", this.Filter.Text),
                new SqlParameter("@sortBy", sSort),
                new SqlParameter("@acctMgr", acctMgr)
            };

            var dt = FillDataSet.FillDt("spFilterDeviceCoList", dbParams);
            return dt;
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }

        protected void Filter_TextChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            this.Filter.Text = string.Empty;
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            var s = this.ViewState["sort"].ToString().Split();    //load the last sort
            var sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                {
                    sSort += " DESC";
                }
                else
                {
                    sSort += " ASC";
                }
            }
            else
            {
                sSort += " ASC";
            }

            //find which column is being sorted to change its style
            var i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" + sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            //get the sorted data
            var dt = this.GetDataForGrid(sSort);
            this.gvGrid.DataSource = dt;
            this.gvGrid.DataBind();

            this.gvGrid.DataSource = this.GetDataForGrid(sSort);
            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }
        
        protected void DropDownListAccountManager_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }
    }
}
 