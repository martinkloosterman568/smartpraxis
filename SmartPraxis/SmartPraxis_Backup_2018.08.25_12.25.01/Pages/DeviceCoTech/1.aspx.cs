﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _1 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    var ch = new CircleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/2.aspx?x={patientGuid}",
                        Radius = 100,
                        X = 861,
                        Y = 590
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/1.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}