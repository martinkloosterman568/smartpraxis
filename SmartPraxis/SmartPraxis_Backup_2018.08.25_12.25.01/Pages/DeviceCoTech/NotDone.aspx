﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NotDone.aspx.cs" Inherits="SmartPraxis.Pages.DeviceCoTech.NotDone"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutDeviceMobile.Master" Title="Smart-Praxis" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50508.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page on this section.--%>
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <script type="text/javascript">
        function OnDayRender(calendarInstance, args) {
            // convert the date-triplet to a javascript date
            // we need Date.getDay() method to determine 
            // which days should be disabled (e.g. every Saturday (day = 6) and Sunday (day = 0))                
            var jsDate = new Date(args.get_date()[0], args.get_date()[1] - 1, args.get_date()[2]);
            if (jsDate.getDay() == 0 || jsDate.getDay() == 6) {
                var otherMonthCssClass = "rcOutOfRange";
                args.get_cell().className = otherMonthCssClass;
                // replace the default cell content (anchor tag) with a span element 
                // that contains the processed calendar day number -- necessary for the calendar skinning mechanism 
                args.get_cell().innerHTML = "<span>" + args.get_date()[2] + "</span>";
                // disable selection and hover effect for the cell
                args.get_cell().DayId = "";
            }
        }
    </script>
    <style type="text/css">
        .modalBackground {
            background-color:Gray;
            filter:alpha(opacity=70);
            opacity:0.7;
        }
        
        .modalPopup {
	        background-color:#ffffdd;
	        border-width:3px;
	        border-style:solid;
	        border-color:Gray;
	        padding:3px;
	        width:250px;
        }
    </style>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
	    var counter;
	    counter = 0;

	    function KeepSessionAlive() {
	        // Increase counter value, so we'll always get unique URL (sample source from page)
	        // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
	        counter++;

	        // Gets reference of image
	        var img = document.getElementById("imgSessionAlive");

	        // Set new src value, which will cause request to server, so
	        // session will stay alive
	        
	        img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;
	        

	        // Schedule new call of KeepSessionAlive function after 60 seconds
	        setTimeout(KeepSessionAlive, 60000);
	    }

	    // Run function for a first time
	    KeepSessionAlive();
	</script> 
   <div>
       <br/><br/>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-lg-10">
                            <div class="panel panel-default">
                                
                                <div style="vertical-align: bottom;" class="panel-heading">
                                    <h4>Patient Procedure</h4>    
                                </div>
                             
                                <div runat="server" id="divbody" class="panel-body">
                                    <asp:Panel ID="Panel1" runat="server">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div id="divSafeModeText" runat="server" class="form-group">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Patient Name</label>
                                                    <asp:TextBox ID="PatientName" alt="required" CssClass="form-control" name="firstname" runat="server" accept="First Name" placeholder="First Name" ValidationGroup="Required" SkinID="Required" Enabled="False" />
                                                    <asp:HiddenField id="hfErrorMessage" runat="server"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Patient DOB</label><br/>
                                                    <telerik:RadDatePicker ID="DOB" runat="server" MinDate="1900-01-01" Enabled="False">
                                                        <Calendar EnableWeekends="True" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                                        </Calendar>
                                                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                                                            <EmptyMessageStyle Resize="None" />
                                                            <ReadOnlyStyle Resize="None" />
                                                            <FocusedStyle Resize="None" />
                                                            <DisabledStyle Resize="None" />
                                                            <InvalidStyle Resize="None" />
                                                            <HoveredStyle Resize="None" />
                                                            <EnabledStyle Resize="None" />
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        MRI Type</label>
                                                    <asp:TextBox ID="MRIType" alt="required" CssClass="form-control" runat="server" placeholder="Model SN" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        MRI Tesla</label>
                                                    <asp:DropDownList ID="MRITesla" CssClass="form-control" runat="server" SkinID="Required" Enabled="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Model Description</label>
                                                    <asp:TextBox ID="DeviceModelDescription" alt="required" CssClass="form-control" runat="server" placeholder="Model Description" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Device SN #</label>
                                                    <asp:TextBox ID="DeviceCoDeviceModelSN" alt="required" CssClass="form-control" runat="server" placeholder="Model SN" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Lead-1 Model</label>
                                                    <asp:TextBox ID="LeadNo1Model" alt="required" CssClass="form-control" runat="server" placeholder="Model SN" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Lead-1 SN #</label>
                                                    <asp:TextBox ID="LeadNo1ModelSerialNumber" alt="required" CssClass="form-control" runat="server" placeholder="Model SN" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Lead-2 Model</label>
                                                    <asp:TextBox ID="LeadNo2Model" alt="required" CssClass="form-control" runat="server" placeholder="Model SN" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Lead-2 SN #</label>
                                                    <asp:TextBox ID="LeadNo2ModelSerialNumber" alt="required" CssClass="form-control" runat="server" placeholder="Model SN" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Lead-3 Model</label>
                                                    <asp:TextBox ID="LeadNo3Model" alt="required" CssClass="form-control" runat="server" placeholder="Model SN" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Lead-3 SN #</label>
                                                    <asp:TextBox ID="LeadNo3ModelSerialNumber" alt="required" CssClass="form-control" runat="server" placeholder="Model SN" ValidationGroup="Required" SkinID="Required" Enabled="True" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label runat="server" id="Label3">Presenting Mode</label><asp:Label ID="Label6" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=""></asp:Label>
                                                    <asp:DropDownList ID="DeviceCoPresentingMode" CssClass="form-control" runat="server" SkinID="Required">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label runat="server" id="Label1">Safe Mode</label><asp:Label ID="Label2" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=""></asp:Label>
                                                    <asp:DropDownList ID="DeviceTechPickedSafeMode" CssClass="form-control" runat="server" SkinID="Required">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Comments </label>
                                                    <asp:TextBox ID="DeviceCoCommentsAtTimeOfProcedure" alt="required" CssClass="form-control" name="comments" runat="server" accept="Comments" placeholder="Comments" ValidationGroup="Required" SkinID="NotRequired" Rows="3" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>
                                                        Date / Time Of Procedure</label>
                                                    <table width="900px">
                                                        <tr>
                                                            <td style="width:200px">
                                                                <asp:TextBox ID="DateTimeOfProcedure" alt="required" CssClass="form-control" name="timeofprocedure" runat="server" accept="TimeOfProcedure" placeholder="Time Of Procedure" ValidationGroup="Required" SkinID="Required" Enabled="False" />            
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="Refresh" runat="server" Text="Refresh" class="btn btn-primary" OnClick="btnRefreshTime_Click" /> 
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Device programmed back to original settings</label><br/>
                                                    <asp:ImageButton ID="ImageButtonPostMRI1" runat="server" ImageUrl="~/Content/Images/CheckOffsmall.png" />
                                                </div>
                                            </div>
                                        </div>
                                       <%-- <br/>--%>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Device set up for automatic reprogramming post MRI</label><br/>
                                                    <asp:ImageButton ID="ImageButtonPostMRI2" runat="server" ImageUrl="~/Content/Images/CheckOffsmall.png" />
                                                </div>
                                            </div>
                                        </div>
                                        <%--<br/>--%>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Baseline settings required reprogramming</label><br/>
                                                    <asp:ImageButton ID="ImageButtonPostMRI3" runat="server" ImageUrl="~/Content/Images/CheckOffsmall.png" />
                                                </div>
                                            </div>
                                        </div>
                                        <%--<br/>--%>
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Post MRI Comments </label>
                                                    <asp:TextBox ID="PostMRIComments" alt="required" CssClass="form-control" name="comments" runat="server" accept="Comments" placeholder="Comments" ValidationGroup="Required" SkinID="NotRequired" Rows="3" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                 
                                        <%--<div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <div runat="server" id="div1"><asp:Button ID="btnSaveComplete" Width="250px" runat="server" Text="Save / MRI Complete" class="btn btn-success" OnClick="btnSaveComplete_Click" Enabled="False" /></div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>--%>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <div runat="server" id="div3"><asp:Button ID="Button1" Width="250px" runat="server" Text="Save / MRI Rescheduled" class="btn btn-danger" OnClick="btnSaveNotComplete_Click" /></div>
                                                </div>
                                            </div>
                                        </div>
                                        <%--<div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <div runat="server" id="div2">
                                                        <label>
                                                            Send&nbsp;Emails</label><br/>
                                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Content/Images/CheckOffsmall.png" OnClick="ImageButtonSendEmails_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--%>
                                        <!-- /.row (nested) -->
                                        </asp:Panel>
                                        <!-- /.row (nested) -->
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>  
           
<br/>     <br/><br/><br/>
    </div>
    <!-- DataTables JavaScript -->
    <%--<script src="/Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="/Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gvGrid').DataTable({
                responsive: true
            });
        });
    </script>--%>
</asp:Content>
