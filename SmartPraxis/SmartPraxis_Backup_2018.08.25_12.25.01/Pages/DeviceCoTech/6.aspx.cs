﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _6 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/1.aspx?x={patientGuid}",
                        Bottom = 700,
                        Left = 18,
                        Right = 77,
                        Top = 641
                    };

                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/NotDone.aspx?x={patientGuid}&y=6" ,
                        Bottom = 567,
                        Left = 412,
                        Right = 602,
                        Top = 486
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/6.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}