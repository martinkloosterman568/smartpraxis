﻿using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class NotDone : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (this.Request.QueryString["x"] != null && this.Request.QueryString["y"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patient = this.Request.QueryString["x"];
                    var pageNumber = this.Request.QueryString["y"];

                    this.PopulatePresentingModeDropDown();
                    this.PopulateSafeModeDropDown();
                    this.PopulateMRITeslaDropDown();

                    var intVal = int.Parse(pageNumber);
                    var ListAnswers = (from d in this.db.ListAnswers
                        where d.AnswerPage == intVal
                        select d).FirstOrDefault();
                    if (ListAnswers != null)
                    {
                        this.divSafeModeText.InnerHtml = ListAnswers.AnswerText;
                    }

                    this.DateTimeOfProcedure.Text = DateTime.Now.ToString(CultureInfo.InvariantCulture);

                    const string tableName = "DataPatientRequests"; // <-- put table name here
                    const string fieldName = "PatientGuid"; // <-- put the field name here
                    string outError;
                    var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByGuid(tableName, fieldName, patient, out outError);
                    if (destination != null)
                    {
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            this.FindOnReturnOfRedirectId(result.PatientId.ToString());
                        }
                    }
                    else
                    {
                        this.Session.Abandon();
                        this.Response.Redirect(Global.returnToPage);
                    }
                }
            }
            else
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
        }

        private void PopulatePresentingModeDropDown()
        {
            var repository = new ListPresentingModeRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.DeviceCoPresentingMode.DataSource = destination;
            this.DeviceCoPresentingMode.DataValueField = "Description";
            this.DeviceCoPresentingMode.DataTextField = "Description";
            this.DeviceCoPresentingMode.DataBind();

            this.DeviceCoPresentingMode.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void PopulateSafeModeDropDown()
        {
            var repository = new ListSelectedSafeModeRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.DeviceTechPickedSafeMode.DataSource = destination;
            this.DeviceTechPickedSafeMode.DataValueField = "Description";
            this.DeviceTechPickedSafeMode.DataTextField = "Description";
            this.DeviceTechPickedSafeMode.DataBind();

            this.DeviceTechPickedSafeMode.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void PopulateMRITeslaDropDown()
        {
            var repository = new ListMRITeslaRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.MRITesla.DataSource = destination;
            this.MRITesla.DataValueField = "Code";
            this.MRITesla.DataTextField = "Description";
            this.MRITesla.DataBind();

            // this is remarked because we always want to show 1.5 as default
            //this.MRITesla.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataPatientRequests"; // <-- put table name here
            const string fieldName = "PatientId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        protected void btnRefreshTime_Click(object sender, EventArgs e)
        {
            this.DateTimeOfProcedure.Text = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }
        
        protected void btnSaveNotComplete_Click(object sender, EventArgs e)
        {
            var required = false;

            if (this.DeviceCoDeviceModelSN.Text == string.Empty)
            {
                this.DeviceCoDeviceModelSN.BackColor = Color.FromArgb(0xffdae0);
                required = true;
            }
            else
            {
                this.DeviceCoDeviceModelSN.BackColor = Color.White;
            }

            if (required == false)
            {
                var patient = new Guid(this.Request.QueryString["x"]);
                var patientRecord = (from d in this.db.DataPatientRequests
                                     where d.PatientGuid == patient
                                     select d).FirstOrDefault();

                if (patientRecord != null)
                {
                    var patientGuid = this.Request.QueryString["x"];
                    var pageNo = this.Request.QueryString["y"];
                    var userGuid = this.Session["UserGuid"].ToString();
                    var theDate = Convert.ToDateTime(this.DateTimeOfProcedure.Text);
                    var theTime = theDate.ToString("H:mm:ss");

                    var imageMRI1 = this.ImageButtonPostMRI1.ImageUrl.Contains("On");
                    var imageMRI2 = this.ImageButtonPostMRI2.ImageUrl.Contains("On");
                    var imageMRI3 = this.ImageButtonPostMRI3.ImageUrl.Contains("On");

                    patientRecord.BluePageAnswerPageNo = int.Parse(pageNo);
                    patientRecord.PatientGuid = new Guid(patientGuid);

                    // ------- 
                    patientRecord.MRIScanDatePerformed = theDate;
                    patientRecord.MRIScanTimePerformed = theTime;
                    patientRecord.DeviceCoCommentsAfterOfProcedure = this.PostMRIComments.Text;
                    patientRecord.Completed = true;
                    patientRecord.NotCompleted = false;
                    patientRecord.IsDeviceProgrammedBackToOriginalSettings = imageMRI1;
                    patientRecord.IsDeviceSetupForAutomaticReporgrammingAtMRI = imageMRI2;
                    patientRecord.IsBaselineSettingsRequiredReprogramming = imageMRI3;
                    patientRecord.PatientStatusDDL = "Reschedule";
                    patientRecord.OverAllStatus = "Reschedule";
                    patientRecord.LastUpdatedDate = Convert.ToDateTime(this.DateTimeOfProcedure.Text);
                    patientRecord.LastUpdatedByGuid = new Guid(this.Session["UserGuid"].ToString());
                    patientRecord.LastUpdatedBy = this.Session["UserName"].ToString();
                    patientRecord.DeviceCompanyLastStatus = "Reschedule";
                    patientRecord.DeviceCoTechLastUpdateDate = DateTime.Now;
                    patientRecord.DeviceCoTechUserGuid = new Guid(this.Session["UserGuid"].ToString());
                    patientRecord.DeviceModelDescription = this.DeviceModelDescription.Text;
                    patientRecord.DeviceCoDeviceModelSN = this.DeviceCoDeviceModelSN.Text;
                    patientRecord.DeviceCoPresentingMode = this.DeviceCoPresentingMode.SelectedItem.Text;
                    patientRecord.DeviceTechPickedSafeMode = this.DeviceTechPickedSafeMode.SelectedItem.Text;
                    patientRecord.DeviceCoCommentsAtTimeOfProcedure = this.DeviceCoCommentsAtTimeOfProcedure.Text;
                    patientRecord.DeviceCoRepAtTimeOfProcedure = this.Session["UserName"].ToString();
                    patientRecord.DeviceCoTechUserGuidAtTimeOfProcedure = new Guid(userGuid);
                    patientRecord.LeadNo1Model = this.LeadNo1Model.Text;
                    patientRecord.LeadNo1ModelSerialNumber = this.LeadNo1ModelSerialNumber.Text;
                    patientRecord.LeadNo2Model = this.LeadNo2Model.Text;
                    patientRecord.LeadNo2ModelSerialNumber = this.LeadNo2ModelSerialNumber.Text;
                    patientRecord.LeadNo3Model = this.LeadNo3Model.Text;
                    patientRecord.LeadNo3ModelSerialNumber = this.LeadNo3ModelSerialNumber.Text;
                    
                    // generates the pdf
                    var fileName = this.ExportToPdfNotComplete(patientRecord);
                    // save again to update the AttachedFilename
                    patientRecord.AttachedFile = Path.GetFileName(fileName);

                    this.db.SaveChanges();

                    // go back to the list -- fyi, if you are expert it will autolog you off
                    this.Response.Redirect("MobileList.aspx", false);
                }
            }
        }

        private string ExportToPdfNotComplete(DataPatientRequest patientGuid)
        {
            var reportProcessor = new ReportProcessor();
            var deviceInfo = new Hashtable();
            const string reportName =
                "SmartPraxis.ReportBuilder.MRINotCompletedReport2, SmartPraxis, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            var typeReportSource = new TypeReportSource() { TypeName = reportName };
            //typeReportSource.Parameters.Clear();
            typeReportSource.Parameters.Add("patientGuid", patientGuid.PatientGuid.ToString());

            if (patientGuid.MRIScanDatePerformed != null)
                typeReportSource.Parameters.Add("scanDate", patientGuid.MRIScanDatePerformed.Value.ToShortDateString());

            typeReportSource.Parameters.Add("scanTime", patientGuid.MRIScanTimePerformed);
            typeReportSource.Parameters.Add("modelDescription", patientGuid.DeviceModelDescription);
            typeReportSource.Parameters.Add("modelSN", patientGuid.DeviceCoDeviceModelSN);
            typeReportSource.Parameters.Add("lead1Model", patientGuid.LeadNo1Model);
            typeReportSource.Parameters.Add("lead1SN", patientGuid.LeadNo1ModelSerialNumber);
            typeReportSource.Parameters.Add("lead2Model", patientGuid.LeadNo2Model);
            typeReportSource.Parameters.Add("lead2SN", patientGuid.LeadNo2ModelSerialNumber);
            typeReportSource.Parameters.Add("lead3Model", patientGuid.LeadNo3Model);
            typeReportSource.Parameters.Add("lead3SN", patientGuid.LeadNo3ModelSerialNumber);

            typeReportSource.Parameters.Add("deviceRep", this.Session["UserName"].ToString());
            
            typeReportSource.Parameters.Add("commentsAfter", patientGuid.DeviceCoCommentsAtTimeOfProcedure);

            var result = reportProcessor.RenderReport("PDF", typeReportSource, deviceInfo);
            var fileName = "NotComplete-" + patientGuid.PatientGuid.ToString() + "." + result.Extension;
            var filePath = this.MapPath("~/Documents/" + fileName);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            // this looks like it does all the work
            using (var fs = new FileStream(filePath, FileMode.Create))
            {
                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
            }

            return filePath;
        }
    }
}

