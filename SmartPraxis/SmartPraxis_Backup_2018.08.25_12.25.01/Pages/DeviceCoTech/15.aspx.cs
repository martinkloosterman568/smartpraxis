﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _15 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    //YES
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 722,
                        Left = 18,
                        Right = 77,
                        Top = 642
                    };

                    //NO
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 721,
                        Left = 892,
                        Right = 947,
                        Top = 654
                    };

                    // VVI
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/16.aspx?x={patientGuid}",
                        Bottom = 429,
                        Left = 235,
                        Right = 467,
                        Top = 218
                    };

                    // DDD
                    var ch4 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/30.aspx?x={patientGuid}",
                        Bottom = 429,
                        Left = 561,
                        Right = 778,
                        Top = 222
                    };

                    // VVI2
                    var ch5 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/16.aspx?x={patientGuid}",
                        Bottom = 575,
                        Left = 479,
                        Right = 611,
                        Top = 522
                    };

                    // DDD2
                    var ch6 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/30.aspx?x={patientGuid}",
                        Bottom = 573,
                        Left = 627,
                        Right = 777,
                        Top = 519
                    };

                    // DDI
                    var ch7 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/53.aspx?x={patientGuid}",
                        Bottom = 662,
                        Left = 477,
                        Right = 611,
                        Top = 608
                    };

                    // VDD
                    var ch8 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/76.aspx?x={patientGuid}",
                        Bottom = 663,
                        Left = 626,
                        Right = 779,
                        Top = 609
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/15.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    im.HotSpots.Add(ch5);
                    im.HotSpots.Add(ch6);
                    im.HotSpots.Add(ch7);
                    im.HotSpots.Add(ch8);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}
