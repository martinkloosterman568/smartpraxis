﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class Done : Page
    {
        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (this.Request.QueryString["x"] != null && this.Request.QueryString["y"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    int pageNumber;
                    if (!int.TryParse(Request.QueryString["y"], out pageNumber))
                    {
                        this.Session.Abandon();
                        this.Response.Redirect(Global.returnToPage);
                    }

                    this.PopulatePresentingModeDropDown();
                    this.PopulateSafeModeDropDown();
                    this.PopulateMRITeslaDropDown();

                    this.DateTimeOfProcedure.Text = DateTime.Now.ToString(CultureInfo.InvariantCulture);

                    using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                    {
                        ListAnswer listAnswer = db.ListAnswers.FirstOrDefault(listAnswr => listAnswr.AnswerPage == pageNumber);               
                        if (listAnswer != null)
                        {
                            this.divSafeModeText.InnerHtml = listAnswer.AnswerText;
                        }

                        DataPatientRequest dataPatientRequest = db.DataPatientRequests.FirstOrDefault(dataPatientRqst => dataPatientRqst.PatientGuid == PatientGuid);
                        if (dataPatientRequest != null)
                        {
                            this.FindOnReturnOfRedirectId(dataPatientRequest.PatientId.ToString());
                        }
                        else
                        {
                            this.Session.Abandon();
                            this.Response.Redirect(Global.returnToPage);
                        }
                    }

                    // this saves the entire page
                    Save();
                }
            }
            else
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
        }

        private void PopulatePresentingModeDropDown()
        {
            var repository = new ListPresentingModeRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.DeviceCoPresentingMode.DataSource = destination;
            this.DeviceCoPresentingMode.DataValueField = "Description";
            this.DeviceCoPresentingMode.DataTextField = "Description";
            this.DeviceCoPresentingMode.DataBind();

            this.DeviceCoPresentingMode.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void PopulateSafeModeDropDown()
        {
            var repository = new ListSelectedSafeModeRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.DeviceTechPickedSafeMode.DataSource = destination;
            this.DeviceTechPickedSafeMode.DataValueField = "Description";
            this.DeviceTechPickedSafeMode.DataTextField = "Description";
            this.DeviceTechPickedSafeMode.DataBind();

            this.DeviceTechPickedSafeMode.Items.Insert(0, new ListItem("- Select One -", "0"));
        }

        private void PopulateMRITeslaDropDown()
        {
            var repository = new ListMRITeslaRepository(new DB_SmartPraxisEntities());
            var destination = repository.GetAll();

            this.MRITesla.DataSource = destination;
            this.MRITesla.DataValueField = "Code";
            this.MRITesla.DataTextField = "Description";
            this.MRITesla.DataBind();

            this.MRITesla.Items.Insert(0, "- Select One -");
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataPatientRequests"; // <-- put table name here
            const string fieldName = "PatientId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out var outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        protected void btnRefreshTime_Click(object sender, EventArgs e)
        {
            this.DateTimeOfProcedure.Text = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }
        
        protected void btnSaveComplete_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                this.Save(true, "Done");
            }
        }

        private void Save(bool doExportToPDF = false, string status = null)
        {
            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
                DataPatientRequest patientRecord = db.DataPatientRequests.FirstOrDefault(dataPatientRequest => dataPatientRequest.PatientGuid == PatientGuid);
                Guid deviceCoTechUserGuid = Guid.Parse(this.Session["UserGuid"].ToString());

                if (!string.IsNullOrWhiteSpace(status))
                {
                    patientRecord.PatientStatusDDL = status;
                    patientRecord.OverAllStatus = status;
                    patientRecord.DeviceCompanyLastStatus = status;
                }

                patientRecord.IsDonePage = true;
                patientRecord.BluePageAnswerPageNo = int.Parse(Request.QueryString["y"]);
                patientRecord.MRIScanDatePerformed = Convert.ToDateTime(this.DateTimeOfProcedure.Text);
                patientRecord.MRIScanTimePerformed = patientRecord.MRIScanDatePerformed.Value.ToString("H:mm:ss");
                patientRecord.DeviceCoCommentsAfterOfProcedure = this.DeviceCoCommentsAfterOfProcedure.Text;
                patientRecord.Completed = true;
                patientRecord.NotCompleted = false;
                patientRecord.IsDeviceProgrammedBackToOriginalSettings = this.ImageButtonPostMRI1.ImageUrl.Contains("On");
                patientRecord.IsDeviceSetupForAutomaticReporgrammingAtMRI = this.ImageButtonPostMRI2.ImageUrl.Contains("On");
                patientRecord.IsBaselineSettingsRequiredReprogramming = this.ImageButtonPostMRI3.ImageUrl.Contains("On");
                patientRecord.LastUpdatedDate = Convert.ToDateTime(this.DateTimeOfProcedure.Text);
                patientRecord.LastUpdatedByGuid = deviceCoTechUserGuid;
                patientRecord.LastUpdatedBy = this.Session["UserName"].ToString();
                patientRecord.DeviceCoTechLastUpdateDate = DateTime.Now;
                patientRecord.DeviceCoTechUserGuid = deviceCoTechUserGuid;
                patientRecord.DeviceModelDescription = this.DeviceModelDescription.Text;
                patientRecord.DeviceCoDeviceModelSN = this.DeviceCoDeviceModelSN.Text;
                patientRecord.DeviceCoPresentingMode = this.DeviceCoPresentingMode.SelectedItem.Text;
                patientRecord.DeviceTechPickedSafeMode = this.DeviceTechPickedSafeMode.SelectedItem.Text;
                patientRecord.DeviceCoCommentsAtTimeOfProcedure = this.DeviceCoCommentsAtTimeOfProcedure.Text;
                patientRecord.DeviceCoRepAtTimeOfProcedure = this.Session["UserName"].ToString(); //this.DateTimeOfProcedure.Text;
                patientRecord.DeviceCoTechUserGuidAtTimeOfProcedure = deviceCoTechUserGuid;
                patientRecord.LeadNo1Model = this.LeadNo1Model.Text;
                patientRecord.LeadNo1ModelSerialNumber = this.LeadNo1ModelSerialNumber.Text;
                patientRecord.LeadNo2Model = this.LeadNo2Model.Text;
                patientRecord.LeadNo2ModelSerialNumber = this.LeadNo2ModelSerialNumber.Text;
                patientRecord.LeadNo3Model = this.LeadNo3Model.Text;
                patientRecord.LeadNo3ModelSerialNumber = this.LeadNo3ModelSerialNumber.Text;

                if (doExportToPDF)
                {
                    string fileName = this.ExportToPdfComplete(patientRecord); // generates the pdf
                    patientRecord.AttachedFile = Path.GetFileName(fileName);

                    db.SaveChanges();

                    // go back to the list -- fyi, if you are expert it will autolog you off
                    Response.Redirect("MobileList.aspx", false);
                }
                else
                {
                    db.SaveChanges();
                }
            }
        }

        private string ExportToPdfComplete(DataPatientRequest patientRequest)
        {
            //var pageNo = this.Request.QueryString["y"];

            var reportProcessor = new ReportProcessor();
            var deviceInfo = new Hashtable();
            const string reportName =
                "SmartPraxis.ReportBuilder.MRICompletedReport3, SmartPraxis, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            var typeReportSource = new TypeReportSource() { TypeName = reportName };

            typeReportSource.Parameters.Add("patientGuid", patientRequest.PatientGuid.ToString());
            typeReportSource.Parameters.Add("pageNo", patientRequest.BluePageAnswerPageNo);

            typeReportSource.Parameters.Add("scanDate", patientRequest.MRIScanDatePerformed);
            typeReportSource.Parameters.Add("scanTime", patientRequest.MRIScanTimePerformed);
            typeReportSource.Parameters.Add("modelDescription", patientRequest.DeviceModelDescription);
            typeReportSource.Parameters.Add("modelSN", patientRequest.DeviceCoDeviceModelSN);
            typeReportSource.Parameters.Add("lead1Model", patientRequest.LeadNo1Model);
            typeReportSource.Parameters.Add("lead1SN", patientRequest.LeadNo1ModelSerialNumber);
            typeReportSource.Parameters.Add("lead2Model", patientRequest.LeadNo2Model);
            typeReportSource.Parameters.Add("lead2SN", patientRequest.LeadNo2ModelSerialNumber);
            typeReportSource.Parameters.Add("lead3Model", patientRequest.LeadNo3Model);
            typeReportSource.Parameters.Add("lead3SN", patientRequest.LeadNo3ModelSerialNumber);

            var deviceProgrammed = patientRequest.IsDeviceProgrammedBackToOriginalSettings == true ? "Yes" : "No";
            var deviceAutomatic = patientRequest.IsDeviceSetupForAutomaticReporgrammingAtMRI == true ? "Yes" : "No";
            var deviceBaseline = patientRequest.IsBaselineSettingsRequiredReprogramming == true ? "Yes" : "No";
            typeReportSource.Parameters.Add("isDeviceProgrammedBack", deviceProgrammed);
            typeReportSource.Parameters.Add("isDeviceAutomatic", deviceAutomatic);
            typeReportSource.Parameters.Add("isBaselineRequired", deviceBaseline);

            typeReportSource.Parameters.Add("commentsBefore", patientRequest.DeviceCoCommentsAtTimeOfProcedure);
            typeReportSource.Parameters.Add("commentsAfter", patientRequest.DeviceCoCommentsAfterOfProcedure);
            typeReportSource.Parameters.Add("deviceCoRepAtTimeOfProcedure", patientRequest.DeviceCoRepAtTimeOfProcedure);
            
            var result = reportProcessor.RenderReport("PDF", typeReportSource, deviceInfo);
            var fileName = "Complete-" + patientRequest.PatientGuid + "." + result.Extension;
            var filePath = this.MapPath("~/Documents/" + fileName);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            // this looks like it does all the work
            using (var fs = new FileStream(filePath, FileMode.Create))
            {
                fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
            }
            System.Threading.Thread.Sleep(1000);  // sleep for 1 second

            return filePath;
        }

        protected void ImageButtonPostMRI_Click(object sender, ImageClickEventArgs e)
        {
            if (sender == ImageButtonPostMRI1)
            {
                this.ImageButtonPostMRI1.ImageUrl = "/Content/Images/CheckOnsmall.png";
                this.ImageButtonPostMRI2.ImageUrl = "/Content/Images/CheckOffsmall.png";
                this.ImageButtonPostMRI3.ImageUrl = "/Content/Images/CheckOffsmall.png";
            }
            else if (sender == ImageButtonPostMRI2)
            {
                this.ImageButtonPostMRI1.ImageUrl = "/Content/Images/CheckOffsmall.png";
                this.ImageButtonPostMRI2.ImageUrl = "/Content/Images/CheckOnsmall.png";
                this.ImageButtonPostMRI3.ImageUrl = "/Content/Images/CheckOffsmall.png";
            }
            else
            {
                this.ImageButtonPostMRI1.ImageUrl = "/Content/Images/CheckOffsmall.png";
                this.ImageButtonPostMRI2.ImageUrl = "/Content/Images/CheckOffsmall.png";
                this.ImageButtonPostMRI3.ImageUrl = "/Content/Images/CheckOnsmall.png";
            }
        }

        protected void btnSavePostMRIComments_Click(object sender, EventArgs e)
        {
            this.Save();
        }

        protected void btnSavePreMRIComments_Click(object sender, EventArgs e)
        {
            this.Save();
            Telerik.Web.UI.RadScriptManager.RegisterStartupScript(this, GetType(), "btnSavePreMRIComments_Click", "ShowPreMriSavedBanner();", true);
        }

        protected void Lead1ModelTextChanged(object sender, EventArgs e)
        {
            this.LeadNo1Model.Text = this.LeadNo1Model.Text.ToUpper();
        }

        protected void Lead1TextChanged(object sender, EventArgs e)
        {
            this.LeadNo1ModelSerialNumber.Text = this.LeadNo1ModelSerialNumber.Text.ToUpper();
        }

        protected void Lead2ModelTextChanged(object sender, EventArgs e)
        {
            this.LeadNo2Model.Text = this.LeadNo2Model.Text.ToUpper();
        }

        protected void Lead2SNTextChanged(object sender, EventArgs e)
        {
            this.LeadNo2ModelSerialNumber.Text = this.LeadNo2ModelSerialNumber.Text.ToUpper();
        }

        protected void Lead3ModelTextChanged(object sender, EventArgs e)
        {
            this.LeadNo3Model.Text = this.LeadNo3Model.Text.ToUpper();
        }

        protected void Lead3SNTextChanged(object sender, EventArgs e)
        {
            this.LeadNo3ModelSerialNumber.Text = this.LeadNo3ModelSerialNumber.Text.ToUpper();
        }

        protected void RequiredControlValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Control controlToValidate = Panel1.FindControl(((CustomValidator)source).ControlToValidate);

            if (controlToValidate is System.Web.UI.WebControls.TextBox)
            {
                args.IsValid = !string.IsNullOrWhiteSpace(((System.Web.UI.WebControls.TextBox)controlToValidate).Text);
            }
            else if (controlToValidate is System.Web.UI.WebControls.DropDownList)
            {
                args.IsValid = ((System.Web.UI.WebControls.DropDownList)controlToValidate).SelectedIndex > 0;
            }
            else
            {
                throw new NotSupportedException("The server method 'RequiredControlValidator_ServerValidate' has not been configured to use this type of control.");
            }

            ((WebControl)controlToValidate).BackColor = args.IsValid ? Color.White : Color.FromArgb(0xffdae0); // 0xffdae0 is a reddish color indicating a required field that needs to be filled in.
        }

        protected void PostMriImageButtonValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = 
                ImageButtonPostMRI1.ImageUrl.EndsWith("CheckOnsmall.png") || // 'CheckOnsmall.png' is the white checkbox image, indicating that an ImageButton has been clicked
                ImageButtonPostMRI2.ImageUrl.EndsWith("CheckOnsmall.png") || 
                ImageButtonPostMRI3.ImageUrl.EndsWith("CheckOnsmall.png");
        }

        private Guid PatientGuid
        {
            get
            {
                return Guid.Parse(this.Request.QueryString["x"]);
            }
        }
    }
}

