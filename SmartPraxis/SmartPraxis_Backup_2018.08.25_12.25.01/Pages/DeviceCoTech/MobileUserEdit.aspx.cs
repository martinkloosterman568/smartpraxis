﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class MobileUserEdit : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        public static string TypedPassword;
        public static int strRoleId;
        static readonly GeneralHelper dHelper = new GeneralHelper();

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific for this form
            var responsed = string.Empty;
            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            if ((this.Session["Role"]?.ToString() == "MRI Center") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if ((this.Session["Role"]?.ToString() == "Device Co") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Role.Items.Count == 0)
                {
                    this.PopulateRoles();
                }

                var id = this.Request.QueryString["id"];
                if (!string.IsNullOrEmpty(id))
                {
                    const string tableName = "DataUserMast"; // <-- put table name here
                    const string fieldName = "UserGuid"; // <-- put the field name here
                    string outError;
                    
                    var repository = new UserListRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByUserGuid(tableName, fieldName, id, out outError);
                    var result = destination.FirstOrDefault();
                    if (result != null)
                    {
                        this.FindOnReturnOfRedirectId(result.ID.ToString());
                    }

                    UserName.Text = result.UserName;
                    UserName.Enabled = false;
                }
            }
        }

        private void PopulateRoles()
        {
            List<ListRoleMast> roles;
            var role = this.Session["Role"]?.ToString();
            if (this.Session["Role"]?.ToString() == "Super Admin")
            {
                roles = (from d in this.db.ListRoleMasts orderby d.DisplayName select d).ToList();
            }
            else if (this.Session["Role"]?.ToString() == "MRI Center")
            {
                roles = (from d in this.db.ListRoleMasts where d.DisplayName == "MRI Tech" || d.DisplayName == "MRI Doctor" 
                             orderby d.DisplayName select d).ToList();
            }
            else if (this.Session["Role"]?.ToString() == "Device Co")
            {
                roles = (from d in this.db.ListRoleMasts where d.DisplayName == "Device Co Tech"
                         orderby d.DisplayName select d).ToList();
            }
            else
            {
                // device co tech
                // mri doctor
                // mri tech
                roles = (from d in this.db.ListRoleMasts
                    where d.DisplayName == role
                         orderby d.DisplayName
                    select d).ToList();
            }

            // add from database
            this.Role.DataSource = roles;
            this.Role.DataValueField = "ID";
            this.Role.DataTextField = "DisplayName";
            
            // add extra row
            this.Role.Items.Insert(0, new ListItem("- Select One -", "- Select One -"));
            this.Role.DataBind();
            this.Role.Enabled = string.IsNullOrWhiteSpace(this.Request.QueryString["id"]);
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataUserMast"; // <-- put table name here
            const string fieldName = "ID"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls

        // these controls are actually on the page
        protected void btnnew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/DeviceCoTech/MobileUserEdit.aspx");
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/DeviceCoTech/MobileUserList.aspx");
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }
        #endregion

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.Response.Redirect("/Pages/DeviceCoTech/MobileUserList.aspx");
        }

        public string SaveRecord(List<string> arr)
        {
            #region "Entry Table"

            string ret;
            var ret2 = "0";
            var ret3 = "0";
            var ret4 = "0";
           
            var fieldname = string.Empty;
            try
            {
                object destination = new DataUserMast();
                // this line needs to be changed to be table specific <<---------------------------------------------------------
                var repository = new UserListRepository(new DB_SmartPraxisEntities());
                // this line needs to be changed to be Repository specific <<---------------------------------------------------------

                #region hidden

                var obj = new ControlSaver();

                var id = obj.GetIdFromControls(ref destination, arr, ref fieldname);
                if (id != string.Empty)
                {
                    var id2 = int.Parse(id);
                    repository.GetById(id2);
                }

                obj.SaveControls(ref destination, arr);
                
                #endregion

                var saving = (DataUserMast)destination;
                // this line here needs the model object <<---------------------------------------------------------

                var myobj = new MobileUserEdit();

                if (!string.IsNullOrEmpty(saving.Role))
                {
                    saving.RoleID = myobj.GetRole(saving.Role);
                }

                #region hidden

                saving.LastModified = GetTimeNow.GetTime();
                saving.FullName = saving.FirstName + " " + saving.LastName;

                if (saving.UserGuid == null) saving.UserGuid = Guid.NewGuid();
                saving.LastUpdatePerson = myobj.GetLastUserUpdated();
                if (saving.Password != null) saving.Password = dHelper.Encrypt(saving.Password);

                if (saving.EmailAddress != null)
                {
                    if (saving.EmailAddress != string.Empty)
                    {
                        saving.IsSendEmail = true;
                    }
                    else
                    {
                        saving.IsSendEmail = false;
                    }
                }
                else
                {
                    saving.IsSendEmail = false;
                }

                if (saving.CellPhone != null)
                {
                    if (saving.CellPhone != string.Empty)
                    {
                        saving.IsSendText = true;
                    }
                    else
                    {
                        saving.IsSendText = false;
                    }
                }
                else
                {
                    saving.IsSendText = false;
                }

                if (!string.IsNullOrEmpty(saving.CellPhone))
                {
                    // remove anything but numbers
                    var tmpPhone = Regex.Replace(saving.CellPhone, "[^0-9.]", "");

                    //format the phone number
                    saving.CellPhone = Regex.Replace(tmpPhone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                }

                if (!string.IsNullOrEmpty(saving.OfficePhone))
                {
                    // remove anything but numbers
                    var tmpPhone = Regex.Replace(saving.OfficePhone, "[^0-9.]", "");

                    //format the phone number
                    saving.OfficePhone = Regex.Replace(tmpPhone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                }

                var typeofaction = string.Empty;
                if (id == string.Empty)
                {
                    repository.Add(saving);
                    typeofaction = "Add";
                }
                else if (id != string.Empty)
                {
                    repository.Update(saving);
                    typeofaction = "Update";
                }

                repository.Save();

                if (typeofaction == "Add" && this.CellPhone.Text != string.Empty)
                {
                    if (this.CellPhone.Text.Substring(0, 1) != "1")
                    {
                        this.CellPhone.Text = @"1" + Regex.Replace(this.CellPhone.Text, "[^0-9.]", "");
                    }
                    else
                    {
                        this.CellPhone.Text = Regex.Replace(this.CellPhone.Text, "[^0-9.]", "");
                    }

                    var phones = new List<string>
                    {
                        this.CellPhone.Text
                    };

                    SendTextMsg.TextMessage(phones, "Welcome to Smart-Praxis you can login @ http://mrisafemode.com", this.Session["CompanyName"]?.ToString());
                }

                #endregion

                ret = saving.ID.ToString();
            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    ret4 = "Error, the user name already exists";
                }

                ret = "failure";
            }

            #endregion
            
            var ret5 = $"success|{ret}|{ret2}|{ret3}|{ret4}";
            return ret5;
        }

        private int GetRole(string rol)
        {
            if (!string.IsNullOrEmpty(rol))
            {
                var result = (from d in this.db.ListRoleMasts
                              where d.DisplayName == rol
                              select d).FirstOrDefault();
                if (result != null)
                {
                    // update ** but nothing to do here
                    return result.ID;
                }
            }
            return 0;
        }

        public static bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, "^\\d+$");
        }

        //public string GetAccountId()
        //{
        //    return this.Session["AccountGuid"].ToString();
        //}

        public string GetLastUserUpdated()
        {
            return this.Session["UserName"].ToString();
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields();
            if (result != null)
            {
                var valid = this.SaveRecord(result);
                if (!valid.Contains("failure"))
                {
                    this.Response.Redirect("/Pages/DeviceCoTech/MobileUserList.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotification('hello')", true);
                }
            }
        }

        protected void btnsubmit2a_Click(object sender, EventArgs e)
        {
            var result = this.CheckFields();
            if (result != null)
            {
                this.SaveRecord(result);
                this.Response.Redirect("/Pages/DeviceCoTech/MobileUserList.aspx");
            }
        }

        private List<string> CheckFields()
        {
            var required = false;
            var arr = new List<string>();

            var id = this.Request.QueryString["id"];
            
            foreach (var text in this.Panel1.Controls)
            {
                if (text.ToString().Contains("TextBox"))
                {
                    var btn = (TextBox)text;
                    if (btn.Text == string.Empty && btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }

                    if (btn.ClientID.Contains("Password") && btn.Text != string.Empty && id == null)
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                    else if (btn.ClientID.Contains("Password") && btn.Text == string.Empty)
                    {
                        const string tableName = "DataUserMast"; // <-- put table name here
                        const string fieldName = "UserGuid"; // <-- put the field name here
                        string outError;
                        if (id == null)
                        {
                            continue;
                        }

                        var repository = new UserListRepository(new DB_SmartPraxisEntities());
                        var destination = repository.FindAllByUserGuid(tableName, fieldName, id, out outError);
                        var result = destination.FirstOrDefault();
                        if (result != null)
                        {
                            var pwd = dHelper.Decrypt(result.Password);
                            arr.Add($"{btn.ClientID}:{pwd}");
                            required = false;
                            btn.BackColor = Color.FromArgb(255,255,255);
                        }
                    }
                    else
                    {
                        arr.Add($"{btn.ClientID}:{btn.Text}");
                    }
                }
                else if (text.ToString().Contains("Combo") || text.ToString().Contains("Drop"))
                {
                    var btn = (DropDownList)text;
                    if ((btn.Text == string.Empty || btn.Text == "- Select One -")&& btn.SkinID == "Required")
                    {
                        btn.BackColor = Color.FromArgb(0xffdae0);
                        required = true;
                    }
                    else
                    {
                        btn.BackColor = Color.White;
                    }
                    arr.Add($"{btn.ClientID}:{btn.SelectedItem.Text}");
                }
                else if (text.ToString().Contains("Hidden"))
                {
                    var btn = (HiddenField)text;
                    arr.Add($"{btn.ClientID}:{btn.Value}");
                }
            }

            if (required)
            {
                return null;
            }

            return arr;
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/DeviceCoTech/MobileUserList.aspx");
        }

        protected void txtPassword_TextChanged(object sender, EventArgs e)
        {
            TypedPassword = this.Password.Text;
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            var roleSelected = this.Role.SelectedItem;
            if (IsNumeric(roleSelected.ToString()))
            {
                strRoleId = int.Parse(roleSelected.ToString());
            }
            else
            {
                this.Role.BackColor = Color.FromArgb(0xffdae0);
            }
        }

        protected void ImageButtonShowPassword_Click(object sender, ImageClickEventArgs e)
        {
            if (this.Password.TextMode == TextBoxMode.SingleLine)
            {
                this.ImageButtonShowPassword.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                this.Password.TextMode = TextBoxMode.Password;
            }
            else
            {
                this.ImageButtonShowPassword.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                this.Password.TextMode = TextBoxMode.SingleLine;
            }
        }
    }
}