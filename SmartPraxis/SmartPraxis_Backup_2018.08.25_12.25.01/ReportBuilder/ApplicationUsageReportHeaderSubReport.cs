namespace SmartPraxis.ReportBuilder
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ApplicationUsageReportHeaderSubReport.
    /// </summary>
    public partial class ApplicationUsageReportHeaderSubReport : Telerik.Reporting.Report
    {
        public ApplicationUsageReportHeaderSubReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}