namespace SmartPraxis.ReportBuilder
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ApplicationUsageReportDetailSubReport.
    /// </summary>
    public partial class ApplicationUsageReportDetailSubReport : Telerik.Reporting.Report
    {
        public ApplicationUsageReportDetailSubReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}