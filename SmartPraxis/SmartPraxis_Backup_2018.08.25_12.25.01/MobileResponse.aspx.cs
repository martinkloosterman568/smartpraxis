﻿using System;
using System.Web.UI;
using SmartPraxis.Helper;
using SmartPraxis.Models;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class MobileResponse : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        public static string TypedPassword;
        public static int strRoleId;
        static readonly GeneralHelper dHelper = new GeneralHelper();

        void Page_PreInit(Object sender, EventArgs e)
        {
            //if (this.Session["Role"] == null)
            //{
            //    this.Session.Abandon();
            //    this.Response.Redirect(Global.returnToPage);
            //}
            //this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            //var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            //var formname = Path.GetFileName(this.Request.PhysicalPath);
            //var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            //if (!FormsAuthorizer.CheckAuthorize(forms))
            //{
            //    this.Session.Abandon();
            //    this.Response.Redirect(Global.returnToPage);
            //}
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            // need to make a change to the send part in patient to add a value to end of 8 chars to be 9th, this way I can track the person I send it to
            if (this.Request.QueryString["r"] != null)
            {
                var result = this.Request.QueryString["r"];
                if (result == "1")
                {
                    this.Error.Visible = true;
                }
                else if(result == "2")
                {
                    this.Accepted.Visible = true;
                }
                else if (result == "3")
                {
                    this.Rejected.Visible = true;
                }
                else if (result == "4")
                {
                    this.Commented.Visible = true;
                }

                this.Session.Abandon();
            }
            else
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
        }
    }
}