﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SmartPraxis.Shared
{
    public static class FillDataSet
    {
        public static DataSet FillDs(string procName, SqlParameter[] sqlparms)
        {
            var ds = new DataSet();
            var command = new SqlCommand();

            var cs = ConfigurationManager.ConnectionStrings[Global.entityName].ConnectionString;
            var index1 = cs.ToLower().IndexOf("data source", StringComparison.Ordinal);
            var index2 = cs.ToLower().IndexOf("app", StringComparison.Ordinal);
            var connectionString = cs.Substring(index1, index2 - index1);

            using (var connection = new SqlConnection(connectionString))
            {
                command.Connection = connection;
                connection.Open();

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = procName;
                if (sqlparms != null)
                {
                    command.Parameters.Clear();
                    command.Parameters.AddRange(sqlparms);
                }

                try
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
                catch(Exception)
                {
                    // ignore
                }
                finally
                {
                    connection.Close();
                }
            }

            return ds;
        }

        public static DataTable FillDt(string procName, SqlParameter[] sqlparms)
        {
            var ds = FillDs(procName, sqlparms);
            if (ds.Tables.Count > 0)
            {
                var result = ds.Tables[0];
                return result;
            }
            return null;
        }
    }
}