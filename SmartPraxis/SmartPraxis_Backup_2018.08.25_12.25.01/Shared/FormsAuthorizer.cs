﻿using System.Linq;
using System.Web;

namespace SmartPraxis.Shared
{
    public static class FormsAuthorizer
    {
        public static bool CheckAuthorize(FormAuthorizer form)
        {
            var userid = HttpContext.Current.Session["UserID"];
            if (userid == null)
            {
                return false;
            }

            // fix this to add Role, or pass in 
            var verify = (from item in Global.formAuth where item.FormName == form.FormName && item.Path == form.Path && item.Role == form.Role select item)
                .FirstOrDefault();

            return verify != null;
        }
    }
}