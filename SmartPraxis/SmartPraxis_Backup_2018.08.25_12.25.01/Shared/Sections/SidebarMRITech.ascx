﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarMRITech.ascx.cs" 
    Inherits="SmartPraxis.Shared.Sections.SidebarMRITech" %>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav wiwet-navigation">
        <li class="sidebar-brand hidden-xs">
            <a href="/">
                Smart-Praxis
            </a>
        </li>
        
        <script type="text/javascript">
            function GetTimeZoneOffset() {
                var d = new Date();
                var gmtOffSet = -d.getTimezoneOffset();
                var gmtHours = Math.floor(gmtOffSet / 60);
                var GMTMin = Math.abs(gmtOffSet % 60);
                var dot = ".";
                var retVal = "" + gmtHours + dot + GMTMin;
                document.getElementById('<%= this.screenSize.ClientID%>').value = screen.width + ';' + screen.height;
            }
        </script>
        <asp:HiddenField ID="screenWidth" runat="server" />
        <asp:HiddenField ID="screenHeight" runat="server" />
        <div style="display:none">
            <asp:TextBox BorderColor="white" BackColor="yellow"  ID="screenSize" placeholder="screenSize" runat="server"></asp:TextBox>    
        </div>

        <li style="display: none;">
            <a href="/Pages/MRITech/Dashboard.aspx"><i class="fa fa-home"></i> Dashboard</a>
        </li>
        <li>
            <a href="/Pages/MRICompany/PatientList.aspx"><i class="fa fa-folder"></i> Patient Order Requests</a>
        </li>
        <li>
            <a href="/Pages/Account/UserList.aspx"><i class="fa fa-child"></i> My Profile</a>
        </li>   
        
        <li style="margin-top:100px">
            <a href="/Pages/Contactus.aspx?y=R"><i class="fa fa-life-ring"></i> Contact Us</a>
        </li>   
        <li class="ui-components">
            <a role="button" id="urgent" data-toggle="collapse" href="#collapseUI2" aria-expanded="false" aria-controls="collapseUI">
                <i class="fa fa-stethoscope"></i> Urgent Contact
            </a>
            <div class="collapse" id="collapseUI2">
                <ul>
                    <li><a href="#">(954) 261-3262</a></li>
                </ul>
            </div>
        </li>
        <li>
            <a href="/Pages/Contactus.aspx?y=E"><i class="fa fa-life-ring"></i> Expert Consult</a>
        </li> 
        <li>
            <asp:HyperLink ID="HyperLink1" runat="server"><i class="fa fa-legal"></i> Terms</asp:HyperLink>
        </li>  
        <li>
            <a href="/Index.aspx"><i class="fa fa-power-off"></i> Logout</a>
        </li> 
    </ul>
</div>