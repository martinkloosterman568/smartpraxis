﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="SmartPraxis.Shared.Sections.Header" %>
<h1 class="wiwet-wildomar visible-xs">Smart-Praxis</h1>
<div class="top-bar-mobile visible-xs">
    <div class="row">
        <div class="col-xs-6" style="position: relative; top: -5px;">
            <a href="#menu-toggle" id="menu-toggle">
            <i  class="fa fa-bars"></i>
            </a>
        </div>
        <%--<div class="col-xs-6">
            <p>
                <asp:Label ID="lblMobilePageTitle" runat="server" Text="SmartPraxis" />
            </p>
        </div>--%>
    </div>
</div>
<div class="top-bar hidden-xs">
    <div class="row">
        <div class="col-sm-5" style="position: relative; top: -5px;">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        <a href="#"><i class="fa fa-bell-o"></i></a>
        <div class="dropdown">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <%=this.Session["UserName"].ToString() %>
            <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dLabel">
            <li><a href="../../Login.aspx">Log Off</a></li>
            </ul>
        </div>
        <a href="#"><i class="fa fa-user"></i></a>
    </div>
</div>