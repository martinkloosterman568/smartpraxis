﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarNotify.ascx.cs" 
    Inherits="SmartPraxis.Shared.Sections.SidebarNotify" %>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav wiwet-navigation">
        <li class="sidebar-brand hidden-xs">
            <a href="/">
                Smart-Praxis
            </a>
        </li>
    </ul>
</div>