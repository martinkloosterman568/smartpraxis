﻿namespace SmartPraxis.Shared
{
    public static class PreInitMasterPageFile
    {
        public static string MasterPageChooser(string role)
        {
            var results = string.Empty;

            if (role.Contains("Super Admin"))
            {
                results = "~/Shared/MasterPages/SiteLayoutSuperAdmin.Master";
            }
            if (role.Contains("MRI Center"))
            {
                results = "~/Shared/MasterPages/SiteLayoutMRICompany.Master";
            }
            if (role.Contains("MRI Tech"))
            {
                results = "~/Shared/MasterPages/SiteLayoutMRITech.Master";
            }
            if (role.Contains("MRI Doctor"))
            {
                results = "~/Shared/MasterPages/SiteLayoutMRIDoctor.Master";
            }
            if (role.Contains("Device Co Tech"))
            {
                results = "~/Shared/MasterPages/SiteLayoutDeviceCoTech.Master";
            }
            else if (role.Contains("Device Co"))
            {
                results = "~/Shared/MasterPages/SiteLayoutDeviceCompany.Master";
            }
            else if (role.Contains("Expert"))
            {
                results = "~/Shared/MasterPages/SiteLayoutEmpty.Master";
            }

            return results;
        }
    }
}