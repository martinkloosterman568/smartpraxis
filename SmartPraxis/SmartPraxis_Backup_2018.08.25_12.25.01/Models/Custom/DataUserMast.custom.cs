﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SmartPraxis.Helper;
using SmartPraxis.Shared;
using Newtonsoft.Json;

namespace SmartPraxis.Models
{
    public partial class DataUserMast
    {
        public static bool Login(Guid userGuid, string screenSize = null)
        {
            return Authenticate(new SqlParameter[3]
            {
                new SqlParameter("@valuePassedIn", string.Empty),
                new SqlParameter("@filter", userGuid.ToString()),
                new SqlParameter("@typeOfUser", "U")
            }, screenSize);
        }

        public static bool Login(string userName, string password, string screenSize = null)
        {
            if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
            {
                return Authenticate(new SqlParameter[3]
                {
                    new SqlParameter("@valuePassedIn", string.Empty),
                    new SqlParameter("@filter", JsonConvert.SerializeObject(new { UserName = userName, Password = new GeneralHelper().Encrypt(password) })),
                    new SqlParameter("@typeOfUser", "Login")
                }, screenSize);
            }

            return false;
        }

        private static bool Authenticate(SqlParameter[] dbParams, string screenSize = null)
        {
            // Authenticate the user with spFilterUserList
            DataView dv = FillDataSet.FillDt("spFilterUserList", dbParams).DefaultView;

            if (dv.Count == 1)
            {
                // Load FormAuthorizers
                using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                {
                    string role = (string)dv[0]["Role"];

                    Global.formAuth.AddRange(db.FormAuthorizes
                        .Where(formAuthorize => formAuthorize.Role == role)
                        .ToList()
                        .Select(formAuthorize => new FormAuthorizer() { Role = formAuthorize.Role, Path = formAuthorize.Path, FormName = formAuthorize.FormName }));
                }

                // Set DataUserMast fields
                HttpContext.Current.Session["UserID"] = (int)dv[0]["ID"];
                HttpContext.Current.Session["Role"] = (string)dv[0]["Role"];
                HttpContext.Current.Session["UserGuid"] = (Guid)dv[0]["UserGuid"];
                HttpContext.Current.Session["UserName"] = (string)dv[0]["UserName"];
                HttpContext.Current.Session["UserID"] = (int)dv[0]["ID"];
                HttpContext.Current.Session["Role"] = (string)dv[0]["Role"];
                HttpContext.Current.Session["RoleId"] = (int)dv[0]["RoleID"];
                HttpContext.Current.Session["EmailAddress"] = dv[0]["EmailAddress"];
                HttpContext.Current.Session["OfficePhone"] = dv[0]["OfficePhone"];
                HttpContext.Current.Session["CellPhone"] = dv[0]["CellPhone"];

                // Set DataAccount-related fields
                HttpContext.Current.Session["DataAccountIdsDelimited"] = (string)dv[0]["DataAccountIdsDelimited"];
                HttpContext.Current.Session["DataAccountIds"] = dv[0]["DataAccountIdsDelimited"].ToString().Split('|').Select(int.Parse).ToArray();
                HttpContext.Current.Session["CompanyNamesDelimited"] = (string)dv[0]["CompanyNamesDelimited"];
                HttpContext.Current.Session["CompanyNames"] = dv[0]["CompanyNamesDelimited"].ToString().Split('|');
                HttpContext.Current.Session["CompanyTypesDelimited"] = (string)dv[0]["CompanyTypesDelimited"];
                HttpContext.Current.Session["CompanyTypes"] = dv[0]["CompanyTypesDelimited"].ToString().Split('|');

                // Set IsAdminUser field. FYI, only users with 'Device Co' and 'Device Co Tech' roles may have more than 1 DataAccount record, so a Admin user will only have 1 DataAccount record.
                HttpContext.Current.Session["IsAdmin"] = ((string[])HttpContext.Current.Session["CompanyTypes"]).First() == "SuperAdmin";

                if (!string.IsNullOrWhiteSpace(screenSize))
                {
                    if (screenSize.Contains(";"))
                    {
                        HttpContext.Current.Session["ScreenWidth"] = screenSize.Split(';')[0];
                        HttpContext.Current.Session["ScreenHeight"] = screenSize.Split(';')[1];
                    }

                    if (Convert.ToInt32(HttpContext.Current.Session["ScreenWidth"]) < 1366)
                    {
                        HttpContext.Current.Session["UsingDevice"] = "Mobile";
                    }
                    else
                    {
                        HttpContext.Current.Session["UsingDevice"] = Utils.fBrowserIsMobile() ? "Mobile" : "Desktop";
                    }

                    // forcing this so I can test the mobile sides
                    // this.Session["UsingDevice"] = "Mobile";
                }

                if (HttpContext.Current.Session["Role"] != null)
                {
                    HttpContext.Current.Response.Redirect((bool)dv[0]["IsForceChangePassword"] ?
                        "~/Pages/Account/ChangePassword.aspx?id=" + dv[0]["UserGuid"].ToString() :
                        "~/Pages/Account/Notify.aspx");
                }
            }

            return false;
        }
    }
}