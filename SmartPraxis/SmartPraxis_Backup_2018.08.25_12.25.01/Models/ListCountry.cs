//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartPraxis.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ListCountry
    {
        public int CountryId { get; set; }
        public Nullable<System.Guid> CountryGuid { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
