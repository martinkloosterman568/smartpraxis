using System;
using System.Data.Entity;

public class UnitOfWork : IUnitOfWork
{
    private DbContext _context;

    public UnitOfWork(DbContext context)
    {
        this._context = context;
    }

	//Delete this default constructor if using an IoC container
	public UnitOfWork()
	{
		//_context = new DbContext();
	}
	
    
    public void Save()
    {
        this._context.SaveChanges();
    }

    public void Dispose()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (this._context != null)
            {
                this._context.Dispose();
                this._context = null;
            }
        }
    }
}
