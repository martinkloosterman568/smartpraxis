﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using SmartPraxis.Models;
using SmartPraxis.Shared;

namespace SmartPraxis.Helper
{
    public class GeneralHelper
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        public string Encrypt(string clearText)
        {
            var EncryptionKey = "MAKV2SPBNI99212";
            var clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor != null)
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            return clearText;
        }

        public string Decrypt(string cipherText)
        {
            var EncryptionKey = "MAKV2SPBNI99212";
            var cipherBytes = Convert.FromBase64String(cipherText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor != null)
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            return cipherText;
        }

        public string GetUserGuid()
        {
            return HttpContext.Current.Session["UserGuid"].ToString();
        }

        //public void LogAction(String message)
        //{
        //    try
        //    {
        //        if (HttpContext.Current.Session["UserID"] != null)
        //        {
        //            var browser = HttpContext.Current.Request.Browser;
        //            var userID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
        //            var lg = new UserLog();
        //            lg.Message = message;
        //            lg.MoreInfo = browser.Browser + " " + browser.Version;
        //            lg.LogTime = GetTimeNow.GetTimeNow();
        //            lg.UserID = userID;
        //            lg.IPAddress = this.GetVisitorIpAddress();
        //            this.db.UserLogs.Add(lg);
        //            this.db.SaveChanges();
        //        }
        //    }
        //    finally
        //    {
        //    }
        //}


        // get visitor ip address
        public string GetVisitorIpAddress()
        {
            var ipAdd = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipAdd))
                ipAdd = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            return ipAdd;
        }

        // generate module url
        //public string GetModuleSlug(String moduleName, int parentModuleID)
        //{
        //    var result = string.Empty;
        //    moduleName = moduleName.Replace(" ", "");
        //    if (String.IsNullOrEmpty(result))
        //        result = moduleName;
        //    if (parentModuleID > 0)
        //        result = this.GetModuleByID(parentModuleID).ModuleName.Replace(" ", "") + "_" + result;
        //    result = "li" + result;
        //    return this.RemoveIllegalCharacters(result);
        //}

        //public ModuleMast GetModuleByID(int? categoryID)
        //{
        //    var module = (from c in this.db.ModuleMasts
        //                    where c.ID == categoryID
        //                    select c).FirstOrDefault();
        //    return module;
        //}

        private string RemoveExtraHyphen(string text)
        {
            if (text.Contains("__"))
            {
                text = text.Replace("__", "_");
                return this.RemoveExtraHyphen(text);
            }
            return text;
        }

        private string RemoveDiacritics(string text)
        {
            var normalize = text.Normalize(NormalizationForm.FormD);
            var sb = new StringBuilder();
            for (var i = 0; i <= normalize.Length - 1; i++)
            {
                var c = normalize[i];
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public string RemoveIllegalCharacters(string text)
        {
            if (string.IsNullOrEmpty(text)) return text;

            text = text.Replace(":", string.Empty);
            text = text.Replace("/", string.Empty);
            text = text.Replace("?", string.Empty);
            text = text.Replace("#", string.Empty);
            text = text.Replace("[", string.Empty);
            text = text.Replace("]", string.Empty);
            text = text.Replace("@", string.Empty);
            text = text.Replace(",", string.Empty);
            text = text.Replace("\"", string.Empty);
            text = text.Replace("&", string.Empty);
            text = text.Replace(".", string.Empty);
            text = text.Replace("'", string.Empty);
            //text = text.Replace("_", string.Empty);
            text = text.Replace(" ", "-");
            text = this.RemoveDiacritics(text);
            text = this.RemoveExtraHyphen(text);

            var urlEncode = HttpUtility.UrlEncode(text.ToLower());
            if (urlEncode != null)
            {
                return urlEncode.Replace("%", string.Empty);
            }

            return text;
        }

        public Bitmap GetScaledPicture(Stream streamImage, int maxWidth)
        {
            var originalImage = new Bitmap(streamImage);
            var ratio = (Double)originalImage.Width / maxWidth;
            var newWidth = maxWidth;
            var newHeight = (int)(originalImage.Height / ratio);

            var newImage = new Bitmap(originalImage, newWidth, newHeight);
            var thumbGraph = Graphics.FromImage(newImage);
            thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbGraph.DrawImage(originalImage, imageRectangle);
            originalImage.Dispose();

            return newImage;
        }

        public String GetTimeAgo(DateTime date)
        {
            var str = "";
            var ts = GetTimeNow.GetTime() - date;

            if (ts.Days < 1)
            {
                if (ts.Hours < 1)
                {
                    if (ts.Minutes < 1)
                        str = "Just now";
                    else if (ts.Minutes > 0 && ts.Minutes < 61)
                        str = ts.Minutes + " mins ago";
                }
                else
                    str = ts.Hours + " hours ago";
            }
            else if ((ts.Days) < 7)
                str = ts.Days + " days ago";
            else
                str = date.ToString("MMM dd,yyyy");

            return str;
        }
    }
}