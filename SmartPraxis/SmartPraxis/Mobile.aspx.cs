﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class Mobile : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();

        void Page_PreInit(Object sender, EventArgs e)
        {
            
        }

        private void PageSetup()
        {
     
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            // need to make a change to the send part in patient to add a value to end of 8 chars to be 9th, this way I can track the person I send it to
            if (this.Request.QueryString["v"] != null)
            {
                var vlookup = this.Request.QueryString["v"];
                var deviceCoTech = new Guid();
                var lookup = (from d in this.db.MobilePageSecurities
                    where d.Patient81 == vlookup
                    select d).FirstOrDefault();

                if (lookup != null)
                {
                    if (lookup.DeviceCoTechGuid != null) deviceCoTech = lookup.DeviceCoTechGuid.Value;
                }
                else
                {
                    this.Response.Redirect("MobileResponse.aspx?r=1");
                }

                if (deviceCoTech != new Guid())
                {
                    this.Session["MobilePageTitle"] = "Dashboard";

                    if (!this.Page.IsPostBack)
                    {
                        var patient8 = vlookup.Substring(0, 8);
                        const string tableName = "DataPatientRequests"; // <-- put table name here
                        const string fieldName = "Patient8"; // <-- put the field name here
                        var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
                        var outError = string.Empty;
                        var destination = repository.FindAllByGuid(tableName, fieldName, patient8, out outError);
                        if (destination != null)
                        {
                            var result = destination.FirstOrDefault();
                            if (result != null)
                            {
                                this.FindOnReturnOfRedirectId(result.PatientId.ToString());
                            }
                        }
                        else
                        {
                            this.Session.Abandon();
                            this.Response.Redirect(Global.returnToPage);
                        }
                    }
                }
                else
                {
                    this.Session.Abandon();
                    this.Response.Redirect(Global.returnToPage);
                }
            }
            else
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataPatientRequests"; // <-- put table name here
            const string fieldName = "PatientId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls

        protected void btnreject_Click(object sender, EventArgs e)
        {
            var vlookup = this.Request.QueryString["v"];
            if (vlookup != null)
            {
                var patient8 = vlookup.Substring(0, 8);
                var myrecord = (from d in this.db.MobilePageSecurities
                    where d.Patient81.Contains(patient8)
                    select d).FirstOrDefault();

                if (myrecord != null)
                {
                    this.CleanUp("Rejected");
                    this.ResponseToRequest("Rejected By Device Tech (No Comments)", "Rejected this invite (No Comments)");
                    this.Response.Redirect("MobileResponse.aspx?r=3");
                }
            }
        }

        #endregion

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            var vlookup = this.Request.QueryString["v"];
            var patient8 = vlookup.Substring(0, 8);
            var myrecord = (from d in this.db.MobilePageSecurities
                            where d.Patient81.Contains(patient8)
                            select d).FirstOrDefault();

            if (myrecord != null)
            {
                this.CleanUp("Accepted");
                this.ResponseToRequest("Accepted By Device Tech (No Comments)", "Agreed to show up for appointment (No Comments)");
                this.Response.Redirect("MobileResponse.aspx?r=2");
            }
        }

        protected void btnComment_Click(object sender, EventArgs e)
        {
            if (this.TechCommentsReply.Text == string.Empty)
            {
                this.TechCommentsReply.BackColor = Color.LightPink;
            }
            else
            {
                var vlookup = this.Request.QueryString["v"];
                if (vlookup != null)
                {
                    var patient8 = vlookup.Substring(0, 8);
                    var myrecord = (from d in this.db.MobilePageSecurities
                        where d.Patient81.Contains(patient8)
                        select d).FirstOrDefault();

                    if (myrecord != null)
                    {
                        this.CleanUp("Comments");
                        this.ResponseToRequest("Accept/Reply/Comment", this.TechCommentsReply.Text);
                        this.Response.Redirect("MobileResponse.aspx?r=4");
                    }
                }
            }
        }

        protected void btnCommentReject_Click(object sender, EventArgs e)
        {
            if (this.TechCommentsReply.Text == string.Empty)
            {
                this.TechCommentsReply.BackColor = Color.LightPink;
            }
            else
            {
                var vlookup = this.Request.QueryString["v"];
                if (vlookup != null)
                {
                    var patient8 = vlookup.Substring(0, 8);
                    var myrecord = (from d in this.db.MobilePageSecurities
                        where d.Patient81.Contains(patient8)
                        select d).FirstOrDefault();

                    if (myrecord != null)
                    {
                        this.CleanUp("Rejected");
                        this.ResponseToRequest("Reject/Reply/Comment", this.TechCommentsReply.Text);
                        this.Response.Redirect("MobileResponse.aspx?r=4");
                    }
                }
            }
        }

        private void ResponseToRequest(string status, string comment)
        {
            var vlookup = this.Request.QueryString["v"];
            if (vlookup != null)
            {
                var patient8 = vlookup.Substring(0, 8);
                var myrecord = (from d in this.db.MobilePageSecurities
                    join u in this.db.DataUserMasts on d.DeviceCoTechGuid equals u.UserGuid
                    where d.Patient81 == vlookup
                    select u).FirstOrDefault();

                if (myrecord != null)
                {
                    var record = (from d in this.db.DataPatientRequests
                        where d.Patient8 == patient8
                        select d).FirstOrDefault();

                    if (record != null)
                    {
                        record.DeviceCompanyLastStatus = status;
                        record.DeviceCoTechLastUpdateDate = GetTimeNow.GetTime();
                        record.DeviceCoTechUserGuid = myrecord.UserGuid;
                        record.TechCommentsReply = comment;

                        // this is for the overall record
                        record.OverAllStatus = record.DeviceCompanyLastStatus;
                        record.LastUpdatedBy = myrecord.FullName;
                        record.LastUpdatedByGuid = record.DeviceCoTechUserGuid;
                        record.LastUpdatedDate = record.DeviceCoTechLastUpdateDate;
                        this.db.SaveChanges();
                    }
                }
            }
        }

        private void CleanUp(string Status)
        {
            var vlookup = this.Request.QueryString["v"];
            if (vlookup != null)
            {
                var patient8 = vlookup.Substring(0, 8);
                var myrecord = (from d in this.db.MobilePageSecurities
                    where d.Patient81 == vlookup
                    select d).FirstOrDefault();

                if (myrecord != null)
                {
                    var lookup = (from d in this.db.MobilePageSecurities
                        where d.Patient81.Contains(patient8)
                        select d).ToList();

                    // this cleans up any other invitees to this record but 
                    // leaves only the one who responded
                    foreach (var line in lookup)
                    {
                        if (line.Patient81 != vlookup)
                        {
                            this.db.MobilePageSecurities.Remove(line);
                            this.db.SaveChanges();
                        }
                    }

                    // update the patient request to put the company account
                    var update = (from d in this.db.DataPatientRequests
                        where d.Patient8 == patient8
                        select d).FirstOrDefault();
                    if (update != null)
                    {
                        //if (this.Session["AccountGuid"] != null)
                        //{
                        //    update.DeviceCompanyGuid = new Guid(this.Session["AccountGuid"].ToString());
                        //}
                        
                        update.DeviceCoTechLastUpdateDate = DateTime.Now;
                        //update.DeviceCompanyLastStatus = Status;
                        //;update.OverAllStatus = Status;
                        update.PatientStatusDDL = Status;
                        this.db.SaveChanges();
                    }
                }
                else
                {
                    this.Response.Redirect("MobileResponse.aspx?r=1");
                }
            }
        }
    }
}