﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.DeviceCompany
{
    public partial class SelectMRIList : Page
    {
        public int DeviceCo = 0;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (this.Request.QueryString["id"] != "")
            {
                this.DeviceCo = Convert.ToInt32(this.Request.QueryString["id"]);
            }
            if (this.Session["UserID"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (this.Session["Role"].ToString() != "Super Admin" &&
               this.Session["Role"].ToString() != "MRI Center" &&
               this.Session["Role"].ToString() != "Device Co" && 
               this.Session["Role"].ToString() != "HCO Admin" &&
               this.Session["Role"].ToString() != "Company Admin"
               )
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            var responsed = string.Empty;

            if ((this.Session["Role"].ToString() == "MRI Center") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            //if ((this.Session["Role"].ToString() == "Device Co") && (responsed == string.Empty))
            //{
            //    this.Session.Abandon();
            //    this.Response.Redirect(Global.returnToPage);
            //}

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] != null)
                {
                    this.ViewState["sort"] = "GroupName ASC, CompanyName ASC";
                    this.PopulateGrid();
                }
                else
                {
                    // var id = Convert.ToInt32(this.Request.QueryString["id"]);
                    string outError;
                    var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                    var resp = repository.FindById("DataAccount", "AccountId", this.Request.QueryString["id"], out outError);
                    this.divTitle.InnerText = "Select MRI's for: " + resp.CompanyName;
                }
            }
        }

        private void PopulateGrid()
        {
            var destination = this.GetDataForGrid("GroupName ASC, CompanyName ASC");

            this.gvGrid.DataSource = destination;
            this.gvGrid.DataBind();

            if (destination != null && destination.Rows.Count > 0)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void LinkButtonCheckClick(object sender, EventArgs e)
        {
            LinkButton commandSource = sender as LinkButton;

            string commandText = commandSource.Text;
            string commandName = commandSource.CommandName;
            int commandArgument = Convert.ToInt32(commandSource.CommandArgument);
            if (commandText.Contains("On"))
            {
                var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                repository.UpdateCheck(commandArgument, this.DeviceCo, "Off");
            }
            else
            {
                var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                repository.UpdateCheck(commandArgument, this.DeviceCo, "On");
            }

            var id = this.Request.QueryString["id"];
            this.Response.Redirect("/Pages/DeviceCompany/SelectMRIList.aspx?id=" + id + "&x=edit");
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (this.Session["Role"]?.ToString() != "Super Admin")
                {
                    foreach (DataControlField dcfColumn in this.gvGrid.Columns)
                    {
                        if (dcfColumn.HeaderText == @"Short Company Name")
                        {
                            dcfColumn.Visible = false;
                        }
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow) 
            {
                var selected = DataBinder.Eval(e.Row.DataItem, "Selected").ToString();
                var button = (LinkButton)e.Row.FindControl("LinkButtonCheck");
                if (button != null)
                {
                    button.Visible = true;
                    if (selected == "1")
                    {
                        button.Text = @"<img src='/Content/Images/CheckOnsmallGreyback.png' alt='check' />";
                    }
                    else
                    {
                        button.Text = @"<img src='/Content/Images/CheckOffsmallGreyback.png' alt='check' />";
                    }
                    
                }

            }

                //    var status = DataBinder.Eval(e.Row.DataItem, "Status").ToString().ToLower();
                //    var lStatus = (Literal)e.Row.FindControl("ltrStatus");
                //    if (status.Contains("active"))
                //        lStatus.Text = @"<span class=""label label-primary"">Active</span>";
                //    else if (status.Contains("follow-up"))
                //        lStatus.Text = @"<span class=""label label-warning"">Follow-up</span>";
                //    else if ((status.Contains("sold")) && (status.Contains("installed")))
                //        lStatus.Text = @"<span class=""label label-success"">Installed Sold</span>";
                //    else if ((status.Contains("sold")) && (status.Contains("deposited")))
                //        lStatus.Text = @"<span class=""label label-success"">Deposited</span>";
                //    else if (status.Contains("email"))
                //        lStatus.Text = @"<span class=""label label-info"">Emailed</span>";
                //    else if (status.Contains("lost"))
                //        lStatus.Text = @"<span class=""label label-danger"">Lost</span>";
                //    else if (status.Contains("dead"))
                //        lStatus.Text = @"<span class=""label label-danger"">Dead Quote File</span>";
                //    else if ((status.Contains("invoice")) && (status.Contains("paid")))
                //        lStatus.Text = @"<span class=""label label-success"">Paid</span>";
                //}


                //if (this.Session["Role"].ToString().Contains("MRI Center") ||
                //    this.Session["Role"].ToString().Contains("Device Co"))
                //{
                //    var deletebutton = (LinkButton)e.Row.FindControl("LinkButtonDelete");
                //    if (deletebutton != null)
                //    {
                //        deletebutton.Visible = false;
                //    }
                //}

                //else
                //{
                //    e.Row.Cells[0].Visible = false;
                //}


                /*


                if (this.Session["Role"].ToString().Contains("Super Admin") ||
                   this.Session["Role"].ToString().Contains("MRI Center") ||
                   this.Session["Role"].ToString().Contains("Device Co"))
                {
                    if (deletebutton != null)
                    {
                        deletebutton.Visible = true;    
                    }
                }
                else
                {
                    if (deletebutton != null)
                    {
                        deletebutton.Visible = false;
                    }
                }

                 */

        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/MRI.aspx");
           // this.Response.Redirect("MRI.aspx");
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
            this.PopulateGrid();
        }

        private DataTable GetDataForGrid(string sSort)
        {
            bool role = false;
            var tenent = "";
            if (this.Session["Role"].ToString() == "HCO Admin" || this.Session["Role"].ToString() == "Company Admin")
            {
                role = true;
                tenent = this.Session["TenantID"].ToString();
            }
            var accountId = this.Request.QueryString["id"];
            return FillDataSet.FillDt("spFilterSelectMRIList", new[]
                {
                    new SqlParameter("@valuePassedIn", this.Filter.Text),
                    new SqlParameter("@accountId", accountId),
                    new SqlParameter("@TenentGuid",tenent),
                    new SqlParameter("@IsHcoAdmin",(bool)role)
                });
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }

        protected void Filter_TextChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            this.Filter.Text = string.Empty;
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] s = this.ViewState["sort"].ToString().Split();    //load the last sort
            string sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                    sSort += " DESC";
                else
                    sSort += " ASC";
            }
            else
                sSort += " ASC";

            //find which column is being sorted to change its style
            int i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" + sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            //get the sorted data
            var dt = this.GetDataForGrid(sSort);
            this.gvGrid.DataSource = dt;
            this.gvGrid.DataBind();

            this.gvGrid.DataSource = this.GetDataForGrid(sSort);
            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }


            //save the new sort
            this.ViewState["sort"] = sSort;
        }

        protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            LinkButton commandSource = e.CommandSource as LinkButton;

            string commandText = commandSource.Text;
            string commandName = commandSource.CommandName;
            int commandArgument = Convert.ToInt32(commandSource.CommandArgument);
            if (commandText.Contains("On"))
            {
                var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                repository.UpdateCheck(commandArgument, this.DeviceCo,"Off");
            }
            else
            {
                var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                repository.UpdateCheck(commandArgument, this.DeviceCo, "On");
            }

            var id = this.Request.QueryString["id"];
            this.Response.Redirect("/Pages/DeviceCompany/SelectMRIList.aspx?id=" + id + "&x=edit");
        }
    }
}