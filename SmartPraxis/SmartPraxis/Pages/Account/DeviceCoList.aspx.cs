﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.Account
{
    public partial class DeviceCoList : Page
    {
        void Page_PreInit(object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (this.Session["UserID"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (this.Session["Role"].ToString() != "Super Admin" &&
               this.Session["Role"].ToString() != "MRI Center" &&
               this.Session["Role"].ToString() != "Device Co" &&
               this.Session["Role"].ToString() != "HCO Admin" &&
               this.Session["Role"].ToString() != "Company Admin" &&
               this.Session["Role"].ToString() != "Device Co Tech" &&
               this.Session["Role"].ToString() != "MRI Center Device Operator"
               )
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            var responsed = string.Empty;

            if (this.Request.QueryString["x"] != null)
            {
                // this is a trick to see if someone types the url without x=edit
                responsed = this.Request.QueryString["x"];
            }

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    this.ViewState["sort"] = "GroupName ASC, CompanyName ASC";
                    this.PopulateGrid();
                }
            }
        }

        private void PopulateGrid()
        {
            var destination = this.GetDataForGrid();

            this.gvGrid.DataSource = destination;
            this.gvGrid.DataBind();

            if (destination.Rows.Count > 0)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.Response.Redirect("/Pages/Account/DeviceCoList.aspx");
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (this.Session["Role"]?.ToString() != "Super Admin")
                {
                    foreach (DataControlField dcfColumn in this.gvGrid.Columns)
                    {
                        if (dcfColumn.HeaderText == @"Short Company Name")
                        {
                            dcfColumn.Visible = false;
                        }
                    }
                }
            }
            if (this.Session["Role"].ToString().Contains("Device Co Tech"))
            {
                var edit = (HyperLink)e.Row.FindControl("HyperLink1");
                if (edit != null)
                {
                   // gvGrid.Rows[1].Visible = false;
                   this.gvGrid.Columns[0].Visible = false;
                    edit.Visible = false;
                }

            }

            if (this.Session["Role"].ToString().Contains("MRI Center") ||
                this.Session["Role"].ToString().Contains("Device Co"))
            {
                var deletebutton = (LinkButton)e.Row.FindControl("LinkButtonDelete");
                if (deletebutton != null)
                {
                    deletebutton.Visible = false;
                }
            }
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("DeviceCo.aspx");
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
            this.PopulateGrid();
        }

        private DataTable GetDataForGrid()
        {
            bool role = false;
            var tenent = "";
            if (this.Session["Role"].ToString() == "HCO Admin" || this.Session["Role"].ToString() == "Company Admin")
            {
                role = true;
                tenent = this.Session["TenantID"].ToString();
            }
            return FillDataSet.FillDt("spFilterDeviceCoList", new[]
            {
                new SqlParameter("@valuePassedIn", this.Filter.Text),
                new SqlParameter("@DataAccountIds", (string)this.Session["DataAccountIdsDelimited"]),
                new SqlParameter("@IsAdmin", (bool)this.Session["IsAdmin"]),
                new SqlParameter("@TenentGuid",tenent),
                new SqlParameter("@IsHcoAdmin",(bool)role)
            });
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }

        protected void Filter_TextChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            this.Filter.Text = string.Empty;
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] s = this.ViewState["sort"].ToString().Split();    //load the last sort
            string sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                    sSort += " DESC";
                else
                    sSort += " ASC";
            }
            else
                sSort += " ASC";

            //find which column is being sorted to change its style
            int i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" + sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            //get the sorted data
            var dt = this.GetDataForGrid();
            this.gvGrid.DataSource = dt;
            this.gvGrid.DataBind();

            this.gvGrid.DataSource = this.GetDataForGrid();
            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }
    }
}