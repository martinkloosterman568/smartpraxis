﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeviceCo.aspx.cs" Inherits="SmartPraxis.Pages.Account.DeviceCo" MaintainScrollPositionOnPostback="True"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" Title="Smart-Praxis" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50508.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page on this section.--%>
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <script type="text/javascript">
        function showNotificationAccount(message)
        {
            $find("<%= this.RadNotificationAccount.ClientID %>").show();
        }
        function showNotificationEmail(message)
        {
            $find("<%= this.RadNotificationEmail.ClientID %>").show();
        }
    </script>
    <style type="text/css">
		.gridHeader { font-weight:bold; color:white; background-color:#80a0a0; }
		.gridHeader A { padding-right:15px; padding-left:3px; background-color:#80a0a0; padding-bottom:0px; color:#ffffff; padding-top:0px; text-decoration:none; }
		.gridHeader A:hover { background-color:#80a0a0; text-decoration: underline; }
		.gridHeaderSortASC A { background-color:#80a0a0; background: url(/Content/Images/sortdown.gif) no-repeat 95% 50%; }
		.gridHeaderSortDESC A { background-color:#80a0a0; background: url(/Content/Images/sortup.gif) no-repeat 95% 50%; }
    </style>
    <style type="text/css">
          .autocomplete_CompletionListElement
        {
            margin: 0px;
            background-color: White;
            cursor: default;
            list-style-type: none;
            overflow-y: auto;
            overflow-x: hidden;
            height:180px;
            text-align: left;
            border: 1px solid #777;
            z-index:10000;
        } 

        .modalBackground {
            background-color:Gray;
            filter:alpha(opacity=70);
            opacity:0.7;
        }
        
        .modalPopup {
	        background-color:#ffffdd;
	        border-width:3px;
	        border-style:solid;
	        border-color:Gray;
	        padding:3px;
	        width:250px;
        }
    </style>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
	    var counter;
	    counter = 0;

	    function KeepSessionAlive() {
	        // Increase counter value, so we'll always get unique URL (sample source from page)
	        // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
	        counter++;

	        // Gets reference of image
	        var img = document.getElementById("imgSessionAlive");

	        // Set new src value, which will cause request to server, so
	        // session will stay alive
	        img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;

	        // Schedule new call of KeepSessionAlive function after 60 seconds
	        setTimeout(KeepSessionAlive, 60000);
	    }

	    // Run function for a first time
	    KeepSessionAlive();
	</script> 
   <div>
       <br/><br/>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-lg-10">
                            <div class="panel panel-default">
                                
                                <div style="vertical-align: bottom;" class="panel-heading">
                                    <h4>Add / Edit Device Co</h4>    
                                    <%--<asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New File"   />--%>
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                </div>
                             
                                <div runat="server" id="divbody" class="panel-body">
                                        <asp:Panel ID="Panel1" runat="server">
                                        <asp:HiddenField ID="AccountId" runat="server"/>
                                        <asp:HiddenField ID="UserGuid" runat="server"/>
                                        
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>
                                                        Company Code</label><asp:Label ID="Label1" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:TextBox AutoCompleteType="Disabled" Enabled="false" ID="CompanyCode" alt="required" CssClass="form-control" name="CompanyCode" runat="server" accept="CompanyCode" placeholder="Company Code" ValidationGroup="Required" />
                                                    <asp:CustomValidator ID="csvCompanyCode" runat="server" ValidateEmptyText="true" ControlToValidate="CompanyCode" OnServerValidate="csvRequiredFields_ServerValidate" />
                                                </div>
                                            </div>
                                            <%--<div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>Type</label>
                                                    <asp:DropDownList Style="cursor:pointer;" Enabled="False" ID="TypeOf" runat="server" AppendDataBoundItems="true" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="TypeOf_SelectedIndexChanged" ></asp:DropDownList>
                                                </div>
                                            </div>--%>
                                            <asp:HiddenField ID="TypeOf" Value="Device Company" runat="server"/>
                                            <asp:HiddenField ID="TypeOfGuid" Value="8121748b-f6bf-4edd-94c8-39557232373f" runat="server"/>
                                            <asp:HiddenField ID="IDType" Value="EIN Number" runat="server"/>
                                            <asp:HiddenField ID="IDTypeGuid" Value="8544A327-371D-49B3-82CA-00E2F347C415" runat="server"/>
                                            <asp:HiddenField ID="KeyToAddUsers" runat="server"/>
                                            <asp:HiddenField ID="CreatedDate" runat="server"/>
                                            <asp:HiddenField ID="CreatedBy" runat="server"/>
                                            <%--<div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>ID Type</label><asp:DropDownList Style="cursor:pointer;" ID="IDType" runat="server" AppendDataBoundItems="true" CssClass="form-control" ></asp:DropDownList>
                                                    <asp:HiddenField ID="IDTypeGuid" runat="server"/>
                                                </div>
                                            </div>--%>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>
                                                        EIN Number</label><asp:TextBox AutoCompleteType="Disabled" ID="EINNumber" alt="required" CssClass="form-control" name="einnumber" runat="server" accept="EINNumber" placeholder="EIN Number" ValidationGroup="Required" />
                                                </div>
                                            </div>

                                             <%-- <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>
                                                        Group Name</label><asp:TextBox ID="GroupNameNew" alt="required" CssClass="form-control" name="groupname" runat="server" accept="GroupName" placeholder="Group Name" ValidationGroup="Required" />
                                                </div>
                                            </div>--%>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>
                                                        Company Name</label><asp:Label ID="Label7" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:TextBox AutoCompleteType="Disabled" ID="CompanyName" alt="required" CssClass="form-control" name="companyname" runat="server" accept="CompanyName" placeholder="Company Name" ValidationGroup="Required" SkinID="Required" />
                                                    <asp:CustomValidator ID="csvCompanyName" runat="server" ValidateEmptyText="true" ControlToValidate="CompanyName" OnServerValidate="csvRequiredFields_ServerValidate" />
                                                </div>
                                            </div>
                                            <div id="GroupNameContainer" runat="server" visible="true" class="col-lg-4">
                                                <div class="form-group">
                                                    <label>Group Name</label><asp:Label ID="Label8" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *" />
                                                    <asp:DropDownList ID="GroupName" Visible="True" CssClass="form-control" runat="server" ValidationGroup="Required" SkinID="Required" />
                                                    <asp:CustomValidator ID="csvGroupName" runat="server" ValidateEmptyText="false" ControlToValidate="GroupName" OnServerValidate="csvRequiredFields_ServerValidate" />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <%-- <asp:Button ID="Button1" runat="server" Text="Click here"  OnClick="LinkButtonSelectClick" Class="btn btn-primary" Enabled="false"/>  --%>
                                                    <asp:LinkButton ID="LinkButtonSelect" runat="server" data-toggle="tooltip" data-placement="top"
                                                    title="Select MRIs" 
                                                    OnClick="LinkButtonSelectClick" CommandArgument='<%#this.Eval("AccountId") %>' Class="btn btn-primary">Choose MRI Centers</asp:LinkButton>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                            </div>
                                            <div class="col-lg-2">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>
                                                        Address</label><asp:TextBox AutoCompleteType="Disabled" ID="Address1" alt="required" CssClass="form-control" name="address1" runat="server" accept="Address1" placeholder="Address" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-2">
                                                
                                            </div>
                                        </div>
                                        <%--<div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <asp:TextBox ID="Address2" alt="required" CssClass="form-control" name="address2" runat="server" accept="Address2" placeholder="Address Line 2" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-2">
                                               
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <asp:TextBox ID="Address3" alt="required" CssClass="form-control" name="address3" runat="server" accept="Address3" placeholder="Address Line 3" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-2">
                                                 
                                            </div>
                                        </div>--%>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <asp:TextBox AutoCompleteType="Disabled" ID="City" alt="required" CssClass="form-control" name="city" runat="server" accept="City" placeholder="City" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                             <div class="col-lg-1">
                                                <div class="form-group">
                                                    <label>State</label>
                                                    <asp:DropDownList Style="cursor:pointer;" ID="StateCode" runat="server" AppendDataBoundItems="true" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="StateCode_SelectedIndexChanged" ></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>State Description</label><asp:TextBox AutoCompleteType="Disabled" ID="StateDescription" alt="required" CssClass="form-control" name="statedescription" runat="server" accept="StateDescription" placeholder="State Description" ValidationGroup="Required" />
                                                    <asp:HiddenField ID="StateGuid" runat="server"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                 <div class="form-group">
                                                     <label>Zip</label>
                                                     <asp:TextBox AutoCompleteType="Disabled" ID="ZipCode" alt="required" CssClass="form-control" name="ZipCode" runat="server" accept="ZipCode" placeholder="Zip Code" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-1"></div>
                                        </div>
                                        <div class="row">
                                             <div class="col-lg-1">
                                                <div class="form-group">
                                                        <label>Country</label>
                                                    <asp:DropDownList Style="cursor:pointer;" ID="CountryCode" runat="server" AppendDataBoundItems="true" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="CountryCode_SelectedIndexChanged"  ></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Country Description</label><asp:TextBox AutoCompleteType="Disabled" ID="CountryDescription" alt="required" CssClass="form-control" name="CountryDescription" runat="server" accept="CountryDescription" placeholder="Country Description" ValidationGroup="Required" />
                                                    <asp:HiddenField ID="CountryGuid" runat="server"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        First Name</label><asp:TextBox AutoCompleteType="Disabled" ID="FirstName" alt="required" CssClass="form-control" name="FirstName" runat="server" accept="FirstName" placeholder="First Name" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Last Name</label><asp:TextBox AutoCompleteType="Disabled" ID="LastName" alt="required" CssClass="form-control" name="LastName" runat="server" accept="LastName" placeholder="Last Name" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Phone</label><asp:TextBox AutoCompleteType="Disabled" ID="Phone" alt="required" CssClass="form-control" name="officephone" runat="server" accept="OfficePhone" placeholder="Office Phone" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Fax</label><asp:TextBox AutoCompleteType="Disabled" ID="Fax" alt="required" CssClass="form-control" name="cellphone" runat="server" accept="CellPhone" placeholder="Cell Phone" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Email Address</label><asp:TextBox AutoCompleteType="Disabled" ID="EmailAddress" alt="required" CssClass="form-control" name="emailaddress" runat="server" accept="Email Address" placeholder="Email Address" ValidationGroup="Required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-2"></div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2">
                                            </div>
                                            <div class="col-lg-2">
                                            </div>
                                            <div class="col-lg-2">
                                            </div>
                                            <div class="col-lg-2">
                                            </div>
                                        </div>
                                         <% if (this.Session["Role"].ToString() == "Super Admin")
                                            {
                                         %>
                                        <%--<div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>
                                                        Account Manager</label>
                                                    <asp:TextBox ID="AccountManagerName" alt="" CssClass="form-control" runat="server" accept="UserGuidAccountManager" placeholder="AccountManager" ValidationGroup="NotRequired" SkinID="NotRequired" />
                                                     <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="server"
                                                    CompletionSetCount="20" EnableCaching="true" 
                                                    MinimumPrefixLength="1"  ServiceMethod="GetAccountManagerName" TargetControlID="AccountManagerName" 
                                                    UseContextKey="True" 
                                                    CompletionListCssClass="autocomplete_CompletionListElement">
                                                    </asp:AutoCompleteExtender>
                                                    <asp:HiddenField ID="UserGuidAccountManager" runat="server" />
                                                </div>
                                            </div>
                                        </div>--%>
                                            <% } %>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label>Notes</label>
                                                    <asp:TextBox ID="Notes" AutoCompleteType="Disabled" alt="required" CssClass="form-control" name="notes" runat="server" accept="Notes" placeholder="Notes" ValidationGroup="Required" Rows="5" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.row (nested) -->
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Last Update Person</label>
                                                    <asp:TextBox AutoCompleteType="Disabled" ID="LastUpdatePerson" CssClass="form-control" runat="server" accept="Last Update Person" placeholder="Last Update Person" ValidationGroup="AlwaysDisable" Enabled="False" ReadOnly="True" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Last Update Date</label>
                                                    <asp:TextBox AutoCompleteType="Disabled" ID="LastModified" CssClass="form-control" runat="server" accept="Last Update Date" placeholder="Last Update Date" ValidationGroup="AlwaysDisable" Enabled="False" ReadOnly="True" />
                                                </div>
                                             </div>
                                            <div class="col-lg-2"></div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-2"></div>
                                            
                                        </div>
                                        <div style="float: left;" class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <!-- This html button triggers the jquery above -->
                                                    <div style="float:left" runat="server" id="divBtnSubmit"><asp:Button ID="btnsubmit" runat="server" Text="Save" class="btn btn-success" OnClick="btnsubmit1a_Click" />&nbsp;</div>
                                                    <div style="float:left" runat="server" id="divBtnClear">&nbsp;<asp:Button ID="btnclear" runat="server" Text="Clear" class="btn btn-warning" OnClick="btnclear_Click" /></div>
                                                    <div style="float:left" runat="server" id="divBtnNew">&nbsp;<asp:Button ID="btnnew" runat="server" Text="New" class="btn btn-primary" /></div>
                                                    <div style="float:left" runat="server" id="divBtnCancel">&nbsp;<asp:Button ID="btncancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="btncancel_Click" /></div>
                                                </div>
                                                <br/><br/>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                        <!-- /.row (nested) -->
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-md-10">
                    <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotificationAccount" runat="server" Position="Center"
                             Width="330px" Height="160px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                             Title="Error" Text="Error, the Account name already exists."
                             Style="z-index: 100000">
                    </telerik:RadNotification>
                </div>
            </div>  
         <div class="row">
                        <div class="col-md-1">
                                
                        </div>
                        <div class="col-lg-10">
                           <!-- put content here !-->
                            <asp:GridView ID="gvGrid" runat="server" CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="False" Width="900px" 
                            DataKeyNames="AccountEmailId" 
                            OnRowDataBound="GvGridRowDataBound" EmptyDataText="No Rows Found" AllowPaging="True" OnPageIndexChanging="gvGrid_PageIndexChanging" ShowFooter="True" ShowHeaderWhenEmpty="True" OnRowCommand="gvGrid_RowCommand" AllowSorting="True" OnSorting="gvGrid_Sorting" OnRowCancelingEdit="gvGrid_RowCancelingEdit" OnRowDeleted="gvGrid_RowDeleted" OnRowDeleting="gvGrid_RowDeleting" OnRowEditing="gvGrid_RowEditing" OnRowUpdating="gvGrid_RowUpdating">
                                <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" Font-Size="14pt" />
                                <HeaderStyle  CssClass="gridHeader" />
                            <Columns>
                                <asp:TemplateField ItemStyle-CssClass="tooltip-demo" HeaderText="">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridHeaderSortASC" />
                                    <ItemStyle HorizontalAlign="Center" ForeColor="Black" Width="100px" Wrap="False" BackColor="#EEEEEE"  />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" 
                                                            CommandName="Edit" ToolTip="Edit Record" Text="Edit"><img src="/Content/Images/edit-tools.png" alt="delete group" /></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButtonDelete" runat="server" data-toggle="tooltip" data-placement="top"
                                            title="Delete" OnClientClick="return confirm('Are you sure that you want to delete this record?');"
                                            OnClick="LinkButtonDeleteClick" CommandArgument='<%#this.Eval("AccountEmailId") %>'><img src="/Content/Images/delete32.png" alt="delete group" /></asp:LinkButton>
                                    </ItemTemplate>
                                     <EditItemTemplate>
                                                        <asp:LinkButton ID="lbkUpdate" runat="server" CausesValidation="True"
                                                            CommandName="Update" ToolTip="Save Edits" Text="Update"><img src="/Content/Images/dialog-save-disk.jpg" alt="delete group" /></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancel" runat="server" ToolTip="Cancel Edits" CausesValidation="False"
                                                            CommandName="Cancel" Text="Cancel"><img src="/Content/Images/button_cancel.png" alt="delete group" /></asp:LinkButton>
                                                </EditItemTemplate>
                                    <FooterTemplate>
                                         <asp:LinkButton ID="LinkButtonAdd" runat="server" data-toggle="tooltip" data-placement="top"
                                            title="Add" 
                                            OnClick="LinkButtonAddClick" CommandName="Add" CommandArgument='<%#this.Eval("AccountEmailId") %>'><img src="/Content/Images/plus.png" alt="plus group" /></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Email Address" SortExpression="EmailAddress">
                                    <ControlStyle />
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrEmailAddress" runat="server" Text='<%#this.Eval("EmailAddress") %>' />
                                        <asp:HiddenField ID="ltrAccountEmailId" runat="server" Value='<%#this.Eval("AccountEmailId") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadTextBox AutoCompleteType="Disabled" Height="23px"  Text='<%#this.Eval("EmailAddress") %>' ID="txtEmailAddress" runat="server"></telerik:RadTextBox>
                                        <asp:HiddenField ID="ltrAccountEmailId" runat="server" Value='<%#this.Eval("AccountEmailId") %>' />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <telerik:RadTextBox AutoCompleteType="Disabled" Height="23px" ID="txtEmailAddress" runat="server"></telerik:RadTextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Main Contact" SortExpression="MainContact">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID ="HiddenFieldMainContact" Value='<%#this.Eval("MainContact") %>'/>
                                        <asp:ImageButton ID="imgButtonMainContact" Enabled="False" runat="server" alt="delete group" ImageUrl="/Content/Images/CheckOffsmall.png" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField runat="server" ID ="HiddenFieldMainContact" Value='<%#this.Eval("MainContact") %>'/>
                                        <asp:ImageButton ID="imgButtonMainContact" runat="server" alt="delete group" ImageUrl="/Content/Images/CheckOffsmall.png" CommandName="MainContactClick" />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:HiddenField runat="server" ID ="HiddenFieldMainContact" Value='<%#this.Eval("MainContact") %>'/>
                                        <asp:ImageButton ID="imgButtonMainContact" runat="server" alt="delete group" ImageUrl="/Content/Images/CheckOffsmall.png" CommandName="ButtonMainContact"  />
                                        <%--<asp:LinkButton ID="ltrLinkButtonHaz" runat="server" data-toggle="tooltip" data-placement="top"
                                            title="Haz" CommandName="ButtonHaz"
                                            OnClick="LinkButtonHazClick" CommandArgument='<%#this.Eval("WarehouseRctDetailsId") %>'>
                                            <asp:Image ID="imgButtonHaz" runat="server" alt="delete group" ImageUrl="/Content/Images/CheckOffsmall.png" /></asp:LinkButton>--%>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                
                            </Columns>
                            <FooterStyle></FooterStyle>
                            <%--<EmptyDataTemplate>
                                No record found
                            </EmptyDataTemplate>--%>
                                <RowStyle Wrap="False" />
                        </asp:GridView>
                            <a id="BottomOfGrid"></a>
                        </div>
            </div>   
            <div class="col-md-10">
                    <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotificationEmail" runat="server" Position="Center"
                             Width="330px" Height="160px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                             Title="Error" Text="Error, the email address already exists."
                             Style="z-index: 100000">
                    </telerik:RadNotification>
                </div>
<br/>     <br/><br/><br/>
    </div>
    <!-- DataTables JavaScript -->
    <%--<script src="/Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="/Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gvGrid').DataTable({
                responsive: true
            });
        });
    </script>--%>
</asp:Content>
