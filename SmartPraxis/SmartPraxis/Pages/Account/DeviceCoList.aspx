﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeviceCoList.aspx.cs" Inherits="SmartPraxis.Pages.Account.DeviceCoList"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutSuperAdmin.Master" 
    Title="Smart-Praxis" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
     <style type="text/css">
		.gridHeader { font-weight:bold; color:#000000; background-color:white; }
		.gridHeader A { font-weight:normal; color:#000000; background-color:white; }
		.gridHeader A:hover { text-decoration: underline; }
		.gridHeaderSortASC A { background: url(/Content/Images/sortdown.gif) no-repeat 95% 50%; }
		.gridHeaderSortDESC A { background: url(/Content/Images/sortup.gif) no-repeat 95% 50%; }
    </style>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            
            img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;
            ////img.src = "http://localhost:10240/RefreshSessionState.aspx?c=" + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
       <div class="user-stats">
       <div class="row">
            <div id="divTitle" runat="server" style="font-size: 18pt; font-weight: 500" class="col-md-4">
                Device Co List                
            </div>
        </div>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-10">
                        <div class="panel panel-default">
                        <div class="panel-heading">
                            <% // need to put this into a table so mobile can see the filter next to the Create new button %>
                            <% if (this.Session["Role"].ToString() == "Super Admin" || this.Session["Role"].ToString() == "HCO Admin" || this.Session["Role"].ToString() == "Company Admin" )
                               { %>
                            <div class="col-lg-4">
                                <asp:Button ID="CreateNew" class="btn btn-success" runat="server" Text="Create New Device Co" OnClick="CreateNew_Click"  />
                            </div>
                            <% } %>
                            <div class="col-lg-6">
                                <asp:Panel runat="server">
                                    <label>Filter:&nbsp;</label><asp:TextBox ID="Filter" Placeholder="Filter Text" Height="32px" runat="server" AutoPostBack="True" OnTextChanged="Filter_TextChanged" ></asp:TextBox>&nbsp;
                                    <asp:Button ID="Find" class="btn btn-success" runat="server" Text="Find"  /> 
                                    <asp:Button ID="ClearFilter" class="btn btn-warning" runat="server" Text="Clear" OnClick="ClearFilter_Click"  />
                                    </asp:Panel>
                                
                            </div>
                            <br/><br/><br/>
                        </div>
                                <div class="panel-body">
                                   <!-- put content here !-->
                                    <asp:GridView ID="gvGrid" runat="server" CssClass="table table-striped table-bordered table-hover"
                                    AutoGenerateColumns="False"
                                    DataKeyNames="AccountId,CompanyName" 
                                    OnRowDataBound="GvGridRowDataBound" EmptyDataText="No Rows Found" AllowPaging="True" OnPageIndexChanging="gvGrid_PageIndexChanging" ShowHeaderWhenEmpty="True" OnSorting="gvGrid_Sorting" AllowSorting="True" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" style="border: 1px solid #ddd !important;">
                                        <PagerStyle HorizontalAlign = "Left" CssClass = "GridPager" Font-Size="14pt" BackColor="#99CCCC" ForeColor="#003399" />
                                         <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                         <HeaderStyle CssClass="gridHeader" BackColor="#FFFFFF" ForeColor="#003399" />
                                    <Columns>
                                        <asp:TemplateField ItemStyle-CssClass="tooltip-demo" HeaderText="">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridHeaderSortASC" />
                                            <ItemStyle HorizontalAlign="Center" ForeColor="Black" Width="100px" Wrap="False" BackColor="#EEEEEE"  />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" runat="server" data-toggle="tooltip" data-placement="top"
                                                    title="View/Edit" NavigateUrl='<%# string.Format("/Pages/Account/DeviceCo.aspx?AccountId={0}&action=edit", this.Eval("AccountId")) %>'><img src="/Content/Images/edit-tools.png" alt="edit group" /></asp:HyperLink>
                                                <asp:LinkButton ID="LinkButtonDelete" runat="server" data-toggle="tooltip" data-placement="top"
                                                    title="Delete" OnClientClick="return confirm('Are you sure that you want to delete this record?');"
                                                    OnClick="LinkButtonDeleteClick" CommandArgument='<%#this.Eval("AccountId") %>'><img src="/Content/Images/delete32.png" alt="delete group" /></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company Name" SortExpression="CompanyName">
                                            <ItemStyle Wrap="False" Width="150px" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrCompanyName" Text='<%#this.Eval("CompanyName") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Group Name" SortExpression="GroupName">
                                            <ItemStyle Wrap="False" Width="150px" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrGroupName" Text='<%#this.Eval("GroupName") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contact First Name" SortExpression="FirstName">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrFirstName" runat="server" Text='<%#this.Eval("FirstName") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLastName" runat="server" Text='<%#this.Eval("LastName") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Office Phone" SortExpression="Phone">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrPhone" runat="server" Text='<%#this.Eval("Phone") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" SortExpression="EmailAddress">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrEmailAddress" runat="server" Text='<%#this.Eval("EmailAddress") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Height="10px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No record found
                                    </EmptyDataTemplate>
                                        <RowStyle BackColor="White" ForeColor="#003399" />
                                        <SelectedRowStyle BackColor="#009999" Font-Bold="False" ForeColor="#CCFF99" />
                                        <SortedAscendingCellStyle BackColor="#EDF6F6" />
                                        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                                        <SortedDescendingCellStyle BackColor="#D6DFDF" />
                                        <SortedDescendingHeaderStyle BackColor="#002876" />
                                </asp:GridView>
                            </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="col-md-4">
            </div>
    </div>        
    </div>
</asp:Content>