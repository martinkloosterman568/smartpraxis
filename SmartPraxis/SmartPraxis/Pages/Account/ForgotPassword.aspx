﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="SmartPraxis.Pages.Account.ForgotPassword"
    MasterPageFile="~/Shared/MasterPages/SiteBasicLayoutForgotPassword.Master" Title="Forgot Password | Smart-Praxis" %>

<asp:Content ID="HeadCurrentPage" ContentPlaceHolderID="head" runat="server" >
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            
            img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;
            ////img.src = "http://localhost:10240/RefreshSessionState.aspx?c=" + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
    <%--You can add your custom code for each page header in this section.--%>
</asp:Content>
<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page in this section.--%>
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <div class="sign-box password-reset">
        <h3>
            Forgot?
        </h3>
        <asp:TextBox ID="txtEmail" placeholder="Email/Phone" runat="server"></asp:TextBox>

        <p>
            Enter your email or cell phone #. 
            We'll send you an text message or email with a link to reset your password.
        </p>
        <div class="text-right">
            <asp:Button ID="ButtonForgot" runat="server" Text="Lookup" OnClick="ButtonForgot_Click" />
        </div>
    </div>
    <div id="error_block" runat="server" visible="false" class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            ×</button>
        <asp:Literal ID="ltrError" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="JavaScriptCurrentPage" ContentPlaceHolderID="JavaScriptPage" runat="server">
    <%--You can add your custom JavaScript for each page in this section.--%>
</asp:Content>