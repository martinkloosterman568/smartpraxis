﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Image = System.Web.UI.WebControls.Image;

namespace SmartPraxis.Pages.Account
{
    public partial class MRI : Page
    {
        private static bool GridRowsFound;
        private static int gridRowInt;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific for this page
            var responsed = string.Empty;

            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            if ((this.Session["Role"]?.ToString() == "MRI Center") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if ((this.Session["Role"]?.ToString() == "Device Co") && (responsed == string.Empty))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                //if ((bool) this.Session["IsAdmin"])
                //{
                //    //this.TypeOf.Enabled = true;
                //    this.CompanyCode.Enabled = true;
                //}
                var id = this.Request.QueryString["AccountId"];
                if (id != null)
                {
                    this.CompanyCode.Enabled = false;
                }
                else
                {
                    this.CompanyCode.Enabled = true;
                }

                //if (this.IDType.Items.Count == 0)
                //{
                //    this.PopulateIDType();
                //}

                if (this.GroupName.Items.Count == 0)
                {
                    this.PopulateGroupName();
                }

                //if (this.TypeOf.Items.Count == 0)
                //{
                //    this.PopulateTypeOf();
                //}

                if (this.StateCode.Items.Count == 0)
                {
                    this.PopulateStateCode();
                }

                if (this.CountryCode.Items.Count == 0)
                {
                    this.PopulateCountryCode();
                }

                if (!this.IsAddAction)
                {
                    const string tableName = "DataAccounts"; // <-- put table name here
                    const string fieldName = "AccountId"; // <-- put the field name here
                    string outError;
                    var repository = new AccountsRepository(new DB_SmartPraxisEntities());
                    var destination = repository.FindAllByGuid(tableName, fieldName, this.AccountIdForEditing.ToString(), out outError);
                    var result = destination?.FirstOrDefault();
                    if (result != null)
                    {
                        this.FindOnReturnOfRedirectId(result.AccountId.ToString());
                    }
                }

                this.ViewState["sort"] = "GroupName ASC, CompanyName ASC";
            }
        }

        private void PopulateGroupName()
        {
            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
                this.GroupName.DataSource = db.DataAccountGroups.ToList();
                this.GroupName.DataTextField = "GroupName";
                this.GroupName.DataValueField = "AccountGroupGuid";
                this.GroupName.DataBind();

                this.GroupName.Items.Insert(0, "- Select One -");
            }
        }

        //private void PopulateTypeOf()
        //{
        //    using (var db = new DB_SmartPraxisEntities())
        //    {
        //        var codes = db.ListCompanyTypes.ToList();
        //        this.TypeOf.DataSource = codes;
        //        this.TypeOf.DataValueField = "CompanyTypeGuid";
        //        this.TypeOf.DataTextField = "Description";

        //        this.TypeOf.Items.Insert(0, new ListItem("- Select One -", "0"));
        //        this.TypeOf.DataBind();
        //    }
        //}

        private void PopulateStateCode()
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var codes = db.ListStates.ToList();
                this.StateCode.DataSource = codes;
                this.StateCode.DataValueField = "StateGuid";
                this.StateCode.DataTextField = "Code";

                this.StateCode.Items.Insert(0, new ListItem("- Select -", "0"));
                this.StateCode.DataBind();
            }
        }

        private void PopulateCountryCode()
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var codes = db.ListCountries.ToList();
                this.CountryCode.DataSource = codes;
                this.CountryCode.DataValueField = "CountryGuid";
                this.CountryCode.DataTextField = "Code";

                this.CountryCode.Items.Insert(0, new ListItem("- Select -", "0"));
                this.CountryCode.DataBind();
            }
        }

        //private void PopulateIDType()
        //{
        //    using (var db = new DB_SmartPraxisEntities())
        //    {
        //        var codes = db.ListIDTypes.ToList();
        //        this.IDType.DataSource = codes;
        //        this.IDType.DataValueField = "IDTypeGuid";
        //        this.IDType.DataTextField = "Description";

        //        this.IDType.Items.Insert(0, new ListItem("- Select One -", "0"));
        //        this.IDType.DataBind();
        //    }
        //}

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            var ctrl = this.AccountId;

            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataAccounts"; // <-- put table name here
            const string fieldName = "AccountId"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2.ToString(), out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
            }
            else
            {
                this.ElseError(ctrl, id, outError);
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        private void ElseError(HiddenField ctrl, string ctlvalue, string outError)
        {
            this.ClearButton();
            ctrl.Value = ctlvalue + @" - " + outError;
        }

        #region hidden - protected controls

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(@"/Pages/Account/MRIList.aspx");
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }

        #endregion

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new AccountsRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.FindOnReturnOfRedirectId(id);
            this.PopulateGrid();
        }

        public bool SaveRecord(out string errorMessage)
        {
            try
            {
                using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                {
                    DataAccount saving;

                    if (this.IsAddAction)
                    {
                        
                            saving = new DataAccount() { AccountGuid = Guid.NewGuid() };
                            db.DataAccounts.Add(saving);

                            saving.CreatedBy = this.Session["UserName"].ToString();
                            saving.CreatedDate = GetTimeNow.GetTime();
                            saving.KeyToAddUsers = saving.AccountGuid.ToString().Substring(0, 8).ToUpper();
                        
                    }

                    else
                    {
                        saving = db.DataAccounts.First(dataAcct => dataAcct.AccountId == this.AccountIdForEditing);
                    }
                    if (this.Session["Role"].ToString() == "HCO Admin" || this.Session["Role"].ToString() == "Company Admin")
                    {
                        saving.Tenant = new Guid(this.Session["TenantID"].ToString());

                    }
                    saving.CompanyCode = this.CompanyCode.Text.ToString();
                    saving.LastModified = GetTimeNow.GetTime();
                    saving.LastUpdatePerson = this.Session["UserName"].ToString();

                    if (saving.CountryCode != this.CompanyCode.Text)
                    {
                        saving.CountryCode = this.CompanyCode.Text;
                    }

                    saving.TypeOf = this.TypeOf.Value;
                    saving.TypeOfGuid = new Guid(this.TypeOfGuid.Value);
                    

                    //if (saving.TypeOf != TypeOf.SelectedItem.Text || saving.TypeOfGuid.ToString() != TypeOf.SelectedValue)
                    //{
                    //    saving.TypeOf = TypeOf.SelectedIndex > 0 ? TypeOf.SelectedItem.Text : null;
                    //    saving.TypeOfGuid = TypeOf.SelectedIndex > 0 ? (Guid?)Guid.Parse(TypeOf.SelectedValue) : null;
                    //}

                    if (saving.GroupName != this.GroupName.SelectedItem.Text || this.GroupName.SelectedIndex <= 0)
                    {
                        saving.GroupName = this.GroupName.SelectedIndex > 0 ? this.GroupName.SelectedItem.Text : null;
                        saving.AccountGroupGuid = this.GroupName.SelectedIndex > 0 ? (Guid?)Guid.Parse(this.GroupName.SelectedValue) : null;
                    }

                    saving.IDType = this.IDType.Value;
                    saving.IDTypeGuid = new Guid(this.IDTypeGuid.Value);

                    //if (saving.IDType != IDType.SelectedItem.Text || saving.IDTypeGuid.ToString() != IDType.SelectedValue)
                    //{
                    //    saving.IDType = IDType.SelectedIndex > 0 ? IDType.SelectedItem.Text : null;
                    //    saving.IDTypeGuid = IDType.SelectedIndex > 0 ? (Guid?)Guid.Parse(IDType.SelectedValue) : null;
                    //}

                    if (saving.EINNumber != this.EINNumber.Text)
                    {
                        saving.EINNumber = this.EINNumber.Text;
                    }

                    if (saving.CompanyName != this.CompanyName.Text)
                    {
                        saving.CompanyName = this.CompanyName.Text;
                    }

                    if (saving.Address1 != this.Address1.Text)
                    {
                        saving.Address1 = this.Address1.Text;
                    }

                    // backup
                    //if (saving.Address1 != this.Address1.Text || saving.Address2 != this.Address2.Text || saving.Address3 != this.Address3.Text)
                    //{
                    //saving.Address1 = this.Address1.Text;
                    //    saving.Address2 = this.Address2.Text;
                    //    saving.Address3 = this.Address3.Text;
                    //}

                    if (saving.City != this.City.Text)
                    {
                        saving.City = this.City.Text;
                    }

                    if (saving.StateCode != this.StateCode.SelectedValue || saving.StateGuid.ToString() != this.StateCode.SelectedValue)
                    {
                        saving.StateCode = this.StateCode.SelectedIndex > 0 ? this.StateCode.SelectedItem.Text : null;
                        saving.StateGuid = this.StateCode.SelectedIndex > 0 ? (Guid?)Guid.Parse(this.StateCode.SelectedValue) : null;
                    }

                    if (saving.StateDescription != this.StateDescription.Text)
                    {
                        saving.StateDescription = this.StateDescription.Text;
                    }

                    if (saving.ZipCode != this.ZipCode.Text)
                    {
                        saving.ZipCode = this.ZipCode.Text;
                    }

                    if (saving.CountryCode != this.CountryCode.SelectedItem.Text || saving.CountryGuid.ToString() != this.CountryCode.SelectedValue)
                    {
                        saving.CountryCode = this.CountryCode.SelectedIndex > 0 ? this.CountryCode.SelectedItem.Text : null;
                        saving.CountryGuid = this.CountryCode.SelectedIndex > 0 ? (Guid?)Guid.Parse(this.CountryCode.SelectedValue) : null;
                    }

                    if (saving.CountryDescription != this.CountryDescription.Text)
                    {
                        saving.CountryDescription = this.CountryDescription.Text;
                    }

                    if (saving.FirstName != this.FirstName.Text)
                    {
                        saving.FirstName = this.FirstName.Text;
                    }

                    if (saving.LastName != this.LastName.Text)
                    {
                        saving.LastName = this.LastName.Text;
                    }

                    if (saving.Phone != this.Phone.Text)
                    {
                        saving.Phone = this.Phone.Text;
                    }

                    if (saving.Fax != this.Fax.Text)
                    {
                        saving.Fax = this.Fax.Text;
                    }

                    if (saving.EmailAddress != this.EmailAddress.Text)
                    {
                        saving.EmailAddress = this.EmailAddress.Text;
                    }

                    if (saving.Notes != this.Notes.Text)
                    {
                        saving.Notes = this.Notes.Text;
                    }

                    db.SaveChanges();

                    this.AccountId.Value = saving.AccountId.ToString();
                    errorMessage = null;
                    return true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException?.InnerException != null && ex.InnerException.InnerException.Message.Contains("duplicate") ?
                    "Error, the user name already exists" :
                    "failure";
            }

            return false;
        }

        //private Guid GetIDType(string idType)
        //{
        //    if (!string.IsNullOrEmpty(idType))
        //    {
        //        using (var db = new DB_SmartPraxisEntities())
        //        {
        //            var result = (from d in db.ListIDTypes
        //                where d.Description == idType
        //                select d).FirstOrDefault();

        //            if (result?.IDTypeGuid != null) return result.IDTypeGuid.Value;
        //        }
        //    }

        //    return Guid.Empty;
        //}

        //private Guid GetCountry(string country)
        //{
        //    if (!string.IsNullOrEmpty(country))
        //    {
        //        using (var db = new DB_SmartPraxisEntities())
        //        {
        //            var result = (from d in db.ListCountries
        //                where d.Description == country
        //                select d).FirstOrDefault();

        //            if (result?.CountryGuid != null) return result.CountryGuid.Value;
        //        }
        //    }

        //    return Guid.Empty;
        //}

        //private Guid GetTypeOf(string type)
        //{
        //    if (!string.IsNullOrEmpty(type))
        //    {
        //        using (var db = new DB_SmartPraxisEntities())
        //        {
        //            var result = (from d in db.ListCompanyTypes
        //                where d.Description == type
        //                select d).FirstOrDefault();

        //            if (result?.CompanyTypeGuid != null) return result.CompanyTypeGuid.Value;
        //        }
        //    }

        //    return Guid.Empty;
        //}

        //private Guid GetState(string state)
        //{
        //    // auto add State Name
        //    if (!string.IsNullOrEmpty(state))
        //    {
        //        using (var db = new DB_SmartPraxisEntities())
        //        {
        //            var result = (from d in db.ListStates
        //                where d.Code == state
        //                select d).FirstOrDefault();

        //            // update ** but nothing to do here
        //            if (result?.StateGuid != null) return result.StateGuid.Value;
        //        }
        //    }

        //    return Guid.Empty;
        //}

        //public static bool IsNumeric(string value)
        //{
        //    return Regex.IsMatch(value, "^\\d+$");
        //}

        //public string GetLastUserUpdated()
        //{
        //    return this.Session["UserName"].ToString();
        //}

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                string errorMessage;

                if (this.SaveRecord(out errorMessage))
                {
                    this.Response.Redirect("/Pages/Account/MRIList.aspx");
                }
                else if (errorMessage.Contains("already"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotificationAccount('hello')", true);
                }
            }
        }

        protected void StateCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.ListStates
                             where d.Code == this.StateCode.SelectedItem.Text
                             orderby d.Code
                             select new { d.Description }).FirstOrDefault();

                if (query != null)
                {
                    this.StateDescription.Text = query.Description;
                }
            }
        }

        protected void CountryCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var query = (from d in db.ListCountries
                             where d.Code == this.CountryCode.SelectedItem.Text
                             orderby d.Code
                             select new { d.Description }).FirstOrDefault();

                if (query != null)
                {
                    this.CountryDescription.Text = query.Description;
                }
            }
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
        }

        protected void gvGrid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            this.gvGrid.EditIndex = -1;
        }

        protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Add"))
            {
                string errorMessage;
                this.SaveRecord(out errorMessage);
            }
            else if (e.CommandName.Equals("MainContactClick"))
            {
                var index = Convert.ToInt32(e.CommandArgument);
                var selectedRow = this.gvGrid.Rows[index];
                var mimgButtonMainContact = (ImageButton)e.CommandSource;
                if (mimgButtonMainContact != null && mimgButtonMainContact.ImageUrl.Contains("On"))
                {
                    mimgButtonMainContact.ImageUrl = "/Content/Images/CheckOffsmall.png";
                    ((DataControlFieldCell)(selectedRow.Cells[2])).Enabled = false;
                    ((DataControlFieldCell)(selectedRow.Cells[2])).Text = string.Empty;
                }
                else if (mimgButtonMainContact != null && mimgButtonMainContact.ImageUrl.Contains("Off"))
                {
                    mimgButtonMainContact.ImageUrl = "/Content/Images/CheckOnsmall.png";
                    ((DataControlFieldCell)(selectedRow.Cells[2])).Enabled = true;
                }
            }
            else if (e.CommandName.Equals("ButtonMainContact"))
            {
                var mimgButtonMainContact = this.gvGrid.FooterRow.FindControl("imgButtonMainContact") as Image;
                
                if (mimgButtonMainContact != null && mimgButtonMainContact.ImageUrl.Contains("On"))
                {
                    mimgButtonMainContact.ImageUrl = "/Content/Images/CheckOffsmall.png";
                }
                else if (mimgButtonMainContact != null && mimgButtonMainContact.ImageUrl.Contains("Off"))
                {
                    mimgButtonMainContact.ImageUrl = "/Content/Images/CheckOnsmall.png";
                }
            }
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.BackColor = Color.FromArgb(128, 160, 160);
                // show
                e.Row.Cells[0].Visible = true;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.DataItem != null && GridRowsFound)
                {
                    // maybe use this for the Main Contact checkbox
                    var buttonMainContact = DataBinder.Eval(e.Row.DataItem, "MainContact");
                    var imgButtonMainContact = (ImageButton)e.Row.FindControl("imgButtonMainContact");
                    imgButtonMainContact.CommandArgument = e.Row.RowIndex.ToString();
                    try
                    {
                        if (buttonMainContact != null && (bool)buttonMainContact)
                        {
                            imgButtonMainContact.ImageUrl = "/Content/Images/CheckOnsmall.png";
                        }
                        else
                        {
                            imgButtonMainContact.ImageUrl = "/Content/Images/CheckOffsmall.png";
                        }
                    }
                    catch (Exception)
                    {
                        imgButtonMainContact.ImageUrl = "/Content/Images/CheckOffsmall.png";
                    }

                    // show
                    e.Row.Cells[0].Visible = true;
                }
                else if (GridRowsFound == false)
                {
                    if (gridRowInt == 0)
                    {
                        e.Row.Visible = false;
                        gridRowInt++;
                    }
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                // show
                e.Row.Visible = true;
            }
        }

        protected void gvGrid_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
        }

        protected void gvGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                //var value = e.Keys[0].ToString();

                //var delitem = int.Parse(value);
                //var delete = (from d in this.db.DataAccountEmails
                //              where d.AccountEmailId == delitem
                //              select d).FirstOrDefault();

                //if (delete != null)
                //{
                //    this.db.DataAccountEmails.Remove(delete);
                //    this.db.SaveChanges();
                //}
            }
            catch (Exception)
            {
                // ignored
            }
        }

        protected void gvGrid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            this.gvGrid.EditIndex = e.NewEditIndex;
        }

        protected void gvGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var row = this.gvGrid.Rows[e.RowIndex];
            string errorMessage;

            this.SaveRecord(out errorMessage); // we are just saving the record, not posting here

            var id = row.FindControl("ltrAccountEmailId") as HiddenField;
            //var email = row.FindControl("txtEmailAddress") as RadTextBox;
            //var mimgButtonMainContact = row.FindControl("imgButtonMainContact") as Image;

            // update the record since it is already in the database
            if (id != null)
            {
                //var recordId = int.Parse(id.Value);
                //var record = (from d in this.db.DataAccountEmails
                //    where d.AccountEmailId == recordId
                //    select d).FirstOrDefault();

                //if (record != null)
                //{
                //    if (email != null) record.EmailAddress = email.Text;
                //    record.MainContact = mimgButtonMainContact != null && (!mimgButtonMainContact.ImageUrl.Contains("Off"));

                //    this.db.SaveChanges();
                //}
            }

            this.gvGrid.EditIndex = -1;
            //this.PopulateGrid();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] s = this.ViewState["sort"].ToString().Split(); //load the last sort
            string sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                    sSort += " DESC";
                else
                    sSort += " ASC";
            }
            else
                sSort += " ASC";

            //find which column is being sorted to change its style
            int i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" +
                                                                  sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            if (gridRowInt == 0)
            {
                GridRowsFound = false;
            }
            else
            {
                GridRowsFound = true;
            }

            //this.gvGrid.DataSource = this.getData(sSort);
            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }

        private DataTable getData()
        {
            // A stored proc 'spGetAccountEmails' used to be here, but it references a table 'DataAccountEmails' that no longer exists
            return null;
        }

        private void PopulateGrid()
        {
            var destination = this.getData();

            if (destination == null)
            {
                GridRowsFound = false;
                var dt = new DataTable();
                this.ShowNoResultFound(dt, this.gvGrid);
                gridRowInt = 0;
            }
            else
            {
                GridRowsFound = true;
                this.gvGrid.DataSource = destination;
                gridRowInt = destination.Rows.Count;
            }

            //this.gvGrid.DataBind();

            if (destination != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void ShowNoResultFound(DataTable source, GridView gv)
        {
            source.Columns.Add("AccountEmailId", typeof(int));
            source.Columns.Add("EmailAddress", typeof(string));
            source.Columns.Add("MainContact", typeof(bool));
            source.Rows.Add(0, "No Rows Found", false);

            gv.DataSource = source;
            //gv.DataBind();

            // Get the total number of columns in the GridView to know what the Column Span should be
            var columnsCount = gv.Columns.Count;
            gv.Rows[0].Cells.Clear(); // clear all the cells in the row
            gv.Rows[0].Cells.Add(new TableCell()); //add a new blank cell
            gv.Rows[0].Cells[0].ColumnSpan = columnsCount; //set the column span to the new added cell

            //You can set the styles here
            gv.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            gv.Rows[0].Cells[0].ForeColor = Color.Red;
            gv.Rows[0].Cells[0].Font.Bold = true;
            //set No Results found to the new added cell
            gv.Rows[0].Cells[0].Text = @"NO RESULTS FOUND!";
        }

        protected void LinkButtonAddClick(object sender, EventArgs e)
        {
            // ignore 
        }

        protected void csvRequiredFields_ServerValidate(object source, ServerValidateEventArgs args)
        {
            WebControl controlToValidate = (WebControl) this.Panel1.FindControl(((CustomValidator)source).ControlToValidate);

            if (controlToValidate is TextBox)
            {
                args.IsValid = !string.IsNullOrWhiteSpace(((TextBox)controlToValidate).Text);
            }
            else if (controlToValidate is DropDownList)
            {
                args.IsValid = ((DropDownList)controlToValidate).SelectedIndex > 0;
            }

            controlToValidate.BackColor = args.IsValid ? Color.White : Color.FromArgb(0xffdae0);
        }

        private bool IsAddAction
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Request.QueryString["AccountId"]);
            }
        }

        private int AccountIdForEditing
        {
            get
            {
                return int.Parse(this.Request.QueryString["AccountId"]);
            }
        }
    }
}