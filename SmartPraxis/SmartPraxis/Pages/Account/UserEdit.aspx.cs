﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Reusable;
using SmartPraxis.Shared;
using Telerik.Web.UI;
using Newtonsoft.Json;

namespace SmartPraxis.Pages.Account
{
    public partial class UserEdit : Page
    {
        private readonly GeneralHelper dHelper = new GeneralHelper();

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific for this form
            var responsed = string.Empty;
            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            //if ((this.Session["Role"]?.ToString() == "MRI Center Admin") && (responsed == string.Empty))
            //{
            //    this.Session.Abandon();
            //    this.Response.Redirect(Global.returnToPage);
            //}

            //if ((this.Session["Role"]?.ToString() == "Device Co Admin") && (responsed == string.Empty))
            //{
            //    this.Session.Abandon();
            //    this.Response.Redirect(Global.returnToPage);
            //}
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Password control", $"$('#{this.Password.ClientID }').attr('type', 'password'); ", true);

            if (!this.Page.IsPostBack)
            {
                this.PopulateGroupNames();

                if (this.Role.Items.Count == 0)
                {
                    this.PopulateRoles();
                }
                
                if (this.Session["Role"] != null)
                {
                    if (this.Session["Role"].ToString() == "Super Admin" || 
                        this.Session["Role"].ToString() == "MRI Center Admin" ||
                        this.Session["Role"].ToString() == "Device Co Admin" ||
                        this.Session["Role"].ToString()=="HCO Admin" ||
                        this.Session["Role"].ToString() == "Company Admin"
                        )
                    {
                        this.Role.Enabled = true;
                        this.UserName.Enabled = true;
                        this.divForcePassword.Style.Remove("display");
                        this.divForcePassword.Style.Add("display", "inherit");
                        
                    }
                    else
                    {
                        this.Role.Enabled = false;
                    }

                    if(this.Session["Role"].ToString() == "MRI Center Admin")
                    {
                        this.AccountIdContainerdiv.Visible = false;
                        this.AccountIdContainer.Visible = false;
                        this.GroupNameContainer.Visible = false;
                    }

                    if (this.Session["Role"].ToString() == "Device Co Admin")
                    {
                        this.AccountIdContainer.Enabled = true;
                    }

                    var responsed = string.Empty;
                    if (this.Request.QueryString["id"] != null)
                    {
                        responsed = this.Request.QueryString["id"];
                    }
                    if (this.Session["UserGuid"].ToString() == responsed)
                    {
                        this.Role.Enabled = false;
                      //  this.Role.Visible = false;
                       // this.AccountIdContainerdiv.Visible = false;
                    }
                }

                if (!this.IsAddAction)
                {
                    using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                    {
                        DataUserMast userForEditing = db.DataUserMasts.First(dataUserMast => dataUserMast.UserGuid.Value == this.UserGuid.Value);
                        this.FindOnReturnOfRedirectId(userForEditing.ID.ToString());

                        this.FirstName.Text = userForEditing.FirstName;
                        this.LastName.Text = userForEditing.LastName;
                        this.UserName.Text = userForEditing.UserName;
                        this.Password.Text = userForEditing.Password; // This value is still encrypted
                        this.OfficePhone.Text = userForEditing.OfficePhone;
                        this.CellPhone.Text = userForEditing.CellPhone;
                        this.EmailAddress.Text = userForEditing.EmailAddress;
                        this.Role.SelectedValue =userForEditing.RoleID.ToString();
                    }
                }

                this.PopulateAccountList();
            }
        }

        //private void PopulateHCO()
        //{
        //    using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
        //    {
        //        List<ListTenant> tenant;
        //            tenant = (from d in db.ListTenants orderby d.Description select d).ToList();

        //            this.HCOList.DataSource = tenant;
        //            this.HCOList.DataValueField = "IdTenantGuid";
        //            this.HCOList.DataTextField = "Description";
        //            this.HCOList.Items.Insert(0, new ListItem("- Select One -", "- Select One -"));
        //            this.HCOList.DataBind();
        //    }
        //}
            private void PopulateRoles()
        {
            var responsed = string.Empty;
            if (this.Request.QueryString["id"] != null)
            {
                responsed = this.Request.QueryString["id"];
            }
            

                using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
                List<ListRoleMast> roles;
                var role = this.Session["Role"]?.ToString();
                if (this.Session["Role"]?.ToString() == "HCO Admin" && this.Session["UserGuid"].ToString() == responsed)
                {
                    roles = (from d in db.ListRoleMasts
                             where d.DisplayName == "HCO Admin" && d.DisplayName != "Company Admin"
                             orderby d.DisplayName
                             select d).ToList();
                }
                else if (this.Session["Role"]?.ToString() == "Company Admin" && this.Session["UserGuid"].ToString() == responsed)
                {
                    roles = (from d in db.ListRoleMasts
                        where d.DisplayName == "Company Admin" && d.DisplayName != "HCO Admin"
                             orderby d.DisplayName
                        select d).ToList();
                }
                else if (this.Session["Role"]?.ToString() == "Super Admin")
                {
                    roles = (from d in db.ListRoleMasts orderby d.DisplayName select d).ToList();
                }
                else if (this.Session["Role"]?.ToString() == "MRI Center Admin")
                {
                    roles = (from d in db.ListRoleMasts
                             where d.DisplayName == "MRI Tech" || d.DisplayName == "MRI Doctor" || d.DisplayName == "MRI Center Device Operator"
                             orderby d.DisplayName
                             select d).ToList();
                }
                else if (this.Session["Role"]?.ToString() == "Device Co Admin")
                {
                    roles = (from d in db.ListRoleMasts
                             where d.DisplayName == "Device Co Tech"
                             orderby d.DisplayName
                             select d).ToList();
                }
                else if (this.Session["Role"]?.ToString() == "HCO Admin")
                {
                    roles = (from d in db.ListRoleMasts
                             where d.DisplayName != "Super Admin" && d.DisplayName != "HCO Admin" && d.DisplayName != "Company Admin"
                             orderby d.DisplayName
                             select d).ToList();
                }
                else if (this.Session["Role"]?.ToString() == "Company Admin")
                {
                    roles = (from d in db.ListRoleMasts
                        where d.DisplayName != "Super Admin" && d.DisplayName != "Company Admin" && d.DisplayName != "HCO Admin"
                             orderby d.DisplayName
                        select d).ToList();
                }
                else
                {
                    // device co tech
                    // mri doctor
                    // mri tech
                    roles = (from d in db.ListRoleMasts
                             where d.DisplayName == role
                             orderby d.DisplayName
                             select d).ToList();
                }

                // add from database
                this.Role.DataSource = roles;
                this.Role.DataValueField = "ID";
                this.Role.DataTextField = "DisplayName";

                // add extra row
                this.Role.Items.Insert(0, new ListItem("- Select One -", "- Select One -"));
                this.Role.DataBind();
            }
        }

        private void PopulateGroupNames()
        {
            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
                // Business Rule: Make GroupNames mandatory only for "Device Co Admin" and "Device Co Tech" users. Hide this control for all others.
                // FYI, this 'GroupName' control is only used for filtering the 'AccountId' control.

                this.GroupName.DataSource = db.DataAccountGroups.ToList();
                this.GroupName.DataTextField = "GroupName";
                this.GroupName.DataValueField = "AccountGroupGuid";
                this.GroupName.DataBind();
                this.GroupName.Items.Insert(0, new ListItem("- Select One -", string.Empty));

                if (!this.IsAddAction)
                {
                    

                    Guid? accountGroupGuid = db.DataUserMasts
                        .Where(dataUserMast => dataUserMast.UserGuid == this.UserGuid.Value && (dataUserMast.Role == "Device Co Admin" || dataUserMast.Role == "Device Co Tech"))
                        .Join(db.UserDataAccounts, dataUserMast => dataUserMast.ID, userDataAccount => userDataAccount.UserID, (dataUserMast, userDataAccount) => userDataAccount.AccountId)
                        .Join(db.DataAccounts, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => dataAccount.AccountGroupGuid)
                        .FirstOrDefault();

                    if (accountGroupGuid.HasValue)
                    {
                        this.GroupName.SelectedValue = accountGroupGuid.ToString();
                        this.GroupName.Enabled = (bool) this.Session["IsAdmin"];
                    }
                    else
                    {
                        this.GroupName.SelectedIndex = 0;
                        this.GroupNameContainer.Visible = false;
                    }
                }
            }
        }

        private void PopulateAccountList()
        {
            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
            {
               
                List<ListTenant> TenantListss;
                if (this.Session["AccountGuids"] is DBNull)
                {
                    this.Session["AccountGuids"] = "00000000-0000-0000-0000-000000000000";
                }
                Guid tenantguid = new Guid(this.Session["AccountGuids"].ToString());
                if (tenantguid.ToString() != "00000000-0000-0000-0000-000000000000")
                {
                    TenantListss = (from d in db.ListTenants
                                    where d.IdTenantGuid == tenantguid
                                    select d).ToList();
                }
                else
                {
                    TenantListss = new List<ListTenant>();
                }

                var responsed = string.Empty;
                if (this.Request.QueryString["id"] != null)
                {
                    responsed = this.Request.QueryString["id"];
                }

                if ((this.Session["Role"]?.ToString() == "HCO Admin" || this.Session["Role"]?.ToString() == "Company Admin") && this.Session["UserGuid"].ToString() == responsed)
                {

                    IEnumerable<DataAccount> dataAccountsList = db.DataAccounts
                      .Where(dataAccount =>
                          (this.GroupName.SelectedIndex <= 0 || this.GroupName.SelectedValue == dataAccount.AccountGroupGuid.ToString()) &&   // Filter the DataAccounts by GroupName
                          (this.DataAccountTypeByUserRole == null || this.DataAccountTypeByUserRole == dataAccount.TypeOf))                   // Filter the DataAccounts by Role
                      .OrderBy(dataAccount => dataAccount.CompanyName)
                      .ToList();

                    IEnumerable<int> currentDataAccountIds = !this.IsAddAction ?
                        db.DataUserMasts
                            .Where(dataUserMast => !this.UserGuid.HasValue || dataUserMast.UserGuid == this.UserGuid.Value)
                            .Join(db.UserDataAccounts, dataUserMast => dataUserMast.ID, userDataAccount => userDataAccount.UserID, (dataUserMast, userDataAccount) => userDataAccount.AccountId)
                            .ToList()
                            .Join(dataAccountsList, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => accountId)
                            .ToArray() :
                        new int[0];

                    this.AccountId.ItemDataBound += (sender, args) =>
                    {
                        ImageButton imageButton = (ImageButton)args.Item.FindControl("DataAccountImageButton");
                        DataAccount dataAccount = (DataAccount)args.Item.DataItem;
                        Label Accountlabel = (Label)args.Item.FindControl("DataAccountlabel");


                        imageButton.Enabled = (bool) this.Session["IsAdmin"];
                     
                        imageButton.Attributes["AccountId"] = dataAccount.AccountId.ToString();
                        imageButton.ImageUrl = currentDataAccountIds.Contains(dataAccount.AccountId) ? "~/Content/Images/CheckOnsmall.png" : "~/Content/Images/CheckOffsmall.png";
                        if (this.Session["Role"]?.ToString() == "Device Co Tech" || this.Session["Role"]?.ToString() == "MRI Doctor" || this.Session["Role"]?.ToString() == "MRI Tech" || this.Session["Role"]?.ToString() == "HCO Admin" || this.Session["Role"]?.ToString() == "Company Admin")
                        {
                            if (imageButton.ImageUrl == "~/Content/Images/CheckOffsmall.png")
                            {
                                imageButton.Visible = false;
                                Accountlabel.Visible = false;


                            }
                        }
                    };

                    this.AccountId.DataSource = dataAccountsList;
                    this.AccountId.DataBind();

                    Dictionary<int, bool> isCheckedDictionary = dataAccountsList.ToDictionary(dataAccount => dataAccount.AccountId, dataAccount => currentDataAccountIds.Contains(dataAccount.AccountId));
                    this.hfAccountIds.Value = JsonConvert.SerializeObject(isCheckedDictionary);
                    return;
                }

                if (this.Session["Role"]?.ToString() == "MRI Center Admin" ||
                    this.Session["Role"]?.ToString() == "Device Co Admin")
                {
                    int ids = Convert.ToInt32(this.Session["DataAccountIdsDelimited"]);
                    IEnumerable<DataAccount> dataAccountsListnew = db.DataAccounts
                    .Where(dataAccount =>
                        (this.GroupName.SelectedIndex <= 0 || this.GroupName.SelectedValue == dataAccount.AccountGroupGuid.ToString()) &&   // Filter the DataAccounts by GroupName
                        (this.DataAccountTypeByUserRole == null || this.DataAccountTypeByUserRole == dataAccount.TypeOf) && (dataAccount.AccountId==ids))                   // Filter the DataAccounts by Role
                    .OrderBy(dataAccount => dataAccount.CompanyName)
                    .ToList();
                    
                    IEnumerable<int> currentDataAccountIdsnew = !this.IsAddAction ?
                    db.DataUserMasts
                        .Where(dataUserMast => !this.UserGuid.HasValue || dataUserMast.UserGuid == this.UserGuid.Value)
                        .Join(db.UserDataAccounts, dataUserMast => dataUserMast.ID, userDataAccount => userDataAccount.UserID, (dataUserMast, userDataAccount) => userDataAccount.AccountId)
                        .ToList()
                        .Join(dataAccountsListnew, accountId => ids, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => ids)
                        .ToArray() :
                    new int[0];
                    this.AccountId.ItemDataBound += (sender, args) =>
                    {
                        ImageButton imageButton = (ImageButton)args.Item.FindControl("DataAccountImageButton");
                        DataAccount dataAccount = (DataAccount)args.Item.DataItem;
                        dataAccount.AccountId = Convert.ToInt32(this.Session["DataAccountIdsDelimited"]);
                        imageButton.Enabled = (bool) this.Session["IsAdmin"];
                        //MRI center role  enable
                        if (this.Session["Role"]?.ToString() == "MRI Center Admin" ||
                            this.Session["Role"]?.ToString() == "Device Co Admin")
                        {
                            imageButton.Enabled = true;
                        }
                        imageButton.Attributes["AccountId"] = dataAccount.AccountId.ToString();
                        imageButton.ImageUrl = currentDataAccountIdsnew.Contains(dataAccount.AccountId) ? "~/Content/Images/CheckOnsmall.png" : "~/Content/Images/CheckOffsmall.png";
                    };

                    this.AccountId.DataSource = dataAccountsListnew;
                    this.AccountId.DataBind();

                    Dictionary<int, bool> isCheckedDictionarynew = dataAccountsListnew.ToDictionary(dataAccount => dataAccount.AccountId, dataAccount => currentDataAccountIdsnew.Contains(dataAccount.AccountId));

                    this.hfAccountIds.Value = "{ \"" + ids + "\": true }";
                    return;
                }

                if(this.Session["Role"]?.ToString() == "HCO Admin" || this.Session["Role"]?.ToString() == "Company Admin" ||
                   (this.Session["Role"]?.ToString() == "Device Co Tech" && TenantListss.Count>0) )
                {
                    Guid tenants = new Guid();
                    if (this.Session["Role"]?.ToString() == "HCO Admin" || this.Session["Role"]?.ToString() == "Company Admin")
                    {
                         tenants = new Guid(this.Session["TenantID"].ToString());
                    }
                    else
                    {
                        tenants = new Guid(this.Session["AccountGuids"].ToString());
                    }
                    IEnumerable<DataAccount> dataAccountsList = db.DataAccounts
                       .Where(dataAccount =>(dataAccount.Tenant== tenants) &&
                           (this.GroupName.SelectedIndex <= 0 || this.GroupName.SelectedValue == dataAccount.AccountGroupGuid.ToString()) &&   // Filter the DataAccounts by GroupName
                           (this.DataAccountTypeByUserRole == null || this.DataAccountTypeByUserRole == dataAccount.TypeOf))                   // Filter the DataAccounts by Role
                       .OrderBy(dataAccount => dataAccount.CompanyName)
                       .ToList();

                    IEnumerable<int> currentDataAccountIds = !this.IsAddAction ?
                        db.DataUserMasts
                            .Where(dataUserMast => !this.UserGuid.HasValue || dataUserMast.UserGuid == this.UserGuid.Value)
                            .Join(db.UserDataAccounts, dataUserMast => dataUserMast.ID, userDataAccount => userDataAccount.UserID, (dataUserMast, userDataAccount) => userDataAccount.AccountId)
                            .ToList()
                            .Join(dataAccountsList, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => accountId)
                            .ToArray() :
                        new int[0];

                    this.AccountId.ItemDataBound += (sender, args) =>
                    {
                        ImageButton imageButton = (ImageButton)args.Item.FindControl("DataAccountImageButton");
                        DataAccount dataAccount = (DataAccount)args.Item.DataItem;

                        imageButton.Enabled = (bool) this.Session["IsAdmin"];
                        //MRI center role  enable
                        if (this.Session["Role"]?.ToString() == "MRI Center Admin" ||
                            this.Session["Role"]?.ToString() == "Device Co Admin" ||
                            this.Session["Role"]?.ToString() == "HCO Admin" ||
                            this.Session["Role"]?.ToString() == "Company Admin")
                        {
                            imageButton.Enabled = true;
                        }
                        imageButton.Attributes["AccountId"] = dataAccount.AccountId.ToString();
                        imageButton.ImageUrl = currentDataAccountIds.Contains(dataAccount.AccountId) ? "~/Content/Images/CheckOnsmall.png" : "~/Content/Images/CheckOffsmall.png";
                    };

                    this.AccountId.DataSource = dataAccountsList;
                    this.AccountId.DataBind();

                    Dictionary<int, bool> isCheckedDictionary = dataAccountsList.ToDictionary(dataAccount => dataAccount.AccountId, dataAccount => currentDataAccountIds.Contains(dataAccount.AccountId));
                    this.hfAccountIds.Value = JsonConvert.SerializeObject(isCheckedDictionary);
                    return;
                }
                else
                {

                    IEnumerable<DataAccount> dataAccountsList = db.DataAccounts
                        .Where(dataAccount =>
                            (this.GroupName.SelectedIndex <= 0 || this.GroupName.SelectedValue == dataAccount.AccountGroupGuid.ToString()) &&   // Filter the DataAccounts by GroupName
                            (this.DataAccountTypeByUserRole == null || this.DataAccountTypeByUserRole == dataAccount.TypeOf))                   // Filter the DataAccounts by Role
                        .OrderBy(dataAccount => dataAccount.CompanyName)
                        .ToList();

                    IEnumerable<int> currentDataAccountIds = !this.IsAddAction ?
                        db.DataUserMasts
                            .Where(dataUserMast => !this.UserGuid.HasValue || dataUserMast.UserGuid == this.UserGuid.Value)
                            .Join(db.UserDataAccounts, dataUserMast => dataUserMast.ID, userDataAccount => userDataAccount.UserID, (dataUserMast, userDataAccount) => userDataAccount.AccountId)
                            .ToList()
                            .Join(dataAccountsList, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => accountId)
                            .ToArray() :
                        new int[0];

                    this.AccountId.ItemDataBound += (sender, args) =>
                    {
                        ImageButton imageButton = (ImageButton)args.Item.FindControl("DataAccountImageButton");
                        DataAccount dataAccount = (DataAccount)args.Item.DataItem;
                        Label Accountlabel = (Label)args.Item.FindControl("DataAccountlabel");
                        

                        imageButton.Enabled = (bool) this.Session["IsAdmin"];
                    //MRI center role  enable
                    if (this.Session["Role"]?.ToString() == "MRI Center Admin" ||
                        this.Session["Role"]?.ToString() == "Device Co Admin" ||
                        this.Session["Role"]?.ToString() == "HCO Admin" ||
                        this.Session["Role"]?.ToString() == "Company Admin")
                        {
                            imageButton.Enabled = true;
                        }

                        imageButton.Attributes["AccountId"] = dataAccount.AccountId.ToString();
                        imageButton.ImageUrl = currentDataAccountIds.Contains(dataAccount.AccountId) ? "~/Content/Images/CheckOnsmall.png" : "~/Content/Images/CheckOffsmall.png";
                        if (this.Session["Role"]?.ToString() == "Device Co Tech" || 
                            this.Session["Role"]?.ToString() == "MRI Doctor" || 
                            this.Session["Role"]?.ToString() == "MRI Tech")
                        {
                            if (imageButton.ImageUrl == "~/Content/Images/CheckOffsmall.png")
                            {
                                imageButton.Visible = false;
                                Accountlabel.Visible = false;


                            }
                        }
                    };

                    this.AccountId.DataSource = dataAccountsList;
                    this.AccountId.DataBind();

                    Dictionary<int, bool> isCheckedDictionary = dataAccountsList.ToDictionary(dataAccount => dataAccount.AccountId, dataAccount => currentDataAccountIds.Contains(dataAccount.AccountId));
                    this.hfAccountIds.Value = JsonConvert.SerializeObject(isCheckedDictionary);
                    return;

                }
            }
        }

        protected void DataAccountImageButton_Click(object sender, ImageClickEventArgs e)
        {
            Dictionary<int, bool> isCheckedDictionary = JsonConvert.DeserializeObject<Dictionary<int, bool>>(this.hfAccountIds.Value);
            ImageButton senderImageButton = (ImageButton)sender;

            int accountId = int.Parse(senderImageButton.Attributes["AccountId"]);
            bool isChecked = isCheckedDictionary[accountId];

            senderImageButton.ImageUrl = isChecked ? "~/Content/Images/CheckOnsmall.png" : "~/Content/Images/CheckOffsmall.png";

            if (!this.IsMultipleDataAccountsEnabled)
            {
                // The user cannot have more than 1 DataAccount, so reset 'isCheckedDictionary' and set all other ImageButtons in the repeater to unchecked
                isCheckedDictionary.Clear();
                isCheckedDictionary.Add(accountId, isChecked);

                foreach (ImageButton imageButton in this.AccountId.Items.Cast<RepeaterItem>()
                    .Select(repeaterItem => (ImageButton)repeaterItem.FindControl("DataAccountImageButton"))
                    .Where(imageButton => imageButton != sender))
                {
                    imageButton.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                }
            }

            this.hfAccountIds.Value = JsonConvert.SerializeObject(isCheckedDictionary);
        }

        protected void GroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PopulateAccountList();
        }

        private void EnableControls(HtmlGenericControl ctrBody)
        {
            if (this.Session["Role"] != null && this.Session["Role"].ToString() == "View Only")
            {
                // ignore
            }
            else
            {
                var obj = new ControlFiller();
                obj.EnableControls(ctrBody);
            }
        }

        private void FillCtls(object destination)
        {
            var obj = new ControlFiller();
            obj.FillControls(destination, this.Panel1);
            obj.ShowRequiredControls(this.divbody);
        }

        private void FindOnReturnOfRedirectId(string id)
        {
            //---------------------------------------------------------------------------------
            // yes change only this code
            const string tableName = "DataUserMast"; // <-- put table name here
            const string fieldName = "ID"; // <-- put the field name here

            #region hidden

            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------
            // no need to change any of this code
            string outError;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            var id2 = id != string.Empty ? int.Parse(id) : 0;
            var destination = repository.FindById(tableName, fieldName, id2, out outError);

            if (outError == string.Empty)
            {
                this.EnableControls(this.divbody);
                this.FillCtls(destination);
                if (destination != null)
                {
                    if (destination.IsSendEmail == true)
                    {
                        this.ImageButtonSendEmails.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                    }
                    else
                    {
                        this.ImageButtonSendEmails.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                    }

                    if (destination.IsSendText == true)
                    {
                        this.ImageButtonSendTextMessage.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                    }
                    else
                    {
                        this.ImageButtonSendTextMessage.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                    }

                    if (destination.IsForceChangePassword)
                    {
                        this.ImageButtonForcePasswordReset.ImageUrl = "~/Content/Images/CheckOnsmall.png";
                    }
                    else
                    {
                        this.ImageButtonForcePasswordReset.ImageUrl = "~/Content/Images/CheckOffsmall.png";
                    }
                }
            }
            //---------------------------------------------------------------------------------

            #endregion
        }

        #region hidden - protected controls

        // these controls are actually on the page
        protected void btnnew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/UserEdit.aspx");
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("/Pages/Account/UserList.aspx");
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            this.ClearButton();
        }

        private void ClearButton()
        {
            var obj = new ControlFiller();
            obj.ClearControls(this.Panel1);
        }
        #endregion

        public bool SaveRecord()
        {
            try
            {
                using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                {
                    DataUserMast saving;
                    string temp;

                    if (this.IsAddAction)
                    {
                        Guid newGuid = Guid.NewGuid();
                        saving = new DataUserMast() { UserGuid = newGuid, First8UserGuid = newGuid.ToString().Substring(0, 8) };
                        db.DataUserMasts.Add(saving);
                    }
                    else
                    {
                        saving = db.DataUserMasts.First(dataUserMast => dataUserMast.UserGuid.Value == this.UserGuid.Value);
                    }

                    if (saving.Role != this.Role.SelectedItem.Text)
                    {
                        saving.Role = this.Role.SelectedItem.Text;
                        saving.RoleID = int.Parse(this.Role.SelectedValue);
                    }

                    if (saving.FirstName != this.FirstName.Text)
                    {
                        saving.FirstName = this.FirstName.Text;
                    }

                    if (saving.LastName != this.LastName.Text)
                    {
                        saving.LastName = this.LastName.Text;
                    }

                    temp = saving.FirstName + " " + saving.LastName;
                    if (saving.FullName != temp)
                    {
                        saving.FullName = temp;
                    }

                    if (saving.UserName != this.UserName.Text)
                    {
                        saving.UserName = this.UserName.Text;
                    }

                    // (1) If this is to be a new user, then Password.Text comes back to the server as plaintext.
                    // (2) If this is an existing user being edited, then Password.Text's default value is encrypted. However if Password.Text is changed then it comes back to the server as plaintext.
                    if (saving.Password != this.Password.Text)
                    {
                        saving.Password = this.dHelper.Encrypt(this.Password.Text);
                    }

                    if (saving.AccountGuid == null)
                    {
                        saving.AccountGuid = new Guid();
                    }

                    saving.LastUpdatePerson = this.Session["UserName"].ToString();
                    saving.LastModified = GetTimeNow.GetTime();

                    IEnumerable<DataAccount> Accounts = this.SelectedDataAccountIds
                            .Join(db.DataAccounts, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => dataAccount)
                            .ToList();

                    
                    foreach (var acc in Accounts)
                    {
                        var responsed = string.Empty;
                        if (this.Request.QueryString["id"] != null)
                        {
                            responsed = this.Request.QueryString["id"];
                        }
                        if ((this.Session["Role"]?.ToString() == "HCO Admin" || this.Session["Role"]?.ToString() == "Company Admin") && this.Session["UserGuid"].ToString() == responsed)
                        {
                            saving.Tenant = acc.AccountGuid;
                            saving.AccountGuid = acc.AccountGuid;
                            break;
                        }
                        else if (this.Session["Role"].ToString() == "HCO Admin" || this.Session["Role"].ToString() == "Company Admin"  || this.Session["Role"].ToString() == "Super Admin")
                        {
                            saving.Tenant = acc.Tenant;
                            saving.AccountGuid = acc.Tenant;
                        }
                        else
                        {
                            saving.Tenant = acc.AccountGuid;
                            saving.AccountGuid = acc.Tenant;
                        }
                    }
                   
                    temp = Regex.Replace(this.CellPhone.Text, @"1?\W*(\d{3})\W*(\d{3})\W*(\d{4})", "($1) $2-$3");
                    if (saving.CellPhone != temp)
                    {
                        saving.CellPhone = temp;
                    }

                    temp = Regex.Replace(this.OfficePhone.Text, @"1?\W*(\d{3})\W*(\d{3})\W*(\d{4})", "($1) $2-$3");
                    if (saving.OfficePhone != temp)
                    {
                        saving.OfficePhone = temp;
                    }

                    if (saving.EmailAddress != this.EmailAddress.Text)
                    {
                        saving.EmailAddress = this.EmailAddress.Text;
                    }

                    if (saving.IsForceChangePassword != this.ImageButtonForcePasswordReset.ImageUrl.Contains("On"))
                    {
                        saving.IsForceChangePassword = this.ImageButtonForcePasswordReset.ImageUrl.Contains("On");
                    }

                    if (saving.IsSendEmail != this.ImageButtonSendEmails.ImageUrl.Contains("On"))
                    {
                        saving.IsSendEmail = this.ImageButtonSendEmails.ImageUrl.Contains("On");
                    }

                    if (saving.IsSendText != this.ImageButtonSendTextMessage.ImageUrl.Contains("On"))
                    {
                        saving.IsSendText = this.ImageButtonSendTextMessage.ImageUrl.Contains("On");
                    }

                    db.SaveChanges();

                    // Update the 'UserDataAccounts' table with the selected DataAccounts, adding and removing records as necessary. The DataUserMast record must be created by this point.
                    IEnumerable<DataAccount> currentDataAccounts = db.UserDataAccounts
                        .Where(userDataAccount => userDataAccount.UserID == saving.ID)
                        .Join(db.DataAccounts, userDataAccount => userDataAccount.AccountId, dataAccount => dataAccount.AccountId, (userDataAccount, dataAccount) => dataAccount)
                        .ToList();

                    IEnumerable<DataAccount> selectedDataAccounts = this.SelectedDataAccountIds
                        .Join(db.DataAccounts, accountId => accountId, dataAccount => dataAccount.AccountId, (accountId, dataAccount) => dataAccount)
                        .ToList();

                    IEnumerable<UserDataAccount> removeTheseUserDataAccounts = db.UserDataAccounts
                        .Where(userDataAccount => userDataAccount.UserID == saving.ID)
                        .ToList()
                        .Where(userDataAccount => !this.SelectedDataAccountIds.Contains(userDataAccount.AccountId));

                    IEnumerable<UserDataAccount> addTheseUserDataAccounts = selectedDataAccounts.Except(currentDataAccounts, new DataAccount.Comparer())
                        .Select(dataAccount => new UserDataAccount() { UserID = saving.ID, AccountId = dataAccount.AccountId });

                    db.UserDataAccounts.RemoveRange(removeTheseUserDataAccounts);
                    db.UserDataAccounts.AddRange(addTheseUserDataAccounts);
                    db.SaveChanges();

                    if (this.IsAddAction && !string.IsNullOrWhiteSpace(saving.CellPhone))
                    {
                        string formattedCellPhone = Regex.Match(saving.CellPhone, @"\d+").Value;
                        SendTextMsg.TextMessage(new List<string> { formattedCellPhone }, "Welcome to Smart-Praxis you can login @ http://mrisafemode.com", this.Session["CompanyName"]?.ToString());
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException?.InnerException != null && ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    //ret4 = "Error, the user name already exists";
                }

                return false;
            }
        }

        protected void btnsubmit1a_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (this.SaveRecord())
                {
                    this.Response.Redirect("/Pages/Account/UserList.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotification('Error Saving Record')", true);
                }
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Business Rule: Make GroupNames mandatory only for "Device Co Admin" and "Device Co Tech" users. Hide this control for all others.
            this.GroupNameContainer.Visible = this.Role.SelectedItem.Text == "Device Co Admin" || this.Role.SelectedItem.Text == "Device Co Tech";
            
            if (!this.GroupNameContainer.Visible)
            {
                this.GroupName.SelectedIndex = 0;
            }

            this.PopulateAccountList();
        }

        protected void ImageButtonForcePasswordReset_Click(object sender, ImageClickEventArgs e)
        {
            if (this.ImageButtonForcePasswordReset.ImageUrl.Contains("On"))
            {
                this.ImageButtonForcePasswordReset.ImageUrl = "~/Content/Images/CheckOffsmall.png";
            }
            else
            {
                this.ImageButtonForcePasswordReset.ImageUrl = "~/Content/Images/CheckOnsmall.png";
            }
        }

        protected void ImageButtonSendTextMessage_Click(object sender, ImageClickEventArgs e)
        {
            if (this.ImageButtonSendTextMessage.ImageUrl.Contains("On"))
            {
                this.ImageButtonSendTextMessage.ImageUrl = "~/Content/Images/CheckOffsmall.png";
            }
            else
            {
                this.ImageButtonSendTextMessage.ImageUrl = "~/Content/Images/CheckOnsmall.png";
            }
        }

        protected void ImageButtonSendEmails_Click(object sender, ImageClickEventArgs e)
        {
            if (this.ImageButtonSendEmails.ImageUrl.Contains("On"))
            {
                this.ImageButtonSendEmails.ImageUrl = "~/Content/Images/CheckOffsmall.png";
            }
            else
            {
                this.ImageButtonSendEmails.ImageUrl = "~/Content/Images/CheckOnsmall.png";
            }
        }

        protected void RequiredControlValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            WebControl controlToValidate = (WebControl) this.Panel1.FindControl(((CustomValidator)source).ControlToValidate);

            if (controlToValidate.Enabled && controlToValidate.Visible)
            {
                if (controlToValidate is TextBox)
                {
                    string textBoxValue = ((TextBox)controlToValidate).Text;

                    if (controlToValidate == this.UserName)
                    {
                        if (this.IsAddAction)
                        {
                            using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                            {
                                args.IsValid = !string.IsNullOrWhiteSpace(textBoxValue) && db.DataUserMasts.All(dataUserMast => dataUserMast.UserName != textBoxValue);
                            }
                        }
                    }
                    else if (controlToValidate == this.Password)
                    {
                        args.IsValid = !this.IsAddAction || !string.IsNullOrWhiteSpace(textBoxValue);
                    }
                    else if (controlToValidate == this.CellPhone || controlToValidate == this.OfficePhone)
                    {
                        args.IsValid = string.IsNullOrWhiteSpace(this.OfficePhone.Text) || Regex.IsMatch(textBoxValue, @"1?\W*\d{3}\W*\d{3}\W*\d{4}");
                    }
                    else if (controlToValidate == this.EmailAddress)
                    {
                        args.IsValid = new EmailAddressAttribute().IsValid(textBoxValue);
                    }
                    else if (string.IsNullOrWhiteSpace(textBoxValue))
                    {
                        args.IsValid = false;
                    }
                }
                else if (controlToValidate is DropDownList)
                {
                    args.IsValid = ((DropDownList)controlToValidate).SelectedIndex > 0;
                }
                else if (controlToValidate is RadCheckBoxList)
                {
                    args.IsValid = ((RadCheckBoxList)controlToValidate).SelectedItems.Count() > 0;
                }
                else
                {
                    throw new NotSupportedException("The server method 'RequiredControlValidator_ServerValidate' has not been configured to use this type of control.");
                }

                controlToValidate.BackColor = args.IsValid ? Color.White : Color.FromArgb(0xffdae0); // 0xffdae0 is a reddish color indicating a required field that needs to be filled in.
                 if(controlToValidate.ID== "UserName" && !args.IsValid) {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "text", "showNotification('The UserName is Already Exist')", true);
                }
 
            }
        }

        protected void csvAccountId_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Dictionary<int, bool> isCheckedDictionary = JsonConvert.DeserializeObject<Dictionary<int, bool>>(this.hfAccountIds.Value);
            args.IsValid = isCheckedDictionary.Any(keyValuePair => keyValuePair.Value);

            this.AccountIdContainer.BackColor = args.IsValid ? Color.White : Color.FromArgb(0xffdae0); // 0xffdae0 is a reddish color indicating a required field that needs to be filled in.
        }

        private IEnumerable<int> SelectedDataAccountIds
        {
            get
            {
                Dictionary<int, bool> clickedCheckboxes = JsonConvert.DeserializeObject<Dictionary<int, bool>>(this.hfAccountIds.Value);

                return clickedCheckboxes
                    .Where(keyValuePair => keyValuePair.Value)
                    .Select(keyValuePair => keyValuePair.Key);
            }
        }

        private string DataAccountTypeByUserRole
        {
            get
            {
                return DataAccount.GetDataAccountTypeByUserRole(this.Role.SelectedItem?.Text);
            }
        }

        private bool IsAddAction
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Request.QueryString["id"]);
            }
        }

        private Guid? UserGuid
        {
            get
            {
                return !this.IsAddAction ? (Guid?)Guid.Parse(this.Request.QueryString["id"]) : null;
            }
        }

        private bool IsMultipleDataAccountsEnabled
        {
            get { return DataAccount.IsMultipleDataAccountsEnabled(this.Role.SelectedItem.Text); }
        }
    }
}
