﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using SmartPraxis.Helper;
using SmartPraxis.Models;
using SmartPraxis.Shared;



namespace SmartPraxis.Pages.Account
{
    public partial class ForgotPassword : Page
    {
        static readonly GeneralHelper dHelper = new GeneralHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void ButtonForgot_Click(object sender, EventArgs e)
        {
            using (var db = new DB_SmartPraxisEntities())
            {
                var phone = formatPhone(this.txtEmail.Text);
                var query = (from d in db.DataUserMasts
                    where d.UserName == this.txtEmail.Text || d.CellPhone == phone
                             select new
                    {   d.Password,
                        d.CellPhone,
                        d.UserGuid
                    }).ToList();

                if (query.Count > 0)
                {
                    var phones = new List<string>();
                    var pwd = string.Empty;
                    var userGuid = new Guid?();
                    foreach (var item in query)
                    {
                        // there is only one with this username
                        phones.Add(item.CellPhone);
                        pwd = dHelper.Decrypt(item.Password);
                        userGuid = item.UserGuid;
                    }

                    string purl = "http://smart-praxis.com/pages/account/resetpassword.aspx?id=" + userGuid;
#if DEBUG
                    purl = "http://localhost:4620/pages/account/resetpassword.aspx?id=" + userGuid;
#endif
                    // http://jphellemons.nl/post/Google-URL-shortener-API-(googl)-C-sharp-class-C
                    var goo = new GoogleUrlShortnerApi();
                    var result = goo.Shorten(purl);
                   
                    SendTextMsg.TextMessage(phones, "Reset Password... " + ", login @ " + result, string.Empty);

                }
                else
                {
                    this.txtEmail.Text = "No user email or phone number error";
                }

                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
        }

        private static readonly Regex ShortUrlRegex =
            new Regex("^http[s]?://goo.gl/", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static bool IsShortUrl(string url)
        {
            return ShortUrlRegex.IsMatch(url);
        }

        


        public static string formatPhone(string x)

        {
            var result = "";
            double y = 0;

            if (x.Length <= 8 || x.Length > 15)

            {

                //MessageBox.Show("Your phone number does not contain the correct " +

                //                "number of digits. Please format number " +

                //                "as ###-###-####", "Incorrect Number Format", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                result = "User Error";

                return result;

            }

            else

            {

                if (x.StartsWith("1-"))

                {

                    x = x.Remove(0, 1);

                    x = x.Remove(0, 1);

                    x = x.Remove(3, 1);

                    x = x.Remove(6, 1);

                }

                if (x.Length == 14)

                {

                    x = x.Remove(0, 1);

                    x = x.Remove(3, 1);

                    x = x.Remove(3, 1);

                    x = x.Remove(6, 1);

                    y = Double.Parse(x);

                }

                else if (x.Length == 12)

                {

                    x = x.Remove(3, 1);

                    x = x.Remove(6, 1);

                    y = Double.Parse(x);

                }

                else if (x.Length == 10)

                {

                    y = Double.Parse(x);

                }

                else

                {

                    y = Double.Parse(x);

                }

                x = String.Format("{0:(###) ###-####}", y);

                return x;

            } //end else

        } //End public static string formatPhone(string x)
    }

    public class GoogleUrlShortnerApi
    {
        private const string key = "AIzaSyAUSwBzXSHAsQb4ff1rr13hqKLe-VWNuzY";

        public string Shorten(string url)
        {
            string post = "{\"longUrl\": \"" + url + "\"}";
            string shortUrl = url;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.googleapis.com/urlshortener/v1/url?key=" + key);

            try
            {
                request.ServicePoint.Expect100Continue = false;
                request.Method = "POST";
                request.ContentLength = post.Length;
                request.ContentType = "application/json";
                request.Headers.Add("Cache-Control", "no-cache");

                using (Stream requestStream = request.GetRequestStream())
                {
                    byte[] postBuffer = Encoding.ASCII.GetBytes(post);
                    requestStream.Write(postBuffer, 0, postBuffer.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader responseReader = new StreamReader(responseStream))
                        {
                            string json = responseReader.ReadToEnd();
                            shortUrl = Regex.Match(json, @"""id"": ?""(?<id>.+)""").Groups["id"].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // if Google's URL Shortner is down...
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return shortUrl;
        }
    }
}