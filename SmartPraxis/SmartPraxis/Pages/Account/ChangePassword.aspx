﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="SmartPraxis.Pages.Account.ChangePassword"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutEmpty.Master" Title="Smart-Praxis" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.50508.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page on this section.--%>
    <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrap.css" />
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadNotification1" />
                    <telerik:AjaxUpdatedControl ControlID="ConfigurationPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <script type="text/javascript">
        function showNotification(message)
        {
            //$find("<%= this.RadNotification1.ClientID %>").text = message;
            $find("<%= this.RadNotification1.ClientID %>").show();
        }
    </script>
    <style type="text/css">
          .autocomplete_CompletionListElement
        {
            margin: 0px;
            background-color: White;
            cursor: default;
            list-style-type: none;
            overflow-y: auto;
            overflow-x: hidden;
            height:180px;
            text-align: left;
            border: 1px solid #777;
            z-index:10000;
        } 

        .modalBackground {
            background-color:Gray;
            filter:alpha(opacity=70);
            opacity:0.7;
        }
        
        .modalPopup {
	        background-color:#ffffdd;
	        border-width:3px;
	        border-style:solid;
	        border-color:Gray;
	        padding:3px;
	        width:250px;
        }
    </style>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
	    var counter;
	    counter = 0;

	    function KeepSessionAlive() {
	        // Increase counter value, so we'll always get unique URL (sample source from page)
	        // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
	        counter++;

	        // Gets reference of image
	        var img = document.getElementById("imgSessionAlive");

	        // Set new src value, which will cause request to server, so
	        // session will stay alive
	        
	        img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;
	        

	        // Schedule new call of KeepSessionAlive function after 60 seconds
	        setTimeout(KeepSessionAlive, 60000);
	    }

	    // Run function for a first time
	    KeepSessionAlive();
	</script> 
   <div>
       <br/><br/>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-1">
                            
                        </div>
                        <div class="col-lg-10">
                            <div class="panel panel-default">
                                
                                <div style="vertical-align: bottom;" class="panel-heading">
                                    <h4>Please Change Your Password</h4>    
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-3">
                                        &nbsp;
                                    </div>
                                </div>
                             
                                <div runat="server" id="divbody" class="panel-body">
                                        <asp:Panel ID="Panel1" runat="server">
                                        <asp:HiddenField runat="server" ID="ID" Value="" />
                                        <asp:HiddenField runat="server" ID="UserGuid" Value="" />
                                        
                                        <div class="row" style="display:none">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Account</label><asp:Label ID="Label5" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:DropDownList Style="cursor:pointer;" alt="required" ID="AccountIds" runat="server" AppendDataBoundItems="true" CssClass="form-control" SkinID="NotRequired" Enabled="False"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        User Name</label><asp:Label ID="Label7"  Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *"></asp:Label>
                                                    <asp:TextBox ID="UserName" alt="required" Enabled="False" CssClass="form-control" name="username" runat="server" accept="User Name" placeholder="User Name" ValidationGroup="Required" SkinID="Required" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Password</label>
                                                    <asp:Label ID="Label2" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *" />&nbsp;
                                                    <asp:TextBox ID="Password" alt="required" CssClass="form-control" name="password" runat="server" accept="Password" placeholder="Password" ValidationGroup="Required" TextMode="Password" SkinID="Required" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>
                                                        Repeat Password</label>
                                                    <asp:Label ID="Label6" Visible="True" runat="server" ForeColor="Red" SkinID="Required" Text=" *" />&nbsp;
                                                    <asp:TextBox ID="PasswordAgain" alt="required" CssClass="form-control" name="password" runat="server" accept="Password" placeholder="Password" ValidationGroup="Required" TextMode="Password" SkinID="Required" />
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both;float: left; position: relative; margin-top:40px" class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <!-- This html button triggers the jquery above -->
                                                    <div style="float:left" runat="server" id="divBtnSubmit"><asp:Button ID="btnsubmit" runat="server" Text="Reset Password" class="btn btn-success" OnClick="btnsubmit1a_Click" />&nbsp;</div>
                                                </div>
                                                <br/><br/>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-md-10">
                    <telerik:RadNotification RenderMode="Lightweight" runat="server" ShowSound="/Content/Sounds/alert3.wav" ID="RadNotification1" runat="server" Position="Center"
                             Width="330px" Height="160px" Animation="Fade" EnableRoundedCorners="True" EnableShadow="True"
                             Title="Error" Text="Error, the user name already exists."
                             Style="z-index: 100000">
                    </telerik:RadNotification>
                </div>
            </div>  
           
<br/>     <br/><br/><br/>
    </div>
    <!-- DataTables JavaScript -->
   <%-- <script src="/Content/Scripts/jquery.dataTables.min.js"></script>
    <script src="/Content/Scripts/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gvGrid').DataTable({
                responsive: true
            });
        });
    </script>--%>
</asp:Content>
