﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Notify.aspx.cs" Inherits="SmartPraxis.Pages.Account.Notify"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutNotify.Master" Title="Smart-Praxis" %>

<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">
     <link rel="stylesheet" href="/Content/StyleSheets/dataTables.bootstrapWithButtons.css" />
    <img alt="" id="imgSessionAlive" width="1" height="1" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" >
      // Helper variable used to prevent caching on some browsers
        var counter;
        counter = 0;

        function KeepSessionAlive() {
            // Increase counter value, so we'll always get unique URL (sample source from page)
            // http://www.beansoftware.com/ASP.NET-Tutorials/Keep-Session-Alive.aspx
            counter++;

            // Gets reference of image
            var img = document.getElementById("imgSessionAlive");

            // Set new src value, which will cause request to server, so
            // session will stay alive
            
            img.src = "http://mrisafemode.com/RefreshSessionState.aspx?c=" + counter;
            ////img.src = "http://localhost:10240/RefreshSessionState.aspx?c=" + counter;

            // Schedule new call of KeepSessionAlive function after 60 seconds
            setTimeout(KeepSessionAlive, 60000);
        }

        // Run function for a first time
        KeepSessionAlive();
    </script> 
     
    <div style="visibility: hidden">
        <asp:HyperLink ID="HyperLinkHome"  runat="server"><i class="fa fa-home"></i> / </asp:HyperLink>    
    </div>
    
    <div class="user-stats">
        <div class="row">
            <%-- <div class="col-md-1">
            </div>--%>
            <div class="col-md-4">
            </div>
        </div>
        <div class="row">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-10">
                            <br/>
                            <asp:Label ID="InternalMessage" runat="server" Text="Label" Font-Bold="True" Font-Size="24px" ForeColor="Red"></asp:Label>
                            <br/><br/><asp:Button ID="ClickToContinue" class="btn btn-primary" runat="server" Text="Click To Continue" OnClick="ClickToContinue_Click" />
                            <br/><br/>
                        </div>
                        <br/>
                </div>
                </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-md-4">
                                
                </div>
            </div>        
    </div>
</asp:Content>