﻿using System;
using System.Linq;
using System.Web.UI;
using SmartPraxis.Helper;
using SmartPraxis.Models;

namespace SmartPraxis
{
    public partial class ResetPassword : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:alert(screen.width); ", true);
            this.ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:GetTimeZoneOffset(); ", true);
            //

            if (!this.IsPostBack)
            {

                using (var db = new DB_SmartPraxisEntities())
                {
                    var query2 = (from d in db.DataSettings
                        select d).FirstOrDefault();
                    if (query2 != null)
                    {
                        this.Session["PathName"] = query2.PathName;
                        this.Session["BackgroundColor"] = query2.BackgroundColor;
                        this.Session["PostmarkAuthenticationKey"] = query2.PostmarkAuthenticationKey;
                        this.Session["InternalMessage"] = query2.InternalMessage;
                        this.Session["ExternalMessage"] = query2.ExternalMessage;
                        this.Session["InternalMsgEnabled"] = query2.InternalMsgEnabled;
                        this.Session["ExternalMsgEnabled"] = query2.ExternalMsgEnabled;

                        if (query2.ExternalMsgEnabled.ToString() == "True")
                        {
                            this.ExternalMessage.Visible = true;
                            this.LiteralExternalMessage.Text = query2.ExternalMessage;
                        }
                    }
                }
            }
        }

        protected void ButtonContinue_Click1(object sender, EventArgs e)
        {
            
            // this will change the user password and log them in
            var userid = new Guid(this.Request.QueryString["UserGuid"]);
            var dHelper = new GeneralHelper();
            var password1 = dHelper.Encrypt(this.txtPassword1.Text);
            var password2 = dHelper.Encrypt(this.txtPassword2.Text);

            if (password1 != password2)
            {
                return;
            }

            using (var db = new DB_SmartPraxisEntities())
            {
                // change password
                var update = (from d in db.DataUserMasts where d.UserGuid == userid select d).FirstOrDefault();
                if (update != null)
                {
                    update.Password = password1;
                    db.SaveChanges();
                }
            }

            // do the usual login routine
            if (!DataUserMast.Login(userid, this.screenSize.Text))
            {
                this.error_block.Visible = true;
                this.ltrError.Text = @"Invalid Username or password.";
            }
        }
    }
}


