﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Shared;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace SmartPraxis.Pages.MRICompany
{
    public partial class PatientList : Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();
        public static bool GridRowsFound;
        private static int gridRowInt;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            this.MasterPageFile = PreInitMasterPageFile.MasterPageChooser(this.Session["Role"].ToString());
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific to this page

            this.Session["BackButton"] = null;

            if (!this.Session["Role"].ToString().Contains("MRI"))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (!this.Page.IsPostBack)
            {
                this.PopulateStatusDropDown();
                //this.DropDownListStatus.SelectedIndex = this.DropDownListStatus.Items.IndexOf(this.DropDownListStatus.Items.FindByText(responsed));
            }
            
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    this.ViewState["sort"] = "MRIScanDate Desc, MRIScanTime Desc";
                    this.PopulateGrid();
                }
                string head = this.Session["CompanyNamesDelimited"]?.ToString();
                this.titlenew.InnerText = head.ToString();
            }

            var o = this.Session["Role"]?.ToString();
            this.HyperLinkHome.NavigateUrl = HyperlinkHome.HomeChooser(o);
        }

        private void PopulateGrid()
        {
            var destination = this.GetDataForGrid(this.ViewState["sort"].ToString());

            this.gvGrid.DataSource = destination;
            this.gvGrid.DataBind();

            if (destination.Rows.Count > 0)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            //else
            //{
            //    Debugger.Break();
            //}
        }

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton)sender).CommandArgument;
            var repository = new PatientRequestsRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx");
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            // var isCompleted = false;
            var isCompletedValue = string.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // the term IsCompleted means that the record should not be updated because this is a DONE record
                isCompletedValue = DataBinder.Eval(e.Row.DataItem, "Completed").ToString();

                var status = DataBinder.Eval(e.Row.DataItem, "PatientStatusDDL").ToString().ToLower();
                var lStatus = (Literal)e.Row.FindControl("ltrPatientStatusDDL");
                if (status.Contains("pending"))
                    lStatus.Text = @"<span class=""label label-warning"">Pending</span>";
                else if (status.Contains("rejected"))
                    lStatus.Text = @"<span class=""label label-danger"">Rejected</span>";
                else if (status.Contains("canceled"))
                    lStatus.Text = @"<span class=""label label-danger"">Canceled</span>";
                else if (status.Contains("comments"))
                    lStatus.Text = @"<span class=""label label-info"">Comments</span>";
                else if (status.Contains("accepted"))
                    lStatus.Text = @"<span class=""label label-success"">Accepted</span>";
                else if (status.Contains("done"))
                    lStatus.Text = @"<span class=""label label-primary"">Done</span>";

                // report column
                var report = DataBinder.Eval(e.Row.DataItem, "AttachedFile").ToString().ToLower();
                if (!string.IsNullOrEmpty(report))
                {
                    var document = (HyperLink)e.Row.FindControl("LinkButtonPdfIcon");
                    if (document != null) document.Visible = true;
                }
                else
                {
                    var document = (HyperLink)e.Row.FindControl("LinkButtonPdfIcon");
                    if (document != null) document.Visible = false;
                }

                // order column
                var order = DataBinder.Eval(e.Row.DataItem, "OrderAttachedFile").ToString().ToLower();
                if (!string.IsNullOrEmpty(order))
                {
                    var document = (HyperLink)e.Row.FindControl("LinkButtonOrderPdfIcon");
                    if (document != null) document.Visible = true;
                }
                else
                {
                    var document = (HyperLink)e.Row.FindControl("LinkButtonOrderPdfIcon");
                    if (document != null) document.Visible = false;
                }
            }

            var deletebutton = (LinkButton)e.Row.FindControl("LinkButtonDelete");
            if (this.Session["Role"].ToString().Contains("Super Admin") ||
               this.Session["Role"].ToString().Contains("MRI Center"))
            {
                if (deletebutton != null && isCompletedValue == "False")
                {
                    deletebutton.Visible = true;
                }
                else if (deletebutton != null)
                {
                    deletebutton.Visible = false;
                }
            }
            else
            {
                if (deletebutton != null)
                {
                    deletebutton.Visible = false;
                }
            }

           
        }

        protected void ImageButton1Click(object sender, EventArgs eventArgs)
        {
            //ltrHiddenFileDoc
            var file = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(file))
            {
                this.Session["BackButton"] = $"/pages/MRICompany/PatientList.aspx?x={this.Request.QueryString["x"]}";
                

                //var page = $"~/PdfViewer.aspx?y={file}";
                //this.Response.Write("<script>");
                //var fmt = $"window.open('{page}','_blank')";
                //this.Response.Write(fmt);
                //this.Response.Write("</script>");

                //string filename = this.Server.MapPath("~/Documents/" + file);
                //FileInfo fileInfo = new FileInfo(filename);

                //if (fileInfo.Exists)
                //{
                //    //using (var fs = new FileStream(filename, FileMode.Create))
                //    //{
                //    //    fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                //    //}

                //    this.Response.Clear();
                //    this.Response.AddHeader("Content-Disposition", "inline;attachment; filename=" + fileInfo.Name);
                //    this.Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                //    this.Response.ContentType = "application/octet-stream";
                //    this.Response.Flush();
                //    this.Response.WriteFile(fileInfo.FullName);
                //    this.Response.End();
                //}
            }


        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("Patient.aspx");
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
            this.PopulateGrid();
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }

        public DataTable GetDataForGrid(string sSort)
        {
            DataView dv = FillDataSet.FillDt("spFilterPatientList", new[]
            {
                new SqlParameter("@valuePassedIn", this.Filter.Text),
                new SqlParameter("@DataAccountIds", (string) this.Session["DataAccountIdsDelimited"]),
                new SqlParameter("@dropdownFilter", this.DropDownListStatus.SelectedItem.Text),
                new SqlParameter("@IsAdmin", (bool) this.Session["IsAdmin"])
            }).DefaultView;

            dv.Sort = sSort;
            return dv.ToTable();
        }

        protected void Filter_TextChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            this.Filter.Text = string.Empty;
            this.PopulateGrid();
            this.Filter.Focus();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] s = this.ViewState["sort"].ToString().Split();    //load the last sort
            string sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                    sSort += " DESC";
                else
                    sSort += " ASC";
            }
            else
                sSort += " ASC";

            //find which column is being sorted to change its style
            int i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" + sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            //get the sorted data
            var dt = this.GetDataForGrid(sSort);
            this.gvGrid.DataSource = dt;
            this.gvGrid.DataBind();

            gridRowInt = dt.Rows.Count;
            if (gridRowInt == 0)
            {
                GridRowsFound = false;
            }
            else
            {
                GridRowsFound = true;
            }

            this.gvGrid.DataSource = this.GetDataForGrid(sSort);
            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }

        public void PopulateStatusDropDown()
        {
            const string tableName = "ListStatus"; // <-- put table name here
            const string fieldName = "Role"; // <-- put the field name here
            string outError;

            var destination = this.GetPatientStatusDDL(tableName, fieldName, out outError);

            if (outError == string.Empty)
            {
                this.DropDownListStatus.DataSource = destination;
                this.DropDownListStatus.DataValueField = "StatusGuid";
                this.DropDownListStatus.DataTextField = "Description";
                this.DropDownListStatus.DataBind();

                this.DropDownListStatus.Items.Insert(0, new ListItem("- ALL -", "0"));
            }
        }

        public List<ListStatu> GetPatientStatusDDL(string table, string column, out string outError)
        {
            outError = string.Empty;

            try
            {
                var str = string.Format("Select distinct * from {0} order by Description", table);
                var result = this.db.ListStatus.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() >= 1)
                {
                    return result.ToList();
                }
            }
            finally
            {
            }

            return null;
        }

        protected void DropDownListStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ViewState["sort"] = "PatientId Desc";
            this.PopulateGrid();
        }
    }
}