﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileNotFound.aspx.cs" Inherits="SmartPraxis.Pages.FileNotFound"
    MasterPageFile="~/Shared/MasterPages/SiteLayout.Master" Title="File Not Found | Smart-Praxis" %>

<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">

  

    <div class="text-center page-wrap">
        <h1>404 <i class="fa fa-file"></i></h1>
        <h2>We're sorry, but the page you were looking for doesn't exist.</h2>
    </div>

</asp:Content>