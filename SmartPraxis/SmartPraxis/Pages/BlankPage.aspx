﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BlankPage.aspx.cs" Inherits="SmartPraxis.Pages.BlankPage"
    MasterPageFile="~/Shared/MasterPages/SiteLayoutOffice.Master" Title="Blank Page | Smart-Praxis" %>

<asp:Content ID="HeadCurrentPage" ContentPlaceHolderID="head" runat="server" >
    <%--You can add your custom code for each page header in this section.--%>
</asp:Content>
<asp:Content ID="StyleSheetCurrentPage" ContentPlaceHolderID="StyleSheetPage" runat="server">
    <%--You can add your custom style sheets for each page in this section.--%>
</asp:Content>
<asp:Content ID="bodyPage" ContentPlaceHolderID="ContentBody" runat="server">

    

    <div class="text-center page-wrap">
        <h1>AMAZING HEADER</h1>
        <p>Your amazing content goes here!</p>
    </div>

</asp:Content>
<asp:Content ID="JavaScriptCurrentPage" ContentPlaceHolderID="JavaScriptPage" runat="server">
    <%--You can add your custom JavaScript for each page in this section.--%>
</asp:Content>
