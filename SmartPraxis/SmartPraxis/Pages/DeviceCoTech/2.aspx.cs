﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;
using SmartPraxis.Models;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _2 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    Guid patientGuid = Guid.Parse(this.Request.QueryString["x"]);

                    using (DB_SmartPraxisEntities db = new DB_SmartPraxisEntities())
                    {
                        var groupName = (from d in db.DataPatientRequests where d.PatientGuid == patientGuid
                                            select d).FirstOrDefault();
                        var groupString = groupName.DeviceCompanyName.ToLower();
                     
                        if (groupName != null)
                        {
                            this.Response.Redirect($"~/Pages/DeviceCoTech/2{groupString}.aspx?x={ patientGuid.ToString() }");
                        }
                        else
                        {
                            // need to create a new empty patient record - and get the guid
                            Debugger.Break();
                            this.Response.Redirect($"~/Pages/DeviceCoTech/1.aspx?x={patientGuid}");
                        }
                    }
                }
            }
        }
    }
}
