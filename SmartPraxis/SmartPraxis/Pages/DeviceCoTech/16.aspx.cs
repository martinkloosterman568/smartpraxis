﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _16 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 722,
                        Left = 18,
                        Right = 77,
                        Top = 642
                    };

                    // prev
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/15.aspx?x={patientGuid}",
                        Bottom = 721,
                        Left = 892,
                        Right = 947,
                        Top = 654
                    };

                    // AF
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/17.aspx?x={patientGuid}",
                        Bottom = 281,
                        Left = 424,
                        Right = 517,
                        Top = 216
                    };

                    // SR
                    var ch4 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/24.aspx?x={patientGuid}",
                        Bottom = 280,
                        Left = 696,
                        Right = 780,
                        Top = 216
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/16.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}