﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartPraxis.Models;
using SmartPraxis.Repositories;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class MobileList : Page
    {
        public static bool GridRowsFound;
        private static int gridRowInt;

        void Page_PreInit(Object sender, EventArgs e)
        {
            if (this.Session["Role"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
        }

        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion

            #region specific

            var responsed = string.Empty;

            if (this.Request.QueryString["x"] != null)
            {
                responsed = this.Request.QueryString["x"];
            }

            if (this.Session["Role"]?.ToString() == Global.mriCenter && responsed == string.Empty)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (this.Session["Role"]?.ToString() == Global.deviceCo && responsed == string.Empty)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    this.ViewState["sort"] = "MRIScanDate ASC, MRIScanTime ASC";
                    this.PopulateGrid();
                }
            }
        }

        private void PopulateGrid()
        {
            var role = this.Session["Role"].ToString();
            //var tenant = this.Session["AccountGuids"].ToString();
            var destination = this.GetDataForGrid("MRIScanDate ASC, MRIScanTime ASC");
            if (role == "MRI Center Device Operator")
            {
                 destination = this.GetDataForGridDO("MRIScanDate ASC, MRIScanTime ASC");
            }

            this.gvGrid.DataSource = destination;
            this.gvGrid.DataBind();

            //Attribute to show the Plus Minus Button.
            if (this.gvGrid.Rows.Count > 0)
            {
                this.gvGrid.HeaderRow.Cells[0].Attributes["data-class"] = "expand";
            }
            

            //Attribute to hide column in Phone.
            //this.gvGrid.HeaderRow.Cells[2].Attributes["data-hide"] = "phone";
            //this.gvGrid.HeaderRow.Cells[3].Attributes["data-hide"] = "phone";

            //Adds THEAD and TBODY to GridView.
            //this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;

            if (destination != null && destination.Rows.Count > 0)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void LinkButtonDeleteClick(object sender, EventArgs e)
        {
            var id = ((LinkButton) sender).CommandArgument;
            var repository = new UserListRepository(new DB_SmartPraxisEntities());
            repository.Delete(int.Parse(id));
            repository.Save();
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            this.Response.Redirect(formname);
        }

        protected void GvGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            var deletebutton = (LinkButton)e.Row.FindControl("LinkButtonDelete");
            if (this.Session["Role"].ToString().Contains(Global.superAdmin) ||
                this.Session["Role"].ToString().Contains(Global.mriCenter) ||
                this.Session["Role"].ToString().Contains(Global.deviceCo) && 
                !this.Session["Role"].ToString().Contains(Global.deviceCoTech))
            {
                if (deletebutton != null)
                {
                    deletebutton.Visible = true;
                }
            }
            else
            {
                if (deletebutton != null)
                {
                    deletebutton.Visible = false;
                }
            }
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("UserEdit.aspx");
        }

        protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gvGrid.PageIndex = e.NewPageIndex;
            this.PopulateGrid();
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            this.PopulateGrid();
        }

        public DataTable GetDataForGrid(string sSort)
        {
            this.lblUserList.Text = "Patient List";

            try
            {
                return FillDataSet.FillDt("spFilterDeviceTechPatientList", new SqlParameter[1]
                {
                    new SqlParameter("@filter", (string)this.Session["DataAccountIdsDelimited"]),
                });
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                Debugger.Break();
            }

            return null;
        }
        public DataTable GetDataForGridDO(string sSort)
        {
            this.lblUserList.Text = "Patient List";

            try
            {
                return FillDataSet.FillDt("spFilterDeviceOperatorPatientList", new SqlParameter[2]
                {
                    new SqlParameter("@deviceoperatorguid", (Guid)this.Session["AccountGuids"]),
                    new SqlParameter("@filter", (string)this.Session["DataAccountIdsDelimited"]),
                });
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                Debugger.Break();
            }

            return null;
        }

        protected void Filter_TextChanged(object sender, EventArgs e)
        {
            this.PopulateGrid();
            //this.Filter.Focus();
        }

        protected void ClearFilter_Click(object sender, EventArgs e)
        {
            //this.Filter.Text = string.Empty;
            this.PopulateGrid();
            //this.Filter.Focus();
        }

        protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] s = this.ViewState["sort"].ToString().Split();    //load the last sort
            string sSort = e.SortExpression;

            //if the user is resorting the same column, change the order
            if (s[0] == sSort)
            {
                if (s[1] == "ASC")
                    sSort += " DESC";
                else
                    sSort += " ASC";
            }
            else
                sSort += " ASC";

            //find which column is being sorted to change its style
            int i = 0;
            foreach (TemplateField col in this.gvGrid.Columns)
            {
                if (col.SortExpression == e.SortExpression)
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeaderSort" + sSort.Substring(sSort.Length - 4).Trim();
                else
                    this.gvGrid.Columns[i].HeaderStyle.CssClass = "gridHeader";
                i++;
            }

            //get the sorted data
            var dt = this.GetDataForGrid(sSort);
            this.gvGrid.DataSource = dt;
            this.gvGrid.DataBind();

            gridRowInt = dt.Rows.Count;
            if (gridRowInt == 0)
            {
                GridRowsFound = false;
            }
            else
            {
                GridRowsFound = true;
            }

            if (this.gvGrid.DataSource != null)
            {
                this.gvGrid.UseAccessibleHeader = true;
                this.gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            //save the new sort
            this.ViewState["sort"] = sSort;
        }
    }
}