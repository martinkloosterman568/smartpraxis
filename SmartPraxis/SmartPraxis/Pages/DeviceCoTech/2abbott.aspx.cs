﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _2abbott : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    // Goto Start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/1.aspx?x={patientGuid}",
                        Bottom = 701,
                        Left = 19,
                        Right = 78,
                        Top = 642
                    };


                    // Yes
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/3.aspx?x={patientGuid}",
                        Bottom = 673,
                        Left = 392,
                        Right = 456,
                        Top = 617
                    };

                    // No
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/15.aspx?x={patientGuid}",
                        Bottom = 672,
                        Left = 510,
                        Right = 571,
                        Top = 618
                    };

                    // Goto Prev
                    var ch4 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/1.aspx?x={patientGuid}",
                        Bottom = 697,
                        Left = 893,
                        Right = 946,
                        Top = 655
                    };

                    // Goto St Jude / Abbott
                    var ch5 = new RectangleHotSpot
                    {
                        NavigateUrl = "https://mri.merlin.net/",
                        Target = "_blank",
                        Bottom = 403,
                        Left = 325,
                        Right = 634,
                        Top = 332
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/2abbot.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    im.HotSpots.Add(ch5);
                    this.divPage.Controls.Add(im);
                }
            }
        }
    }
}
