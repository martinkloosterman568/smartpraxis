﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using SmartPraxis.Shared;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public class Dashboard : Page
    {
        private void PageSetup()
        {
            #region generic for reuse
            var path = Path.GetFileName(Path.GetDirectoryName(this.Request.PhysicalPath));
            var formname = Path.GetFileName(this.Request.PhysicalPath);
            var forms = new FormAuthorizer { FormName = formname, Path = path, Role = this.Session["Role"]?.ToString() };

            if (!FormsAuthorizer.CheckAuthorize(forms))
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }
            #endregion
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageSetup();

            if (this.Session["UserID"] == null)
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            if (this.Session["Role"].ToString() != "Device Co Tech")
            {
                this.Session.Abandon();
                this.Response.Redirect(Global.returnToPage);
            }

            this.Session["MobilePageTitle"] = "Dashboard";

            if (!this.Page.IsPostBack)
            {
                if (this.Request.QueryString["id"] == null)
                {
                    DataTable dt = FillDataSet.FillDt("spGetDashboardCounts", new[]
                    {
                        new SqlParameter("@DataAccountIdsDelimited", (string) this.Session["DataAccountIdsDelimited"])
                    });

                    foreach (DataRow row in dt.Rows)
                    {
                        var moder = row.ItemArray[0].ToString();
                        var opened = int.Parse(row.ItemArray[1].ToString());

                        switch (moder)
                        {
                            case "Accepted":
                                this.Session["Accepted"] = opened;
                                break;
                            case "PatientCount":
                                this.Session["PatientCount"] = opened;
                                break;
                            case "Rejected":
                                this.Session["Rejected"] = opened;
                                break;
                            case "Comments":
                                this.Session["Comments"] = opened;
                                break;
                        }
                    }
                }
            }
        }

        protected void Patients_Click(object sender, ImageClickEventArgs e)
        {
            if (this.Session["ScreenWidth"] != null)
            {
                var screenWidth = int.Parse(this.Session["ScreenWidth"].ToString());
                if (screenWidth > 991)
                {
                    // send to large screen
                    // see about coming up with different size screens, or maybe media queries
                    this.Response.Redirect("/Pages/DeviceCoTech/MobileList.aspx?x=Pending");
                }
                else if (screenWidth > 767)
                {
                    // send to large screen
                    // see about coming up with different size screens, or maybe media queries
                    this.Response.Redirect("/Pages/DeviceCoTech/MobileList.aspx?x=Pending");
                }
                else if (screenWidth > 479)
                {
                    // send to small screen
                    // 1 col in grid with a hyperlink to press on, maybe date/time in 2nd filed?
                    this.Response.Redirect("/Pages/DeviceCoTech/MobileList.aspx?x=Pending");
                }
            }
        }

        protected void Accepted_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Accepted");
        }

        protected void Rejected_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Rejected");
        }

        protected void Comments_Click(object sender, ImageClickEventArgs e)
        {
            this.Response.Redirect("/Pages/MRICompany/PatientList.aspx?x=Comments");
        }
    }
}