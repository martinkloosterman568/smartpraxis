﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartPraxis.Pages.DeviceCoTech
{
    public partial class _21 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["x"] != null)
            {
                if (!this.Page.IsPostBack)
                {
                    var patientGuid = this.Request.QueryString["x"];

                    //start
                    var ch1 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/7.aspx?x={patientGuid}",
                        Bottom = 722,
                        Left = 18,
                        Right = 77,
                        Top = 642
                    };
                   
                    //prev
                    var ch2 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/19.aspx?x={patientGuid}",
                        Bottom = 721,
                        Left = 892,
                        Right = 947,
                        Top = 654
                    };

                    // < 50%
                    var ch3 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/23.aspx?x={patientGuid}",
                        Bottom = 499,
                        Left = 295,
                        Right = 406,
                        Top = 447
                    };

                    // >= 50%
                    var ch4 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/22.aspx?x={patientGuid}",
                        Bottom = 501,
                        Left = 523,
                        Right = 646,
                        Top = 449
                    };

                    // done
                    var ch5 = new RectangleHotSpot
                    {
                        NavigateUrl = $"~/Pages/DeviceCoTech/Done.aspx?x={patientGuid}",
                        Bottom = 721,
                        Left = 820,
                        Right = 880,
                        Top = 654
                    };

                    var im = new ImageMap
                    {
                        ImageUrl = "~/Content/Images/21.PNG",
                        HotSpotMode = HotSpotMode.Navigate
                    };
                    im.HotSpots.Add(ch1);
                    im.HotSpots.Add(ch2);
                    im.HotSpots.Add(ch3);
                    im.HotSpots.Add(ch4);
                    im.HotSpots.Add(ch5);
                    this.divPage.Controls.Add(im);

                    
                }
            }
        }
    }
}


