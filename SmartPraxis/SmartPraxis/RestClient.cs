﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace SmartPraxis
{
    public interface IRestClient
    {
        Task<MessageResource> SendMessage(string from, string to, string body, List<Uri> mediaUrl);
    }

    public class RestClient : IRestClient
    {
        private readonly ITwilioRestClient _client;

        public RestClient()
        {
            // Account Sid
            var sid = "ACe2992892fc4fed6fea5a457b6fbb828a";
            var token = "ed1d42782fa5841749cd625c7cd36009";
            this._client = new TwilioRestClient(sid, token);
        }

        public RestClient(ITwilioRestClient client)
        {
            this._client = client;
        }

        public async Task<MessageResource> SendMessage(string fromPhone, string toPhone, string bodyStr, List<Uri> mediaUrlStr)
        {
            try
            {
                var toPhoneNumber = new PhoneNumber(toPhone);
                return await MessageResource.CreateAsync(
                    toPhoneNumber,
                    from: new PhoneNumber(fromPhone),
                    body: bodyStr,
                    mediaUrl: mediaUrlStr,
                    client: this._client);
            }
            catch (Exception e)
            {

                var toPhoneNumber = new PhoneNumber(toPhone);
                return await MessageResource.CreateAsync(
                    toPhoneNumber,
                    from: new PhoneNumber(fromPhone),
                    body: bodyStr,
                    mediaUrl: mediaUrlStr,
                    client: this._client);
            }
        }
    }
}