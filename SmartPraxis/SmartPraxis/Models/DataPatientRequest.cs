//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartPraxis.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DataPatientRequest
    {
        public int PatientId { get; set; }
        public string Patient8 { get; set; }
        public Nullable<System.Guid> PatientGuid { get; set; }
        public string PatientName { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Phone { get; set; }
        public string Insurance { get; set; }
        public string DeviceCompanyName { get; set; }
        public Nullable<System.Guid> DeviceCompanyGuid { get; set; }
        public string MRIType { get; set; }
        public string MRITesla { get; set; }
        public Nullable<System.DateTime> MRIScanDate { get; set; }
        public string MRIScanTime { get; set; }
        public string MRICompany { get; set; }
        public Nullable<System.Guid> MRICompanyAccountGuid { get; set; }
        public string MRITechName { get; set; }
        public Nullable<System.Guid> MRITechUserGuid { get; set; }
        public string MRIDoctorName { get; set; }
        public Nullable<System.Guid> MRIDoctorGuid { get; set; }
        public string Comments { get; set; }
        public string TechCommentsReply { get; set; }
        public Nullable<bool> IsAgreedToFollowMKScan { get; set; }
        public string OverAllStatus { get; set; }
        public string MRICompanyLastStatus { get; set; }
        public Nullable<System.Guid> DeviceCoTechUserGuid { get; set; }
        public Nullable<System.DateTime> DeviceCoTechLastUpdateDate { get; set; }
        public string DeviceCompanyLastStatus { get; set; }
        public string PatientStatusDDL { get; set; }
        public string DeviceCoRepAtTimeOfProcedure { get; set; }
        public Nullable<System.Guid> DeviceCoTechUserGuidAtTimeOfProcedure { get; set; }
        public bool Completed { get; set; }
        public bool NotCompleted { get; set; }
        public string BluePageAnswer { get; set; }
        public Nullable<int> BluePageAnswerPageNo { get; set; }
        public Nullable<System.DateTime> MRIScanDatePerformed { get; set; }
        public string MRIScanTimePerformed { get; set; }
        public string DeviceTechPickedSafeMode { get; set; }
        public string DeviceCoPresentingMode { get; set; }
        public string DeviceCoSelectedSafeMode { get; set; }
        public string DeviceCoCommentsAtTimeOfProcedure { get; set; }
        public string DeviceModelDescription { get; set; }
        public string DeviceCoDeviceModelSN { get; set; }
        public string LeadNo1Model { get; set; }
        public string LeadNo1ModelSerialNumber { get; set; }
        public string LeadNo2Model { get; set; }
        public string LeadNo2ModelSerialNumber { get; set; }
        public string LeadNo3Model { get; set; }
        public string LeadNo3ModelSerialNumber { get; set; }
        public Nullable<System.Guid> LastUpdatedByGuid { get; set; }
        public string LastUpdatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdatedDate { get; set; }
        public string AttachedFile { get; set; }
        public Nullable<bool> IsDeviceProgrammedBackToOriginalSettings { get; set; }
        public Nullable<bool> IsDeviceSetupForAutomaticReporgrammingAtMRI { get; set; }
        public Nullable<bool> IsBaselineSettingsRequiredReprogramming { get; set; }
        public string DeviceCoCommentsAfterOfProcedure { get; set; }
        public Nullable<bool> IsDonePage { get; set; }
        public Nullable<System.Guid> Tenant { get; set; }
        public string DeviceOperator { get; set; }
        public Nullable<System.Guid> DeviceOperatorGuid { get; set; }
        public string OrderAttachedFile { get; set; }
    }
}
