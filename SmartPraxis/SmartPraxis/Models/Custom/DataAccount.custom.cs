﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartPraxis.Models
{
    public partial class DataAccount
    {
        public class Comparer : IEqualityComparer<DataAccount>
        {
            public bool Equals(DataAccount left, DataAccount right)
            {
                return left.AccountId == right.AccountId;
            }

            public int GetHashCode(DataAccount element)
            {
                return element.AccountId.GetHashCode();
            }
        }

        public static bool IsMultipleDataAccountsEnabled(string role)
        {
            //return role == "Device Co Tech" || role == "Device Co";
            return false;
        }

        public static string GetDataAccountTypeByUserRole(string role)
        {
            switch (role)
            {
                case "Device Co":
                case "Device Co Tech":
                    return "Device Company";

                case "MRI Center":
                case "MRI Tech":
                case "MRI Doctor":
                case "MRI Center Device Operator":
                    return "MRI Center";

                case "HCO Admin":
                    return "Health Care Organization";
                case "Company Admin":
                    return "Company List";
                    //case "Super Admin":
                    //case "Expert":
                    //    return null;
            }

            return null;
        }
    }
}