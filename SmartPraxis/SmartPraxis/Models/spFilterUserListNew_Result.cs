//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartPraxis.Models
{
    using System;
    
    public partial class spFilterUserListNew_Result
    {
        public int ID { get; set; }
        public Nullable<System.Guid> AccountGuid { get; set; }
        public string Role { get; set; }
        public Nullable<int> RoleID { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<System.Guid> UserGuid { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
        public string OfficePhone { get; set; }
        public string LastUpdatePerson { get; set; }
        public Nullable<System.DateTime> LastModified { get; set; }
        public bool IsSendEmail { get; set; }
        public bool IsSendText { get; set; }
        public bool IsForceChangePassword { get; set; }
        public string First8UserGuid { get; set; }
        public bool IsTermsAccepted { get; set; }
        public Nullable<System.DateTime> TermsAcceptedDateTime { get; set; }
        public Nullable<System.Guid> Tenant { get; set; }
        public string DataAccountIdsDelimited { get; set; }
        public string CompanyNamesDelimited { get; set; }
        public string CompanyTypesDelimited { get; set; }
    }
}
