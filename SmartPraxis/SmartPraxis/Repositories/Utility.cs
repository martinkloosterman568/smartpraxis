﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace SmartPraxis.Repositories
{
    // this document will tell you how to set iis express authentication when debugging in vs 2013
    // http://stackoverflow.com/questions/19515890/authentication-issue-when-debugging-in-vs2013-iis-express
    public class Utility
    {
        #region Utility((System.Collections.ObjectModel.ReadOnlyCollection<System.Reflection.CustomAttributeData>)(DestProperty.GetCustomAttributesData())), raw, hidden
        // this function will copy the properties of 2 like items
        // http://stackoverflow.com/questions/2624823/copy-values-from-one-object-to-another
        public void CopyPropertyValues(object Source, object Destination)
        {
            var DestProperties = Destination.GetType().GetProperties();

            foreach (var SourceProperty in Source.GetType().GetProperties())
            {
                foreach (var DestProperty in DestProperties)
                {
                    if (DestProperty.Name == SourceProperty.Name && DestProperty.PropertyType.IsAssignableFrom(SourceProperty.PropertyType))
                    {
                        if (SourceProperty.GetValue(Source, new object[] { }) != null)
                        {
                            var value = SourceProperty.GetValue(Source, new object[] { });

                            // check to see if it is a number
                            if (IsNumeric(value))
                            {
                                // check to see if there are any string length annotations applied to the property / field
                                var length = this.GetLengthAttributeToTrimString(Source, SourceProperty.Name);
                                if (length > 0)
                                {
                                    value = this.TrimStringHelper(Source, SourceProperty.Name, length);
                                }
                            }
                            else
                            {
                                // check to see if there are any string length annotations applied to the property / field
                                var length = this.GetLengthAttributeToTrimString(Source, SourceProperty.Name);
                                //var length = value.ToString().Length;
                                if (length > 0)
                                {
                                    value = this.TrimStringHelper(Source, SourceProperty.Name, length);
                                }
                            }

                            if (value.ToString().Trim() != String.Empty)
                            {
                                DestProperty.SetValue(Destination, value);
                            }
                        }
                        else if (SourceProperty.GetValue(Source, new object[] { }) == null)
                        {
                            //DestProperty.SetValue(Destination, null);
                        }
                        
                        break;
                    }
                }
            }
        }

        private string TrimStringHelper(object objectArray, string fieldname, int trimsize)
        {
            var str = string.Empty;
            var stringPropertyNamesAndValues = objectArray.GetType()
                .GetProperties()
                .Where(pi => pi.GetGetMethod() != null)
                .Select(pi => new
                {
                    pi.Name,
                    Value = pi.GetGetMethod().Invoke(objectArray, null)
                });

            try
            {
                foreach (var pair in stringPropertyNamesAndValues)
                {
                    if (pair.Name == fieldname)
                    {
                        var value = pair.Value != null ? pair.Value.ToString() : "";
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (IsNumeric(value))
                            {
                                // if the value is a number then take the last x-length of numbers 18009998888 = 8009998888
                                if (value.Length > trimsize)
                                {
                                    str = value.Substring(value.Length - trimsize, trimsize);
                                }
                                else
                                {
                                    str = value;
                                }
                            }
                            else
                            {
                                // if the value is not a number then take the first x-length of characters
                                if (value.Length > trimsize)
                                {
                                    str = value.Substring(0, trimsize);
                                }
                                else
                                {
                                    str = value;
                                }
                            }
                        }

                        break;
                    }
                }
            }
            catch (Exception)
            {
                Debugger.Break();   
            }

            return str;
        }

        private int GetLengthAttributeToTrimString(object objectArray, string fieldName)
        {
            var trimLength = -1;
            PropertyInfo[] properties = objectArray.GetType().GetProperties();

            var props = objectArray.GetType().GetProperties().Where(
                   prop => Attribute.IsDefined(prop, typeof(StringLengthAttribute)));

            foreach (var propertyInfo in props)
            {
                object[] customAttributes = propertyInfo.GetCustomAttributes(typeof(ValidationAttribute), inherit: true);

                if ((props != null) && (propertyInfo.Name == fieldName))
                {
                    foreach (var customAttribute in customAttributes)
                    {
                        try
                        {
                            trimLength = ((StringLengthAttribute)(customAttribute)).MaximumLength;
                            break;
                        }
                        catch
                        {
                        }
                    }
                }

                if (trimLength > 0)
                { 
                    break;
                }
            }

            return trimLength;
        }

        // this function will copy data from a dictionary key, value pairs to a class object
        public void CopyListToDataObject(Dictionary<string, string> Source, object Destination)
        {
            StringParsers.RegisterParser(input => Int32.Parse(input));
            StringParsers.RegisterParser(input => Int64.Parse(input));
            StringParsers.RegisterParser(input => DateTime.Parse(input));

            var DestProperties = Destination.GetType().GetProperties();

            foreach (var DestProperty in DestProperties)
            {
                // using Linq to see if there is a match in the array
                var SourceItem = (from d in Source where d.Key == DestProperty.Name select d).FirstOrDefault();

                if (DestProperty.Name == SourceItem.Key)
                {
                    if (DestProperty.PropertyType.IsAssignableFrom(SourceItem.Key.GetType()))
                    {
                        // if both types agree then copy directly
                        if (SourceItem.Value != string.Empty)
                        {
                            DestProperty.SetValue(Destination, SourceItem.Value);
                        }
                    }
                    else
                    {
                        if ((DestProperty.PropertyType.ToString() == "System.Int32") && (IsNumeric(SourceItem.Value)))
                        {
                            DestProperty.SetValue(Destination, StringParsers.Parse<int>(SourceItem.Value));
                        }
                        else if ((DestProperty.PropertyType.ToString() == "System.Int64") && (IsNumeric(SourceItem.Value)))
                        {
                            DestProperty.SetValue(Destination, StringParsers.Parse<long>(SourceItem.Value));
                        }
                        else if (DestProperty.PropertyType.ToString() == "System.Char")
                        {
                            if (SourceItem.Value != string.Empty)
                            {
                                if (DestProperty.ToString().Contains("Gender"))
                                {
                                    if ((SourceItem.Value[0] == 'M') || (SourceItem.Value[0] == 'F'))
                                    {
                                        DestProperty.SetValue(Destination, SourceItem.Value[0]);
                                    }
                                }
                                else
                                {
                                    DestProperty.SetValue(Destination, SourceItem.Value[0]);
                                }
                            }
                        }
                        else if (DestProperty.PropertyType.ToString() == "System.DateTime")
                        {
                            if (SourceItem.Value == string.Empty)
                            {
                                DestProperty.SetValue(Destination, null);
                            }
                            else
                            {
                                if (this.validateDate(SourceItem.Value))
                                {
                                    DestProperty.SetValue(Destination, StringParsers.Parse<DateTime>(SourceItem.Value));
                                }
                                else
                                {
                                    DestProperty.SetValue(Destination, null);
                                }
                            }
                        }
                    }
                }
                else
                {
                    DestProperty.SetValue(Destination, null);
                }
            }
        }


        #endregion

        /*
        Validate date format with regular expression
        */
        private bool validateDate(string date)
        {
            StringParsers.RegisterParser(input => DateTime.Parse(input));
            DateTime trydate;
            
            try
            {
                trydate = StringParsers.Parse<DateTime>(date);
            }
            catch (Exception)
            {
                return false;
            }

            int day = trydate.Day;
            int month = trydate.Month;
            int year = trydate.Year;

            if (day == 31 && (month == 4 || month == 6 || month == 9 || month == 11))
            {
                // only 1,3,5,7,8,10,12 has 31 days
                return false;
            }
            if (month == 2)
            {
                //leap year
                if (year % 4 == 0)
                {
                    if (day == 30 || day == 31)
                    {
                        return false;
                    }
                    return true;
                }
                if (day == 29 || day == 30 || day == 31)
                {
                    return false;
                }
                return true;
            }
            return true;
        }

        public static bool IsNumeric(object expression)
        {
            if (expression == null)
                return false;

            double number;
            return Double.TryParse(Convert.ToString(expression.ToString().Replace(" - Error", string.Empty), CultureInfo.InvariantCulture), NumberStyles.Any,
                NumberFormatInfo.InvariantInfo, out number);
        }

        public static bool IsDate(object obj)
        {
            var strDate = obj.ToString();
            try
            {
                var okSoFar = false;

                if (strDate.Contains("-"))
                {
                    okSoFar = true;
                }
                else if(strDate.Contains("/"))
                {
                    okSoFar = true;
                }
                else if (strDate.Contains(" "))
                {
                    okSoFar = true;
                }

                if (okSoFar)
                {
                    var dt = DateTime.Parse(strDate);
                    if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }
    }

    // this is used as a generic converter
    public static class StringParsers
    {
        // http://stackoverflow.com/questions/14257207/convert-string-to-generic-type-basic-or-array-in-c-sharp
        private static Dictionary<Type, object> Parsers = new Dictionary<Type, object>();

        public static void RegisterParser<DataType>(Func<string, DataType> ParseFunction)
        {
            Parsers[typeof(DataType)] = ParseFunction;
        }

        public static DataType Parse<DataType>(string Input)
        {
            object UntypedParser;
            if (!Parsers.TryGetValue(typeof(DataType), out UntypedParser))
                throw new Exception("Could not find a parser for type " + typeof(DataType).FullName);

            Func<string, DataType> Parser = (Func<string, DataType>)UntypedParser;

            return Parser(Input);
        }
    }
}