﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartPraxis.Models;

namespace SmartPraxis.Repositories
{
    public class DataMessageRepository : Repository<DataMessage>
    {
        public DB_SmartPraxisEntities _context { get; set; }

        public DataMessageRepository(DB_SmartPraxisEntities context) : base(context)
        {
            try
            {
                this._context = context;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Save()
        {
            try
            {
                this._context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}