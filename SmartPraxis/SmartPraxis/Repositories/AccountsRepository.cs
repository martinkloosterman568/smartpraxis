﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using SmartPraxis.Models;

namespace SmartPraxis.Repositories
{
    public class AccountsRepository : Repository<DataAccount>
    {
        public DB_SmartPraxisEntities _context { get; set; }

        public AccountsRepository(DB_SmartPraxisEntities context) : base(context)
        {
            try
            {
                this._context = context;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Save()
        {
            try
            {
                this._context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                var t = this.GetById(id);
                if (t != null)
                {
                    this._context.DataAccounts.Remove(t);
                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(DataAccount a)
        {
            try
            {
                var t = this.GetById(a.AccountId);
                if (t != null)
                {
                    var util = new Utility();
                    util.CopyPropertyValues(a, t);

                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateCheck(int a, int b, string action)
        {
            try
            {
                var t = this.GetById(a);
                if (t != null)
                {
                    var connectionString = ConfigurationManager.ConnectionStrings["SmartPraxis"].ToString();
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        var queryString = string.Format("Delete from DeviceCoSelectedMRI where MRIAccountId = {0} and DeviceAccountId = {1}", a, b);
                        SqlCommand command = new SqlCommand(queryString, connection);
                        command.Connection.Open();
                        command.ExecuteNonQuery();

                        if (action == "On")
                        {
                            queryString = string.Format("Insert into DeviceCoSelectedMRI (DeviceAccountId,MRIAccountId) values({0},{1})", b, a);
                            command = new SqlCommand(queryString, connection);
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<DataAccount> FindByLikeAll(string table, string column, string value)
        {
            try
            {
                value = !Utility.IsNumeric(value) ? string.Format("'%{0}%'", value) : string.Format("%{0}%", value);
                var str = string.Format("Select * from {0} where {1} like {2}", table, column, value);
                return this._context.DataAccounts.SqlQuery(str).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataAccount FindByLikeSingle(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                value = !Utility.IsNumeric(value) ? string.Format("'%{}%'", value) : string.Format("%{0}%", value);
                var str = string.Format("Select * from {0} where {1} like {2}", table, column, value);
                var result = this._context.DataAccounts.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() > 1)
                {
                    outError = string.Format("Error: Qty: {0} - Multiples Found", result.Count());
                }
                else if (result.Count() == 1)
                {
                    return result.FirstOrDefault();
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataAccount FindById(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                var str = string.Format("Select * from {0} where {1} = {2}", table, column, value);
                var result = this._context.DataAccounts.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() > 1)
                {
                    outError = string.Format("Error: Qty: {0} - Multiples Found", result.Count());
                }
                else if (result.Count() == 1)
                {
                    return result.FirstOrDefault();
                }
            }
            finally
            {
            }

            return null;
        }

        public List<DataAccount> FindAllByFileControlGuid(string table, string column, string value, out string outError)
        {
            outError = string.Empty;
            
            try
            {
                if (value != null)
                {
                    var str = string.Format("Select * from {0} where {1} = '{2}'", table, column, value);
                    var result = this._context.DataAccounts.SqlQuery(str);
                    if (!result.Any())
                    {
                        outError = "Error: Not Found";
                    }
                    else if (result.Count() > 1)
                    {
                        outError = string.Format("Error: Qty: {0} - Multiples Found", result.Count());
                    }
                    else if (result.Count() == 1)
                    {
                        return result.ToList();
                    }
                }
            }
            finally
            {
            }

            return null;
        }

        public List<DataAccount> FindAllByGuid(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                if (value != null)
                {
                    var str = string.Format("Select * from {0} where {1} = '{2}'", table, column, value);
                    var result = this._context.DataAccounts.SqlQuery(str);
                    if (!result.Any())
                    {
                        outError = "Error: Not Found";
                    }
                    else if (result.Count() >= 1)
                    {
                        return result.ToList();
                    }
                }
            }
            finally
            {
            }

            return null;
        }
    }
}