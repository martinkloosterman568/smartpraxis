﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SmartPraxis.Models;

namespace SmartPraxis.Repositories
{
    public class PatientRequestsRepository : Repository<DataPatientRequest>
    {
        public DB_SmartPraxisEntities _context { get; set; }

        public PatientRequestsRepository(DB_SmartPraxisEntities context) : base(context)
        {
            try
            {
                this._context = context;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Save()
        {
            try
            {
                this._context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                var t = this.GetById(id);
                if (t != null)
                {
                    this._context.DataPatientRequests.Remove(t);
                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(DataPatientRequest a)
        {
            try
            {
                var t = this.GetById(a.PatientId);
                if (t != null)
                {
                    var util = new Utility();
                    util.CopyPropertyValues(a, t);

                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DataPatientRequest> FindByLikeAll(string table, string column, string value)
        {
            try
            {
                value = !Utility.IsNumeric(value) ? string.Format("'%{0}%'", value) : string.Format("%{0}%", value);
                var str = string.Format("Select * from {0} where {1} like {2}", table, column, value);
                return this._context.DataPatientRequests.SqlQuery(str).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataPatientRequest FindByLikeSingle(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                value = !Utility.IsNumeric(value) ? string.Format("'%{0}%'", value) : string.Format("%{0}%", value);
                var str = string.Format("Select * from {0} where {1} like {2}", table, column, value);
                var result = this._context.DataPatientRequests.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() > 1)
                {
                    outError = string.Format("Error: Qty: {0} - Multiples Found", result.Count());
                }
                else if (result.Count() == 1)
                {
                    return result.FirstOrDefault();
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataPatientRequest FindById(string table, string column, int value, out string outError)
        {
            outError = string.Empty;

            try
            {
                var str = string.Format("Select * from {0} where {1} = {2}", table, column, value);
                var result = this._context.DataPatientRequests.SqlQuery(str);
                if (!result.Any())
                {
                    outError = "Error: Not Found";
                }
                else if (result.Count() > 1)
                {
                    outError = string.Format("Error: Qty: {0} - Multiples Found", result.Count());
                }
                else if (result.Count() == 1)
                {
                    return result.FirstOrDefault();
                }
            }
            finally
            {
            }

            return null;
        }

        public List<DataPatientRequest> FindAllByFileControlGuid(string table, string column, string value, out string outError)
        {
            outError = string.Empty;
            
            try
            {
                if (value != null)
                {
                    var str = string.Format("Select * from {0} where {1} = '{2}'", table, column, value);
                    var result = this._context.DataPatientRequests.SqlQuery(str);
                    if (!result.Any())
                    {
                        outError = "Error: Not Found";
                    }
                    else if (result.Count() > 1)
                    {
                        outError = string.Format("Error: Qty: {0} - Multiples Found", result.Count());
                    }
                    else if (result.Count() == 1)
                    {
                        return result.ToList();
                    }
                }
            }
            finally
            {
            }

            return null;
        }

        public List<DataPatientRequest> FindAllByGuid(string table, string column, string value, out string outError)
        {
            outError = string.Empty;

            try
            {
                if (value != null && !string.IsNullOrEmpty(value))
                {
                    var str = string.Format("Select * from {0} where {1} = '{2}'", table, column, value);
                    var result = this._context.DataPatientRequests.SqlQuery(str);
                    if (!result.Any())
                    {
                        outError = "Error: Not Found";
                    }
                    else if (result.Count() >= 1)
                    {
                        return result.ToList();
                    }
                }
                else
                {
                    // look at the value it is missing
                    Debugger.Break();
                }
            }
            finally
            {
            }

            return null;
        }
    }
}