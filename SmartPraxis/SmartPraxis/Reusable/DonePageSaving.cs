﻿using System;
using System.Linq;
using SmartPraxis.Models;

namespace SmartPraxis.Reusable
{
    public static class DonePageSaving
    {
        public static bool SaveBluePagesAnswer(string answer, string patientGuid, string userGuid)
        {
            DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();

            var retVal = false;
            try
            {
                var patient = new Guid(patientGuid);
                var patientRecord = (from d in db.DataPatientRequests
                    where d.PatientGuid == patient
                    select d).FirstOrDefault();

                if (patientRecord != null)
                {
                    var deviceTechGuid = new Guid(userGuid);
                    patientRecord.DeviceCoSelectedSafeMode = answer;
                    patientRecord.DeviceCoTechUserGuid = deviceTechGuid;
                    patientRecord.DeviceCoTechLastUpdateDate = DateTime.Now;

                    db.SaveChanges();
                    retVal = true;
                }
                
            }
            catch (Exception e)
            {
                // if you are here, probably because the x= value is not a guid
                Console.WriteLine(e);
                //throw;
            }

            return retVal;
        }
    }
}

