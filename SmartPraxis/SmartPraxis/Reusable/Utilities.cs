﻿namespace SmartPraxis.Reusable
{
    public static class Utilities
    {
        // Utilities.FormatPhone(1234567890)
        public static string FormatPhone(string TextInput)
        {
            long num;

            if (TextInput.Length == 10 && long.TryParse(TextInput, out num))
            {
                string pn = TextInput;
                TextInput = string.Format("({0}) {1}-{2}", pn.Substring(0, 3), pn.Substring(3, 3), pn.Substring(6));
            }

            return TextInput;
        }
    }
}