﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using SmartPraxis.Models;

namespace SmartPraxis
{
    public partial class MobileTerms : System.Web.UI.Page
    {
        readonly DB_SmartPraxisEntities db = new DB_SmartPraxisEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            string text;
            string filename = this.Server.MapPath("~/Content/TermsConditions/TERMS.html");
            var fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                text = streamReader.ReadToEnd();
            }

            this.DivTerms.InnerHtml = text;
            
            var userGuid = this.Session["UserGuid"].ToString();
            var getTermsAccepted = (from d in this.db.DataUserMasts where d.UserGuid.ToString() == userGuid select d).FirstOrDefault();
            if (getTermsAccepted != null)
            {
                if (getTermsAccepted.IsTermsAccepted == false)
                {
                    this.tableHide.Style.Remove("display");
                    this.tableHide.Style.Add("display", "inherit");

                    this.tableContinue.Style.Remove("display");
                    this.tableContinue.Style.Add("display", "none");
                }
                else
                {
                    this.tableHide.Style.Remove("display");
                    this.tableHide.Style.Add("display", "none");

                    this.tableContinue.Style.Remove("display");
                    this.tableContinue.Style.Add("display", "inherit");
                }
            }
            else
            {
                this.tableHide.Style.Remove("display");
                this.tableHide.Style.Add("display", "inherit");

                this.tableContinue.Style.Remove("display");
                this.tableContinue.Style.Add("display", "none");
            }
        }

        protected void AcceptTerms_Click(object sender, EventArgs e)
        {
            var userGuid = this.Session["UserGuid"].ToString();
            var updateTerms = (from d in this.db.DataUserMasts where d.UserGuid.ToString() == userGuid select d).FirstOrDefault();
            if (updateTerms != null)
            {
                // you found a record, now update it
                updateTerms.IsTermsAccepted = true;
                updateTerms.TermsAcceptedDateTime = DateTime.Now;
                this.db.SaveChanges();
            }

            this.Response.Redirect("~/Pages/Account/Notify.aspx");
        }

        protected void Continue_Click(object sender, EventArgs e)
        {
            var o = this.Session["Role"];
            if (o != null)
            {
                if (o.ToString().Contains("Super Admin"))
                {
                    this.Response.Redirect("~/Pages/SuperAdmin/Dashboard.aspx");
                }
                else if (o.ToString().Contains("HCO Admin"))
                {
                    this.Response.Redirect("~/Pages/HCOAdmin/Dashboard.aspx");
                }
                else if (o.ToString().Contains("Company Admin"))
                {
                    this.Response.Redirect("~/Pages/CompanyAdmin/Dashboard.aspx");
                }
                else if (o.ToString().Contains("Expert"))
                {
                    this.Response.Redirect("~/Pages/Expert/PatientList.aspx?x=All");
                }
                else if (o.ToString().Contains("MRI Center"))
                {
                    this.Response.Redirect("~/Pages/MRICompany/Dashboard.aspx");
                }
                else if (o.ToString().Contains("MRI Tech"))
                {
                    this.Response.Redirect("~/Pages/MRICompany/PatientList.aspx?x=Pending");
                }
                else if (o.ToString().Contains("MRI Doctor"))
                {
                    this.Response.Redirect("~/Pages/MRICompany/PatientList.aspx?x=Pending");
                }
                else if (o.ToString().Contains("Device Co Tech"))
                {
                    this.Response.Redirect("~/Pages/DeviceCoTech/MobileList.aspx?x=Pending");

                    //var width = this.Session["ScreenWidth"].ToString();
                    //if (Convert.ToInt32(width) < 1366)
                    //{
                    //    this.Response.Redirect("~/Pages/DeviceCoTech/MobileList.aspx?x=Pending");
                    //}
                    //else
                    //{
                    //    this.Response.Redirect("~/Pages/DeviceCompany/PatientList.aspx");
                    //}

                    ////this.Response.Redirect($"~/Pages/Account/UserList.aspx?x={this.Session["UserGuid"]}");
                }

                else if (o.ToString().Contains("MRI Center Device Operator"))
                {
                    this.Response.Redirect("~/Pages/DeviceOperator/MobileList.aspx?x=Pending");
                }
                else if (o.ToString().Contains("Device Co"))
                {
                    this.Response.Redirect("~/Pages/DeviceCompany/PatientList.aspx");
                }
            }
        }
    }
}
