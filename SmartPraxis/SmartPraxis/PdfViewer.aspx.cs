﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using PostmarkDotNet;
using PostmarkDotNet.Legacy;
using PostmarkDotNet.Model;

namespace SmartPraxis
{
    public partial class PdfViewer : System.Web.UI.Page
    {
        private static string companyNom = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["BackButtonVisible"] != null)
            {
                if (this.Session["BackButtonVisible"].ToString() == "True")
                {
                    this.btnBack.Visible = true;
                }
            }

            var file = this.Request.QueryString["y"];
            //companyNom = this.Request.QueryString["c"];
            if (this.Session["CompanyName"] != null)
            {
                companyNom = ((string[])(this.Session["CompanyName"]))[0];
            }
            
            if (!string.IsNullOrEmpty(file))
            {
                this.ShowPdf1.FilePath = "~/Documents/" + file;
            }

            var file2 = this.Request.QueryString["z"];
            if (!string.IsNullOrEmpty(file2))
            {
                this.ShowPdf1.FilePath = "~/Content/SampleDocument/" + file2;
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            var emails = this.validateEmail(this.EmailAddress.Text);
            if (emails != string.Empty)
            {
                if (!string.IsNullOrEmpty(companyNom))
                {
                    this.SendEmail(this.Server.MapPath(this.ShowPdf1.FilePath), companyNom);
                }
            }
            else
            {
                this.EmailAddress.Text += @"-Invalid Email";
            }
        }

        public bool SendEmail(string filepath, string companyName)
        {
            try
            {
                var sb = new StringBuilder();
                var nl = Environment.NewLine;
                sb.Append("Hello, [replace]");
                sb.Append("[replace]");
                sb.Append("Below please find the attached Report Sent by: [company] [replace]");
                sb.Append("[replace]");
                sb.Append("Best Regards,[replace]");

                var body1 = sb.ToString();
                var body2 = sb.ToString();
                for (var x = 1; x <= 3; x++)
                {
                    body1 = body1.Replace("[replace]", nl);
                    body1 = body1.Replace("[company]", companyName);
                    body2 = body2.Replace("[replace]", "<br>");
                    body2 = body2.Replace("[company]", companyName);
                }

                var emails = this.EmailAddress.Text;

                var mh = new MailHeader
                {
                    Name = "X-CUSTOM-HEADER",
                    Value = "Header content"
                };
                var header = new HeaderCollection { mh };

                var message = new PostmarkMessage
                {
                    To = emails.TrimEnd(','),
                    From = "smart-praxis@mrisafemode.com",
                    TrackOpens = true,
                    TextBody = body1,
                    HtmlBody = "<html><body>" + body2 + "</body></html>",
                    Subject = "Report Sent By: " + companyName + " - " +
                              DateTime.Now.AddHours(0).ToString("MMM d, yyyy H:mm:ss tt"),
                    Tag = "Report",
                    Headers = header
                };

                message.AddAttachment(filepath, "content", "application/octet-stream");

                var client = new PostmarkClient("6a454310-2605-4eaa-9941-3f04d12ca809");
                var sendResult = client.SendMessage(message);

                if (sendResult.Status == PostmarkStatus.Success)
                {
                    /* Handle success */
                }
                else
                {
                    /* Resolve issue.*/
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private string validateEmail(string Email)
        {
            string pattern = @"^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$";
            Regex rx = new Regex(pattern);
            if (rx.IsMatch(Email))
            {
                return Email;
            }

            return string.Empty;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (this.Session["BackButton"] != null)
            {
                this.Response.Redirect(this.Session["BackButton"].ToString());
            }
        }
    }
}
