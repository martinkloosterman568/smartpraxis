﻿namespace SmartPraxis.Shared
{
    public static class PreInitMasterPageFile
    {
        public static string MasterPageChooser(string role)
        {
            var results = string.Empty;

            if (role.Contains("Super Admin"))
            {
                results = "~/Shared/MasterPages/SiteLayoutSuperAdmin.Master";
            } else if (role.Contains("HCO Admin") || role.Contains("Company Admin"))
            {
                results = "~/Shared/MasterPages/SiteLayoutHCOAdmin.Master";
            }
            else if (role=="MRI Center Device Operator")
            {
                results = "~/Shared/MasterPages/SiteLayoutDeviceCoTech.Master";
                //results = "~/Shared/MasterPages/SiteLayoutDeviceCompany.Master";
                //results = "~/Shared/MasterPages/SiteLayoutDeviceOperator.Master";
            }
            else if (role.Contains("MRI Center"))
            {
                results = "~/Shared/MasterPages/SiteLayoutMRICompany.Master";
            } else if (role.Contains("MRI Tech"))
            {
                results = "~/Shared/MasterPages/SiteLayoutMRITech.Master";
            } else if (role.Contains("MRI Doctor"))
            {
                results = "~/Shared/MasterPages/SiteLayoutMRIDoctor.Master";
            } else if (role.Contains("Device Co Tech"))
            {
                results = "~/Shared/MasterPages/SiteLayoutDeviceCoTech.Master";
            } else if (role.Contains("Device Co"))
            {
                results = "~/Shared/MasterPages/SiteLayoutDeviceCompany.Master";
            } else if (role.Contains("Expert"))
            {
                results = "~/Shared/MasterPages/SiteLayoutEmpty.Master";
            }

            return results;
        }
    }
}