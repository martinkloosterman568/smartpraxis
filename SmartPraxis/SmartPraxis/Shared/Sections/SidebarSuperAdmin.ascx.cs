﻿using System;
using System.Web.UI;

namespace SmartPraxis.Shared.Sections
{
    public partial class SidebarSuperAdmin : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.screenSize.Text != string.Empty && this.screenSize.Text.Contains(";"))
            {
                this.Session["ScreenWidth"] = this.screenSize.Text.Split(';')[0];
                this.Session["ScreenHeight"] = this.screenSize.Text.Split(';')[1];
            }

            if (this.Session["ScreenWidth"] != null)
            {
                var width = this.Session["ScreenWidth"].ToString();
                if (Convert.ToInt32(width) < 1366)
                {
                    this.HyperLink1.NavigateUrl = "~/MobileTerms.aspx";
                }
                else
                {
                    this.HyperLink1.NavigateUrl = "~/Terms.aspx";
                }
            }
            else
            {
                this.HyperLink1.NavigateUrl = "~/Terms.aspx";
            }
        }
    }
}