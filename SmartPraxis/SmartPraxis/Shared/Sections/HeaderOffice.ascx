﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderOffice.ascx.cs" Inherits="SmartPraxis.Shared.Sections.HeaderOffice" %>
<h1 class="wiwet-wildomar visible-xs">Smart-Praxis</h1>
<div class="top-bar-mobile visible-xs">
    <div class="row">
        <div class="col-xs-6" style="position: relative; top: -5px;">
            <a href="#menu-toggle" id="menu-toggle">
            <i  class="fa fa-bars"></i>
            </a>
        </div>
       <%-- <div class="col-xs-6">
            <p>
                <asp:Label ID="lblMobilePageTitle" runat="server" Text="SmartPraxis" />
            </p>
        </div>--%>
    </div>
</div>
<div class="top-bar hidden-xs">
    <div class="row">
        <div class="col-sm-5" style="position: relative; top: -5px;">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
      
        <div style="float: right; font-size: small;" class="dropdown">
            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Logined-in as: 
            <% =this.Session["UserName"].ToString()
            %>
            <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dLabel">
            <li><a href="/SignOut.aspx">Exit</a></li>
            </ul>
            <a href="#"><i class="fa fa-user"></i></a>
        </div>
        
    </div>
</div>