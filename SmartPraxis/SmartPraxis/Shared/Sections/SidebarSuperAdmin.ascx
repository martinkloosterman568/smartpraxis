﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarSuperAdmin.ascx.cs" Inherits="SmartPraxis.Shared.Sections.SidebarSuperAdmin" %>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav wiwet-navigation">
        <li class="sidebar-brand hidden-xs">
            <a href="/">
                Smart-Praxis
            </a>
        </li>
        
        <script type="text/javascript">
            function GetTimeZoneOffset() {
                var d = new Date();
                var gmtOffSet = -d.getTimezoneOffset();
                var gmtHours = Math.floor(gmtOffSet / 60);
                var GMTMin = Math.abs(gmtOffSet % 60);
                var dot = ".";
                var retVal = "" + gmtHours + dot + GMTMin;
                document.getElementById('<%= this.screenSize.ClientID%>').value = screen.width + ';' + screen.height;
            }
        </script>
        <asp:HiddenField ID="screenWidth" runat="server" />
        <asp:HiddenField ID="screenHeight" runat="server" />
        <div style="display:none">
            <asp:TextBox BorderColor="white" BackColor="yellow"  ID="screenSize" placeholder="screenSize" runat="server"></asp:TextBox>    
        </div>


        <br/><br />
        <li>
            <a href="/Pages/Account/HCOList.aspx"><i class="fa fa-users"></i> HCO List</a>
        </li>
        <li>
            <a href="/Pages/Account/CompanyList.aspx"><i class="fa fa-users"></i> Institution List</a>
        </li>
        <br/><br/>
        <li>
            <a href="/Pages/Account/DeviceCoList.aspx"><i class="fa fa-users"></i> Device Co List</a>
        </li>
        <li>
            <a href="/Pages/Account/MRIList.aspx"><i class="fa fa-users"></i> MRI List</a>
        </li>
        
       <%-- <li>
            <a href="/Pages/SuperAdmin/GeographyRegionsList.aspx"><i class="fa fa-globe"></i> Geography Regions List</a>
        </li> --%>
        <li>
            <a href="/Pages/Account/UserList.aspx"><i class="fa fa-male"></i> User List</a>
        </li>  
        <br /><br />
        <li>
            <a href="/Pages/SuperAdmin/SetNotify.aspx"><i class="fa fa-comments"></i> Set Notify</a>
        </li> 
        
        <li style="margin-top:100px">
            <a href="/Pages/ContactUs.aspx?y=R"><i class="fa fa-life-ring"></i> Contact Us</a>
        </li>   
        <li class="ui-components">
            <a role="button" id="urgent" data-toggle="collapse" href="#collapseUI2" aria-expanded="false" aria-controls="collapseUI">
                <i class="fa fa-stethoscope"></i> Urgent Contact
            </a>
            <div class="collapse" id="collapseUI2">
                <ul>
                    <li><a href="#">(954) 261-3262</a></li>
                </ul>
            </div>
        </li>
        <li>
            <a href="/Pages/ContactUs.aspx?y=E"><i class="fa fa-life-ring"></i> Expert Consult</a>
        </li> 
        <li>
            <asp:HyperLink ID="HyperLink1" runat="server"><i class="fa fa-legal"></i> Terms</asp:HyperLink>
        </li>  
        <li>
            <a href="/Index.aspx"><i class="fa fa-power-off"></i> Exit</a>
        </li> 
    </ul>
</div>