﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StyleSheet.ascx.cs" Inherits="SmartPraxis.Shared.Sections.StyleSheet" %>
<link href="../../Content/StyleSheets/bootstrap.min.css" rel="stylesheet">
<link href="../../Content/StyleSheets/wiwet.template.css" rel="stylesheet">
<link href="../../Content/StyleSheets/wiwet.template.responsive.css" rel="stylesheet">
<link href="../../Content/StyleSheets/faa.css" rel="stylesheet" />