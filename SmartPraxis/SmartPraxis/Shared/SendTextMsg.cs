﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using SmartPraxis.Models;

namespace SmartPraxis.Shared
{
    public class SendTextMsg
    {
        public static async Task TextMessage(List<string> sendTophones, string sendMessage, string mriCompanyName = "")
        {
            // using this phone # to send
            const string fromPhone = "19547994659";
            var sendtext = new RestClient();
            foreach (var toPhone in sendTophones)
            {
                // log the message for future analysis
                using (var db = new DB_SmartPraxisEntities())
                {
                    var newTextLog = new DataTextMessageLogging
                    {
                        CellPhone = toPhone,
                        DateSent = GetTimeNow.GetTime(),
                        FlaggedToRemove = false,
                        MessageSent = sendMessage,
                        MRICompanyName = mriCompanyName
                    };

                    db.DataTextMessageLoggings.Add(newTextLog);
                    db.SaveChanges();
                }

                try
                {
                    await sendtext.SendMessage(fromPhone, toPhone, sendMessage, null);
                }
                catch(Exception e)
                {

                }
            }

            
           
            // write to the log table
        }
    }
}