﻿using System;
using System.Web.UI;
using Telerik.Web.UI;

namespace SmartPraxis.Shared.MasterPages
{
    public partial class SiteLayoutMRICompany : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //set the expire timeout for the session 
                this.Session.Timeout = Global.Timeout;
                //configure the notification to automatically show 1 min before session expiration
                this.RadNotification1.ShowInterval = (this.Session.Timeout - 1) * 60 * 1000;
                //set the redirect url as a value for an easier and faster extraction in on the client
                this.RadNotification1.Value = this.Page.ResolveClientUrl(Global.returnToPage);
            }
        }

        protected void OnCallbackUpdate(object sender, RadNotificationEventArgs e)
        {
            //Debugger.Break();
        }

    }
}