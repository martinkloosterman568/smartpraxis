﻿namespace SmartPraxis.Shared
{
    public static class HyperlinkHome
    {
        public static string HomeChooser(string role)
        {
            var results = string.Empty;

            if (role.Contains("Super Admin"))
            {
                results = "/Pages/SuperAdmin/Dashboard.aspx";
            }
            if (role.Contains("HCO Admin"))
            {
                results = "/Pages/HCOAdmin/Dashboard.aspx";
            }
            if (role.Contains("Company Admin"))
            {
                results = "/Pages/CompanyAdmin/Dashboard.aspx";
            }
            if (role.Contains("MRI Center"))
            {
                results = "/Pages/MRICenter/Dashboard.aspx";
            }
            if (role.Contains("MRI Tech"))
            {
                results = "/Pages/MRITech/Dashboard.aspx";
            }
            if (role.Contains("MRI Doctor"))
            {
                results = "/Pages/MRIDoctor/Dashboard.aspx";
            }

            if (role.Contains("Device Co Tech"))
            {
                results = "/Pages/DeviceCoTech/Dashboard.aspx";
            }
            else if (role.Contains("Device Co"))
            {
                results = "/Pages/DeviceCo/Dashboard.aspx";
            }
            else if (role.Contains("MRI Center Device Operator"))
            {
                results = "/Pages/DeviceOperator/Dashboard.aspx";
            }

            return results;
        }
    }
}