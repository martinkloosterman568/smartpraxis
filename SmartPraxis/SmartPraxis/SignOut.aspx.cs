﻿using System;
using System.Web.UI;

namespace SmartPraxis
{
    public partial class SignOut : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Session.Abandon();
            this.Response.Redirect(Global.returnToPage);
        }
    }
}