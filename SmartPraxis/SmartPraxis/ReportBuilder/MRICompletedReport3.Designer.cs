namespace SmartPraxis.ReportBuilder
{
    partial class MRICompletedReport3
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MRICompletedReport3));
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.FormattingRule formattingRule4 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule5 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter10 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter11 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter12 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter13 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter14 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter15 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter16 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter17 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter18 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.sqlDataSource1 = new Telerik.Reporting.SqlDataSource();
            this.panel2 = new Telerik.Reporting.Panel();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.panel3 = new Telerik.Reporting.Panel();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.sqlDataSource2 = new Telerik.Reporting.SqlDataSource();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.029D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox72.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox72.StyleName = "Normal.TableHeader";
            this.textBox72.Value = "Patient Name";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.771D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox82.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox82.StyleName = "Normal.TableHeader";
            this.textBox82.Value = "= Fields.PatientName";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.214D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox70.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox70.StyleName = "Normal.TableHeader";
            this.textBox70.Value = "MRI Center";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.186D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox99.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox99.StyleName = "Normal.TableBody";
            this.textBox99.Value = "= Fields.MRICompany";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.979D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox60.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox60.Style.Font.Bold = false;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox60.StyleName = "Normal.TableHeader";
            this.textBox60.Value = "Device Model";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.19D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox61.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox61.StyleName = "Normal.TableBody";
            this.textBox61.Value = "= Parameters.modelDescription.Value";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.417D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox62.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox62.StyleName = "Normal.TableHeader";
            this.textBox62.Value = "SN#";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.914D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox63.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox63.StyleName = "Normal.TableBody";
            this.textBox63.Value = "= Parameters.modelSN.Value";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.317D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox66.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox66.StyleName = "Normal.TableHeader";
            this.textBox66.Value = "Device Company";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.783D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox67.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox67.StyleName = "Normal.TableBody";
            this.textBox67.Value = "= Fields.DeviceCompanyName";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.95D), Telerik.Reporting.Drawing.Unit.Inch(0.104D));
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.StyleName = "Normal.TableHeader";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.25D), Telerik.Reporting.Drawing.Unit.Inch(0.104D));
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.StyleName = "Normal.TableHeader";
            // 
            // textBox20
            // 
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("= Parameters.isDeviceProgrammedBack.Value", Telerik.Reporting.FilterOperator.Equal, "No"));
            formattingRule1.Style.Visible = false;
            this.textBox20.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.745D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox20.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox20.StyleName = "Normal.TableHeader";
            this.textBox20.Value = "Device programmed back to original settings";
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(7.904D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1,
            this.panel2,
            this.panel3,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.table6,
            this.textBox8,
            this.textBox9,
            this.table8,
            this.textBox47,
            this.table1,
            this.table7});
            this.detailSection1.Name = "detailSection1";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table5});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7D), Telerik.Reporting.Drawing.Unit.Inch(1.4D));
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.029D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.771D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox71);
            this.table5.Body.SetCellContent(0, 1, this.textBox83);
            this.table5.Body.SetCellContent(1, 0, this.textBox126);
            this.table5.Body.SetCellContent(1, 1, this.textBox84);
            this.table5.Body.SetCellContent(2, 0, this.textBox85);
            this.table5.Body.SetCellContent(2, 1, this.textBox105);
            tableGroup1.Name = "patientName";
            tableGroup1.ReportItem = this.textBox72;
            tableGroup2.Name = "group";
            tableGroup2.ReportItem = this.textBox82;
            this.table5.ColumnGroups.Add(tableGroup1);
            this.table5.ColumnGroups.Add(tableGroup2);
            this.table5.DataSource = this.sqlDataSource1;
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71,
            this.textBox83,
            this.textBox126,
            this.textBox84,
            this.textBox85,
            this.textBox105,
            this.textBox72,
            this.textBox82});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(0.1D));
            this.table5.Name = "table5";
            tableGroup4.Name = "group1";
            tableGroup5.Name = "group2";
            tableGroup6.Name = "group3";
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.ChildGroups.Add(tableGroup5);
            tableGroup3.ChildGroups.Add(tableGroup6);
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detail";
            this.table5.RowGroups.Add(tableGroup3);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8D), Telerik.Reporting.Drawing.Unit.Inch(0.8D));
            this.table5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table5.StyleName = "Normal.TableNormal";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.029D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox71.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox71.StyleName = "Normal.TableBody";
            this.textBox71.Value = "DOB";
            // 
            // textBox83
            // 
            this.textBox83.Format = "{0:d}";
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.771D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox83.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox83.StyleName = "Normal.TableBody";
            this.textBox83.Value = "= Fields.DOB";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.029D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox126.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox126.StyleName = "Normal.TableHeader";
            this.textBox126.Value = "Phone";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.771D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox84.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox84.StyleName = "Normal.TableBody";
            this.textBox84.Value = "= Fields.Phone";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.029D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox85.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox85.StyleName = "Normal.TableHeader";
            this.textBox85.Value = "Insurance";
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.771D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox105.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox105.Style.Font.Bold = true;
            this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox105.StyleName = "Normal.TableBody";
            this.textBox105.Value = "= Fields.Insurance";
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionString = "SmartPraxis";
            this.sqlDataSource1.Name = "sqlDataSource1";
            this.sqlDataSource1.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@patientGuid", System.Data.DbType.String, "= Parameters.patientGuid.Value")});
            this.sqlDataSource1.SelectCommand = resources.GetString("sqlDataSource1.SelectCommand");
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table3});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7D), Telerik.Reporting.Drawing.Unit.Inch(1.4D));
            this.panel2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.214D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.186D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox96);
            this.table3.Body.SetCellContent(0, 1, this.textBox68);
            this.table3.Body.SetCellContent(1, 0, this.textBox97);
            this.table3.Body.SetCellContent(1, 1, this.textBox69);
            this.table3.Body.SetCellContent(2, 0, this.textBox152);
            this.table3.Body.SetCellContent(2, 1, this.textBox98);
            this.table3.Body.SetCellContent(3, 0, this.textBox100);
            this.table3.Body.SetCellContent(3, 1, this.textBox135);
            this.table3.Body.SetCellContent(4, 0, this.textBox136);
            this.table3.Body.SetCellContent(4, 1, this.textBox117);
            tableGroup7.Name = "mRICompany";
            tableGroup7.ReportItem = this.textBox70;
            tableGroup8.Name = "group6";
            tableGroup8.ReportItem = this.textBox99;
            this.table3.ColumnGroups.Add(tableGroup7);
            this.table3.ColumnGroups.Add(tableGroup8);
            this.table3.DataSource = this.sqlDataSource1;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox96,
            this.textBox68,
            this.textBox97,
            this.textBox69,
            this.textBox152,
            this.textBox98,
            this.textBox100,
            this.textBox135,
            this.textBox136,
            this.textBox117,
            this.textBox70,
            this.textBox99});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(0.1D));
            this.table3.Name = "table3";
            tableGroup10.Name = "group4";
            tableGroup11.Name = "group5";
            tableGroup12.Name = "group7";
            tableGroup13.Name = "group9";
            tableGroup14.Name = "group8";
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.ChildGroups.Add(tableGroup11);
            tableGroup9.ChildGroups.Add(tableGroup12);
            tableGroup9.ChildGroups.Add(tableGroup13);
            tableGroup9.ChildGroups.Add(tableGroup14);
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "detail";
            this.table3.RowGroups.Add(tableGroup9);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4D), Telerik.Reporting.Drawing.Unit.Inch(1.2D));
            this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table3.StyleName = "Normal.TableNormal";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.214D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox96.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox96.StyleName = "Normal.TableHeader";
            this.textBox96.Value = "Physician";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.186D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox68.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox68.StyleName = "Normal.TableBody";
            this.textBox68.Value = "= Fields.MRIDoctorName";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.214D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox97.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox97.StyleName = "Normal.TableHeader";
            this.textBox97.Value = "MRI Scan Date";
            // 
            // textBox69
            // 
            this.textBox69.Format = "{0:d}";
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.186D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox69.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox69.StyleName = "Normal.TableBody";
            this.textBox69.Value = "= Parameters.scanDate.Value";
            // 
            // textBox152
            // 
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.214D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox152.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox152.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox152.StyleName = "Normal.TableHeader";
            this.textBox152.Value = "MRI Scan Time";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.186D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox98.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox98.Style.Font.Bold = true;
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox98.StyleName = "Normal.TableBody";
            this.textBox98.Value = "= Parameters.scanTime.Value";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.214D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox100.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox100.StyleName = "Normal.TableHeader";
            this.textBox100.Value = "MRI Type";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.186D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox135.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox135.Style.Font.Bold = true;
            this.textBox135.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox135.StyleName = "Normal.TableBody";
            this.textBox135.Value = "= Fields.MRIType";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.214D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox136.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox136.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox136.StyleName = "Normal.TableHeader";
            this.textBox136.Value = "MRI Tesla";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.186D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox117.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox117.Style.Font.Bold = true;
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox117.StyleName = "Normal.TableBody";
            this.textBox117.Value = "= Fields.MRITesla";
            // 
            // panel3
            // 
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2,
            this.table4});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(1.5D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.panel3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.979D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.19D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.417D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.914D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox48);
            this.table2.Body.SetCellContent(0, 1, this.textBox49);
            this.table2.Body.SetCellContent(0, 3, this.textBox50);
            this.table2.Body.SetCellContent(0, 2, this.textBox51);
            this.table2.Body.SetCellContent(1, 1, this.textBox52);
            this.table2.Body.SetCellContent(1, 3, this.textBox53);
            this.table2.Body.SetCellContent(2, 1, this.textBox54);
            this.table2.Body.SetCellContent(2, 3, this.textBox55);
            this.table2.Body.SetCellContent(1, 2, this.textBox56);
            this.table2.Body.SetCellContent(2, 2, this.textBox57);
            this.table2.Body.SetCellContent(1, 0, this.textBox58);
            this.table2.Body.SetCellContent(2, 0, this.textBox59);
            tableGroup15.Name = "deviceModelDescription";
            tableGroup15.ReportItem = this.textBox60;
            tableGroup16.Name = "group11";
            tableGroup16.ReportItem = this.textBox61;
            tableGroup17.Name = "deviceCoDeviceModelSN";
            tableGroup17.ReportItem = this.textBox62;
            tableGroup18.Name = "group12";
            tableGroup18.ReportItem = this.textBox63;
            this.table2.ColumnGroups.Add(tableGroup15);
            this.table2.ColumnGroups.Add(tableGroup16);
            this.table2.ColumnGroups.Add(tableGroup17);
            this.table2.ColumnGroups.Add(tableGroup18);
            this.table2.DataSource = this.sqlDataSource1;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox48,
            this.textBox49,
            this.textBox51,
            this.textBox50,
            this.textBox58,
            this.textBox52,
            this.textBox56,
            this.textBox53,
            this.textBox59,
            this.textBox54,
            this.textBox57,
            this.textBox55,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9D), Telerik.Reporting.Drawing.Unit.Inch(0.1D));
            this.table2.Name = "table2";
            tableGroup20.Name = "group13";
            tableGroup21.Name = "group15";
            tableGroup22.Name = "group14";
            tableGroup19.ChildGroups.Add(tableGroup20);
            tableGroup19.ChildGroups.Add(tableGroup21);
            tableGroup19.ChildGroups.Add(tableGroup22);
            tableGroup19.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup19.Name = "detail";
            this.table2.RowGroups.Add(tableGroup19);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5D), Telerik.Reporting.Drawing.Unit.Inch(0.8D));
            this.table2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.StyleName = "Normal.TableNormal";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.979D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox48.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox48.Style.Font.Bold = false;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox48.StyleName = "Normal.TableHeader";
            this.textBox48.Value = "Lead #1 Model";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.19D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox49.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox49.StyleName = "Normal.TableBody";
            this.textBox49.Value = "= Parameters.lead1Model.Value";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.914D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox50.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox50.StyleName = "Normal.TableBody";
            this.textBox50.Value = "= Parameters.lead1SN.Value";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.417D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox51.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox51.StyleName = "Normal.TableHeader";
            this.textBox51.Value = "SN#";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.19D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox52.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox52.StyleName = "Normal.TableBody";
            this.textBox52.Value = "= Parameters.lead2Model.Value";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.914D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox53.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox53.StyleName = "Normal.TableBody";
            this.textBox53.Value = "= Parameters.lead2SN.Value";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.19D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox54.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox54.StyleName = "Normal.TableBody";
            this.textBox54.Value = "= Parameters.lead3Model.Value";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.914D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox55.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox55.StyleName = "Normal.TableBody";
            this.textBox55.Value = "= Parameters.lead3SN.Value";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.417D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox56.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox56.StyleName = "Normal.TableHeader";
            this.textBox56.Value = "SN#";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.417D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox57.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox57.StyleName = "Normal.TableHeader";
            this.textBox57.Value = "SN#";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.979D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox58.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox58.StyleName = "Normal.TableHeader";
            this.textBox58.Value = "Lead #2 Model";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.979D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox59.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox59.StyleName = "Normal.TableHeader";
            this.textBox59.Value = "Lead #3 Model";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.317D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.783D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox64);
            this.table4.Body.SetCellContent(0, 1, this.textBox65);
            this.table4.Body.SetCellContent(1, 0, this.textBox12);
            this.table4.Body.SetCellContent(1, 1, this.textBox16);
            tableGroup23.Name = "deviceCompanyName";
            tableGroup23.ReportItem = this.textBox66;
            tableGroup24.Name = "group10";
            tableGroup24.ReportItem = this.textBox67;
            this.table4.ColumnGroups.Add(tableGroup23);
            this.table4.ColumnGroups.Add(tableGroup24);
            this.table4.DataSource = this.sqlDataSource1;
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox64,
            this.textBox65,
            this.textBox12,
            this.textBox16,
            this.textBox66,
            this.textBox67});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(0.1D));
            this.table4.Name = "table4";
            tableGroup26.Name = "group17";
            tableGroup27.Name = "group18";
            tableGroup25.ChildGroups.Add(tableGroup26);
            tableGroup25.ChildGroups.Add(tableGroup27);
            tableGroup25.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup25.Name = "detail";
            this.table4.RowGroups.Add(tableGroup25);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1D), Telerik.Reporting.Drawing.Unit.Inch(0.6D));
            this.table4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table4.StyleName = "Normal.TableNormal";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.317D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox64.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox64.StyleName = "Normal.TableHeader";
            this.textBox64.Value = "Company Rep";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.783D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox65.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox65.StyleName = "Normal.TableBody";
            this.textBox65.Value = "= Parameters.deviceCoRepAtTimeOfProcedure.Value";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.317D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox12.StyleName = "Normal.TableHeader";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.783D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox16.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox16.StyleName = "Normal.TableBody";
            this.textBox16.Value = "= Fields.LastUpdatedDate";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.569D), Telerik.Reporting.Drawing.Unit.Inch(4.6D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Name = "Segoe UI";
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.Value = "Comments:";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.131D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox42);
            tableGroup35.Name = "deviceCoCommentsAtTimeOfProcedure";
            this.table1.ColumnGroups.Add(tableGroup35);
            this.table1.DataSource = this.sqlDataSource1;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox42});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.569D), Telerik.Reporting.Drawing.Unit.Inch(5D));
            this.table1.Name = "table1";
            tableGroup36.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup36.Name = "detail";
            this.table1.RowGroups.Add(tableGroup36);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.131D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table1.StyleName = "Normal.TableNormal";
            // 
            // textBox42
            // 
            this.textBox42.KeepTogether = false;
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.131D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            //this.textBox42.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox42.StyleName = "Normal.TableBody";
            this.textBox42.Value = "= Parameters.commentsBefore.Value";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(2.7D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Segoe UI";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "The following conditions were met:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.3D), Telerik.Reporting.Drawing.Unit.Inch(3.1D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4D), Telerik.Reporting.Drawing.Unit.Inch(1.4D));
            this.textBox2.Style.Font.Bold = false;
            this.textBox2.Style.Font.Name = "Segoe UI";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox2.Value = resources.GetString("textBox2.Value");
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(4.6D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Segoe UI";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "Executed MRI Safe Mode Selection MK-Algorithm:";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.95D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.25D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.208D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox6);
            this.table6.Body.SetCellContent(0, 1, this.textBox7);
            tableGroup28.Name = "questionText";
            tableGroup28.ReportItem = this.textBox4;
            tableGroup29.Name = "answerText";
            tableGroup29.ReportItem = this.textBox5;
            this.table6.ColumnGroups.Add(tableGroup28);
            this.table6.ColumnGroups.Add(tableGroup29);
            this.table6.DataSource = this.sqlDataSource2;
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox6,
            this.textBox7,
            this.textBox4,
            this.textBox5});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(5D));
            this.table6.Name = "table6";
            tableGroup30.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup30.Name = "detail";
            this.table6.RowGroups.Add(tableGroup30);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2D), Telerik.Reporting.Drawing.Unit.Inch(0.313D));
            this.table6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table6.StyleName = "Normal.TableNormal";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.95D), Telerik.Reporting.Drawing.Unit.Inch(0.208D));
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.StyleName = "Normal.TableBody";
            this.textBox6.Value = "= Fields.QuestionText";
            // 
            // textBox7
            // 
            formattingRule2.Filters.Add(new Telerik.Reporting.Filter("= Fields.SortOrder", Telerik.Reporting.FilterOperator.Equal, "70"));
            formattingRule2.Style.Font.Bold = true;
            formattingRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            formattingRule3.Filters.Add(new Telerik.Reporting.Filter("= Fields.SortOrder", Telerik.Reporting.FilterOperator.Equal, "99"));
            formattingRule3.Style.Font.Bold = true;
            formattingRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox7.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2,
            formattingRule3});
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.25D), Telerik.Reporting.Drawing.Unit.Inch(0.208D));
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox7.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.StyleName = "Normal.TableBody";
            this.textBox7.Value = "= Fields.AnswerText";
            // 
            // sqlDataSource2
            // 
            this.sqlDataSource2.ConnectionString = "SmartPraxis";
            this.sqlDataSource2.Name = "sqlDataSource2";
            this.sqlDataSource2.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@pageNo", System.Data.DbType.String, "= Parameters.pageNo.Value")});
            this.sqlDataSource2.SelectCommand = "SELECT        AnswerID, AnswerPage, QuestionText, AnswerText, SortOrder\r\nFROM    " +
    "        ListAnswersReport\r\nWHERE        (AnswerPage = @pageNo)\r\nORDER BY SortOrd" +
    "er";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(5.4D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Segoe UI";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "Post MRI scan setting:";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.569D), Telerik.Reporting.Drawing.Unit.Inch(5.4D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Segoe UI";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "Comments:";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.131D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox10);
            tableGroup37.Name = "deviceCoCommentsAtTimeOfProcedure";
            this.table7.ColumnGroups.Add(tableGroup37);
            this.table7.DataSource = this.sqlDataSource1;
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.569D), Telerik.Reporting.Drawing.Unit.Inch(5.8D));
            this.table7.Name = "table7";
            tableGroup38.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup38.Name = "detail";
            this.table7.RowGroups.Add(tableGroup38);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.131D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table7.StyleName = "Normal.TableNormal";
            // 
            // textBox10
            // 
            this.textBox10.KeepTogether = false;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.131D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            //this.textBox10.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox10.StyleName = "Normal.TableBody";
            this.textBox10.Value = "= Parameters.commentsAfter.Value";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.745D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox22);
            this.table8.Body.SetCellContent(1, 0, this.textBox23);
            tableGroup31.Name = "group16";
            tableGroup31.ReportItem = this.textBox20;
            this.table8.ColumnGroups.Add(tableGroup31);
            this.table8.DataSource = this.sqlDataSource1;
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox22,
            this.textBox23,
            this.textBox20});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(5.8D));
            this.table8.Name = "table8";
            tableGroup33.Name = "group1";
            tableGroup34.Name = "group2";
            tableGroup32.ChildGroups.Add(tableGroup33);
            tableGroup32.ChildGroups.Add(tableGroup34);
            tableGroup32.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup32.Name = "detail";
            this.table8.RowGroups.Add(tableGroup32);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.745D), Telerik.Reporting.Drawing.Unit.Inch(0.6D));
            this.table8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table8.StyleName = "Normal.TableNormal";
            // 
            // textBox22
            // 
            formattingRule4.Filters.Add(new Telerik.Reporting.Filter("= Parameters.isDeviceAutomatic.Value", Telerik.Reporting.FilterOperator.Equal, "No"));
            formattingRule4.Style.Visible = false;
            this.textBox22.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule4});
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.745D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox22.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox22.Style.Font.Bold = false;
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox22.StyleName = "Normal.TableBody";
            this.textBox22.Value = "Device set up for automatic reprogramming post MRI\r\n";
            // 
            // textBox23
            // 
            formattingRule5.Filters.Add(new Telerik.Reporting.Filter("= Parameters.isBaselineRequired.Value", Telerik.Reporting.FilterOperator.Equal, "No"));
            formattingRule5.Style.Visible = false;
            this.textBox23.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule5});
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.745D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox23.Style.Font.Bold = false;
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox23.StyleName = "Normal.TableBody";
            this.textBox23.Value = "Baseline settings required reprogramming\r\n";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.7D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox43,
            this.textBox45,
            this.textBox44});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.9D), Telerik.Reporting.Drawing.Unit.Inch(0.7D));
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.7D), Telerik.Reporting.Drawing.Unit.Inch(0.569D));
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Segoe UI";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox43.Value = "MRISAFEMODE.COM\r";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6D), Telerik.Reporting.Drawing.Unit.Inch(0.512D));
            this.textBox45.Style.Font.Bold = false;
            this.textBox45.Style.Font.Name = "Segoe UI";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.Value = "Summary:";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6D), Telerik.Reporting.Drawing.Unit.Inch(0.512D));
            this.textBox44.Style.Font.Bold = false;
            this.textBox44.Style.Font.Name = "Segoe UI";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.Value = "MRI scan in Patient with MRI Conditional CIED.";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.052D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // MRICompletedReport3
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1,
            this.pageHeaderSection1,
            this.pageFooterSection1});
            this.Name = "MRICompletedReport2";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "patientGuid";
            reportParameter1.Value = "fbc446f0-99e8-4a99-97ab-871ae823d0f8";
            reportParameter2.Name = "pageNo";
            reportParameter2.Value = "20";
            reportParameter3.Name = "commentsAfter";
            reportParameter4.Name = "commentsBefore";
            reportParameter5.Name = "modelDescription";
            reportParameter6.Name = "modelSN";
            reportParameter7.Name = "lead1Model";
            reportParameter8.Name = "lead1SN";
            reportParameter9.Name = "lead2Model";
            reportParameter10.Name = "lead2SN";
            reportParameter11.Name = "lead3Model";
            reportParameter12.Name = "lead3SN";
            reportParameter13.Name = "scanDate";
            reportParameter14.Name = "scanTime";
            reportParameter15.Name = "isDeviceProgrammedBack";
            reportParameter16.Name = "isDeviceAutomatic";
            reportParameter17.Name = "isBaselineRequired";
            reportParameter18.Name = "deviceCoRepAtTimeOfProcedure";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            this.ReportParameters.Add(reportParameter10);
            this.ReportParameters.Add(reportParameter11);
            this.ReportParameters.Add(reportParameter12);
            this.ReportParameters.Add(reportParameter13);
            this.ReportParameters.Add(reportParameter14);
            this.ReportParameters.Add(reportParameter15);
            this.ReportParameters.Add(reportParameter16);
            this.ReportParameters.Add(reportParameter17);
            this.ReportParameters.Add(reportParameter18);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Normal.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detailSection1;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.Panel panel3;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.SqlDataSource sqlDataSource2;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.SqlDataSource sqlDataSource1;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox16;
    }
}