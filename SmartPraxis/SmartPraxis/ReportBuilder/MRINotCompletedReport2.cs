namespace SmartPraxis.ReportBuilder
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for MRINotCompletedReport2.
    /// </summary>
    public partial class MRINotCompletedReport2 : Report
    {
        public MRINotCompletedReport2()
        {
            //
            // Required for telerik Reporting designer support
            //
            this.InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}