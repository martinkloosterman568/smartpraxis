namespace SmartPraxis.ReportBuilder
{
    partial class MRINotCompletedReport2
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MRINotCompletedReport2));
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter10 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter11 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter12 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter13 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.sqlDataSource1 = new Telerik.Reporting.SqlDataSource();
            this.panel1 = new Telerik.Reporting.Panel();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.panel3 = new Telerik.Reporting.Panel();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5999221801757812D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox21.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox21.StyleName = "Normal.TableHeader";
            this.textBox21.Value = "";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.029167652130127D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox71.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox71.StyleName = "Normal.TableHeader";
            this.textBox71.Value = "Patient Name";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7708331346511841D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox82.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox82.StyleName = "Normal.TableHeader";
            this.textBox82.Value = "= Fields.PatientName";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2136476039886475D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox69.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox69.StyleName = "Normal.TableHeader";
            this.textBox69.Value = "MRI Center";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1863527297973633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox99.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox99.StyleName = "Normal.TableBody";
            this.textBox99.Value = "= Fields.MRICompany";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97878104448318481D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox59.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox59.Style.Font.Bold = false;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox59.StyleName = "Normal.TableHeader";
            this.textBox59.Value = "Device Model";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1901524066925049D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox60.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox60.StyleName = "Normal.TableBody";
            this.textBox60.Value = "= Parameters.modelDescription.Value";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.41702920198440552D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox61.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox61.StyleName = "Normal.TableHeader";
            this.textBox61.Value = "SN#";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.91403716802597046D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox62.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox62.StyleName = "Normal.TableBody";
            this.textBox62.Value = "= Parameters.modelSN.Value";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3165359497070313D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox65.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox65.StyleName = "Normal.TableHeader";
            this.textBox65.Value = "Device Company";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7834639549255371D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox66.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox66.Style.Font.Bold = true;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox66.StyleName = "Normal.TableBody";
            this.textBox66.Value = "= Fields.DeviceCompanyName";
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(3.7999997138977051D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.panel1,
            this.panel2,
            this.panel3,
            this.textBox46,
            this.textBox1});
            this.detailSection1.Name = "detailSection1";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(7.5999221801757812D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox42);
            tableGroup1.Name = "deviceCoCommentsAtTimeOfProcedure";
            tableGroup1.ReportItem = this.textBox21;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.DataSource = this.sqlDataSource1;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox42,
            this.textBox21});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D), Telerik.Reporting.Drawing.Unit.Inch(3.2999999523162842D));
            this.table1.Name = "table1";
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detail";
            this.table1.RowGroups.Add(tableGroup2);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5999221801757812D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table1.StyleName = "Normal.TableNormal";
            // 
            // textBox42
            // 
            this.textBox42.KeepTogether = false;
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5999221801757812D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox42.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox42.StyleName = "Normal.TableBody";
            this.textBox42.Value = "= Parameters.commentsAfter.Value";
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionString = "SmartPraxis";
            this.sqlDataSource1.Name = "sqlDataSource1";
            this.sqlDataSource1.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@patientGuid", System.Data.DbType.String, "= Parameters.patientGuid.Value")});
            this.sqlDataSource1.SelectCommand = resources.GetString("sqlDataSource1.SelectCommand");
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table5});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D));
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0291675329208374D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7708333730697632D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox70);
            this.table5.Body.SetCellContent(0, 1, this.textBox83);
            this.table5.Body.SetCellContent(1, 0, this.textBox126);
            this.table5.Body.SetCellContent(1, 1, this.textBox84);
            this.table5.Body.SetCellContent(2, 0, this.textBox85);
            this.table5.Body.SetCellContent(2, 1, this.textBox105);
            tableGroup3.Name = "patientName";
            tableGroup3.ReportItem = this.textBox71;
            tableGroup4.Name = "group";
            tableGroup4.ReportItem = this.textBox82;
            this.table5.ColumnGroups.Add(tableGroup3);
            this.table5.ColumnGroups.Add(tableGroup4);
            this.table5.DataSource = this.sqlDataSource1;
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox70,
            this.textBox83,
            this.textBox126,
            this.textBox84,
            this.textBox85,
            this.textBox105,
            this.textBox71,
            this.textBox82});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(0.099960647523403168D));
            this.table5.Name = "table5";
            tableGroup6.Name = "group1";
            tableGroup7.Name = "group2";
            tableGroup8.Name = "group3";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.ChildGroups.Add(tableGroup7);
            tableGroup5.ChildGroups.Add(tableGroup8);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detail";
            this.table5.RowGroups.Add(tableGroup5);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8000009059906006D), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D));
            this.table5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table5.StyleName = "Normal.TableNormal";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.029167652130127D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox70.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox70.StyleName = "Normal.TableBody";
            this.textBox70.Value = "DOB";
            // 
            // textBox83
            // 
            this.textBox83.Format = "{0:d}";
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7708331346511841D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox83.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox83.StyleName = "Normal.TableBody";
            this.textBox83.Value = "= Fields.DOB";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.029167652130127D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox126.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox126.StyleName = "Normal.TableHeader";
            this.textBox126.Value = "Phone";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7708331346511841D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox84.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox84.StyleName = "Normal.TableBody";
            this.textBox84.Value = "= Fields.Phone";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.029167652130127D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox85.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox85.StyleName = "Normal.TableHeader";
            this.textBox85.Value = "Insurance";
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7708331346511841D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox105.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox105.Style.Font.Bold = true;
            this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox105.StyleName = "Normal.TableBody";
            this.textBox105.Value = "= Fields.Insurance";
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table3});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.0000152587890625D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D));
            this.panel2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2136474847793579D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1863527297973633D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19999995827674866D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox96);
            this.table3.Body.SetCellContent(0, 1, this.textBox67);
            this.table3.Body.SetCellContent(1, 0, this.textBox97);
            this.table3.Body.SetCellContent(1, 1, this.textBox68);
            this.table3.Body.SetCellContent(2, 0, this.textBox152);
            this.table3.Body.SetCellContent(2, 1, this.textBox98);
            this.table3.Body.SetCellContent(3, 0, this.textBox100);
            this.table3.Body.SetCellContent(3, 1, this.textBox135);
            this.table3.Body.SetCellContent(4, 0, this.textBox136);
            this.table3.Body.SetCellContent(4, 1, this.textBox117);
            tableGroup9.Name = "mRICompany";
            tableGroup9.ReportItem = this.textBox69;
            tableGroup10.Name = "group6";
            tableGroup10.ReportItem = this.textBox99;
            this.table3.ColumnGroups.Add(tableGroup9);
            this.table3.ColumnGroups.Add(tableGroup10);
            this.table3.DataSource = this.sqlDataSource1;
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox96,
            this.textBox67,
            this.textBox97,
            this.textBox68,
            this.textBox152,
            this.textBox98,
            this.textBox100,
            this.textBox135,
            this.textBox136,
            this.textBox117,
            this.textBox69,
            this.textBox99});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D), Telerik.Reporting.Drawing.Unit.Inch(0.099960647523403168D));
            this.table3.Name = "table3";
            tableGroup12.Name = "group4";
            tableGroup13.Name = "group5";
            tableGroup14.Name = "group7";
            tableGroup15.Name = "group9";
            tableGroup16.Name = "group8";
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.ChildGroups.Add(tableGroup13);
            tableGroup11.ChildGroups.Add(tableGroup14);
            tableGroup11.ChildGroups.Add(tableGroup15);
            tableGroup11.ChildGroups.Add(tableGroup16);
            tableGroup11.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup11.Name = "detail";
            this.table3.RowGroups.Add(tableGroup11);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(1.2000002861022949D));
            this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table3.StyleName = "Normal.TableNormal";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2136476039886475D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox96.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox96.StyleName = "Normal.TableHeader";
            this.textBox96.Value = "Physician";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1863527297973633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox67.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox67.StyleName = "Normal.TableBody";
            this.textBox67.Value = "= Fields.MRIDoctorName";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2136476039886475D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox97.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox97.StyleName = "Normal.TableHeader";
            this.textBox97.Value = "MRI Scan Date";
            // 
            // textBox68
            // 
            this.textBox68.Format = "{0:d}";
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1863527297973633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox68.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox68.StyleName = "Normal.TableBody";
            this.textBox68.Value = "= Parameters.scanDate.Value";
            // 
            // textBox152
            // 
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2136476039886475D), Telerik.Reporting.Drawing.Unit.Inch(0.19999995827674866D));
            this.textBox152.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox152.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox152.StyleName = "Normal.TableHeader";
            this.textBox152.Value = "MRI Scan Time";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1863527297973633D), Telerik.Reporting.Drawing.Unit.Inch(0.19999995827674866D));
            this.textBox98.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox98.Style.Font.Bold = true;
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox98.StyleName = "Normal.TableBody";
            this.textBox98.Value = "= Parameters.scanTime.Value";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2136476039886475D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox100.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox100.StyleName = "Normal.TableHeader";
            this.textBox100.Value = "MRI Type";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1863527297973633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox135.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox135.Style.Font.Bold = true;
            this.textBox135.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox135.StyleName = "Normal.TableBody";
            this.textBox135.Value = "= Fields.MRIType";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2136476039886475D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox136.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox136.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox136.StyleName = "Normal.TableHeader";
            this.textBox136.Value = "MRI Tesla";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1863527297973633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000003278255463D));
            this.textBox117.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox117.Style.Font.Bold = true;
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox117.StyleName = "Normal.TableBody";
            this.textBox117.Value = "= Fields.MRITesla";
            // 
            // panel3
            // 
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2,
            this.table4});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D), Telerik.Reporting.Drawing.Unit.Inch(1.5D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D));
            this.panel3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.97878098487854D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1901524066925049D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.41702923178672791D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.91403704881668091D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox47);
            this.table2.Body.SetCellContent(0, 1, this.textBox48);
            this.table2.Body.SetCellContent(0, 3, this.textBox49);
            this.table2.Body.SetCellContent(0, 2, this.textBox50);
            this.table2.Body.SetCellContent(1, 1, this.textBox51);
            this.table2.Body.SetCellContent(1, 3, this.textBox52);
            this.table2.Body.SetCellContent(2, 1, this.textBox53);
            this.table2.Body.SetCellContent(2, 3, this.textBox54);
            this.table2.Body.SetCellContent(1, 2, this.textBox55);
            this.table2.Body.SetCellContent(2, 2, this.textBox56);
            this.table2.Body.SetCellContent(1, 0, this.textBox57);
            this.table2.Body.SetCellContent(2, 0, this.textBox58);
            tableGroup17.Name = "deviceModelDescription";
            tableGroup17.ReportItem = this.textBox59;
            tableGroup18.Name = "group11";
            tableGroup18.ReportItem = this.textBox60;
            tableGroup19.Name = "deviceCoDeviceModelSN";
            tableGroup19.ReportItem = this.textBox61;
            tableGroup20.Name = "group12";
            tableGroup20.ReportItem = this.textBox62;
            this.table2.ColumnGroups.Add(tableGroup17);
            this.table2.ColumnGroups.Add(tableGroup18);
            this.table2.ColumnGroups.Add(tableGroup19);
            this.table2.ColumnGroups.Add(tableGroup20);
            this.table2.DataSource = this.sqlDataSource1;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox47,
            this.textBox48,
            this.textBox50,
            this.textBox49,
            this.textBox57,
            this.textBox51,
            this.textBox55,
            this.textBox52,
            this.textBox58,
            this.textBox53,
            this.textBox56,
            this.textBox54,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox62});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8999998569488525D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.table2.Name = "table2";
            tableGroup22.Name = "group13";
            tableGroup23.Name = "group15";
            tableGroup24.Name = "group14";
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.ChildGroups.Add(tableGroup23);
            tableGroup21.ChildGroups.Add(tableGroup24);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "detail";
            this.table2.RowGroups.Add(tableGroup21);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4999997615814209D), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D));
            this.table2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.StyleName = "Normal.TableNormal";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97878104448318481D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox47.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox47.Style.Font.Bold = false;
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox47.StyleName = "Normal.TableHeader";
            this.textBox47.Value = "Lead #1 Model";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1901524066925049D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox48.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox48.StyleName = "Normal.TableBody";
            this.textBox48.Value = "= Parameters.lead1Model.Value";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.91403716802597046D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox49.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox49.StyleName = "Normal.TableBody";
            this.textBox49.Value = "= Parameters.lead1SN.Value";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.41702920198440552D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox50.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox50.StyleName = "Normal.TableHeader";
            this.textBox50.Value = "SN#";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1901524066925049D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox51.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox51.StyleName = "Normal.TableBody";
            this.textBox51.Value = "= Parameters.lead2Model.Value";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.91403716802597046D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox52.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox52.StyleName = "Normal.TableBody";
            this.textBox52.Value = "= Parameters.lead2SN.Value";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1901524066925049D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox53.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox53.StyleName = "Normal.TableBody";
            this.textBox53.Value = "= Parameters.lead3Model.Value";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.91403716802597046D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox54.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox54.StyleName = "Normal.TableBody";
            this.textBox54.Value = "= Parameters.lead3SN.Value";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.41702920198440552D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox55.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox55.StyleName = "Normal.TableHeader";
            this.textBox55.Value = "SN#";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.41702920198440552D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox56.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox56.StyleName = "Normal.TableHeader";
            this.textBox56.Value = "SN#";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97878104448318481D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox57.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox57.StyleName = "Normal.TableHeader";
            this.textBox57.Value = "Lead #2 Model";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97878104448318481D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox58.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox58.StyleName = "Normal.TableHeader";
            this.textBox58.Value = "Lead #3 Model";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3165357112884522D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7834641933441162D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox63);
            this.table4.Body.SetCellContent(0, 1, this.textBox64);
            tableGroup25.Name = "deviceCompanyName";
            tableGroup25.ReportItem = this.textBox65;
            tableGroup26.Name = "group10";
            tableGroup26.ReportItem = this.textBox66;
            this.table4.ColumnGroups.Add(tableGroup25);
            this.table4.ColumnGroups.Add(tableGroup26);
            this.table4.DataSource = this.sqlDataSource1;
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.table4.Name = "table4";
            tableGroup27.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup27.Name = "detail";
            this.table4.RowGroups.Add(tableGroup27);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.table4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table4.StyleName = "Normal.TableNormal";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3165359497070313D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox63.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox63.StyleName = "Normal.TableHeader";
            this.textBox63.Value = "Company Rep";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7834639549255371D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox64.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox64.StyleName = "Normal.TableBody";
            this.textBox64.Value = "= Parameters.deviceRep.Value";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D), Telerik.Reporting.Drawing.Unit.Inch(3.0999996662139893D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999996662139893D), Telerik.Reporting.Drawing.Unit.Inch(0.29999977350234985D));
            this.textBox46.Style.Font.Bold = false;
            this.textBox46.Style.Font.Name = "Segoe UI";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.Value = "Comments:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D), Telerik.Reporting.Drawing.Unit.Inch(2.6999998092651367D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5999221801757812D), Telerik.Reporting.Drawing.Unit.Inch(0.3125593364238739D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Segoe UI";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "MRI scan safe CIED conditions criteria were not met; MRI scan was not performed. " +
    "";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox43,
            this.textBox45,
            this.textBox44});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8999612331390381D), Telerik.Reporting.Drawing.Unit.Inch(0.69996064901351929D));
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9000003337860107D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.568691074848175D));
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Segoe UI";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox43.Value = "MRISAFEMODE.COM\r";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999996662139893D), Telerik.Reporting.Drawing.Unit.Inch(0.51248031854629517D));
            this.textBox45.Style.Font.Bold = false;
            this.textBox45.Style.Font.Name = "Segoe UI";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.Value = "Summary:";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.51248031854629517D));
            this.textBox44.Style.Font.Bold = false;
            this.textBox44.Style.Font.Name = "Segoe UI";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.Value = "MRI scan in Patient with MRI Conditional CIED.";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // MRINotCompletedReport2
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1,
            this.pageHeaderSection1,
            this.pageFooterSection1});
            this.Name = "MRINotCompletedReport2";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "patientGuid";
            reportParameter1.Value = "4a6bf9ae-6deb-474b-901f-9b3ea0770a07";
            reportParameter2.Name = "commentsAfter";
            reportParameter3.Name = "modelDescription";
            reportParameter4.Name = "modelSN";
            reportParameter5.Name = "lead1Model";
            reportParameter6.Name = "lead1SN";
            reportParameter7.Name = "lead2Model";
            reportParameter8.Name = "lead2SN";
            reportParameter9.Name = "lead3Model";
            reportParameter10.Name = "lead3SN";
            reportParameter11.Name = "scanDate";
            reportParameter12.Name = "scanTime";
            reportParameter13.Name = "deviceRep";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            this.ReportParameters.Add(reportParameter10);
            this.ReportParameters.Add(reportParameter11);
            this.ReportParameters.Add(reportParameter12);
            this.ReportParameters.Add(reportParameter13);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Normal.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableHeader")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9000000953674316D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detailSection1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.SqlDataSource sqlDataSource1;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.Panel panel3;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox1;
    }
}