﻿using System;
using System.Collections.Generic;
using System.Web;

namespace SmartPraxis
{
    public class Global : HttpApplication
    {
        public static List<FormAuthorizer> formAuth = new List<FormAuthorizer>();
        
        public static string returnToPage = "/Index.aspx";
            
        public static string entityName = "DB_SmartPraxisEntities";

        public static int Timeout = 10;
        #region role names
        public static string superAdmin = "Super Admin";

        public static string mriCenter = "MRI Center";
        public static string mriTech = "MRI Tech";
        public static string mriDoctor = "MRI Doctor";

        public static string deviceCo = "Device Co";
        public static string deviceCoTech = "Device Co Tech";
        public static string hcoAdmin = "HCO Admin";
        public static string companyAdmin = "Company Admin";

        #endregion

        protected void Application_Start(object sender, EventArgs e)
        {
            //Telerik.Reporting.Services.WebApi.ReportsControllerConfiguration.RegisterRoutes(System.Web.Http.GlobalConfiguration.Configuration);
        }
    }

    public class FormAuthorizer
    {
        public string Role { get; set; }

        public string Path { get; set; }

        public string FormName { get; set; }
    }
}